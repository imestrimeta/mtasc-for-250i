/* ------------------------------------------------------------------ */
// Handrad / Jog-Fenster
// %1: vdim
// %2: "AsClient" (optional) when called by SMC Client
/* ------------------------------------------------------------------ */

CJogMove = ObjDef T_classDscr
"formText" T_object 0 // Fenstertitel
"iAx"      T_long 0  // Handrad Index Achse
"iFac"     T_long 0  // Handrad Index Faktor
"lab0"    T_object %1 // Handrad Label Texte
"labFac"  T_object %1
"fac"        T_double %1  // Handrad Geschw.faktoren
"key"        T_object 0  // gedr�ckte Handrad Taste
"oc0"      T_double %1
"ofs"      T_double %1
"ofsUpdateRq" T_long 0
"check"    T_function 0
;

if(%P > 1) {if(%2 == "AsClient") {return;}}   // SMC Client needs Class Definition only

jog = gcnew CJogMove 0;
jog.formText = &("Achsen �ber Handrad bewegen");

// optional %1: initialize
(MacroDef "CJogMove_check") =
" local \"jw\" \"jd\";
  jw => trcJw;
  jd => *Stat[0].jogData;
  if(jw.nDev == 0) {return 0;}
  local \"c0\" \"c1\" \"changed\";
  c0 = 0xF7 and _plc.iport2[0];  // previous port status
  c1 = 0xF7 and _plc.iport2[1];  // current status
  changed = (c0 != c1);
  if(%P > 0)
  {
    jd.oc0 = getoc;
    jd.ofs = jd.oc0 - jd.oc0;
    changed = 1;
  }
  if(jw.stopRq) {jw.stopRq = 0; changed = 1;}
  if(changed)
  {
    local \"code\" \"iAx\";
    code = 0xF0 and c1;  // Axis
    iAx = -1;
    jw.amask = 0;
    if(code == 0x10)  {jw.amask = 1; iAx = 0;}
    if(code == 0x20)  {jw.amask = 2; iAx = 1;}
    if(code == 0x40)  {jw.amask = 4; iAx = 2;}
    if(code == 0x80)  {jw.amask = 8; iAx = 3;}
    code = 0x07 and c1;  // Factor
    jw.stepPosFac = 0.0;
    if(code == 4)  {jw.stepPosFac = -0.25;   jd.iFac = 2;}
    if(code == 2)  {jw.stepPosFac = -0.025;  jd.iFac = 1;}
    if(code == 1)  {jw.stepPosFac = -0.0025; jd.iFac = 0;}
    mtIAg0.fag.mstop 3; mtIAg0.reset;
    jd.key = &(\"\");
    //if(c1 and 8)  {jd.key = &(\"TEACH\");}
    //if(iAx >= mtag[0].vDim) {iAx = mtag[0].vDim - 1;}
    jw.oc = jw.oc0 = mtIAg0.ocNext;
    //jw.oc[iAx] = jw.oc0[iAx] = CoordRnd jw.oc[iAx];
    jd.iAx = iAx;
    jw.stepPos0 = jw.stepPos;
    mp_f = array (mtag[0].vDim) 100.0 100.0 100.0 50.0;
    MvPar;
  }
  if(jw.active)
  {
    mtIAg0.mp jw.oc;
    jd.ofs = getoc - jd.oc0;
    jd.ofsUpdateRq = 1;
  }
  return changed;
";

jog.labFac[0] = &("1");  // Anzeigetext f�r Faktor (Schritte)
jog.labFac[1] = &("10");
jog.labFac[2] = &("100");
Stat[0].jogData = &(jog);

LoadDll MtApp.path & "/JogWheelDll.dll";
trcJw = gcnew CJogWheel 0 0;   // teensy++ 2.0 via rawhid
if(trcJw.nDev == 0)
{
  //MsgBox "No USB JogWheel Device found";
}
else
{
  trcJw.kHz = 60;    // max. frequency (only 20, 30 or 60)
  trcJw.nAx = 1;
  trcJw.open;
  trcJw.ctrlFlag = 8; // lesen aktivieren
  trcJw.stepPosFac = 0.01;
  trcJw.amask = 1;
  trcJw.stepFac = -1;
  trcJw.timeResMask = 15;
  trcJw.setPort 'D' 255 0x3F;     // Pull Up bei D0-D5
  trcJw.setPortDir 'D' 255 0xC0;  // D6,D7 als Ausg�nge
  trcJw.setPort 'C' 255 255;      // Pull Up bei C0-C7
}
