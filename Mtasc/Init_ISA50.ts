
if (CtType.ISA50)  
{
//======================================================================================================================
// Macrodefinierungen f�r Ein/Ausg�nge STM ISA50
//=======================================================================================================================
(MacroDef "in_WD_AX_enab" ) " 0 == ((trc.getPort 'B') and 0x0400);"; // -- 1. --- Watchdog aktiv --------
(MacroDef "in_BUSAK"      ) " 0 == ((trc.getPort 'B') and 0x0800);"; // -- 2. --- Endstufe will Bus �berbnehmen   
(MacroDef "in_Interrupt"  ) " 0 == ((trc.getPort 'C') and 0x0008);"; // -- 3. --- Endstufe fordert Interrupt  
(MacroDef "in_Eingang1"   ) " 0 == ((trc.getPort 'F') and 0x0001);"; // -- 4. --- Eingang 1    
(MacroDef "in_Eingang2"   ) " 0 == ((trc.getPort 'F') and 0x0002);"; // -- 5. --- Eingang 2   
(MacroDef "in_Eingang3"   ) " 0 == ((trc.getPort 'F') and 0x0004);"; // -- 6. --- Eingang 3  
(MacroDef "in_Eingang4"   ) " 0 == ((trc.getPort 'F') and 0x0008);"; // -- 7. --- Eingang 4  
(MacroDef "in_Eingang5"   ) " 0 == ((trc.getPort 'F') and 0x0010);"; // -- 8. --- Eingang 5 
(MacroDef "in_Eingang6"   ) " 0 == ((trc.getPort 'F') and 0x0020);"; // -- 9. --- Eingang 6 
(MacroDef "in_Eingang7"   ) " 0 == ((trc.getPort 'F') and 0x0040);"; // -- 10. -- Eingang 7  
(MacroDef "in_Eingang8"   ) " 0 == ((trc.getPort 'F') and 0x0080);"; // -- 11. -- Eingang 8    
(MacroDef "in_Power_ok"   ) " 0 == ((trc.getPort 'F') and 0x0100);"; // -- 12. -- Endstufe meldet bereit   
(MacroDef "in_LMT_1"      ) " 0 == ((trc.getPort 'F') and 0x0200);"; // -- 13. -- L�ngenme�taster 1
(MacroDef "in_LMT_2"      ) " 0 == ((trc.getPort 'F') and 0x0400);"; // -- 14. -- L�ngenme�taster 2     
(MacroDef "in_LMT_3"      ) " 0 == ((trc.getPort 'F') and 0x0800);"; // -- 15. -- L�ngenme�taster 3   
(MacroDef "in_Notaus_zu"  ) " 0 == ((trc.getPort 'F') and 0x1000);"; // -- 16. -- Notauskreis  geschlossen   
(MacroDef "in_AX_enable"  ) " 0 == ((trc.getPort 'F') and 0x2000);"; // -- 17. -- Freigabe der Achsen durch das SK Modul    
(MacroDef "in_Cover_lock" ) " 0 == ((trc.getPort 'F') and 0x4000);"; // -- 18. -- Haube verriegelt 
(MacroDef "in_Spind_steht") " 0 == ((trc.getPort 'F') and 0x8000);"; // -- 19. -- Spindelstillstand   
(MacroDef "in_Still"      ) " 0 == ((trc.getPort 'B') and 0x0002);"; // -- 20. -- Meldung von Endstufe stillstand   
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
(MacroDef "out_PWM")      = "if (%1) {trc.setPort 'A' 0x0008 0x0000;} else {trc.setPort 'A' 0x0008 0x0008;}"; // -- 21. --- Erzeugung 0-10V --
(MacroDef "out_Curr_up ") = "if (%1) {trc.setPort 'B' 0x0001 0x0000;} else {trc.setPort 'B' 0x0001 0x0001;}"; // -- 22. --- bei high Stromabsenkung auf 60%    
(MacroDef "out_Still")    = "if (%1) {trc.setPort 'B' 0x0010 0x0000;} else {trc.setPort 'B' 0x0010 0x0010;}"; // -- 23. --- schaltet Stillstand Achsen weg -- 
(MacroDef "out_Spindel")  = "if (%1) {trc.setPort 'B' 0x0020 0x0000;} else {trc.setPort 'B' 0x0020 0x0020;}"; // -- 24. --- Spindel freigabe --------------    
(MacroDef "out_AX_Rdy")   = "if (%1) {trc.setPort 'B' 0x0100 0x0000;} else {trc.setPort 'B' 0x0100 0x0100;}"; // -- 25. --- setzen, wenn Endstufen initialisiert sind ---    
(MacroDef "out_Cov_dis")  = "if (%1) {trc.setPort 'B' 0x0200 0x0000;} else {trc.setPort 'B' 0x0200 0x0200;}"; // -- 26. --- Haube sperren -------------------------------    
(MacroDef "out_AX_ena")   = "if (%1) {trc.setPort 'B' 0x1000 0x0000;} else {trc.setPort 'B' 0x1000 0x1000;}"; // -- 27. --- gibt Endstufen frei -------------------------    
(MacroDef "out_Int_ES")   = "if (%1) {trc.setPort 'C' 0x0001 0x0000;} else {trc.setPort 'C' 0x0001 0x0001;}"; // -- 28. --- f�r schnelle Reaktionen der Endstufen -------    
(MacroDef "out_BUS_Req")  = "if (%1) {trc.setPort 'C' 0x0002 0x0000;} else {trc.setPort 'C' 0x0002 0x0002;}"; // -- 29. --- Busanforderung an Endstufe ------------------    
(MacroDef "out_IO_ena")   = "if (%1) {trc.setPort 'C' 0x0004 0x0000;} else {trc.setPort 'C' 0x0004 0x0004;}"; // -- 30. --- gibt Ausg�nge frei --------------------------    
(MacroDef "Out_Trigg_WD") = "if (%1) {trc.setPort 'C' 0x0010 0x0000;} else {trc.setPort 'C' 0x0010 0x0010;}"; // -- 31. --- Watchdog Ausgang, alle 700ms LH Flanke     
(MacroDef "Out_Reset_WD") = "if (%1) {trc.setPort 'C' 0x0020 0x0000;} else {trc.setPort 'C' 0x0020 0x0020;}"; // -- 32. --- Watchdog reset, HL zum resetten --    
(MacroDef "out_CS_DAC1")  = "if (%1) {trc.setPort 'C' 0x0040 0x0000;} else {trc.setPort 'C' 0x0010 0x0040;}"; // -- 33. --- Erzeugung 0-10V -------    
(MacroDef "out_CS_DAC2")  = "if (%1) {trc.setPort 'C' 0x0080 0x0000;} else {trc.setPort 'C' 0x0080 0x0080;}"; // -- 34. --- Erzeugung 0-10V -------    
(MacroDef "out_CS_ADC1")  = "if (%1) {trc.setPort 'C' 0x0100 0x0000;} else {trc.setPort 'C' 0x0100 0x0100;}"; // -- 35. --- einlesen 0-10V ---------    
(MacroDef "out_CS_ADC2")  = "if (%1) {trc.setPort 'C' 0x0200 0x0000;} else {trc.setPort 'C' 0x0200 0x0200;}"; // -- 36. --- einlesen 0-10V ---------   
(MacroDef "out_BootL")    = "if (%1) {trc.setPort 'C' 0x2000 0x0000;} else {trc.setPort 'C' 0x2000 0x2000;}"; // -- 37. --- startet Bootloader auf Endstufe -- 
(MacroDef "out_CS_B")     = "if (%1) {trc.setPort 'G' 0x0001 0x0000;} else {trc.setPort 'G' 0x0001 0x0001;}"; // -- 38. --- Selekt Achse B ---    
(MacroDef "out_CS_X")     = "if (%1) {trc.setPort 'G' 0x0002 0x0000;} else {trc.setPort 'G' 0x0002 0x0002;}"; // -- 39. --- Selekt Achse X ---    
(MacroDef "out_CS_A")     = "if (%1) {trc.setPort 'G' 0x0004 0x0000;} else {trc.setPort 'G' 0x0004 0x0004;}"; // -- 40. --- Selekt Achse A ---    
(MacroDef "out_CS_Z")     = "if (%1) {trc.setPort 'G' 0x0008 0x0000;} else {trc.setPort 'G' 0x0008 0x0008;}"; // -- 41. --- Selekt Achse Z ---    
(MacroDef "out_CS_Y")     = "if (%1) {trc.setPort 'G' 0x0010 0x0000;} else {trc.setPort 'G' 0x0010 0x0010;}"; // -- 42. --- Selekt Achse Y ---    
(MacroDef "out_Muxer")    = "if (%1) {trc.setPort 'G' 0x0020 0x0000;} else {trc.setPort 'G' 0x0020 0x0020;}"; // -- 43. --- Multiplexer umschalten ------    
(MacroDef "out_CS_2") 	  = "if (%1) {trc.setPort 'G' 0x0040 0x0000;} else {trc.setPort 'G' 0x0040 0x0040;}"; // -- 44. --- CS --    
(MacroDef "out_CS_3")     = "if (%1) {trc.setPort 'G' 0x0080 0x0000;} else {trc.setPort 'G' 0x0080 0x0080;}"; // -- 45. --- CS -----    
(MacroDef "out_Ausgang1") = "if (%1) {trc.setPort 'G' 0x0100 0x0000;} else {trc.setPort 'G' 0x0100 0x0100;}"; // -- 46. --- Ausgang 1 --    
(MacroDef "out_Ausgang2") = "if (%1) {trc.setPort 'G' 0x0200 0x0000;} else {trc.setPort 'G' 0x0200 0x0200;}"; // -- 47. --- Ausgang 2 --  
(MacroDef "out_Ausgang3") = "if (%1) {trc.setPort 'G' 0x0400 0x0000;} else {trc.setPort 'G' 0x0400 0x0400;}"; // -- 48. --- Ausgang 3 --   
(MacroDef "out_Ausgang4") = "if (%1) {trc.setPort 'G' 0x0800 0x0000;} else {trc.setPort 'G' 0x0800 0x0800;}"; // -- 49. --- Ausgang 4 --   
(MacroDef "out_Ausgang5") = "if (%1) {trc.setPort 'G' 0x1000 0x0000;} else {trc.setPort 'G' 0x1000 0x1000;}"; // -- 50. --- Ausgang 5 --   
(MacroDef "out_Ausgang6") = "if (%1) {trc.setPort 'G' 0x2000 0x0000;} else {trc.setPort 'G' 0x2000 0x2000;}"; // -- 51. --- Ausgang 6 --     
(MacroDef "out_Ausgang7") = "if (%1) {trc.setPort 'G' 0x4000 0x0000;} else {trc.setPort 'G' 0x4000 0x4000;}"; // -- 52. --- Ausgang 7 --   
(MacroDef "out_Ausgang8") = "if (%1) {trc.setPort 'G' 0x8000 0x0000;} else {trc.setPort 'G' 0x8000 0x8000;}"; // -- 53. --- Ausgang 8 --     


(MacroDef "WD_reset")   =  " Out_Reset_WD 1 ; Out_ResetWD 0;";   //  WatchDog reset  ----- 
(MacroDef "WD_Trigger") =  " Out_Trigg_WD 1 ; Out_TriggWD 0;";   //  Watchdog trigger ----

// -- Chipselekte f�r Achsen definieren  %1 Achsnummer  %2 =1 select; =0 deselect ------
(MacroDef "Sel_AX") =
  "trc.setport 'G' 0x00ff 0xffff; 
   if(%1 == 0) { out_CS_X %2; } 
   if(%1 == 1) { out_CS_Y %2; } 
   if(%1 == 2) { out_CS_Z %2; } 
   if(%1 == 3) { out_CS_A %2; } 
   if(%1 == 4) { out_CS_B %2; } 
 ";
 
(MacroDef "Sel_off") = " trc.setport 'G' 0x001f 0xffff; ";  // alle AX_Selecte off

/////////////////////////////////////////////////////////////////////////////////////////////////////////   

//============================================================================
// Macrodefinierungen f�r serielle Kommunikation ISA50
//============================================================================
// === Kommunikation auf Steuerung -> DAC , ADC , I2C ========================
// Reihenfolge : Startbyte, Ziel, Nummer
// es m�ssen immer 4 byte �bertragen werden
// Kanal	   : UART 4
 RW_START      = 0x24 ; //  '$'    Start der �bertragung
 R_TMP275      = 20;  // Temp sensor TMP275 lesen  in �C
 R_NAME        = 21;  // Version lesen
 
 I2C_START     = 10;  // Startbedingung
 I2C_STOP      = 11;  // Stopbedingung
 I2C_R_START   = 12;  // ReStart
 I2C_READ_ACK  = 13;  // I2C direkt lesen  ACK senden
 I2C_READ_NACK = 14;  // I2C direkt lesen  NACK senden
 I2C_WRITE     = 15;  // I2C direkt schreiben

 WRITE_1A_1B   = 30;  // auf I2C mit 1 Adressen 1 Byte schreiben
 WRITE_2A_1B   = 31;  // auf I2C mit 2 Adressen 1 Byte schreiben
 WRITE_2A_2B   = 32;  // auf I2C mit 2 Adressen 2 Byte schreiben
 READ_1A_1B    = 33;  // von I2C mit 1 Adressen 1 Byte lesen
 READ_2A_1B    = 34;  // von I2C mit 2 Adressen 1 Byte lesen
 READ_2A_2B    = 35;  // von I2C mit 2 Adressen 2 Byte lesen
 PING_I2C      = 40;  // ist Kommunikation m�glich?     
 SUCHE_I2C     = 41;  // ist ein Ger�t mit dieser Adresse vorhanden ?
 Dummy         =  0;
//---------------------------------------------------------------------- 
// --- DAC Byte1 ------------------
// B07 B06 B05 B04 B03 B02 B01 B00
// A/B  -  GA  SD  D11 D10  D9  D8
// --- DAC Byte2 ------------------
// B07 B06 B05 B04 B03 B02 B01 B00
//  D7  D6  D5  D4  D3  D2  D1  D0
//----------------------------------
// A/B - 0 select A , 1 select B
// GA  - 0 Gain 2   , 1 Gain 1
// SD  - 0 ShutDown , 1 normal mode
//------------------------------------------------------------------------

(MacroDef "WR_DAC1") =  " out_CS_DAC1 1 ;  SPI2 %1 %2; out_CS_DAC1 0 ; ";
(MacroDef "WR_DAC2") =  " out_CS_DAC2 1 ;  SPI2 %1 %2; out_CS_DAC2 0 ; ";
(MacroDef "SPI2") = " trc.spiOutIn 2 %1;  trc.spiOutIn 2 %2 ; ";

//-------------------------------------------------------------------------
// Beispile : WR_DAC1 0x1f 0xff;   // DAC 1 Kanal A, Gain 2
// Beispile : WR_DAC1 0x9f 0xff;   // DAC 1 Kanal B, Gain 2
// Beispile : WR_DAC2 0x3f 0xff;   // DAC 2 Kanal A, Gain 1
// Beispile : WR_DAC2 0xBf 0xff;   // DAC 2 Kanal B, Gain 1
//===================================================================
//-------------------------------------------------------------------------
// --- ADC Byte1 ------------------
// T01 T02 T03 T04 T05 T06 T07 T08 
//   0   0   0   0   0   0   0   1  <- Start
// --- ADC Byte2 ------------------
// T09 T10 T11 T12 T13 T14 T15 T16 
// SGL ODD MSB   x   x   x   x   x 
// --- ADC Byte3 ------------------
// T17 T18 T19 T20 T21 T22 T23 T24 
//   x   x   x   x   x   x   x   x 
//----------------------------------
// SGL ODD  Kanal
//  1    0    0   
//  1    1    1 
// MSB 1 -> MSB first MSB 0 -> LSB first
//----------------------------------------------------------------------------------------------------------------------------

(MacroDef "RD_ADC1") =  " out_CS_ADC1 1 ;  ? trc.spiOutIn 2 %1; ?  trc.spiOutIn 2 %2 ;? trc.spiOutIn 2 %3; out_CS_ADC1 0 ; ";
(MacroDef "RD_ADC2") =  " out_CS_ADC2 1 ;  ? trc.spiOutIn 2 %1; ?  trc.spiOutIn 2 %2 ;? trc.spiOutIn 2 %3; out_CS_ADC2 0 ; ";

//------------------------------------------------------------------------------------------------------------------------------ 
// Beispile : RD_ADC1 0x01 0xA0 0x00;
// Beispile : RD_ADC2 0x01 0xA0 0x00;
//-------------------------------------------------------------------------------------

(MacroDef "Mach_es") = 
   " trc.uartTransmit 4 RW_START ;  // 
     trc.uartTransmit 4       %1 ;  // Befehl
     trc.uartTransmit 4       %2 ;  // Adresse1
     trc.uartTransmit 4       %3 ;  // Adresse2
     trc.uartTransmit 4       %4 ;  // Wert1
     trc.uartTransmit 4       %5 ;  // Wert2
   ";

(MacroDef "Lese_Version") = 
   " trc.uartTransmit 4 RW_START;  // Startzeichen
     trc.uartTransmit 4 R_NAME  ;  // Befehl lese Name 
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     wait 0.1; ? trc.uartreceive 4; 
     wait 0.1; ? trc.uartreceive 4;  
     wait 0.1; ? trc.uartreceive 4; 
     wait 0.1; ? trc.uartreceive 4; 
   ";


(MacroDef "Lese_PING") = 
   " trc.uartTransmit 4 RW_START;  // Startzeichen
     trc.uartTransmit 4 PING_I2C;  // Befehl PING
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     wait 0.1; ? trc.uartreceive 4;   
     wait 0.1; ? trc.uartreceive 4; 
     wait 0.1; ? trc.uartreceive 4;     
   ";

(MacroDef "Lese_Sensor") = 
   " trc.uartTransmit 4 RW_START;  // Startzeichen
     trc.uartTransmit 4 R_TMP275;  // Befehl lese Sensor
     trc.uartTransmit 4      %1 ;  // Nummer
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     trc.uartTransmit 4       0 ;  // Dummy
     wait 0.1; ? trc.uartreceive 4;   
   ";
//-------------------------------------------------------------------------------------------
// === Kommunikation mit Endstufen ======================================
// --- DRV8711 ----------------------------------------------------------
// Reihenfolge : Startbyte, AchsNr,Ziel, Datum1, Datum2, Stopbyte
// Kanal	   : UART 1

 RW_START = 0x24 ; //  '$'    Start der �bertragung
 RW_STOP  = 0x23 ; //  '#'    Ende der �bertragung 

// -- m�gliche Ziele -------------------------------- 
 W_STROM  = 0x4D ; //  'M'    Motorstrom schreiben
 W_RESOL  = 0x41 ; //  'A'    Schrittaufl�sung schreiben
 R_NAME   = 0x4E ; //  'N'    Namen lesen
 R_STATUS = 0x53 ; //  'S'    Statusregister lesen

// --- Ziele f�r erweiterten bereich -------------------
 W_TEMPO  = 0x54 ; // 'T'  Daten nur tempor�r ablegen
 W_EEPROM = 0x45 ; // 'E'  Daten im EEPROM ablegen
 R_EEPROM = 0x46 ; // 'F'' Daten aus EEPROM lesen
 W_STATUS = 0x55 ; // 'U'  Statusregister schreiben
 
  (MacroDef "RW_Register_DRV") = 
   " Sel_AX  %1 0;
     trc.uartTransmit 1 RW_START ; // Startzeichen
     trc.uartTransmit 1       %1 ; // Achsnummer
     trc.uartTransmit 1       %2 ; // Ziel
     trc.uartTransmit 1       %3 ; // High Byte
     trc.uartTransmit 1       %4 ; // Low Byte
     trc.uartTransmit 1  RW_STOP ; // Endezeichen
     Sel_AX  %1 1;
   ";

/////////////////////////////////////////////////////////////////////////////////////////////

  (MacroDef "Set_Current") = 
   " Sel_AX  %1 0;
     trc.uartTransmit 1 RW_START ;  // 
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1  W_STROM ;  // Ziel : Strom schreiben
     trc.uartTransmit 1       %2 ;  // Strom in 1/10 A
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Set_Resol") = 
   " Sel_AX  %1 0;
     trc.uartTransmit 1 RW_START ;  // 
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1  W_RESOL ;  // Ziel : Aufl�sung schreiben
     trc.uartTransmit 1       %2 ;  // Aufl�sung, z.B. 16 => 1/16tel Schritt
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Read_Name") = 
   " Sel_AX  %1 0; 
     trc.uartTransmit 1 RW_START ;
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1   R_NAME ;  // Ziel : Namen lesen
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Read_Status") = 
   " Sel_AX  %1 0; 
     trc.uartTransmit 1 RW_START ;
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1 R_STATUS ;  // Ziel : Status lesen
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
// ===========================================================================
// **  Motor Amplifier Properties *****************************************
// == maximale Anzahl Achsen  : 5
//===================================================================
// -- xml Element anlegen falls nicht vorhanden und vorbelegen ------

  //CMotAmp = ObjDef T_classDscr
  //"Strom"    T_double  0
  //"Step"     T_long    0
  //;

  Anzahl_Achsen = 5 ;			// maximale Anzahl anlegen
  MotAmp = gcnew CMotAmp Anzahl_Achsen;

  if(0 == MtApp.xml.Hardware._elExists "MotAmp")
    {
      local "i";
      i = 0; 
      for Anzahl_Achsen
       {
         MotAmp[i].strom  = 2.6;	// Strom in Ampere
         MotAmp[i].Step   = 16 ;    	// 1/16 Schritt
         i = i + 1;
       }
      MtApp.xml.Hardware.!E!MotAmp._Mt2Xml MotAmp;
    }

//================================================================================================
// Strom absenken bzw. anheben 
// �bergabewert ist der Sollstromwert in Prozent
//==================================================================== 
 (MacroDef "Strom_Prozent") = 
   "
    MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --
    local \"ax\" ;
    local \"Mot_Strom\" ;
    Mot_Strom = 0 ;
    ax        = 0 ; 
    for Anzahl_Achsen
     {
        Mot_Strom = (MotAmp[ax].strom  * %1 / 10); // Angabe effektiv Strom in 1/10 A
        Set_Current ax Mot_Strom;   
        ax = ax + 1;
      }
   "; 

//----------------------------------------------------------------------------------------------------------  

  local "ax";
  local "Step_Res";
  local "Mot_Strom";
  Mot_Strom = 0;
  Step_Res  = 0;
  ax        = 0; 
	
   MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --
   Anzahl_Achsen = mtag[0].vDim;

   if (Anzahl_Achsen > 5) 
   {
    // MsgBox "Zuviele Endstufen eingestellt!"; 
    SendError "ERR_Controller_InvalidNumberOfAxes";
    exit;
   } 
   
   out_Ausgang_ena 0; 
   if (in_Power1_ok == 0)
   {
     // MsgBox "Spannung einschalten.";
     SendError "ERR_Controller_NoMainPower";
     
     while "in_Power1_ok == 0" {}
   }
   
  // while "in_Power1_ok == 0"  {MsgBox "Spannung einschalten."   "Power"  Mb_iconhand +  Mb_okcancel; }
   i = 1;    for 12 { _oset i 0; i = i + 1;_dspupdate;}   // alle Ausg�nge ausschalten
   sel_off; 				 	     	  // alle Achsselekte abschalten
  

   for Anzahl_Achsen
     {
       Mot_Strom = MotAmp[ax].strom * 10 ; // Angabe effektiv Strom in 1/10A
       Set_Current ax Mot_Strom;
   
       Step_Res  = MotAmp[ax].Step;
       Set_Resol ax Step_Res;
   
       ax = ax + 1;
     }
   out_AX_Ready_SK 1;           // Meldung : "Achsen bereit" ausgeben f�r SK
   out_Ausgang_ena 1;       	// enable Ausg�nge  
   out_Bremse 1;                // Bremse freigeben
}

else
{
  // MsgBox "Fehler"; 
  SendError "ERR_Controller_InvalidController";
  exit;
} 
  
//===============================================================================