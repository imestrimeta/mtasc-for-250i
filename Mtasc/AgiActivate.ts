/*====================================================================*/
/* Activates AGI                                                      */
/*====================================================================*/
/* This file is called from AgiActivate macro.                        */ 
/* Parameters: AGI, local in AgiActivate.                             */ 
/*--------------------------------------------------------------------*/

mtagC => mtag[mtagI];   // global alias to current mtag data
Thread_SetAgi *mtagC.agi;

// --------------------------------------------------------------
// GCode settings
// --------------------------------------------------------------

// define as thread local for main thread (graphic + immediate)
localThr "_gFuncTable" "_gFuncGroup" "_gFuncFHandler" "_gFuncSHandler";

if(T_Undefined != (Typeof "GCode").type)
{
  _gFuncTable    = &(gCode[mtagI].fTable);
  _gFuncGroup    = &(gCode[mtagI].fGroup);
  _gFuncFHandler = &(gCode[mtagI].FHandler);
  _gFuncSHandler = &(gCode[mtagI].SHandler);
}

