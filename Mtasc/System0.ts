
// Initialisierungen vor Laden des Treibers

if(MtApp.xml._elExists "MtDrv")  // nur prüfen wenn Treiber genutzt wird
{
  if("TRC2" == MtApp.xml.MtDrv.laa.v0.PdType.o0.!A?v0)
  {
    if(0 == "T_string" == MtApp.xml.MtDrv.laa.v0.HwType.o0.!A!t)   // update xml data
    {
      local "vdim" "ht" "df" "i";
      vdim = scall MtApp.xml.MtDrv.laa.v0.vDim.!A?v0;
      ht = gcnew T_object vdim;
      df = array vdim 0.0;
      i = 0; for vdim
      {
        ht[i] = &("0");
        df[i] = 256000.0;
        i = i + 1;
      }
      MtApp.xml.MtDrv.laa.v0.HwType._Mt2Xml ht;
      MtApp.xml.MtDrv.laa.v0.dAcdrDFac._Mt2Xml df;
    }
    LoadDll MtApp.path & "/TriCon2.dll"; trc = gcnew CTriCon2 0 1;
  }
}