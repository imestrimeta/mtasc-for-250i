//============================================================================
// Macrodefinierungen f�r Ein/Ausg�nge STM   MC4-40
//============================================================================
//---------------------------------------------------------------------------------------------------------------------------------------------
//Pos  Makro                    Beschreibung                           Zusatz
//---------------------------------------------------------------------------------------------------------------------------------------------
// 1    in_AX_ready	        Endstufe bereit 			Achsen bereit
// 2    in_Notaus_zu       	Notauskreis  geschlossen		�berwachung Notaus
// 3    in_Power1_ok        Power vorhanden				48V eingeschaltet
// 4    in_Cover_lock       Haube verriegelt                        Haube geschlossen und verriegelt, Achsen d�rfen bewegt werden
// 5    in_Spindel_steht    Spindelstillstand                       wird vom FU bereitgestellt, wenn Spindel im Stillstand
//---------------------------------------------------------------------------------------------------------------------------------------------
// 6    out_Curr_down       bei high Stromabsenkung auf 60%    	sobald Takte ausgegeben werden, wird der Strom auf 100% gesetzt
// 7    out_Achsen_ena     	Endstufen freigeben			TMC bekommen enabel
// 8    out_Ausgang_ena     gibt Ausg�nge frei
// 9    out_PWM             Erzeugung 0-10V
// 10   out_Spindel         Spindel freigabe                        Spindel einschalten
// 11   out_WZW    	        Werkzeuwechsel                        
//---------------------------------------------------------------------------------------------------------------------------------------------

//  1. low, wenn Endstufenkarte bereit -------------------------------------
  (MacroDef "in_AX_ready")      =  " 0 == ((trc.getPort 'E') and 0x0080);";
         
//  2. Notauskreis geschlossen ---------------------------------------------
  (MacroDef "in_Notaus_zu")     =  " 0 == ((trc.getPort 'E') and 0x0100);"; 

//  3. Netzteil 48V ein   ---------------------------------------------
  (MacroDef "in_Power1_ok")     =  " 0 == ((trc.getPort 'E') and 0x0200);";    

//  4. Signal von Haube : verriegelt --------------------------------------
  (MacroDef "in_Cover_lock")    =  " 0 == ((trc.getPort 'E') and 0x0400);";    

//  5. Signal vom FU   : Spindelstillstand --------------------------------
  (MacroDef "in_Spindel_steht") =  " 0 == ((trc.getPort 'E') and 0x0800);";
  
//-------------------------------------------------------------------------------

//  6. Strom f�r Endstufen normal oder abgesenkt ---------------------
  (MacroDef "out_Curr_down") =
    "if (%1) {trc.setPort 'B' 0x0001 0x0000;}
     else    {trc.setPort 'B' 0x0001 0x0001;}";

  //  7. Endstufen freigeben -------------------------------------------
  (MacroDef "out_AX_enable") =
    "if (%1) {trc.setPort 'B' 0x1000 0x0000;}
     else    {trc.setPort 'B' 0x1000 0x1000;}"; 

  //  8. gibt die Ausg�nge frei ----------------------------------------
  (MacroDef "out_Ausgang_ena") =
    "if (%1) {trc.setPort 'C' 0x0004 0x0000;}
     else    {trc.setPort 'C' 0x0004 0x0004;}";

  //  9. PWM ausgeben --------------------------------------------------

  //  10. Spindel schalten ----------------------------------------------
  (MacroDef "out_Spindel") =
    "if (%1) {trc.setPort 'B' 0x0020 0x0000;}
     else    {trc.setPort 'B' 0x0020 0x0020;}"; 

  //  11. Werkzeugwechsel ----------------------------------------------
  (MacroDef "out_WZW") =
    "if (%1) {trc.setPort 'C' 0x0010 0x0000;}
     else    {trc.setPort 'C' 0x0010 0x0010;}";
  
  //  12. Dummy f�r Bremse ----------------------------------------------
  (MacroDef "out_Bremse")   =   "";
 
  //  13. Dummy f�r Achsen bereit Meldung ----------------------------------
  (MacroDef "out_AX_Ready") =  "";
//***************************************************************************************************   

