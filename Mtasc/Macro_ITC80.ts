//============================================================================
// Macrodefinierungen f�r Ein/Ausg�nge STM ITC80
//============================================================================
//---------------------------------------------------------------------------------------------------------------------------------------------
//Pos  Makro                 Beschreibung                               Zusatz
//---------------------------------------------------------------------------------------------------------------------------------------------
// 1    in_X_ready	    	Karte 1 ist initialisiert				Achse X bereit
// 2    in_Y_ready	    	Karte 2 ist initialisiert				Achse Y bereit
// 3    in_Z_ready          Karte 3 ist initialisiert	        	Achse Z bereit
// 4    in_A_ready          Karte 4 ist initialisiert	       		Achse A bereit
// 5    in_B_ready          Karte 5 ist initialisiert	        	Achse B bereit
// 6    in_C_ready	      	Karte 6 ist initialisiert	       		Achse C bereit
// 7    in_D_ready          Karte 7 ist initialisiert	        	Achse D bereit
// 8    in_E_ready          Karte 8 ist initialisiert	        	Achse E bereit
// 9    in_Notaus_zu       	Notauskreis  geschlossen				�berwachung Notaus
// 10   in_AX enable        Freigabe der Achsen durch das SK Modul  �berwachung AX-enable-Eingang
// 11   in_Cover_lock       Haube verriegelt                        Haube geschlossen und verriegelt, Achsen d�rfen bewegt werden
// 12   in_Spindel_steht    Spindelstillstand                       wird vom FU bereitgestellt, wenn Spindel im Stillstand
// 13   in_WD_AX_enab       Watchdog aktiv	                 		Bei high hat Watchdog die Achsen freigegeben
//---------------------------------------------------------------------------------------------------------------------------------------------
// 14   out_Curr_up         	bei high Stromabsenkung auf 60%    	   	sobald Takte ausgegeben werden, wird der Strom auf 100% gesetzt
// 15   out_AX_Ready_SK     	setzen, wenn Endstufen initialisiert sind	Signal f�r das SK Modul, da� Steuerung bereit
// 16   out_Bremse          	gibt Bremse frei
// 17   out_Cover_disable   	Haube sperren                                   um versehentliches �ffnen w�hrend einer Bearbeitung zu vermeiden
// 18   out_AX_enable         	gibt Endstufen frei         		   	ver-und-et mit  in_AX enable 
// 19   out_Ausgang_ena       	gibt Ausg�nge frei
// 20   out_PWM               	Erzeugung 0-10V
// 21   out_Spindel           	Spindel freigabe                                Signal zum SK , SK schaltet Spindel
// 22	Out_WD_reset	      	Watchdog reset					braucht eine HL Flanke zum reseten des Watchdgs
// 23	Out_WD_trigger	      	Watchdog Ausgang	   			mu� sp�testens alle 150ms eine LH Flanke haben	
//----------------------------------------------------------------------------------------------------------------------------------------------
// 24   Sel_X	      	 	Selekt Karte 1 			  	  Achse X w�hlen
// 25   Sel_Y	      		Selekt Karte 2 			  	  Achse Y w�hlen
// 26   Sel_Z             	Selekt Karte 3 	          		  Achse Z w�hlen
// 27   Sel_A              	Selekt Karte 4 	       		  	  Achse A w�hlen
// 28   Sel_B              	Selekt Karte 5         	 		  Achse B w�hlen
// 29   Sel_C	      		Selekt Karte 6       		  	  Achse C w�hlen
// 30   Sel_D              	Selekt Karte 7                		  Achse D w�hlen
// 31   Sel_E              	Selekt Karte 8 		                  Achse E w�hlen
//---------------------------------------------------------------------------------------------------------------------------------------------
// -- 1. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_X_ready") =  " 0 == ((trc.getPort 'C') and 0x0100);";
// -- 2. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_Y_ready") =  " 0 == ((trc.getPort 'C') and 0x0200);";
// -- 3. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_Z_ready") =  " 0 == ((trc.getPort 'C') and 0x0400);";
// -- 4. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_A_ready") =  " 0 == ((trc.getPort 'C') and 0x0800);";
// -- 5. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_B_ready") =  " 0 == ((trc.getPort 'C') and 0x1000);";
// -- 6. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_C_ready") =  " 0 == ((trc.getPort 'C') and 0x2000);";
// -- 7. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_D_ready") =  " 0 == ((trc.getPort 'C') and 0x4000);";
// -- 8. low, wenn Endstufenkarte bereit -------------------------------------
(MacroDef "in_E_ready") =  " 0 == ((trc.getPort 'C') and 0x0080);";   

// -- 9. Notauskreis geschlossen ---------------------------------------------
(MacroDef "in_Notaus_zu")     =  " 0 == ((trc.getPort 'F') and 0x1000);"; 
// -- 10. Endstufen haben freigabe durch SK Modul ----------------------------
(MacroDef "in_AX_enable")     =  " 0 == ((trc.getPort 'F') and 0x2000);";    
// -- 11. Signal von Haube : verriegelt --------------------------------------
(MacroDef "in_Cover_lock")    =  " 0 == ((trc.getPort 'F') and 0x4000);";    
// -- 12. Signal vom FU oder SK : Spindelstillstand --------------------------
(MacroDef "in_Spindel_steht") =  " 0 == ((trc.getPort 'F') and 0x8000);";
// -- 13. Freigabesignal vom Watchdog ----------------------------------------
(MacroDef "in_WD_AX_enable")  =  " 0 == ((trc.getPort 'B') and 0x0400);";   

// -- f�r Kompatibilit�t Power Ok ---------------------------------------
(MacroDef "in_Power1_ok")     =  " 0 == ((trc.getPort 'C') and 0x0400);"; 
//(MacroDef "in_Power1_ok")     =  " 0 == ((trc.getPort 'F') and 0x2000);"; 

//  14. Strom f�r Endstufen normal oder abgesenkt ---------------------
(MacroDef "out_Curr_down") =
  "if (%1) {trc.setPort 'B' 0x0001 0x0000;}
   else    {trc.setPort 'B' 0x0001 0x0001;}";

//  15. Meldung an SK : Steuerung bereit ------------------------------
(MacroDef "out_AX_rdy_SK") =
  "if (%1) {trc.setPort 'B' 0x0100 0x0000;}
   else    {trc.setPort 'B' 0x0100 0x0100;}";

//  16. Bremse schalten -----------------------------------------------
(MacroDef "out_Bremse") =
  "if (%1) {trc.setPort 'C' 0x0002 0x0000;}
   else    {trc.setPort 'C' 0x0002 0x0002;}";
 
//  16. Meldung an SK : Achsen sind initialisiert und bereit ----------
(MacroDef "out_AX_Ready_SK") =
  "if (%1) {trc.setPort 'B' 0x0100 0x0000;}
   else    {trc.setPort 'B' 0x0100 0x0100;}"; 

//  17. Haube sperren, z.B weil noch ein Programm l�uft ---------------
(MacroDef "out_Cover_disable") =
  "if (%1) {trc.setPort 'B' 0x0200 0x0000;}
   else    {trc.setPort 'B' 0x0200 0x0200;}"; 

//  18. Endstufen freigeben (ver-undet mit SK) ------------------------
(MacroDef "out_AX_enable") =
  "if (%1) {trc.setPort 'B' 0x1000 0x0000;}
   else    {trc.setPort 'B' 0x1000 0x1000;}"; 

//  19. gibt die Ausg�nge frei ----------------------------------------
(MacroDef "out_Ausgang_ena") =
  "if (%1) {trc.setPort 'C' 0x0004 0x0000;}
   else    {trc.setPort 'C' 0x0004 0x0004;}";

//  20. PWM ausgeben --------------------------------------------------

//  21. Spindel schalten ----------------------------------------------
(MacroDef "out_Spindel") =
  "if (%1) {trc.setPort 'B' 0x0020 0x0000;}
   else    {trc.setPort 'B' 0x0020 0x0020;}"; 

//  22. WatchDog reset ------------------------------------------------
(MacroDef "out_WD_reset")   =
  "trc.setPort 'C' 0x0020 0x0000; trc.setPort 'C' 0x0020 0x0020;";    

//  23. Watchdog trigger ----------------------------------------------
(MacroDef "out_WD_Trigger") =
  "trc.setPort 'C' 0x0010 0x0010; trc.setPort 'C' 0x0010 0x0000;";      
 
// 24. - 31.  Chipselekte f�r Achsen definieren ---------------------------------------------------
// ester Wert %1 Achsnummer, zweiter Wert %2 =0 select; =1 deselect

(MacroDef "Sel_AX") =
  "trc.setport 'G' 0x00ff 0xffff; 
   if(%1 == 0) { if (%2) {trc.setPort 'G' 0x0020 0x0020;} else {trc.setPort 'G' 0x0020 0x0000;} }
   if(%1 == 1) { if (%2) {trc.setPort 'G' 0x0010 0x0010;} else {trc.setPort 'G' 0x0010 0x0000;} }
   if(%1 == 2) { if (%2) {trc.setPort 'G' 0x0008 0x0008;} else {trc.setPort 'G' 0x0008 0x0000;} }
   if(%1 == 3) { if (%2) {trc.setPort 'G' 0x0004 0x0004;} else {trc.setPort 'G' 0x0004 0x0000;} }
   if(%1 == 4) { if (%2) {trc.setPort 'G' 0x0001 0x0001;} else {trc.setPort 'G' 0x0001 0x0000;} }
   if(%1 == 5) { if (%2) {trc.setPort 'G' 0x0002 0x0002;} else {trc.setPort 'G' 0x0002 0x0000;} }
   if(%1 == 6) { if (%2) {trc.setPort 'G' 0x0080 0x0080;} else {trc.setPort 'G' 0x0080 0x0000;} }
   if(%1 == 7) { if (%2) {trc.setPort 'G' 0x0040 0x0040;} else {trc.setPort 'G' 0x0040 0x0000;} }
 ";
 
(MacroDef "Sel_off") = " trc.setport 'G' 0x00ff 0xffff; ";  // alle AX_Selecte off

//============================================================================
// Macrodefinierungen f�r serielle Kommunikation ITC80 
//============================================================================
 RW_START = 0x24 ; //  '$'    Start der �bertragung
 RW_STOP  = 0x23 ; //  '#'    Ende der �bertragung 

// -- m�gliche Ziele -------------------------------- 
 W_STROM  = 0x4D ; //  'M'    Motorstrom schreiben
 W_RESOL  = 0x41 ; //  'A'    Schrittaufl�sung schreiben
 R_NAME   = 0x4E ; //  'N'    Namen lesen
 R_STATUS = 0x53 ; //  'S'    Statusregister lesen

// --- Ziele f�r erweiterten bereich -------------------
 W_TEMPO  = 0x54 ; // 'T'  Daten nur tempor�r ablegen
 W_EEPROM = 0x45 ; // 'E'  Daten im EEPROM ablegen
 R_EEPROM = 0x46 ; // 'F'' Daten aus EEPROM lesen
 W_STATUS = 0x55 ; // 'U'  Statusregister schreiben

//============================================================================
// Macrodefinierungen f�r serielle Kommunikation ITC80 
//============================================================================
// --- DRV8711 ----------------------------------------------------------
// Reihenfolge : Startbyte, AchsNr,Ziel, Datum1, Datum2, Stopbyte

(MacroDef "RW_Register_DRV") = 
 " Sel_AX  %1 0;
   trc.uartTransmit 1 RW_START ; // Startzeichen
   trc.uartTransmit 1       %1 ; // Achsnummer
   trc.uartTransmit 1       %2 ; // Ziel
   trc.uartTransmit 1       %3 ; // High Byte
   trc.uartTransmit 1       %4 ; // Low Byte
   trc.uartTransmit 1  RW_STOP ; // Endezeichen
   Sel_AX  %1 1;
 ";

// --- TMC262 ------------------------------------------------------------
// Reihenfolge : Startbyte, AchsNr,Ziel, Datum1, Datum2, Datum3, Stopbyte

(MacroDef "RW_REgister_TMC") = 
 " Sel_AX  %1 0;
   trc.uartTransmit 1 RW_START ; // Startzeichen
   trc.uartTransmit 1       %1 ; // Achsnummer
   trc.uartTransmit 1       %2 ; // EEPROM oder temp
   trc.uartTransmit 1       %3 ; // Byte1
   trc.uartTransmit 1       %4 ; // Byte3
   trc.uartTransmit 1       %5 ; // Byte3
   trc.uartTransmit 1  RW_STOP ; // Endezeichen
   Sel_AX  %1 1;
 ";

 /////////////////////////////////////////////////////////////////////////////////////////////

(MacroDef "Set_Current") = 
 " Sel_AX  %1 0;
   trc.uartTransmit 1 RW_START ;  // 
   trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
   trc.uartTransmit 1  W_STROM ;  // Ziel : Strom schreiben
   trc.uartTransmit 1       %2 ;  // Strom in 1/10 A
   trc.uartTransmit 1  RW_STOP ;  // Endezeichen
   Sel_AX  %1 1;
 ";
 
(MacroDef "Set_Resol") = 
 " Sel_AX  %1 0;
   trc.uartTransmit 1 RW_START ;  // 
   trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
   trc.uartTransmit 1  W_RESOL ;  // Ziel : Aufl�sung schreiben
   trc.uartTransmit 1       %2 ;  // Aufl�sung, z.B. 16 => 1/16tel Schritt
   trc.uartTransmit 1  RW_STOP ;  // Endezeichen
   Sel_AX  %1 1;
 ";
 
(MacroDef "Read_Name") = 
 " Sel_AX  %1 0; 
   trc.uartTransmit 1 RW_START ;
   trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
   trc.uartTransmit 1   R_NAME ;  // Ziel : Namen lesen
   trc.uartTransmit 1  RW_STOP ;  // Endezeichen
   Sel_AX  %1 1;
 ";
 
(MacroDef "Read_Status") = 
 " Sel_AX  %1 0; 
   trc.uartTransmit 1 RW_START ;
   trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
   trc.uartTransmit 1 R_STATUS ;  // Ziel : Status lesen
   trc.uartTransmit 1  RW_STOP ;  // Endezeichen
   Sel_AX  %1 1;
 ";
   