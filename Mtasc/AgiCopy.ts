/*====================================================================*/
/* Copy thread local variables from main thread                       */
/*====================================================================*/

localThr "Zup";      Zup      = _fParm.Zup;     
localThr "Zdn";      Zdn      = _fParm.ZdnMill[0]; 
localThr "Zdn0";     Zdn0     = _fParm.Zdn0; 
localThr "Zdn0Flag"; Zdn0Flag = _fParm.Zdn0Flag;

localThr "AnzRep" "CloneCount"; AnzRep = CloneCount = 0;
/*--------------------------------------------------------------------*/
// thread local AGI variables to be initialized
/*--------------------------------------------------------------------*/
_grMode = 1;                            /* initialize in graphic mode */ 
_ncMode = 2;

/*--------------------------------------------------------------------*/
/* thread local other variables to be initilized                      */
/*--------------------------------------------------------------------*/
localThr "_MOpen";  _MOpen  = 0;        /* spline flags (from shapes) */
localThr "_MClose"; _MClose = 0;

localThr "_ContOn";    _ContOn    = 0;  /* contour flags (from Dxf)   */
localThr "_ContState"; _ContSTate = 0; 

