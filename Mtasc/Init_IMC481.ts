// -- IMC481 Endstufen Initialisierung --------------------------------------  

local "ax";
local "Step_Res";
local "Mot_Strom";
local "Int_Dedge";
local "Temp";
local "EEP";
local "i";

Mot_Strom = 0;
Step_Res  = 0;
Int_Dedge = 0;
ax        = 0; 
Temp      = 0x54;
EEP       = 0x45;

sel_off; 											// alle Achsselekte abschalten
MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; 		// -- Werte aus der xml lesen --
Anzahl_Achsen = mtag[0].vDim;
if(Anzahl_Achsen > 5) 
{
 // MsgBox "Zuviele Endstufen eingestellt!"; 
 SendError "ERR_Controller_InvalidNumberOfAxes";
 exit;
}
 
for Anzahl_Achsen
 {
    Step_Res = 4;
    if (MotAmp[ax].Step == 256) {Step_Res = 0;}
    if (MotAmp[ax].Step == 128) {Step_Res = 1;}
    if (MotAmp[ax].Step ==  64) {Step_Res = 2;}
    if (MotAmp[ax].Step ==  32) {Step_Res = 3;}
    if (MotAmp[ax].Step ==  16) {Step_Res = 4;}
    if (MotAmp[ax].Step ==   8) {Step_Res = 5;}
    if (MotAmp[ax].Step ==   4) {Step_Res = 6;}
    if (MotAmp[ax].Step ==   2) {Step_Res = 7;}
    if (MotAmp[ax].Step ==   1) {Step_Res = 8;}

    if ((MotAmp[ax].intPol < 0) or (MotAmp[ax].intPol > 1)) {MotAmp[ax].intPol = 1;}
    if ((MotAmp[ax].DEDGE  < 0) or (MotAmp[ax].DEDGE  > 1)) {MotAmp[ax].DEDGE  = 0;}

    if (MotAmp[ax].strom > 4.5) {MotAmp[ax].strom = 4.5;}
     Mot_Strom = (MotAmp[ax].strom * 7.071) + 0.5 ; // Angabe effektiv Strom
    // Mot_Strom = (MotAmp[ax].strom * 5)     ; // Angabe Spitzenstrom  
 	 
    Int_Dedge = (MotAmp[ax].intPol * 2) + MotAmp[ax].DEDGE;

    // -- Werte in die Endstufen schreiben --------------------------------
    // Standardwerte vorbelegen, Auflösung und Stromwerte werden übergeben
    // %1 -> Achsnummer 
    // %2 -> Strom in Current Scale
    // %3 -> Auflösung
    // %4 -> Interpolation, Flankenauswertung

    Init_AX ax Mot_Strom Step_Res Int_DEDGE;
    ax = ax + 1;
   }
