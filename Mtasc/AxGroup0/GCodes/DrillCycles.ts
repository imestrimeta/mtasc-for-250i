/*====================================================================*/
/* Macros to execute special G-Code for Canned Drill Cycles           */
/*====================================================================*/
/* Parameters: GCode (81-89, 73, 74, 76), local in calling macro.     */
/* ------------------------------------------------------------------ */
local "px" "py" "pz" "pr" "pq" "pp" "pf" "pl" "ps" "pi" "pj";

px = *_gc.reg['X'];                            /* XYZ coordinates      */
py = *_gc.reg['Y'];
pz = *_gc.reg['Z'];

pr = *_gc.reg['R'];                            /* retract point (z)    */
pq = *_gc.reg['Q'];                            /* peck depth    (z)    */
pp = *_gc.reg['P'];                            /* dwell time           */
 
pl = *_gc.reg['L'];                            /* number of repetitions*/

ps = *_gc.reg['S'];                            /* spindle rotation     */

pi = *_gc.reg['I'];                            /* delta positioning    */
pj = *_gc.reg['J'];                            /* delta spindle        */

(MacroDef "WaitX") = "if(_mode != 'G') {Wait %1;}";

local "iPlane"; iPlane = _GCodePar.PlaneIdx;  // PlaneIdx aus G-Code-Struktur einf�gen.
local "rPeck";  rPeck  = 0.5; // ##### m-Wert, in Sammlung von Einstellungen.

/*====================================================================*/
/* get parameters and locations                                       */ 
/*====================================================================*/
if(1 > px.n + py.n + pz.n + pr.n)
 {mreset sprint "G%d requires X, Y, Z or P!" GCode;}

if(pl.n)
 {mreset sprint "Parameter L ist not yet implemented in G%d" GCode;}

if(pp.n) {GcMod.P = pp.r[0];}
local "Dwell"; Dwell = GcMod.P;               /* localize dwell (P)   */

/* ------------------------------------------------------------------ */
/* Get initial point pIni                                             */ 
/* ------------------------------------------------------------------ */
local "pIni"; pIni = _OcNext;                 /* get current location */

if(_gc.ggStat.v[3] == 91)                     /* relative xy-position */
{
  if((px.n > 0) AND (iPlane != 0)) {pIni[0] = pIni[0] + px.r[0];}
  if((py.n > 0) AND (iPlane != 1)) {pIni[1] = pIni[1] + py.r[0];}
  if((pz.n > 0) AND (iPlane != 2)) {pIni[2] = pIni[2] + pz.r[0];}
}
else                                          /* absolute xy-position */
{
  if((px.n > 0) AND (iPlane != 0)) {pIni[0] = px.r[0];}
  if((py.n > 0) AND (iPlane != 1)) {pIni[1] = py.r[0];}
  if((pz.n > 0) AND (iPlane != 2)) {pIni[2] = pz.r[0];}
}

/* ------------------------------------------------------------------ */
/* Get retract point pRet                                             */
/* ------------------------------------------------------------------ */
local "pRet"; pRet = pIni;                    /* retract point        */

if(pr.n)                                      /* retract is present   */
{ 
  GcMod.R = pr.r[0];                          /* absolute R-position  */
  if(_gc.ggStat.v[3] == 91)
   {GcMod.R = pIni[iPlane] - pr.r[0];}       /* relative R-position  */
}

pRet[iPlane] = GcMod.R;

/* ------------------------------------------------------------------ */
/* Get Zdn as the bottom of the hole (global in this thread!)         */
/* ------------------------------------------------------------------ */
if((iPlane == 0) AND (px.n > 0))              /* drill depth in x     */
{
  Zdn = px.r[0];                              /* absolute position    */ 
  if(_gc.ggStat.v[3] == 91)
   {Zdn = pIni[0] + px.r[0];}                 /* relative position    */
}

if((iPlane == 1) AND (py.n > 0))              /* drill depth in Y     */
{
  Zdn = py.r[0];                              /* absolute position    */
  if(_gc.ggStat.v[3] == 91)
   {Zdn = pIni[1] + py.r[0];}                 /* relative position    */
}

if((iPlane == 2) AND (pz.n > 0))              /* drill depth in Z     */
{
  Zdn = pz.r[0];                              /* absolute position    */
  if(_gc.ggStat.v[3] == 91)
   {Zdn = pIni[2] + pz.r[0];}                 /* relative position    */
}

/* ------------------------------------------------------------------ */
/* check conditions                                                   */
/* ------------------------------------------------------------------ */
if((GcMod.R > Zdn) AND (GcMod.R > pIni[iPlane]))
 {mreset sprint "G%d: Invalid retract point location" gCode;}

if((GcMod.R < Zdn) AND (GcMod.R < pIni[iPlane])) 
 {mreset sprint "G%d: Invalid retract point location" gCode;}

/*====================================================================*/
/* Actions 1-3: Move to top of the hole                               */ 
/*====================================================================*/
/* Action 2 is used only for tap drilling                             */
/* and therefore coded in tapdrill macro (action 4)                   */
/* ------------------------------------------------------------------ */

_OcNext = pIni; mf;                           /* move to initial point*/
if(_ideFlag) {Wait 0; MsgBox "At initial point";}

_OcNext = pRet; mf;                           /* move to Retract point*/
if(_ideFlag) {Wait 0; MsgBox "On top of the hole";}

/* ------------------------------------------------------------------ */
/* Action 4: Go down                                                  */ 
/* ------------------------------------------------------------------ */
local "Done"; Done = 0;

if((GCode == 83) OR (GCode == 73))            /* peck drill           */
{ 
  local
  "z0"            /* initial height     , absolute    */
  "z1"            /* final depth        , absolute    */
  "zi"            /* current depth      , absolute    */
  "dz";           /* cut amount per pass, incremental */

  z0 = _OcNext[iPlane];                       /* set z values         */  
  Z1 = Zdn;
  dz = Z1 - Z0;                               /* preset dz            */ 

  if(pq.n) {GcMod.Q = pq.r[0];}               /* modal peck amount    */

  if(GcMod.Q != 0) {dz = GcMod.Q;}            /* set dz               */
  if((Z1 < Z0) AND (dz > 0)) {dz = -dz;}      /* adjust sign of dz    */
  if((Z1 > Z0) AND (dz < 0)) {dz = -dz;}

  if((Z1 < Z0) AND (rPeck > 0)) {rPeck = -rPeck;}
  if((Z1 > Z0) AND (rPeck < 0)) {rPeck = -rPeck;}

  zi = z0 + dz;                               /* set first level      */                      

  if((Z1 <= Z0) AND (zi <= z1)) {zi = z1; Done = 1;}  /* adjust level */
  if((Z1 >= Z0) AND (zi >= z1)) {zi = z1; Done = 1;}

  _OcNext[iPlane] = zi; ml;                   /* go in slowly         */                    

  while ("Done == 0")                         /* bottom not reached   */
  {                    
    if(GCode == 83)
     {_OcNext[iPlane] = z0; mf;}              /* full retract         */
    else
     {waitX dwell;}                           /* rPeck retract        */ 

    _OcNext[iPlane] = zi - rPeck; mf;         /* prepare again        */

    zi = zi + dz;                             /* set next level       */ 

    if((Z1 <= Z0) AND (zi <= z1)) {zi = z1; Done = 1;} /* adjust level*/
    if((Z1 >= Z0) AND (zi >= z1)) {zi = z1; Done = 1;}

    _OcNext[iPlane] = zi; ml;                 /* go in slowly         */
  }
}

if((GCode == 84) OR (GCode == 74))            /* tap drill            */
{  
  if(T_Undefined == (typeof "TapDrill").Type)
   {mreset sprint "G%d: Macro \"TapDrill\" is missing" GCode;}

  if(GcMod.G84Pitch == 0)
  {
    local "pF"; pF = *_gc.reg['F'];
    if(pF.n == 0)
     {mReset sprint "G%d: F value is missing." GCode;}

    if(GCode == 84) {GcMod.G84Pitch =  pF.r[0];}
    if(GCode == 74) {GcMod.G84Pitch = -pF.r[0];}
  }

  local "pDwn" "Rpm";

  pDwn = pRet; pDwn[iPlane] = Zdn;

  Rpm = mtag[mtagI].spdRpm;
    
  TapDrill pRet pDwn GcMod.G84Pitch Rpm Dwell;
  Done = 1;
}

if(Done == 0) {_OcNext[iPlane] = Zdn; ml;}    /* normal drill         */

if(_ideFlag) {Wait 0; MsgBox "Down";}

/* ------------------------------------------------------------------ */
/* Action 5-6: Dwell, spindle command; retract; spindle command       */ 
/* ------------------------------------------------------------------ */
if(GCode == 81) {                         _OcNext = pRet; mf;}
if(GCode == 82) {waitX Dwell;             _OcNext = pRet; mf;}
if(GCode == 83) {                         _OcNext = pRet; mf;}
if(GCode == 84) { /* see tapdrill macro */                   }
if(GCode == 85) {                         _OcNext = pRet; ml;}
if(GCode == 86) {waitX Dwell; GcMcall 5;  _OcNext = pRet; mf; GcMcall 3;}
if(GCode == 87) {             GcMcall 3;  _OcNext = pRet; ml;}
if(GCode == 88) {waitX Dwell; GcMcall 5;  _OcNext = pRet; mf; GcMcall 3;}
if(GCode == 89) {waitX Dwell;             _OcNext = pRet; ml;}
if(GCode == 73) {waitX Dwell;             _OcNext = pRet; mf;}
if(GCode == 74) { /* see tapdrill macro */                   }

if(GCode == 76)
{
  GcMcall 19;                                    /* oriented stop M19 */

  local "dx" "dy" "dz" "pRetX"; pRetX = pRet;

  dx = gcParDbl "G76dx";                         /* move away         */
  dy = gcParDbl "G76dy";  
  dz = gcParDbl "G76dz";  

  if(iPlane != 0) {_ocNext[1] = _OcNext[1] + dy; _ocNext[2] = _OcNext[2] + dz; mf;}
  if(iPlane != 1) {_ocNext[0] = _OcNext[0] + dx; _ocNext[2] = _OcNext[2] + dz; mf;}
  if(iPlane != 2) {_ocNext[0] = _OcNext[0] + dx; _ocNext[1] = _OcNext[1] + dy; mf;}

  _ocNext[iplane] = pRet[iPlane]; mf;}           /* retract           */
  _OcNext = pRet; mf;                            /* move back         */
  GcMcall 3;                                     /* spindle ON right  */
}

if(_ideFlag) {Wait 0; MsgBox "Retract point again";}

/* ------------------------------------------------------------------ */
/* Action 7: return to I-point                                        */ 
/* ------------------------------------------------------------------ */
if(_gc.ggStat.v[10] == 98) {_OcNext = pIni; mf;}

if(_ideFlag) {Wait 0; MsgBox "Initial point";}
