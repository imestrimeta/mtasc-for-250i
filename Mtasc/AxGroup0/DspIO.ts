/*----------------------------------------------------------------*/
/* Standard Output Values for Program Execution.                  */
/* This file will be read each time a program is started.         */
/* It will be rewritten by the save command of the i/O-Form.      */
/*----------------------------------------------------------------*/
_oStatPrv = ARRAY (1 + _nOutMax) 0 1 0 0 0 0 0;
mtag[mtagI].oav = 29370L;
