/*====================================================================*/
/* initialize graphic interface for MTASC                             */
/*====================================================================*/
Local "Colors" "i" "_FCol" "_LCol" "_SCol" "_CCol" "_XCol";

if(_ColorIndex <= 1) {i = 0;}
if(_ColorIndex >= 2) {i = 1;}

_LCol = Array 2 0xFFFFFFL 0x000000L;          /* Mill ML white/black  */ 
_FCol = Array 2 0x808080L 0x808080L;          /* Fast MF grey/grey    */
_SCol = Array 2 0x00FF00L 0xFF8080L;          /* Search green/blue    */

_CCol = Array 2 0x0000FFL 0xFF00FFL;          /* action red/pink      */
_XCol = Array 2 0xFF00FFL 0xFF8080L;          /* approach pink/blue   */

Thread_agi.g3.search.iPenMark = 3;            /* search pen index     */
Thread_agi.g3.search.g3sFlag = 0;             // pen index of lines/arcs to search for

SetDrawPar 'L' (_LCol[i]) PS_SOLID 0 R2_COPYPEN;       /* same as '0' */
SetDrawPar 'F' (_FCol[i]) PS_SOLID 0 R2_COPYPEN;       /* same as '1' */  
SetDrawPar '3' (_SCol[i]) PS_DASH  0 R2_CopyPEN;

SetDrawPar '0' (_CCol[i]) PS_SOLID 0 R2_COPYPEN Thread_agi.crcG3;  /* action   */
SetDrawPar '1' (_XCol[i]) PS_SOLID 0 R2_COPYPEN Thread_agi.crcG3;  /* approach */

SetDrawPar 'L' (Tools[_toolNcEmu].Color) -1 -1 -1; /* set tool color  */
/*--------------------------------------------------------------------*/
/*   definition and initialization of thread local variables          */
/*--------------------------------------------------------------------*/
MCrv.dim = 3;                                /* spline dimensions     */

if(T_undefined != (TypeOf "CMhel").type)     /* if helix class exists */ 
{
  localThr "MhelD";                          /* create helix object   */ 
  MhelD    = ObjDef CMhel;
  MhelD.h  = &(ObjDef CMObjHelix);           /* init helix object     */
  MhelD.hd.dim= 3;                           /* set dimension         */
}

localThr "_gCode";
_gCode = _gCodeGlb[mtagI];

/*--------------------------------------------------------------------*/
/* CMtransChain Instanzen anlegen                                     */
/*--------------------------------------------------------------------*/
tr = trOri = ObjDef CMtransChain;
if(FileExists MtApp.path & sprint "/AxGroup%d/MtransChain/MtcInit.ts" mtagI)
{
 fcall MtApp.path & sprint "/AxGroup%d/MtransChain/MtcInit.ts" mtagI;   /* load extended transfm. */
}
