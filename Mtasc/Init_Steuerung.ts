// ===========================================================================
// **  Motor Amplifier Properties *****************************************
// ------ Beschreibung siehe Achsen.txt -------------------------------------
// == maximale Anzahl Achsen  : 8
//===================================================================

//=========================================================================
//============================================================================
// Macrodefinierungen f�r serielle Kommunikation ITC80, MC4-40
//============================================================================

 RW_START = 0x24 ; //  '$'    Start der �bertragung
 RW_STOP  = 0x23 ; //  '#'    Ende der �bertragung 

// -- m�gliche Ziele -------------------------------- 
 W_STROM  = 0x4D ; //  'M'    Motorstrom schreiben
 W_RESOL  = 0x41 ; //  'A'    Schrittaufl�sung schreiben
 R_NAME   = 0x4E ; //  'N'    Namen lesen
 R_STATUS = 0x53 ; //  'S'    Statusregister lesen

// --- Ziele f�r erweiterten bereich -------------------
 W_TEMPO  = 0x54 ; // 'T'  Daten nur tempor�r ablegen
 W_EEPROM = 0x45 ; // 'E'  Daten im EEPROM ablegen
 R_EEPROM = 0x46 ; // 'F'' Daten aus EEPROM lesen
 W_STATUS = 0x55 ; // 'U'  Statusregister schreiben

// -- Reihenfolge : Startbyte, AchsNr, Ziel, Datum, Stopbyte --------------------------
  (MacroDef "Set_Current") = 
   " Sel_AX  %1 0;
     trc.uartTransmit 1 RW_START ;  // 
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1  W_STROM ;  // Ziel : Strom schreiben
     trc.uartTransmit 1       %2 ;  // Strom in 1/10 A
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Set_Resol") = 
   " Sel_AX  %1 0;
     trc.uartTransmit 1 RW_START ;  // 
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1  W_RESOL ;  // Ziel : Aufl�sung schreiben
     trc.uartTransmit 1       %2 ;  // Aufl�sung, z.B. 16 => 1/16tel Schritt
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Read_Name") = 
   " Sel_AX  %1 0; 
     trc.uartTransmit 1 RW_START ;
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1   R_NAME ;  // Ziel : Namen lesen
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";
  (MacroDef "Read_Status") = 
   " Sel_AX  %1 0; 
     trc.uartTransmit 1 RW_START ;
     trc.uartTransmit 1       %1 ;  // Achse, wegen Kompatibilit�t mit mehrachs MC4-40usw.
     trc.uartTransmit 1 R_STATUS ;  // Ziel : Status lesen
     trc.uartTransmit 1  RW_STOP ;  // Endezeichen
     Sel_AX  %1 1;
   ";

//================================================================================================
// Strom absenken bzw. anheben 
// �bergabewert ist der Sollstromwert in Prozent
//====================================================================  
(MacroDef "Strom_Prozent") = 
 "
  local \"ax\" \"n\";
  local \"Mot_Strom\" ;
  Mot_Strom = 0 ;
  ax        = 0 ; 
  n  = trc.nAx;
  
  if(%P > 1) {ax = %2; n = 1;}  // nur f�r eine Achse
  
  for n
   {
      Mot_Strom = (MotAmp[ax].strom  * %1 / 10); // Angabe effektiv Strom in 1/10 A
      Set_Current ax Mot_Strom;   
      ax = ax + 1;
      out_WD_trigger;  
   }
 "; 
//----------------------------------------------------------------------------------------------------   
if (CtType.IMC481)  // ist noch Sonderfall
{ 
  fcall MtApp.path & "\\Macro_IMC481.ts"; 
  out_Ausgang_ena 0;                		// disable Ausg�nge
  out_PIC_STM  1;       					// STM soll Achsen initialisieren
  out_ES_frei  1;        					// Treiber enable

  if (in_Power1_ok == 0)
  {
    // MsgBox "Spannung einschalten.";
    SendError "ERR_Controller_NoMainPower";

    // while "in_Power1_ok == 0" {MsgBox "Spannung einschalten."   "Power"  Mb_iconhand +  Mb_okcancel; }
  }

  i = 1;    for 12 { _oset i 0; i = i + 1;_dspupdate;} 	// alle Ausg�nge ausschalten
  fcall MtApp.path & "\\Init_IMC481.ts";
  out_AX_enable 1; 
  out_Ausgang_ena 1;                // enable Ausg�nge
  out_AX_Ready_SK 1;                // Meldung : "Achsen bereit" ausgeben f�r SK
}
//----------------------------------------------------------------------------------------------------------  
else  
{
  if (CtType.ISA50) { fcall MtApp.path & "\\Init_ISA50.ts ";}
  
  else
  {
    Anzahl_Achsen = mtag[0].vDim;

    if (CtType.ITC80) 
    {
      if (Anzahl_Achsen > 8)
      {
       // MsgBox "Zuviele Endstufen eingestellt!"; 
       SendError "ERR_Controller_InvalidNumberOfAxes";
       exit;
      }      
      fcall MtApp.path & "\\Macro_ITC80.ts" ; 
      
      out_Ausgang_ena 0;          // disable Ausg�nge
      out_PIC_STM  1;       					 // STM soll Achsen initialisieren
      out_ES_frei  1;        					// Treiber enable
      
      fcall MtApp.path & "\\Init_ITC80.ts";
    }
    
    if (CtType.MC4_40)
    {
      if (Anzahl_Achsen > 4))
      {
        // MsgBox "Zuviele Endstufen eingestellt!"; 
        SendError "ERR_Controller_InvalidNumberOfAxes";
        exit;
      }
       
      fcall MtApp.path & "\\Macro_MC4_40.ts";
      out_Ausgang_ena 0;                			// disable Ausg�nge 
      
      if (in_Power1_ok == 0)
      {
        // MsgBox "Spannung einschalten.";
        SendError "ERR_Controller_NoMainPower";
        
        while "in_Power1_ok == 0"  {MsgBox "Spannung einschalten."   "Power"  Mb_iconhand +  Mb_okcancel; }
      }
      
      i = 1;   for 4 { _oset i 0; i = i + 1;_dspupdate;} 	// alle Ausg�nge ausschalten  
    }
    
    out_AX_enable 1; 
    out_AX_Ready_SK 1;                // Meldung : "Achsen bereit" ausgeben f�r SK
  }
}

out_Ausgang_ena 1;       		// enable Ausg�nge  
out_Bremse 1;              // Bremse freigeben

//===============================================================================