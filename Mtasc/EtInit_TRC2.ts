/*====================================================================*/
/* Dieses MTASC-Programm wird beim Aufruf von EdiTasc ausgef�hrt      */
/*====================================================================*/
/* Schalter setzen                                                    */
/* ------------------------------------------------------------------ */
_Test = 0;        /* Test ist ein Bit-Feld. _Test = 0: Alle Abl�ufe real.
                     Bit 0 Wert  1 gesetzt: Taster mit Msgbox simulieren
                     Bit 1 Wert  2 gesetzt: Taster �bergehen
                     Bit 2 Wert  4 gesetzt: Automatik �bergehen
                     Bit 3 Wert  8 gesetzt: Werkzeugwechsel �bergehen
                     Bit 4 Wert 16 gesetzt: Referenzfahrt simulieren
                     Bit 5 Wert 32 gesetzt: SPS abschalten
                  */

/* ------------------------------------------------------------------ */
// Steuerungstypen definieren
/* ------------------------------------------------------------------ */
CControllerType = ObjDef T_classDscr
"IMC481"     T_long   0
"ISA50"      T_long   0
"ITC80"      T_long   0
"ITC81"      T_long   0
"MC4_40"     T_long   0
"STM332F4Discovery" T_long 0  // zus�tzlich setzen f�r Testmodus mit diesem Board
;

// hier verwendete Steuerung eintragen
// Es ist zu beachten, dass im Haupt-Thread und in der XML noch die Abfrage f�r die 
// Hauptspannungsverorgung f�r die jeweilige Endstufe angepasst werden muss. 
// IMC481: "_MotStat = 3 * ((_iStat[30] != 0) and (_iStat[29] == 0));" --> (pi 1Pow ok --> PortF Pin 8) AND (pi 2Pow ok --> PortC Pin 7)
// ITC80:  "_MotStat = 3 * (_iStat[11] != 0)                           --> (pi Z_rdy   --> PortC Pin 10)

CtType = gcnew CControllerType;
CtType.IMC481 = 1;   
CtType.ISA50  = 0; 
CtType.ITC80  = 0; 
CtType.STM332F4Discovery = 0;

if(MtApp.AppName == "EdiTasc")
{
  fcall MtApp.path & "\\MctlInit.ts";
  Mds.ovr = 100;
  ThreadFlags "AllowPeekMessage" 1;
}

// keine direkten Makros f�r Motoren EIN/AUS
// _MotStat wird unabh�ngig aus externen IOs ermittelt
(MacroDef "_MotOn")  = "";
(MacroDef "_MotOff") = "";

/* ================================================================== */
// Diverse Makrodefinitionen der iselGroup
/* ================================================================== */

fcall MtApp.path & "\\Macros\\iselGroup\\Various.ts";           // Macro - Verschiedene Definitionen
fcall MtApp.path & "\\Macros\\iselGroup\\ToolPathRounding.ts";  // Macro - Definition: ToolPathRounding
// fcall MtApp.path & "\\Macros\\iselGroup\\Gantry.ts";            // Macro - Definition: Gantry-Achse
fcall MtApp.path & "\\Macros\\iselGroup\\ErrorHandling.ts";     // Macro - Definition: ErrorHandling

/* ================================================================== */
// Plugin zur Kommunikation mit TriCon Device und Einstellungen
/* ================================================================== */
// LoadDll MtApp.path & "/TriCon2.dll";   now in Disoft.xml: ExecSys0
// trc = gcnew CTriCon2 0 1;
if(trc.nDev == 0)
{
  // MsgBox "Kein TRC2 USB-Ger�t gefunden";
  SendError "ERR_EtInit_USBConnection";
  (MacroDef "_iGet6") = "0";  // Puffer-Unterlauf lesen
}
else
{
  trc.isrCyclesWithSameSpeed = 64;
  trc.kHz = (trc.isrCyclesWithSameSpeed * MtApp.xml.MtDrv.laa.v0.PIDFrq._Xml2Mt) / 1000;    // max. Frequenz in kHz
  if((trc.kHz > 100) or (trc.kHz < 20))
  {
    // MsgBox "Ung�ltige Interuptfrequenz f�r Taktgenerierung";
    SendError "ERR_EtInit_InvalidInteruptFrequenc";
    mreset "";
  }
  trc.nAx = mtag[0].vDim;      // Anzahl Achsen
  trc.speedBufSizeMax = 120;
  trc.speedBufSizeMin = 60;
  trc.version[0] = 3;  // Version der Firmware pr�fen
  trc.version[1] = 2;
  trc.version[2] = 0;
  trc.version[3] = 6;
  if('0' == (MtApp.xml.MtDrv.laa.v0.HwType.o0._Xml2Mt)[0])
  {
    trc.stepMask = 0xAAAA;  // Pin 2=Step, Pin 3=Dir, ...
    trc.dirMask  = 0x5555;
  }
  else
  {
    trc.stepMask = 0x5555;  // Pin 2=Dir, Pin 3=Step, ...
    trc.dirMask  = 0xAAAA;
  }
  trc.open;	// �nderung wegen der aktuellen Version von MTasc
  trc.ctrlFlag = 1;
  threadCall "trc.SpeedService;" 100;
  trc.ctrlFlag = 4 + 8;  // 8 enables input watch thread
  trc.setPort 9 1 1;     // Puffer-Unterlauf �berwachen
  (MacroDef "_iGet6") = "trc.ioStat[10]";  // Puffer-Unterlauf lesen
}

mlPortDst = array mtag[0].vDim 0.0;  // global for use with mlPort
// Wartezeit in ms nach jeder Messung zum Entprellen
mlPortWait = 100;
// Extra Messstrecke in mm nach jeder Messung zum Entprellen
mlPortXMove = 0.0;

RefAbortRq = 0;     // global flag for "Abort Referencing Request"

// Move Relative Until Port Event
// %1: direction Vector = maximum relative motion distance in all axes
// %2: input descriptor
// %3: input polarity (0= wait for low, 1=wait for high)
// %4 (optional): speed
(MacroDef "mlPort") =
"
  if(trc.nDev == 0) {sleep 300; return 1;}  // simulate referencing and return
  if(RefAbortRq) {return -1;}

  local \"ret\" \"l\" \"dst\" \"CallOnExit\" \"CallOnError\";
  CallOnExit = sprint                         // restore speed and rmpMode
  \"ml_f = %.3lf; MvPar; rmpMode '%c';
    trc.stopPort = 0x7F;  // cancel stop request
    trc.stopRequest = 0;
    trc.setStopParam;
    ObjLeaveCS mlPortDst;
    mtIAg0.debugMode = 1;
  \"  ml_f (rmpMode '?');
  CallOnError = sprint                         // restore speed and rmpMode
  \"ml_f = %.3lf; MvPar; rmpMode '%c';
    sleep 100;
    mreset 'P';
    sleep 100;
    trc.stopPort = 0x7F;  // cancel stop request
    trc.stopRequest = 0;
    trc.setStopParam;
    ObjLeaveCS mlPortDst;
  \"  ml_f (rmpMode '?');

  mtIAg0.debugMode = 0;  // no abort motion messages
  ObjEnterCS mlPortDst;
  ret = 2;
  if(%P > 3) {ml_f = %4; MvPar;}
  dst = _ocNext + %1;
  // ----------------------------------
  l = VLen %1;
  l = mlPortXMove / l;
  _ocNext = _ocNext - l * %1; ml; // do retract move
  wait 0.2;
  // ----------------------------------
  if(%3 == (0 != (%2.mask and trc.getPort %2.adr) xor (%2.inv)))
  {
    LogWrite sprint \"mlPort %d %.2lf z=%d: no motion\" %3 ml_f trc.stepPosReal[2];   // final switch status aready set
    return 2;
  }
  local \"n\"; n = 0;
  for 3    // debouncing: try up to 3x
  {
    n = n + 1;
    trc.stopPort = %2.adr;
    trc.stopIMask = %2.mask;
    if(%3) {trc.stopIInv = %2.inv;}
    else   {trc.stopIInv = %2.inv xor %2.mask;}
    trc.stopRamp = 500;
    trc.stopRequest = 0;
    trc.setStopParam;
    if(ml_f < 0.0001)
    {
      LogWrite \"mlPort: speed too small\";
      ml_f = 0.5; MvPar;
    }
    _ocNext = dst; ml; // move
    local \"pz\"; pz = 0;
    while 1
    {
      if(trc.stopStatus == 2)
      {ret = 1; pz = trc.stepPosReal[2]; break;}
      if(mstat == 0)
      {ret = 0; LogWrite sprint \"mlPort %d %.2lf z=%d: no signal\" %3 ml_f trc.stepPosReal[2]; sleep 100; break;}
      if(RefAbortRq)
      {
        LogWrite sprint \"mlPort %d %.2lf z=%d: aborted by user\" %3 ml_f trc.stepPosReal[2];
        //MsgBox \"RefAbort\";
        ret = -1; break;
      }
      sleep 10;
    }
    if(ret == 1)
    {
      sleep 10;
      local \"i\" \"ok\";
      ok = 0; i = 0; for trc.nAx
      {
        if((trc.stepPosReal[i] << 14) == in (IoDscr \"TRC2\" (0x30 + i) 1 -1 4)) {ok = ok + 1;}
        i = i + 1;
      }
      if(ok != trc.nAx)
      {
        sleep 20;
        ok = 0; i = 0; for trc.nAx
        {
          if((trc.stepPosReal[i] << 14) == in (IoDscr \"TRC2\" (0x30 + i) 1 -1 4)) {ok = ok + 1;}
          i = i + 1;
        }
        if(ok != trc.nAx)
        {
          // MsgBox \"Timeout Error in mlPort\";
          SendError \"ERR_EtInit_mlPortTimeOutError\";
        }
      }
      mreset 'P';
      trc.speedFlag = -1 xor -1 << trc.nAx;
      while 1
      {
        if(trc.speedFlag == 0) {break;}
        sleep 10;
      }
    }
    sleep 10 + 0 = trc.spbCnt * trc.isrCyclesWithSameSpeed / 1.0 * trc.kHz;
    if(ret != 1) {return ret;}  // if signal was not OK, leave
    trc.stopPort = 0x7F;  // cancel stop request
    trc.stopRequest = 0;
    trc.setStopParam;
    l = VLen %1;
    l = mlPortXMove / l;
    _ocNext = _ocNext + l * %1; ml; // do extra move
    wait mlPortWait / 1000.0;
    // if expected switch status persists we are finished
    if(%3 == (0 != (%2.mask and trc.getPort %2.adr) xor (%2.inv)))
    {
      _ocNext = _ocNext - l * %1; ml; // restore original pos.
      wait 0;
      LogWrite sprint \"mlPort %d %.2lf z=%d: OK\" %3 ml_f pz;
      break;
    }
    LogWrite sprint \"mlPort: try again (%d)\" n;
  }
  return ret;
";

(MacroDef "RefSearch") =
"
  if(RefAbortRq) {return;}

  Local \"IdxMask\";
  IdxMask = GetXmlAppGrp.Ref.IdxMask._Xml2Mt;
  IdxMask = IdxMask AND RefMask AND Thread_fag.amask;

  if(RefMask and Thread_fag.amask and 1 << %1)
  {
    if('0' != mlimit '?')                // Limit watching must be off
      {mreset GetIniTxt \"System\" \"ErrLimits\";}

    local \"iax\" \"dst\";
    iax = %1;
    dst = array mtag[0].vDim 0.0;

    if(0 == (0 != In %2))
    {
      dst[iax] = -100 * %4;                 // move off from switch
      if(0 == mlPort dst %2 1 spf[iax])
      {
       // MsgBox GetIniTxt \"System\" \"ErrRefInp\"; 
       SendError \"ERR_EtInit_ErrRefInp\";
       return;
      }
    }
    dst[iax] = 10000 * %4;                  // move towards switch
    if(0 == mlPort dst %2 0 spf[iax])
    {
     // MsgBox GetIniTxt \"System\" \"ErrRefInp\"; 
     SendError \"ERR_EtInit_ErrRefInp\";
     return;
    }

    dst[iax] = -10 * %4;                    // leave switch slowly
    if(0 == mlPort dst %2 1 sps[iax])
    {
     // MsgBox GetIniTxt \"System\" \"ErrRefInp\"; 
     SendError \"ERR_EtInit_ErrRefInp\";
     return;
    }
    
    if(IdxMask and 1 << iax)                // index bit is set
    {
      // MsgBox \"Index Search not supported\";
      SendError \"ERR_EtInit_NoIndexSearchSupport\";
      exit;
    }

    wait 0.2;
    _OcNext[iax] = _OcNext[iax] - %7 * %4; mf; Wait 0;
    _ocNext = getmc; _OcNext[iax] = 0; setmc;
    RefDone = RefDone or RefMask and Thread_fag.amask and 1 << iax; 
  }
";

if(trc.nDev != 0)
{
  (MacroDef "DrvAfterRef") =
  "
    local \"i\" \"bmh\" \"bml\" \"sc\";
    bml = 0x3FFF; bmh = -1 xor bml;
    i = 0; for trc.nAx
    {
      sc = bmh and in (IoDscr \"TRC2\" (0x30 + i) 1 -1 4);
      // sync step counters in driver and Microcontroller
      out (IoDscr \"TRC2\" (0x30 + i) 1 -1 4) sc or bml and trc.getStepCnt i;
      i = i + 1;
    }
    
    // AxLink 1;
  ";
}

// AxLink 1;

/* ------------------------------------------------------------------ */
(MacroDef "_iGet") = 
"if(0 != _iMode[%1] and 2)
 {return _iStat[%1] = scall sprint \"_iGet%d;\" 0 + %1;}
 else
 {
   return _iStat[%1] = _inpDsc[%1].inv xor trc.ioStat[_inpDsc[%1].adr] and _inpDsc[%1].mask;
 }
";

//////////////////////////////////
// Stromverwaltung
fcall MtApp.path & "\\Macros\\iselGroup\\StromManagement.ts";   // Macro - Definition: StromManagement

/* ------------------------------------------------------------------ */
// Steuerung initialisieren
/* ------------------------------------------------------------------ */
if(trc.nDev > 0)
{
  fcall MtApp.path & "\\Init_Steuerung.ts";

  (MacroDef "_oSet" '@') =
  " local \"stPrv\" \"st\";
  
    stPrv = _oStat[%1];
    if(%2 == -1){st = 1 - _oStat[%1];}
    else        {st = %2;}
    if(0 != _oMode[%1] and 2) {scall sprint \"_oSet%hd st;\" %1;}
    else
    {
      if(%1 >= _nOutputs) {_oStat[%1] = st; return;}
      if(_outDsc[%1].type != 0x1c) {LogWrite sprint \"output[%d]: illegal type\" %1;}
      if(_outDsc[%1].mask != _outDsc[%1].mask or _outDsc[%1].inv) {LogWrite sprint \"output[%d]: illegal inv mask\" %1;}
    }
    if(st)
    {
      trc.setPort _outDsc[%1].adr _outDsc[%1].mask _outDsc[%1].mask xor _outDsc[%1].inv;
    }
    else
    {
      trc.setPort _outDsc[%1].adr _outDsc[%1].mask (-1 xor _outDsc[%1].mask) xor _outDsc[%1].inv;
    }
    _oStat[%1] = st;
  ";
}

if(trc.nDev == 0)
{
  (MacroDef "CMds_read") =
  "
    return %0;
  ";
  Mds.pwrStat = 3;
  Mds.sccStat = 1;
  Mds.initOK  = 1;
  Mds.refOK   = 1;
  _MotStat = 3;
}

if(CtType.STM332F4Discovery)
{
    // einige Eing�nge von Port F auf E umlenken
  _inpDsc[1].adr = 4;  // Luftdruck -> E0
  _inpDsc[1].mask = 1;
  _inpDsc[1].inv = 1;
  _inpDsc[9].adr = 4;  // LMT       -> E1
  _inpDsc[9].mask = 2;
  _inpDsc[9].inv = 0;
  _inpDsc[3].adr = 4;  // Spindel steht -> E3
  _inpDsc[3].mask = 8;
  _inpDsc[3].inv = 0;
  _inpDsc[19].mask = 2; // Ref1A auf Taster umlenken
  _inpDsc[19].inv = 2;
}

/* ------------------------------------------------------------------ */
// Spindel-Ausgang (nicht verwendet)
/* ------------------------------------------------------------------ */

(MacroDef "_oSet1") =
"if(%1)      {if(_mode != 'G'){_oSet 5 1; wait 0.5 0;}}
 if(%1 == 0) {if(_mode != 'G'){_oSet 5 0;}}
 _dspUpdate;
";

/* ------------------------------------------------------------------ */
// Bahnsynchroner Ausgang
// Hardware/Outputs/v13/IoMode muss 6 sein
// Hardware/Outputs/v14/IoMode muss 2 sein
/* ------------------------------------------------------------------ */

(MacroDef "_oSet13") =  // Enable / Disable
" if(_mode == 'G'){return;}
  if(%1)
  {
    if(_oStat[14]) {out _outDsc[14] 0;} // sofort schalten
  }
  if(%1 == 0)
  {
    if(_oStat[14]) {out _outDsc[14] 1;}
  }
 _dspUpdate;
";

(MacroDef "_oSet14") =  // setzen
" if(_mode == 'G'){return;}
  if(%1)
  {
    if(_oStat[13]) {outd _outDsc[14] 0;} // bahnsynchron schalten
  }
  if(%1 == 0)
  {
    if(_oStat[13]) {outd _outDsc[14] 1;}
  }
 _dspUpdate;
";

/* ------------------------------------------------------------------ */
/* Analog-Ausgang                                                     */
/* ------------------------------------------------------------------ */
mtag[mtagI].oaVMin = 6000L;
mtag[mtagI].oaVMax = 60000L;

(MacroDef "_oaSet") =
"mtag[mtagI].oaV = %1;
 if(mtag[mtagI].oaV < mtag[mtagI].oaVMin){mtag[mtagI].oaV = mtag[mtagI].oaVMin;}
 if(mtag[mtagI].oaV > mtag[mtagI].oaVMax){mtag[mtagI].oaV = mtag[mtagI].oaVMax;}
  local \"v\"; v = 0;
  v = 4096 * (mtag[mtagI].oaV / mtag[mtagI].oaVMax + 0.0);
  if(trc.nDev != 0) {trc.setPWM 0 v;}
";

/* ------------------------------------------------------------------ */
/* Werkzeugwechsel                                                    */
/* ------------------------------------------------------------------ */
/* hier nicht implementiert                                           */


/* ------------------------------------------------------------------ */
/* Notaus oder T�r-Behandlung                                         */
/* ------------------------------------------------------------------ */

/* ================================================================== */
/* Abschluss-Arbeiten                                                 */
/* ================================================================== */

//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------
MhelAng0To2PiFlag = 1;          // Kreisb�gen mit L�nge 0 als Vollkreis

(MacroDef "DrvBeforeRef") =
"
  if((CtType.IMC481 == 1) and (CtType.STM332F4Discovery == 0))
  {
    if(RefMask and 4)
    {
      trc.ctrlFlag = 1;
      sleep 200;
      fcall MtApp.path & \"/Init_IMC481.ts\"; // neu initialisieren
      trc.ctrlFlag = 4;
      sleep 200;
      trc.setPort 10 1 0;    // reset speedBufWatch flag
      sleep 200;
      drv.laa.icSet 0 (trc.stepPosReal[0] * 2);
      drv.laa.icSet 1 (trc.stepPosReal[1] * 2);
      drv.laa.icSet 2 (trc.stepPosReal[2] * 2);
      _MotStat = 3;
      trc.ctrlFlag = 4 + 8;  // 8 enables input watch thread
    }
  }
  local \"i\";
  i = 0; for mtag[0].vDim
  {
    agi0.ptc.vvwFac[i] = 1.0;
    i = i + 1;
  }
  agi0.ptcWrite;
";

sleep 100;

//----------------------------------------------------------------------------------------
SwitchCounter = 0;
trc.dbgFlag = 0;
ThrCnt = 0; // Z�hlt Thread Zyklen
ThrReadInp = 1;
ThreadCall  // permanent asynchronous reading of inputs
"
  Thread_SetDrv drv;
  local \"odHwl\" \"i\" \"k\" \"cd\"; cd = 0; d = 0.0;
  ThrCnt = 0; // cycle counter 0-255
  odHwl = IoDscr \"MMAP\" 0 0 0x3FF 2;  // Bitmaske HW Limits 5 Achsen
  sleep 400;
  out_WD_reset;
  
  while 1
  {
   Mctlio.readByte 0;
   Mctlio.readByte 1;
   Mctlio.readByte 2;
   Mctlio.readByte 3;
   Mctlio.oSet;
	
    if(trc.nDev != 0) {trc.setPort 'C' 0x0010 0x0010 * (ThrCnt and 1);}  // WD trigger					
    if(0 != trc.ctrlFlag and 0x18)
    {
      if(trc.nCmdCurr > 240)
      {
        trc.ctrlFlag = 16; // stop communication
        LogWrite sprint \"Speed Buffer Overflow (err=%d) - Please Reastart Program\" (trc.dbgFlag >> 8);
        // MsgBox sprint \"Speed Buffer Overflow (err=%d) - Please Reastart Program\" (trc.dbgFlag >> 8);
        SendError \"ERR_EtInit_SpeedBufferOverflow\";
        return;
      }
      odHwl.val = 0;
      if(_iStat[14] == 0)  {odHwl.val = odHwl.val or 1;}     // X-
      if(_iStat[13] == 0)  {odHwl.val = odHwl.val or 2;}     // X+
      if(_iStat[15] == 0)  {odHwl.val = odHwl.val or 4;}     // Y-
      if(_iStat[16] == 0)  {odHwl.val = odHwl.val or 8;}     // Y+
      if(_iStat[17] == 0)  {odHwl.val = odHwl.val or 0x10;}  // Z-
      if(_iStat[18] == 0)  {odHwl.val = odHwl.val or 0x20;}  // Z+
      //if(_iStat[20] == 0)  {odHwl.val = odHwl.val or 0x40;}  // A-
      if(_iStat[19] == 0)  {odHwl.val = odHwl.val or 0x80;}  // A+
      if(_iStat[21]   == 0)  {odHwl.val = odHwl.val or 0x100;} // B-
      //if(_iStat[22] == 0)  {odHwl.val = odHwl.val or 0x200;} // B+
	  
      if(CtType.STM332F4Discovery) {odHwl.val = 0;} // keine HW-ES �berw. im Testmodus
	  
      out odHwl odHwl.val;
      _MotStat = 3 * ((_iStat[30] != 0) and (_iStat[29] == 0));
      if(_MotStat == 0) {RefDone = 0;}
    }
   
   // Stromverwaltung - Beginn	
   k = 0;
   sMiddle = 0;
   
   if (sLowRq == 0)
   {
     for trc.nAx
     {
       if (SetCurrentToMiddle[k] == ThrCnt)
       {
         if (MiddleRq[k] == 1)
         {
           MiddleRq[k] = 0;
           
           speedStateSet k 2;
           trc.speedStateReg[k] = 7;
         }
       }
       
       if(StromLevel[k] == 2)
       {
         sMiddle = sMiddle + 1;
       }
       
       k = k + 1;
     }
     
     if (trc.speedFlag == 0)
     {
       if (sMiddle == trc.nAx)
       {
         if (sLowRq == 0)
         {
           sLowRq = 1;
           SetCurrentToLow = ThrCnt + Times.CurrentToLow;
         }
       }
     } 
   }
     
   else
   {
     if (SetCurrentToLow == ThrCnt)
     {
       sLowRq = 0;
       
       if (trc.speedFlag == 0)
       {
         for trc.nAx
         {
           speedStateSet k 1;
           trc.speedStateReg[k] = 1;
        
           k = k + 1;
         }
       }
     }
   }
   // Stromverwaltung - Ende
   
   ThrCnt = ThrCnt + 1;
   Sleep 10;
   if(ThrReadInp == 0) {return;}
  }
" 100;

Mtest '0';       // Kein Testmodus: Kopplung an reale Motorpositionen

if(CtType.STM332F4Discovery)
{
  _drvHwLimWatch 0;  // bei Test HW Limits Watching OFF
}
else
{
  _drvHwLimWatch 1;  // HW Limits Watching ON
}

if(trc.nDev != 0)
{
  MinPower;
  
  trc.setPWM 0 0;     	// PWM wert ausgeben
  trc.setPort 9 1 1;    // speedBuffer Ueberwachung aktivieren
  trc.setPort 10 3 0;  	// Fehlerflag l�schen
}

wait 1.0;
out_WD_reset;
out_WD_trigger;

LogWrite "Leave EtInit_TRC2.ts";
