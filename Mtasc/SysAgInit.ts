/*====================================================================*/
/* Initialize AGI                                                     */
/*====================================================================*/
/* This file is called from SyAgInit macro.                           */ 
// Parameters: mtagI
/*--------------------------------------------------------------------*/
local "xml";
xml => GetXmlAppGrp mtagI;

(*mtag[mtagI].agi).outctrl.drv = 1;

(*mtag[mtagI].IniPath) = MtApp.path & sprint "/AxGroup%d" mtagI;

mtag[mtagI].fileCurr = &("");
mtag[mtagI].coTrans  = &("O");       /* default coordinate display mode */
mtag[mtagI].FmtDbl   = &("%9.3lf");  /* default display format          */
mtag[mtagI].manStepEnable = 1;       // allow manual jogging with AltGr+Cursor Keys

/* ================================================================== */
/* Selecting command sequences                                        */
/* ================================================================== */
/* Initialisation of stop parameters                                  */
/* ------------------------------------------------------------------ */
mtag[mtagI].vciStopped = 0L;

if(FileExists (*mtag[mtagI].IniPath) & "/FileStopped.ts") /* overwrites _VciStop    */
 {fcall (*mtag[mtagI].IniPath) & "/FileStopped.ts";}

/*====================================================================*/
/* GCode settings
/*====================================================================*/
mtag[mtagI].refMask                   =  xml.Ref.Mask._Xml2Mt;
mtag[mtagI].mirrX                     =  scall xml.!A?MirrX;
mtag[mtagI].mirrMode                  =  scall xml.!A?MirrMode;
(*mtag[mtagI].agi).crcCtrl.m_useFirstCmdAsEntry = scall xml.GCode.!A?CrcUseFirstCmdAsEntry;
(*mtag[mtagI].agi).GCodePar.IJKMode   = (xml.GCode.!A?IJKMode)[0];
(*mtag[mtagI].agi).GCodePar.FFac      = scall xml.GCode.!A?FFac;
(*mtag[mtagI].agi).GCodePar.IJKTol    =  scall xml.GCode.!A?IJKTol;
(*mtag[mtagI].agi).GCodePar.HelixMode =  scall xml.GCode.!A?HelixMode;
(*mtag[mtagI].agi).GCodePar.crcCloseByG40 = scall xml.GCode.!A?CrcCloseByG40;
(*mtag[mtagI].agi).GCodePar.BLOCKSKIP =  scall xml.GCode.!A?BlockSkip;
(*mtag[mtagI].agi).GCodePar.XYZFac._  =  array mtag[mtagI].vDim scall ((xml.GCode.!A?XYZFac)
                                       & " 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0");
(*mtag[mtagI].agi).GCodePar.iMap17    =  ObjDef T_short 3 scall xml.GCode.!A?iMap17;
(*mtag[mtagI].agi).GCodePar.iMap18    =  ObjDef T_short 3 scall xml.GCode.!A?iMap18;
(*mtag[mtagI].agi).GCodePar.iMap19    =  ObjDef T_short 3 scall xml.GCode.!A?iMap19;
(*mtag[mtagI].agi).GCodePar.subProgNMax   =  scall xml.GCode.!A?subProgNMax;
(*mtag[mtagI].agi).GCodePar.subProgMarker = (xml.GCode.!A?subProgMarker)[0];
(*mtag[mtagI].agi).GCodePar.subProgCaller = (xml.GCode.!A?subProgCaller)[0];

/*====================================================================*/
/* Positions, Origins, PickPos, WearOfs                               */
/*====================================================================*/
if(FileExists MtApp.path & "/Macros/PosLoad.ts")
{
  fCall MtApp.path & "/Macros/PosLoad.ts";
}
else
{
  mtag[mtagI].nPosOfs = 4;                /* 4 default machine positions*/
  mtag[mtagI].pMcPos  = &ObjDef CMc2Oc mtag[mtagI].nPosOfs;

  local "i"; i = 0L; for mtag[mtagI].nPosOfs
  {
    McPos[i].ofs   = ARRAY FagVDimMax 0.0;
    McPos[i].iMt   = 0;
    McPos[i].Flags = 0;
    i = i + 1;
  }

  (*McPos[0].Text) = "Machine origin (fixed)";
  (*McPos[1].Text) = "Service position";
  (*McPos[2].Text) = "Parking position";
  (*McPos[3].Text) = "Toold change position";


  mtag[mtagI].nOriOfs = 2;                /* only two default origins   */
  mtag[mtagI].pMc2Oc  = &ObjDef CMc2Oc mtag[mtagI].nOriOfs;

  i = 0L; for mtag[mtagI].nOriOfs
  {
    Mc2Oc[i].ofs   = ARRAY FagVDimMax 0.0;
    Mc2Oc[i].iMt   = 0;
    Mc2Oc[i].Flags = 0;
    i = i + 1;
  }

  (*Mc2Oc[0].Text) = "Machine origin (fixed)";
  (*Mc2Oc[1].Text) = "Temporary origin";


  mtag[mtagI].nPickPos = 1;               /* define tool switch only    */
  mtag[mtagI].pPickPos = &ObjDef CMc2Oc mtag[mtagI].nPickPos;

  PickPos[0].ofs     = ARRAY FagVDimMax 0.0;
  PickPos[0].iMt     = 0;
  PickPos[0].Flags   = 0;
  (*PickPos[0].Text) = "Tool switch";


  mtag[mtagI].nWearOfs = 1;               /* define null wear offset    */
  mtag[mtagI].pWearOfs = &ObjDef CMc2Oc mtag[mtagI].nWearOfs;

  WearOfs[0].ofs     = ARRAY FagVDimMax 0.0;
  WearOfs[0].iMt     = 0;
  WearOfs[0].Flags   = 0;
  (*WearOfs[0].Text) = "";
}

(*mtag[mtagI].ToolFile)      = "no file loaded";
(*mtag[mtagI].ToolFileAsked) = "no file loaded";

/*====================================================================*/
/* Tools                                                              */
/*====================================================================*/
if((Typeof "CToolData").Type == T_Classdscr)      /* class is defined     */
{
  mtag[mtagI].nTools = 1;                       /* use one default tool */
  mtag[mtagI].pTools = &ObjDef CToolData 1;         /* default instance     */

  (*mtag[mtagI].pTools)[0].Crc     = 0;         /* initialize zero tool */
  (*mtag[mtagI].pTools)[0].EMU     = 0;
  (*mtag[mtagI].pTools)[0].Ident   = 0;
  (*mtag[mtagI].pTools)[0].Ofs     = Array FagVDimMax 0.0;
  (*mtag[mtagI].pTools)[0].Rad     = 0.0;
  (*mtag[mtagI].pTools)[0].Len     = 0.0;
  (*mtag[mtagI].pTools)[0].Speed   = 0.0;
  (*mtag[mtagI].pTools)[0].Rpm     = 0L;
  (*mtag[mtagI].pTools)[0].Out     = 0;
  (*mtag[mtagI].pTools)[0].FPath   = 0.0;
  (*mtag[mtagI].pTools)[0].FTime   = 0.0;
  (*mtag[mtagI].pTools)[0].SPath   = 0.0;
  (*mtag[mtagI].pTools)[0].STime   = 0.0;
  (*mtag[mtagI].pTools)[0].DPath   = 0.0;
  (*mtag[mtagI].pTools)[0].DTime   = 0.0;
  (*mtag[mtagI].pTools)[0].Flags   = 0;
  (*(*mtag[mtagI].pTools)[0].BTC)  = "Default"; 
  (*(*mtag[mtagI].pTools)[0].ATC)  = "Default";
  (*(*mtag[mtagI].pTools)[0].DXF)  = "Default"; 
  (*(*mtag[mtagI].pTools)[0].File) = "Default"; 
  (*(*mtag[mtagI].pTools)[0].Text) = GetIniTxt "GetTool" "TxtTool0";
  (*mtag[mtagI].pTools)[0].Color   = 0L;
}

/*====================================================================*/
/* Speed, Acceleration                                                */
/*====================================================================*/
mtag[mtagI].ovr[0] = 1.0;
mtag[mtagI].ovr[1] = 1.0;
mtag[mtagI].ovr[2] = 1.0;

(*mtag[mtagI].agi).outctrl.ncMode = 3;
(*mtag[mtagI].agi).outctrl.grMode = 0;
(*mtag[mtagI].agi).outctrl.drv = 1;
(*mtag[mtagI].agi).laaLink 1;             // same as mtest '0'

(*mtag[mtagI].agi).mf_f  = 20.0;          /* default settings for speed */
(*mtag[mtagI].agi).ml_f  = 10.0;
(*mtag[mtagI].agi).mld_f =  5.0;

(*mtag[mtagI].agi).mf_r  = 500.0;         /* default acceleration values*/

(*mtag[mtagI].agi).ml_r  = 500.0;
(*mtag[mtagI].agi).mld_r = 500.0;

(*mtag[mtagI].agi).mf_f  = xml.SpeedAccel.mf_f._Xml2Mt;
(*mtag[mtagI].agi).ml_f  = xml.SpeedAccel.ml_f._Xml2Mt;
(*mtag[mtagI].agi).mld_f = xml.SpeedAccel.mld_f._Xml2Mt;
if((*mtag[mtagI].agi).mld_f == 0.0)
  {(*mtag[mtagI].agi).mld_f = (*mtag[mtagI].agi).ml_f;}

(*mtag[mtagI].agi).mf_r  = xml.SpeedAccel.mf_r._Xml2Mt;
(*mtag[mtagI].agi).ml_r  = xml.SpeedAccel.ml_r._Xml2Mt;
(*mtag[mtagI].agi).mld_r = xml.SpeedAccel.mld_r._Xml2Mt;
if((*mtag[mtagI].agi).mld_r == 0.0)
  {(*mtag[mtagI].agi).mld_r = (*mtag[mtagI].agi).ml_r;}

if((*mtag[mtagI].agi).fagIsDef)         /* values for positioning     */   
{
  (*mtag[mtagI].agi).fag.mp_f._ = xml.SpeedAccel.mp_f._Xml2Mt;
  (*mtag[mtagI].agi).fag.mp_r._ = xml.SpeedAccel.mp_r._Xml2Mt;
  (*mtag[mtagI].agi).fag.MPWrite;
}

/*====================================================================*/
/* Project parameters                                                 */
/*====================================================================*/
if((Typeof "CPParm").Type == T_Classdscr)     /* class is defined     */
{
  mtag[mtagI].pPParm = &ObjDef CPParm 0;        /* define instance      */

  (*mtag[mtagI].pPParm).nLines      = 0;        /* project parameters   */
  (*mtag[mtagI].pPParm).ColorMode   = 0;  
  (*mtag[mtagI].pPParm).Tool        = 0;  
  (*mtag[mtagI].pPParm).CrcMode     = 0;  
  (*mtag[mtagI].pPParm).CrcCheckLen = 2.0;

  (*mtag[mtagI].pPParm).Ofs     = Array 3 0.0 0.0 0.0;        /* Mtrans */
  (*mtag[mtagI].pPParm).Fac     = Array 3 1.0 1.0 1.0;         
  (*mtag[mtagI].pPParm).Rot     = Array 3 0.0 0.0 0.0;
  (*mtag[mtagI].pPParm).Flag    = 0; 

  (*mtag[mtagI].pPParm).RepN    = ObjDef T_short 3 1 1 1;    /* Clone parameters */
  (*mtag[mtagI].pPParm).RepD    = ObjDef T_double 3 0.0;

  (*mtag[mtagI].pPParm).HpUnitX = 0.025;            /* Hpgl parameters  */ 
  (*mtag[mtagI].pPParm).HpUnitY = 0.025;
  (*mtag[mtagI].pPParm).HpUFlag = 0;
  //(*mtag[mtagI].pPParm).HpglPen = ObjDef T_short 9 0;
  //(*mtag[mtagI].pPParm).HpglZdn = ObjDef T_double 9 0.0;

  (*(*mtag[mtagI].pPParm).ToolFile) = "";       /* initialize file info */
}

/*====================================================================*/
/* File parameters                                                    */
/*====================================================================*/
if((Typeof "CFParm").Type == T_Classdscr)     /* class is defined     */
{
  mtag[mtagI].pFParm = &ObjDef CFParm 0;        /* define instance      */

  (*mtag[mtagI].pFParm).Enabled       = 1;  
  (*mtag[mtagI].pFParm).MirrFlag      = 0;
  (*mtag[mtagI].pFParm).CrcMode       = 0;
  (*mtag[mtagI].pFParm).CrcHpgl       = 0;
  (*mtag[mtagI].pFParm).CrcBuffer     = 3;
  (*mtag[mtagI].pFParm).CrcNOpen      = 2;
  (*mtag[mtagI].pFParm).CmdSuv        = 0;
  (*mtag[mtagI].pFParm).Tool          = 0;
  (*mtag[mtagI].pFParm).Type          = 0;
  (*mtag[mtagI].pFParm).Cnv           = 0;

  (*mtag[mtagI].pFParm).Zup           = 2.0;
  (*mtag[mtagI].pFParm).Zdn0          = 0.0;
  (*mtag[mtagI].pFParm).Zdn0Flag      = 0;
  (*mtag[mtagI].pFParm).ZdnMill       = array 2 0.0 0.0;
  (*mtag[mtagI].pFParm).ZdnMillFlag   = 0;
  (*mtag[mtagI].pFParm).ZdnDrill      = array 2 0.0 0.0;
  (*mtag[mtagI].pFParm).ZdnDrillFlag  = 0;

  (*mtag[mtagI].pFParm).HookFlag      = 0;
  (*mtag[mtagI].pFParm).HookSrc       = array 3 0.0 0.0 0.0;
  (*mtag[mtagI].pFParm).HookDst       = array 3 0.0 0.0 0.0;

  (*mtag[mtagI].pFParm).MlColor       = 0L;

  (*(*mtag[mtagI].pFParm).ToolActive) = "0";
  (*(*mtag[mtagI].pFParm).Name)       = "NoName";
  (*(*mtag[mtagI].pFParm).Text)       = "NoText";
}
