//============================================================================
// Macrodefinierungen f�r Ein/Ausg�nge STM  IMC481
//============================================================================
//---------------------------------------------------------------------------------------------------------------------------------------------
//Pos  Makro                 Beschreibung                                 Zusatz
//---------------------------------------------------------------------------------------------------------------------------------------------
// 1    in_ES1_vorhanden      Karte 1 angeschlossen			   Achsen 1 bis 4
// 2    in_ES2_vorhanden      Karte 2 angeschlossen			   Achsen 5 bis 8
// 3    in_Power1_ok          Karte 1 meldet Motorspannung okay            Meldung von der Karte kann erst kommen, wenn po 24V on auf low
// 4    in_Power2_ok          Karte 2 meldet Motorspannung okay            Meldung von der Karte kann erst kommen, wenn po 24V on auf low
// 5    in_ES1_freigabe       Karte 1 erlaubt SPI Zugriff auf TMC          bei high hat der Endstufen Prozessor die SPI Signale unter Kontrolle
// 6    in_ES2_freigabe       Karte 2 erlaubt SPI Zugriff auf TMC          bei high hat der Endstufen Prozessor die SPI Signale unter Kontrolle
// 7    in_24V_IO             24V vorhanden                                �berpr�ft Versorgung der I/Os
// 8    in_SK_AX enable       Freigabe der Achsen durch das SK Modul       Bei low k�nnen Achsen bewegt werden
// 9    in_Cover_lock         Haube verriegelt                             Haube geschlossen und verriegelt, Achsen d�rfen bewegt werden
// 10   in_Notaus_zu          Notauskreis 1 geschlossen
// 11   in _Spindel_steht     Spindelstillstand                            wird vom FU bereitgestellt, wenn Spindel im Stillstand
//---------------------------------------------------------------------------------------------------------------------------------------------
// 12   out_ES1_Curr_down     Karte 1, f�hrt zur Stromabsenkung auf 70%    wenn ES Prozessor Initialisierung machen soll
// 13   out_ES2_Curr_down     Karte 2, f�hrt zur Stromabsenkung auf 70%    wenn ES Prozessor Initialisierung machen soll
// 14   out_ES24V_on          schaltet 24V Spannung auf den Endstufen ein
// 15   out_AX_Ready_SK       setzen, wenn Endstufen initialisiert sind
// 16   out_Bremse            gibt Bremse frei
// 17   out_Cover_disable     Haube sperren                                um versehentliches �ffnen w�hrend einer Bearbeitung zu vermeiden
// 18   out_Da_Adr            IO Erweiterung                               dient der Kennzeichnung des SPI Bytes als Datum oder Adresse
// 19   out_AX_enable         gibt Endstufen f�r beide Karten frei         nur, wenn AX Enab vom SK ebenfalls low ist
// 20   out_Ausgang_ena       gibt Ausg�nge frei
// 21   out_PWM               Erzeugung 0-10V
// 22   out_Spindel           Spindel freigabe                             Signal zum SK , SK schaltet Spindel
// 23   out_Motor_steht       setzen, wenn keine Achse in Bewegung ist     ohne gesetztes Still out mu� Haube verriegelt bleiben
// 24   out_PIC_STM           Init der Endstufen durch PIC oder STM        bei low mu� Endstufe durch STM initialisiert werden
// 25   out_ES_frei           Treiber f�r Endstufensignale freigeben
//------------------------------------------------------------------------------------------------------------------------------------------------
 
//  1. 2. low, wenn Endstufenkarte angeschlossen ---------------------------
(MacroDef "in_ES1_vorhanden") =  " 0 == ((trc.getPort 'C') and 0x0200);";
(MacroDef "in_ES2_vorhanden") =  " 0 == ((trc.getPort 'C') and 0x4000);";

//  3. 4. Endstufen haben 48V Betriebsspannung -----------------------------
(MacroDef "in_Power1_ok")     =  " 0 == ((trc.getPort 'F') and 0x0100);";          
(MacroDef "in_Power2_ok")     =  " 0 == ((trc.getPort 'C') and 0x0080)";  
       
//  5. 6. STM kann per SPI und Chipselekt Endstufen zugreifen --------------
(MacroDef "in_ES1_Freigabe")  =  " 0 == ((trc.getPort 'C') and 0x0040);";         
(MacroDef "in_ES2_Freigabe")  =  " 0 == ((trc.getPort 'C') and 0x0100)";  
        
//  7. 24V f�r IO sind angeschlossen ---------------------------------------
(MacroDef "in_24V_IO")        =  " 0 == ((trc.getPort 'F') and 0x0800);"; 
                
//  8. Freigabesignal vom SK Modul -----------------------------------------
(MacroDef "in_SK_AX_enable")  =  " 0 == ((trc.getPort 'F') and 0x2000);";  
        
//  9. Signal von Haube : verriegelt ---------------------------------------
(MacroDef "in_Cover_lock")    =  " 0 == ((trc.getPort 'F') and 0x4000);"; 
         
// 10. Notauskreis geschlossen ---------------------------------------------
(MacroDef "in_Notaus_zu")     =  " 0 == ((trc.getPort 'F') and 0x1000);"; 
           
// 11. Signal vom FU oder SK : Spindelstillstand ----------------------------
(MacroDef "in_Spindel_steht") =  " 0 == ((trc.getPort 'F') and 0x8000);";

//------------------------------------------------------------------------------------

// -- 12. 13 Strom f�r ES1,ES2 normal oder abgesenkt, wenn PIC EndS initialisiert ---
(MacroDef "out_ES1_Curr_down") =
  "if (%1) {trc.setPort 'B' 0x0002 0x0000;}
   else    {trc.setPort 'B' 0x0002 0x0002;}";

(MacroDef "out_ES2_Curr_down") =
  "if (%1) {trc.setPort 'B' 0x0001 0x0000;}
   else    {trc.setPort 'B' 0x0001 0x0001;}";

//-- 14. gibt die Betriebsspannungen f�r die Endstufen frei -------------------------
(MacroDef "out_ES24V_on") =
  "if (%1) {trc.setPort 'C' 0x0001 0x0000;}
   else    {trc.setPort 'C' 0x0001 0x0001;}";
 
//-- 15. Meldung an SK : Achsen sind initialisiert und bereit -----------------------
(MacroDef "out_AX_Ready_SK") =
  "if (%1) {trc.setPort 'B' 0x0100 0x0000;}
   else    {trc.setPort 'B' 0x0100 0x0100;}"; 

//-- 16. Bremse schalten ------------------------------------------------------------
(MacroDef "out_Bremse") =
  "if (%1) {trc.setPort 'C' 0x0002 0x0000;}
   else    {trc.setPort 'C' 0x0002 0x0002;}";

//-- 17. Haube sperren, z.B weil noch ein Programm l�uft ----------------------------
(MacroDef "out_Cover_disable") =
  "if (%1) {trc.setPort 'B' 0x0200 0x0000;}
   else    {trc.setPort 'B' 0x0200 0x0200;}"; 

//-- 18. IO Erweiterung: n�chsten gesendeten Bytes Daten  oder Adressen -------------
(MacroDef "out_SPI_Da_Adr") =
  "if (%1) {trc.setPort 'A' 0x8000 0x0000;}
   else    {trc.setPort 'A' 0x8000 0x8000;}";

//-- 19. Endstufen einschalten (ver-undet mit SK) -----------------------------------


(MacroDef "out_AX_enable") =
  "if (%1) {trc.setPort 'B' 0x1000 0x0000;}
   else    {trc.setPort 'B' 0x1000 0x1000;}"; 

//-- 20. gibt die Ausg�nge frei ----------------------------------------------------
(MacroDef "out_Ausgang_ena") =
  "if (%1) {trc.setPort 'C' 0x0004 0x0000;}
   else    {trc.setPort 'C' 0x0004 0x0004;}";
 
//-- 21. PWM ausgeben --------------------------------------------------------------

//-- 22. Spindel schalten ----------------------------------------------------------
(MacroDef "out_Spindel") =
  "if (%1) {trc.setPort 'B' 0x0020 0x0000;}
   else    {trc.setPort 'B' 0x0020 0x0020;}"; 

//-- 23. Stillstandskontrolle f�r SK Modul -----------------------------------------
(MacroDef "out_Motor_steht") =
  "if (%1) {trc.setPort 'B' 0x0010 0x0000;}
   else    {trc.setPort 'B' 0x0010 0x0010;}";
 
//-- 24. Endstufen Init durch STM oder PIC (Zugriff per SPI freigegeben) -----------
(MacroDef "out_PIC_STM") =
  "if (%1) {trc.setPort 'C' 0x0020 0x0000;}
   else    {trc.setPort 'C' 0x0020 0x0020;}"; 

//-- 25. Endstufen Treiber freigeben -----------------------------------------------
(MacroDef "out_ES_frei") =
  "if (%1) {trc.setPort 'C' 0x8000 0x0000;}
   else    {trc.setPort 'C' 0x8000 0x8000;}"; 

//============================================================================
// Macrodefinierungen f�r Motor/Achse
//============================================================================
//----------------------------------------------------------------------------
// Pos  Makro                 Beschreibung                                    
//----------------------------------------------------------------------------
// 1   SEL_'Achse'           Chipselekt f�r TMC X,Y,Z,A,B,C,D,E               
// 2   Register              gibt 3 Byte �ber SPI1 aus
// 3   Status_'Achse'        liest das Statusregister der entsprechenden Achse
// 4   INIT_'Achse''         initialisiert entsprechende Achse
//----------------------------------------------------------------------------
// == Chipselekte f�r Achsen definieren ===============================
// ester Wert %1 Achsnummer, zweiter Wert %2 =0 select; =1 deselect

(MacroDef "Sel_AX") =
  "trc.setport 'G' 0x00ff 0xffff;
   if(%1 == 0) { if (%2) {trc.setPort 'G' 0x0020 0x0020;} else {trc.setPort 'G' 0x0020 0x0000;} }
   if(%1 == 1) { if (%2) {trc.setPort 'G' 0x0010 0x0010;} else {trc.setPort 'G' 0x0010 0x0000;} }
   if(%1 == 2) { if (%2) {trc.setPort 'G' 0x0008 0x0008;} else {trc.setPort 'G' 0x0008 0x0000;} }
   if(%1 == 3) { if (%2) {trc.setPort 'G' 0x0004 0x0004;} else {trc.setPort 'G' 0x0004 0x0000;} }
   if(%1 == 4) { if (%2) {trc.setPort 'G' 0x0001 0x0001;} else {trc.setPort 'G' 0x0001 0x0000;} }
 ";
 
(MacroDef "Sel_off") = " trc.setport 'G' 0x00ff 0xffff; ";  // alle AX_Selecte off 

// ==== Endstufen Initialisierung Makros =====================================
// Register TMC default
// DRVCTRL  00004
// CHOPCONF 9A374
// SMARTEN  A2460
// SGCSCONF C0110
// DRVCONF  E0000
// -- die ersten  4 Achsen werden �ber SPI1 initialisiert ---------------------
// -- die zweiten 4 Achsen werden �ber SPI2 initialisiert ---------------------
// trc.SPIoutin (SPI Nr.)(Byte)

(MacroDef "SPI1") =
 " trc.spiOutIn 1 %1;
   trc.spiOutIn 1 %2 ;
   trc.spiOutIn 1 %3;
 ";
(MacroDef "SPI2") =
 " trc.spiOutIn 2 %1;
   trc.spiOutIn 2 %2 ;
   trc.spiOutIn 2 %3;
 ";

// Standardwerte vorbelegen, Aufl�sung und Stromwerte werden �bergeben
// %1 -> Achsnummer 
// %2 -> Strom in Current Scale
// %3 -> Aufl�sung
// %4 -> Interpolation, Flankenauswertung

(MacroDef "Init_AX") =
 "
   if(%1 < 4)
    {
     SEL_AX %1 0; SPI1 0x00   %4   %3 ; SEL_AX %1 1; // DRVCTRL
     SEL_AX %1 0; SPI1 0x08 0xA7 0x74 ; SEL_AX %1 1; // CHOPCONF
     SEL_AX %1 0; SPI1 0x0A 0x24 0x60 ; SEL_AX %1 1; // SMARTEN
     SEL_AX %1 0; SPI1 0x0C 0x01   %2 ; SEL_AX %1 1; // SGCSCONF
     SEl_AX %1 0; SPI1 0x0E 0x04 0x00 ; SEL_AX %1 1; // DRVCONF
    }
   else 
    {
     SEL_AX %1 0; SPI2 0x00   %4   %3 ; SEL_AX %1 1; // DRVCTRL
     SEL_AX %1 0; SPI2 0x08 0xA7 0x74 ; SEL_AX %1 1; // CHOPCONF
     SEL_AX %1 0; SPI2 0x0A 0x24 0x60 ; SEL_AX %1 1; // SMARTEN
     SEL_AX %1 0; SPI2 0x0C 0x01   %2 ; SEL_AX %1 1; // SGCSCONF
     SEL_AX %1 0; SPI2 0x0E 0x04 0x00 ; SEL_AX %1 1; // DRVCONF
    }
 ";

//====================================================================
// Strom absenken bzw. anheben 
// %1: Sollstromwert in Prozent
// %2 (optional): Achs-Index
//====================================================================

(MacroDef "Strom_Prozent") =
 "
  //MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --
  local \"ax\" \"n\";
  local \"Mot_Strom\";
  Mot_Strom = 0;
  ax = 0;
  n  = trc.nAx;
  if(%P > 1) {ax = %2; n = 1;}  // nur f�r eine Achse

  for n
  {
    // -- 4.5A RMS = 6,4A ;
    if (MotAmp[ax].strom > 6.4) {MotAmp[ax].strom = 6.4;}
    Mot_Strom = ((MotAmp[ax].strom * 5) * %1 / 100.0) + 0.5;
    // LogWrite sprint \"Axis %d: Strom=%d\" ax Mot_Strom;
    // -- Werte in die Endstufen schreiben --------------------
    if(ax < 4) { SEL_AX ax 0; SPI1 0x0C 0x01 Mot_Strom ;  SEL_AX ax 1; }
    else       { SEL_AX ax 0; SPI2 0x0C 0x01 Mot_Strom ;  SEL_AX ax 1; }
    ax = ax + 1;
  }
";
//***************************************************************************************************   

