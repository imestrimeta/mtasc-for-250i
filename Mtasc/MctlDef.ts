/* definitions for Isel CNC API */

CTrace = ObjDef T_classDscr
 "sizeMax" T_long 0
 "sizeCut" T_long 0
 "buf"     T_object 0
 "file"    T_object 0;

Trace = ObjDef CTrace;

Trace.sizeMax = 10000L;	/* size of trace buffer = size of log file for error messages */
Trace.sizeCut =  9000L;
(*Trace.buf) = "";
(*Trace.file) = MtApp.path & "/" & GetIniString (MtApp.path & "/Mctl_Mtasc.ini") "Communication" "TraceFile" "MctlLog.txt";

(MacroDef "TraceOut") =
"(*Trace.buf) = (*Trace.buf) & %1 & \"\\n\";
 if(Trace.sizeMax < sizeofArray *Trace.buf){
  local \"tb\";
  tb = *Trace.buf;
  (*Trace.buf) = ARRAY Trace.sizeCut 'x';
  memcpy (*Trace.buf) 0 tb ((sizeof \"tb\") - Trace.sizeCut)  Trace.sizeCut;
 }
 fwrites (*Trace.file) (*Trace.buf);
";

TraceOut "";	/* clear the log file */

//(MacroDef "_ErrHandler") =
/* "TraceOut \"Error Message ---------------------------\n\" & ThreadMsgText;"; */

(MacroDef "TraceOutInfo") =
"TraceOut \"Info Message ---------------------------\n\" & %1;";

/* -------------------------- Arc-, Circle- and Helix Interpolation */

/* these are usually the parameters for the mh function: */
hlxD = ObjDef CHelixDat;	/* global buffer for defining a helix */
hlx  = ObjDef CHelix;		/* global buffer for the helix data created */
hlxD.dim= Thread_agi.vdim;

(MacroDef "MctlHelix") = "mh hlxD hlx;";


Thread_agi.outCtrl.drv = 1;        // enable output via Driver object

/* -------------------------- give access to motion function for all new threads */
CallOnNewStack =
"
  Thread_SetDrv drv;
  Thread_NewObjList;        // thread local variable list
  mtag[mtagI].thAgi = &(gcnew CMtAgi 0 *mtag[mtagI].agi);
  Thread_setAgi (*mtag[mtagI].thAgi) (*mtag[mtagI].agi).fag;
  Thread_agi.outCtrl.drv = 1;
  _ncMode = 1; _grMode = 0;
  ml_f = Mvp.ml_f;
  ml_r = Mvp.ml_r;
  mf_f = Mvp.mf_f;
  mf_r = Mvp.mf_r;
  MvPar; Mreset;
";