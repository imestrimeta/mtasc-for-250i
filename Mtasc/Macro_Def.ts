
//============================================================================
// Macrodefinierungen f�r Motor/Achse
//============================================================================
//----------------------------------------------------------------------------
// Pos  Makro                 Beschreibung                                    
//----------------------------------------------------------------------------
// 1   CS_'Achse'            Chipselekt f�r TMC X,Y,Z,A,B,C,D,E               
// 2   Register              gibt 3 Byte �ber SPI1 aus
// 3   Status_'Achse'        liest das Statusregister der entsprechenden Achse
// 4   INIT_'Achse''         initialisiert entsprechende Achse
//----------------------------------------------------------------------------
 
 
// -- Chipselekte f�r Achsen definieren -------------------------------

  (MacroDef "CS_X") =
    "if (%1) {trc.setPort 'G' 0x0020 0x0020;}
     else    {trc.setPort 'G' 0x0020 0x0000;}";
  (MacroDef "CS_Y") =
    "if (%1) {trc.setPort 'G' 0x0010 0x0010;}
     else    {trc.setPort 'G' 0x0010 0x0000;}";
  (MacroDef "CS_Z") =
    "if (%1) {trc.setPort 'G' 0x0008 0x0008;}
     else    {trc.setPort 'G' 0x0008 0x0000;}";
  (MacroDef "CS_A") =
    "if (%1) {trc.setPort 'G' 0x0004 0x0004;}
     else    {trc.setPort 'G' 0x0004 0x0000;}";
  (MacroDef "CS_B") =
    "if (%1) {trc.setPort 'G' 0x0001 0x0001;}
     else    {trc.setPort 'G' 0x0001 0x0000;}";
  (MacroDef "CS_C") =
    "if (%1) {trc.setPort 'G' 0x0002 0x0002;}
     else    {trc.setPort 'G' 0x0002 0x0000;}";
  (MacroDef "CS_D") =
    "if (%1) {trc.setPort 'G' 0x0080 0x0080;}
     else    {trc.setPort 'G' 0x0080 0x0000;}";
  (MacroDef "CS_E") =
    "if (%1) {trc.setPort 'G' 0x0040 0x0040;}
     else    {trc.setPort 'G' 0x0040 0x0000;}";
 /*
  //---------------------------------------------------------------------------
  // bei den ersten 10 Mustern werden die Achsen von rechts nach links gez�hlt
  (MacroDef "CS_E") =
    "if (%1) {trc.setPort 'G' 0x0001 0x0001;}
     else    {trc.setPort 'G' 0x0001 0x0000;}";
  (MacroDef "CS_D") =
    "if (%1) {trc.setPort 'G' 0x0002 0x0002;}
     else    {trc.setPort 'G' 0x0002 0x0000;}";
  (MacroDef "CS_C") =
    "if (%1) {trc.setPort 'G' 0x0080 0x0080;}
     else    {trc.setPort 'G' 0x0080 0x0000;}";
  (MacroDef "CS_B") =
    "if (%1) {trc.setPort 'G' 0x0040 0x0040;}
     else    {trc.setPort 'G' 0x0040 0x0000;}";
  //----------------------------------------------------------------------------
 */
 
// ==== Endstufen Initialisierung Makros =====================================
// Register TMC default
// DRVCTRL  00004
// CHOPCONF 9A374
// SMARTEN  A2460
// SGCSCONF C0110
// DRVCONF  E0000
// -- die ersten  4 Achsen werden �ber SPI1 initialisiert ---------------------
// -- die zweiten 4 Achsen werden �ber SPI2 initialisiert ---------------------
// trc.SPIoutin (SPI Nr.)(Byte)

(MacroDef "SPI1") =
 " trc.spiOutIn 1 %1;
   trc.spiOutIn 1 %2 ;
   trc.spiOutIn 1 %3;
 ";
(MacroDef "SPI2") =
 " trc.spiOutIn 2 %1;
   trc.spiOutIn 2 %2 ;
   trc.spiOutIn 2 %3;
 ";
// zum Status auslesen werden 20bit hereingeschoben
// und dabei die 20 herausgeschobenen bits gelesen
// ich verwende die SMARTEN Register
// PH -> Macro PrintHex
(MacroDef "Status_X") = " ? \"AX X\"; CS_X 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 1 0x24;PH trc.spiOutIn 1 0x60;
   CS_X 1; ";
(MacroDef "Status_Y") = " ? \"AX Y\"; CS_Y 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 1 0x24;PH trc.spiOutIn 1 0x60;
   CS_Y 1; ";
(MacroDef "Status_Z") = " ? \"AX Z\"; CS_Z 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 1 0x24;PH trc.spiOutIn 1 0x60;
   CS_Z 1; ";
(MacroDef "Status_A") = " ? \"AX A\"; CS_A 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 1 0x24;PH trc.spiOutIn 1 0x60;
   CS_A 1; ";

(MacroDef "Status_B") = " ? \"AX B\"; CS_X 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 2 0x24;PH trc.spiOutIn 2 0x60;
   CS_X 1; ";
(MacroDef "Status_C") = " ? \"AX C\"; CS_Y 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 2 0x24;PH trc.spiOutIn 2 0x60;
   CS_Y 1; ";
(MacroDef "Status_D") = " ? \"AX D\"; CS_Z 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 2 0x24;PH trc.spiOutIn 2 0x60;
   CS_Z 1; ";
(MacroDef "Status_E") = " ? \"AX E\"; CS_A 0;
   PH trc.spiOutIn 1 0x0A;PH trc.spiOutIn 2 0x24;PH trc.spiOutIn 2 0x60;
   CS_A 1; ";
// Standardwerte vorbelegen, Aufl�sung und Stromwerte werden �bergeben
// %1 -> Achsnummer 
// %2 -> Strom in Current Scale
// %3 -> Aufl�sung
// %4 -> Interpolation, Flankenauswertung
(MacroDef "Init_AX") =
 "
 if (%1 == 0)
  {
   CS_X 0; SPI1 0x00   %4   %3 ; CS_X 1; // DRVCTRL
   CS_X 0; SPI1 0x08 0xA7 0x74 ; CS_X 1; // CHOPCONF
   CS_X 0; SPI1 0x0A 0x24 0x60 ; CS_X 1; // SMARTEN
   CS_X 0; SPI1 0x0C 0x01   %2 ; CS_X 1; // SGCSCONF
   CS_X 0; SPI1 0x0E 0x04 0x00 ; CS_X 1; // DRVCONF
  };

 if (%1 == 1)
  {
   CS_Y 0; SPI1 0x00   %4   %3 ; CS_Y 1;
   CS_Y 0; SPI1 0x08 0xA7 0x74 ; CS_Y 1;
   CS_Y 0; SPI1 0x0A 0x24 0x60 ; CS_Y 1;
   CS_Y 0; SPI1 0x0C 0x01   %2 ; CS_Y 1;
   CS_Y 0; SPI1 0x0E 0x04 0x00 ; CS_Y 1;
  };

 if (%1 == 2)
  { 
   CS_Z 0; SPI1 0x00   %4   %3 ; CS_Z 1;
   CS_Z 0; SPI1 0x08 0xA7 0x74 ; CS_Z 1;
   CS_Z 0; SPI1 0x0A 0x24 0x60 ; CS_Z 1;
   CS_Z 0; SPI1 0x0C 0x01   %2 ; CS_Z 1;
   CS_Z 0; SPI1 0x0E 0x04 0x00 ; CS_Z 1;
  };

 if (%1 == 3)
  { 
   CS_A 0; SPI1 0x00   %4   %3 ; CS_A 1;
   CS_A 0; SPI1 0x08 0xA7 0x78 ; CS_A 1;
   CS_A 0; SPI1 0x0A 0x24 0x60 ; CS_A 1;
   CS_A 0; SPI1 0x0C 0x01   %2 ; CS_A 1;
   CS_A 0; SPI1 0x0E 0x04 0x00 ; CS_A 1;
  };

 if (%1 == 4)
  { 
   CS_B 0; SPI2 0x00   %4   %3 ; CS_B 1; // DRVCTRL
   CS_B 0; SPI2 0x08 0xA7 0x78 ; CS_B 1; // CHOPCONF
   CS_B 0; SPI2 0x0A 0x24 0x60 ; CS_B 1; // SMARTEN
   CS_B 0; SPI2 0x0C 0x01   %2 ; CS_B 1; // SGCSCONF
   CS_B 0; SPI2 0x0E 0x04 0x00 ; CS_B 1; // DRVCONF
  };

 if (%1 == 5)
  { 
   CS_C 0; SPI2 0x00   %4   %3 ; CS_C 1;
   CS_C 0; SPI2 0x08 0xA7 0x78 ; CS_C 1;
   CS_C 0; SPI2 0x0A 0x24 0x60 ; CS_C 1;
   CS_C 0; SPI2 0x0C 0x01   %2 ; CS_C 1;
   CS_C 0; SPI2 0x0E 0x04 0x00 ; CS_C 1;
  };

 if (%1 == 6)
  {  
   CS_D 0; SPI2 0x00   %4   %3 ; CS_D 1;
   CS_D 0; SPI2 0x08 0xA3 0x78 ; CS_D 1;
   CS_D 0; SPI2 0x0A 0x24 0x60 ; CS_D 1;
   CS_D 0; SPI2 0x0C 0x01   %2 ; CS_D 1;
   CS_D 0; SPI2 0x0E 0x04 0x00 ; CS_D 1;
  };

 if (%1 == 7)
  {
   CS_E 0; SPI2 0x00   %4   %3 ; CS_E 1;
   CS_E 0; SPI2 0x08 0xA3 0x78 ; CS_E 1;
   CS_E 0; SPI2 0x0A 0x24 0x60 ; CS_E 1;
   CS_E 0; SPI2 0x0C 0x01   %2 ; CS_E 1;
   CS_E 0; SPI2 0x0E 0x04 0x00 ; CS_E 1;
  };

 ";

//==========================================================================================================================================
// Macrodefinierungen f�r Ein-/Ausg�nge
//====================================================================
//---------------------------------------------------------------------------------------------------------------------------------------------
//Pos  Makro                 Beschreibung                                 Zusatz
//---------------------------------------------------------------------------------------------------------------------------------------------
// 1    in_ES1_vorhanden      Karte 1 angeschlossen						            Achsen 1 bis 4
// 2    in_ES2_vorhanden      Karte 2 angeschlossen						            Achsen 5 bis 8
// 3    in_ES1_Power_ok       Karte 1 meldet Motorspannung okay            Meldung von der Karte kann erst kommen, wenn po 24V on auf low
// 4    in_ES2_Power_ok       Karte 2 meldet Motorspannung okay            Meldung von der Karte kann erst kommen, wenn po 24V on auf low
// 5    in_ES1_freigabe       Karte 1 erlaubt SPI Zugriff auf TMC          bei high hat der Endstufen Prozessor die SPI Signale unter Kontrolle
// 6    in_ES2_freigabe       Karte 2 erlaubt SPI Zugriff auf TMC          bei high hat der Endstufen Prozessor die SPI Signale unter Kontrolle
// 7    in_24V_IO             24V vorhanden                                �berpr�ft Versorgung der I/Os
// 8    in_SK_AX enable       Freigabe der Achsen durch das SK Modul       Bei low k�nnen Achsen bewegt werden
// 9    in_Cover_lock         Haube verriegelt                             Haube geschlossen und verriegelt, Achsen d�rfen bewegt werden
// 10   in_Notaus_zu          Notauskreis 1 geschlossen
// 11   in _Spindel_steht     Spindelstillstand                            wird vom FU bereitgestellt, wenn Spindel im Stillstand
//---------------------------------------------------------------------------------------------------------------------------------------------
// 12   out_ES1_Curr_down     Karte 1, f�hrt zur Stromabsenkung auf 70%    wenn ES Prozessor Initialisierung machen soll
// 13   out_ES2_Curr_down     Karte 2, f�hrt zur Stromabsenkung auf 70%    wenn ES Prozessor Initialisierung machen soll
// 14   out_ES24V_on          schaltet 24V Spannung auf den Endstufen ein
// 15   out_AX_Ready_SK       setzen, wenn Endstufen initialisiert sind
// 16   out_Bremse            gibt Bremse frei
// 17   out_Cover_disable     Haube sperren                                um versehentliches �ffnen w�hrend einer Bearbeitung zu vermeiden
// 18   out_Da_Adr            IO Erweiterung                               dient der Kennzeichnung des SPI Bytes als Datum oder Adresse
// 19   out_AX_enable         gibt Endstufen f�r beide Karten frei         nur, wenn AX Enab vom SK ebenfalls low ist
// 20   out_Ausgang_ena       gibt Ausg�nge frei
// 21   out_PWM               Erzeugung 0-10V
// 22   out_Spindel           Spindel freigabe                             Signal zum SK , SK schaltet Spindel
// 23   out_Motor_steht       setzen, wenn keine Achse in Bewegung ist     ohne gesetztes Still out mu� Haube verriegelt bleiben
// 24   out_PIC_STM           Init der Endstufen durch PIC oder STM        bei low mu� Endstufe durch STM initialisiert werden
// 25   out_ES_frei           Treiber f�r Endstufensignale freigeben
//------------------------------------------------------------------------------------------------------------------------------------------------
 
//  1. 2. low, wenn Endstufenkarte angeschlossen ---------------------------
(MacroDef "in_ES1_vorhanden") =  " 0 == ((trc.getPort 'C') and 0x0200);";
(MacroDef "in_ES2_vorhanden") =  " 0 == ((trc.getPort 'C') and 0x4000);";

//  3. 4. Endstufen haben 48V Betriebsspannung -----------------------------
(MacroDef "in_ES1_Power_ok")  =  " 0 == ((trc.getPort 'F') and 0x0100);";          
(MacroDef "in_ES2_Power_ok")  =  " 0 == ((trc.getPort 'C') and 0x0080)";  
       
//  5. 6. STM kann per SPI und Chipselekt Endstufen zugreifen --------------
(MacroDef "in_ES1_Freigabe")  =  " 0 == ((trc.getPort 'C') and 0x0040);";         
(MacroDef "in_ES2_Freigabe")  =  " 0 == ((trc.getPort 'C') and 0x0100)";  
        
//  7. 24V f�r IO sind angeschlossen ---------------------------------------
(MacroDef "in_24V_IO")        =  " 0 == ((trc.getPort 'F') and 0x0800);"; 
                
//  8. Freigabesignal vom SK Modul -----------------------------------------
(MacroDef "in_SK_AX_enable")  =  " 0 == ((trc.getPort 'F') and 0x2000);";  
        
//  9. Signal von Haube : verriegelt ---------------------------------------
(MacroDef "in_Cover_lock")    =  " 0 == ((trc.getPort 'F') and 0x4000);"; 
         
// 10. Notauskreis geschlossen ---------------------------------------------
(MacroDef "in_Notaus_zu")     =  " 0 == ((trc.getPort 'F') and 0x1000);"; 
           
// 11. Signal vom FU oder SK : Spindelstillstand ----------------------------
(MacroDef "in_Spindel_steht") =  " 0 == ((trc.getPort 'F') and 0x8000);";

//------------------------------------------------------------------------------------

// -- 12. 13 Strom f�r ES1,ES2 normal oder abgesenkt, wenn PIC EndS initialisiert ---
(MacroDef "out_ES1_Curr_down") =
  "if (%1) {trc.setPort 'B' 0x0002 0x0000;}
   else    {trc.setPort 'B' 0x0002 0x0002;}";

(MacroDef "out_ES2_Curr_down") =
  "if (%1) {trc.setPort 'B' 0x0001 0x0000;}
   else    {trc.setPort 'B' 0x0001 0x0001;}";

//-- 14. gibt die Betriebsspannungen f�r die Endstufen frei -------------------------
(MacroDef "out_ES24V_on") =
  "if (%1) {trc.setPort 'C' 0x0001 0x0000;}
   else    {trc.setPort 'C' 0x0001 0x0001;}";
 
//-- 15. Meldung an SK : Achsen sind initialisiert und bereit -----------------------
(MacroDef "out_AX_Ready_SK") =
  "if (%1) {trc.setPort 'B' 0x0100 0x0000;}
   else    {trc.setPort 'B' 0x0100 0x0100;}"; 

//-- 16. Bremse schalten ------------------------------------------------------------
(MacroDef "out_Bremse") =
  "if (%1) {trc.setPort 'C' 0x0002 0x0000;}
   else    {trc.setPort 'C' 0x0002 0x0002;}";

//-- 17. Haube sperren, z.B weil noch ein Programm l�uft ----------------------------
(MacroDef "out_Cover_disable") =
  "if (%1) {trc.setPort 'B' 0x0200 0x0000;}
   else    {trc.setPort 'B' 0x0200 0x0200;}"; 

//-- 18. IO Erweiterung: n�chsten gesendeten Bytes Daten  oder Adressen -------------
(MacroDef "out_SPI_Da_Adr") =
  "if (%1) {trc.setPort 'A' 0x8000 0x0000;}
   else    {trc.setPort 'A' 0x8000 0x8000;}";

//-- 19. Endstufen einschalten (ver-undet mit SK) -----------------------------------
/*
// dieses Makro nur f�r Muster 1-----------------------
(MacroDef "out_AX_enable") =
  "if (%1) {trc.setPort 'C' 0x0020 0x0000;}
   else    {trc.setPort 'C' 0x0020 0x0020;}"; 
*/

(MacroDef "out_AX_enable") =
  "if (%1) {trc.setPort 'B' 0x1000 0x0000;}
   else    {trc.setPort 'B' 0x1000 0x1000;}"; 

//-- 20. gibt die Ausg�nge frei ----------------------------------------------------
(MacroDef "out_Ausgang_ena") =
  "if (%1) {trc.setPort 'C' 0x0004 0x0000;}
   else    {trc.setPort 'C' 0x0004 0x0004;}";
 
//-- 21. PWM ausgeben --------------------------------------------------------------

//-- 22. Spindel schalten ----------------------------------------------------------
(MacroDef "out_Spindel") =
  "if (%1) {trc.setPort 'B' 0x0020 0x0000;}
   else    {trc.setPort 'B' 0x0020 0x0020;}"; 

//-- 23. Stillstandskontrolle f�r SK Modul -----------------------------------------
(MacroDef "out_Motor_steht") =
  "if (%1) {trc.setPort 'B' 0x0010 0x0000;}
   else    {trc.setPort 'B' 0x0010 0x0010;}";
 
//-- 24. Endstufen Init durch STM oder PIC (Zugriff per SPI freigegeben) -----------
(MacroDef "out_PIC_STM") =
  "if (%1) {trc.setPort 'C' 0x0020 0x0000;}
   else    {trc.setPort 'C' 0x0020 0x0020;}"; 
/*
// dieses Makro nur f�r Muster 1-----------------------
(MacroDef "out_PIC_STM") =
  "if (%1) {trc.setPort 'B' 0x0002 0x0000;}
   else    {trc.setPort 'B' 0x0002 0x0002;}"; 
*/

//-- 25. Endstufen Treiber freigeben -----------------------------------------------
(MacroDef "out_ES_frei") =
  "if (%1) {trc.setPort 'C' 0x8000 0x0000;}
   else    {trc.setPort 'C' 0x8000 0x8000;}";    
  

 
// -- Dezimal Zahl als HEX Zahl zeigen ------------------------ 
(MacroDef "PH") =
 " dezi = %1 ;
   z1 = dezi / 16; h1 = dezi - z1 * 16;
	 if ( h1 <= 9  ) {Hex1 = SPRINT \"%hd\" h1;};	
	 if ( h1 == 10 ) {Hex1 = \"A\";};
	 if ( h1 == 11 ) {Hex1 = \"B\";};
	 if ( h1 == 12 ) {Hex1 = \"C\";};
	 if ( h1 == 13 ) {Hex1 = \"D\";};
	 if ( h1 == 14 ) {Hex1 = \"E\";};
	 if ( h1 == 15 ) {Hex1 = \"F\";};
   z2 = z1 / 16;    h2 = z1 - z2 * 16;
	 if ( h2 <= 9  ) {Hex2 = SPRINT \"%hd\" h2;};	
 	 if ( h2 == 10 ) {Hex2 = \"A\";};
	 if ( h2 == 11 ) {Hex2 = \"B\";};
	 if ( h2 == 12 ) {Hex2 = \"C\";};
	 if ( h2 == 13 ) {Hex2 = \"D\";};
	 if ( h2 == 14 ) {Hex2 = \"E\";};
	 if ( h2 == 15 ) {Hex2 = \"F\";};
   z3 = z2 / 16;    h3 = z2 - z3 * 16;
	 if ( h3 <= 9  ) {Hex3 = SPRINT \"%hd\" h3;};	
	 if ( h3 == 10 ) {Hex3 = \"A\";};
	 if ( h3 == 11 ) {Hex3 = \"B\";};
	 if ( h3 == 12 ) {Hex3 = \"C\";};
	 if ( h3 == 13 ) {Hex3 = \"D\";};
	 if ( h3 == 14 ) {Hex3 = \"E\";};
	 if ( h3 == 15 ) {Hex3 = \"F\";};
   z4 = z3 / 16;    h4 = z3 - z4 * 16;
	 if ( h4 <= 9  ) {Hex4 = SPRINT \"%hd\" h4;};	
	 if ( h4 == 10 ) {Hex4 = \"A\";};
	 if ( h4 == 11 ) {Hex4 = \"B\";};
	 if ( h4 == 12 ) {Hex4 = \"C\";};
	 if ( h4 == 13 ) {Hex4 = \"D\";};
	 if ( h4 == 14 ) {Hex4 = \"E\";};
	 if ( h4 == 15 ) {Hex4 = \"F\";};
   hex = sprint \"%s%s%s%s\" Hex4,Hex3,Hex2,Hex1; ? hex ;
  ";
// ------------------------------------------------------------  