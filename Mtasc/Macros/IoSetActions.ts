/*--------------------------------------------------------------------*/
// Generating input descriptors
/*--------------------------------------------------------------------*/
local "io" "i" "todo";

todo = 15;      // read all data as default;
if(%P > 0) {todo = %1;}

if(todo and 1)
{
  _nINputs = scall MtApp.xml.Hardware.Inputs.!A?nData;
  if(_nInputs < 0) {_nInputs = 0;}
  i = 1; for _nINputs
  {
    io => MtApp.xml.Hardware.Inputs.(sprint "v%d" i);
    _inpDsc[i].type= io.!A?Type;
    _inpDsc[i].adr = scall io.!A?Address;
    _inpDsc[i].mask = scall io.!A?Mask;
    _inpDsc[i].inv = scall io.!A?InvMask;
    _inpDsc[i].size = scall io.!A?ByteSize;
    _inpDsc[i].mapFlags = scall io.!A?MapMode;
    _iMode[i]      = scall io.!A?IoMode;

   (*_iLabel[i].Text) = io.!A?LabelText;

   _drvIoDTab.set (_nIotMax + i - 1) _inpDsc[i];

   i = i + 1;
  }
}

/*--------------------------------------------------------------------*/
// Generating output descriptors
/*--------------------------------------------------------------------*/
if(todo and 2)
{
  _nOutputs = scall MtApp.xml.Hardware.Outputs.!A?nData;
  if(_nOutputs < 0) {_nOutputs = 0;}
  i = 1; for _nOutputs
  {
    io => MtApp.xml.Hardware.Outputs.(sprint "v%d" i);
    _outDsc[i].type= io.!A?Type;
    _outDsc[i].adr = scall io.!A?Address;
    _outDsc[i].mask = scall io.!A?Mask;
    _outDsc[i].inv = scall io.!A?InvMask;
    _outDsc[i].size = scall io.!A?ByteSize;
    _outDsc[i].mapFlags = scall io.!A?MapMode;
    _oMode[i]      = scall io.!A?IoMode;

   (*_oLabel[i].Text) = io.!A?LabelText;

   _drvIoDTab.set (_nIotMax + _nInpMax + i - 1) _OutDsc[i];

   i = i + 1;
  }
}

if(0 == todo and 4) {return;}

/*====================================================================*/
/* Generating action table and descriptors from MTasc.ini             */
/*====================================================================*/
/* The parameters in MTasc.ini are:                                   */
/* action Source SrcBit Dest DstBit nBits Text                        */ 
/*--------------------------------------------------------------------*/

if((MtApp.xml.Hardware.!E!HardInputs.!A!nData) == "")
{
  MtApp.xml.Hardware.!E!HardInputs.!A!nData "0";
  MtApp.xml.Hardware.!E!HardOutputs.!A!nData "0";
  MtApp.xml.Hardware.!E!IoMapping.!A!nData "0";
  MtApp.xml.Hardware.!E!IoMapping.!A!nDataMax "20";
}
_nActions =
 (scall MtApp.xml.Hardware.!E!HardInputs.!A?nData) +
 (scall MtApp.xml.Hardware.!E!IoMapping.!A?nData) +
 (scall MtApp.xml.Hardware.!E!HardOutputs.!A?nData);

if(_drvIoATab.dim != _nActions) {_drvIoATab.dim= 0;}                                         
_drvIoATab.dim= _nActions;
_ActLabel = ObjDef CInOut _nActions + 1;
(*_ActLabel[0].Text) = "Aktionen";

i = 1L; for _nActions                     /* label initialization     */
{
 (*_ActLabel[i].Text) = "";
 i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Hard input descriptors and actions (Input->MMap):                  */
/*--------------------------------------------------------------------*/
local "nLines" "nAction" "TmpDsc" "TmpAction";

nLines    = nAction = 0;
TmpDsc    = gcnew CIoDscr;
TmpAction = gcnew CInOutMapping;

nLines = scall MtApp.xml.Hardware.HardInputs.!A?nData;

i = 1; for nLines               // Hard-Deskriptor von 0..._nIotMax-1
{
  io => MtApp.xml.Hardware.HardInputs.(sprint "v%d" i);
  TmpDsc.type= io.!A?Type;
  TmpDsc.adr = scall io.!A?Address;
  TmpDsc.mask = scall io.!A?Mask;
  TmpDsc.inv = scall io.!A?InvMask;
  TmpDsc.size = scall io.!A?ByteSize;
  TmpDsc.mapFlags = scall io.!A?MapMode;

 _drvIoDTab.set nAction TmpDsc;

 TmpAction.iomType  = 0x0105;              /* Deskriptor->Temp->Map    */
 TmpAction.iSrc    = nAction;
 TmpAction.iSrcBit = 0;
 TmpAction.iDst    = 0x0010 + i - 1;      /* Offset der Input-Tabelle */
 TmpAction.iDstBit = 0;
 TmpAction.nBits   = 8 * TmpDsc.size;

 _drvIoATab.set nAction TmpAction;        /* Hard-Aktion speichern    */
 (*_ActLabel[nAction].Text) = io.!A?LabelText;

 nAction = nAction + 1;
 i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Soft actions (MMap->MMap):                                         */
/*--------------------------------------------------------------------*/
nLines = scall MtApp.xml.Hardware.!E!IoMapping.!A?nData;

i = 1; for nLines
{
  io => MtApp.xml.Hardware.IoMapping.(sprint "v%d" i);
  TmpAction.iomType = scall io.!A?iomType;
  TmpAction.iSrc    = scall io.!A?iSrc;
  TmpAction.iSrcBit = scall io.!A?iSrcBit;
  TmpAction.iDst    = scall io.!A?iDst;
  TmpAction.iDstBit = scall io.!A?iDstBit;
  TmpAction.nBits   = scall io.!A?nBits;

  _drvIoATab.set nAction TmpAction;        /* Soft-Aktion speichern    */
  (*_ActLabel[nAction].Text) = io.!A?LabelText;

  nAction = nAction + 1;
  i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Hard output descriptors and actions (MMap->Output):                */
/*--------------------------------------------------------------------*/
nLines = scall MtApp.xml.Hardware.HardOutputs.!A?nData;

i = 1; for nLines               // Hard-Deskriptor von 0..._nIotMax-1
{
  io => MtApp.xml.Hardware.HardOutputs.(sprint "v%d" i);
  TmpDsc.type= io.!A?Type;
  TmpDsc.adr = scall io.!A?Address;
  TmpDsc.mask = scall io.!A?Mask;
  TmpDsc.inv = scall io.!A?InvMask;
  TmpDsc.size = scall io.!A?ByteSize;
  TmpDsc.mapFlags = scall io.!A?MapMode;

 _drvIoDTab.set nAction TmpDsc;

 TmpAction.iomType  = 0x010A;              /* Map->Temp->Deskriptor    */
 TmpAction.iSrc    = 0x0040 + i - 1;       /* Offset der Output-Tabelle*/
 TmpAction.iSrcBit = 0;
 TmpAction.iDst    = nAction;
 TmpAction.iDstBit = 0;
 TmpAction.nBits   = 8 * TmpDsc.size;

 _drvIoATab.set nAction TmpAction;        /* Hard-Aktion speichern    */
 (*_ActLabel[nAction].Text) = io.!A?LabelText;

 nAction = nAction + 1;
 i = i + 1;
}

