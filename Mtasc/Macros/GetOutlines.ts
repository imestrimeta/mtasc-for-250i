/* ================================================================== */
/* Erzeugung einer Konturliste                                        */
/* ================================================================== */
// *gbx.flPboIn: Vollständiger Pfad der zu wandelnden Datei
/*            Zur Zeit sind nur MTASC-Dateien möglich!                */ 
/* ------------------------------------------------------------------ */
if(_mode != 'G'){Mreset "Nur im Grafikmodus erlaubt!";}

if((TypeOf "mlist").type != 1)
{
 DeleteDgiMObjList;
 erase pbo mlist;
}

local "CallPath" "CallOnExit"; CallOnExit = "@gbx.mode = 0;";
gbx.mode = 1;   // mode for collecting data in mlist

mlist = gcnew CMObjList;
mlist.<0>.dim= 2;
mlist.addObjGrpMode = 1;
Thread_agi.SetMObjList &mlist;

Thread_agi.outCtrl.mObjList = 1;

_octMinMax_stat  = 1;                  // calculate object dimensions
_octMinMax_sflag = 3;

CallPath = FileGetFolder *gbx.flPboIn;
if(gbx.flType == 1) {fCall    *gbx.flPboIn;}                /* Datei ausführen */
if(gbx.flType == 2) {dinCall  *gbx.flPboIn;}
if(gbx.flType == 3) {hpglCall *gbx.flPboIn;}
if(gbx.flType == 4) {iselCall *gbx.flPboIn;}

local "Left" "Right" "Bot" "Top";
Left  = _OctMin[0] - 2.0;
Right = _OctMax[0] + 2.0;
Bot   = _OctMin[1] - 2.0;
Top   = _OctMax[1] + 2.0;

if(gbx.flBorder)
{
  GbxInvert 0;
  GetXTool 6;
  Thread_agi.mact.mfl.tool = Thread_agi.mact.mfl.tool or 0x200;  // do not draw in outlines
  MoveTo  1  (Left  + 0.2) (Bot + 0.2);
  x (Right - 0.2); y (Bot + 0.2); ml;
  x (Right - 0.2); y (Top - 0.2); ml;
  x (Left  + 0.2); y (Top - 0.2); ml;
  x (Left  + 0.2); y (Bot + 0.2); ml;

  GbxInvert 1;
  GetXTool 6;
  Thread_agi.mact.mfl.tool = Thread_agi.mact.mfl.tool or 0x200;  // do not draw in outlines
__test mlist;
  fcall *gbx.flBorder;
}

Thread_agi.outCtrl.mObjList = 0;

pbo = gcnew CPboPrj 0 mlist Left Bot Right Top gbx.grid Thread_agi.g3;
pbo.scanXY;

pbo.GetOutlines;
