/*====================================================================*/
/* Drill-Macros f�r Leiterplatten Excellon/Sieb & Mayer               */
/*====================================================================*/
/* t-Befehle in Bohrdateien (t = Tool, ruft GetTool-Macro)            */
/*--------------------------------------------------------------------*/
(MacroDef "t") = 
" local \"rPrv\" \"msg\"; rPrv = 0.0;
  if((typeof \"r\").type != T_undefined) {rPrv = r;}
  r = Tools[%1].rad;
  if(%P > 1) {if(%2 > 0.0) {r = %2;}}
  if(_fParm.tool)
  {
    if(_mode != 'G')
    {
      if(rPrv < Tools[_fParm.tool].rad)
      {
        if(r < Tools[_fParm.tool].rad)
        {
          msg = sprint \"Bitte Bohrwerkzeug mit Durchmesser %.2lf einspannen!\" 2.0 * r;
        }
        else
        {
          msg = sprint \"Bitte Standard-Fr�sbohrer (Durchmesser %.2lf) einspannen!\" 2.0 * Tools[_fParm.tool].rad;
        }
        z zup + 20; mf;                       /* etwas weiter hochfahren */
        wait 0;
        _oStatPrv = _oStat; _PrgOff;          /* Spindel usw. aus        */
        msgbox msg \"Manueller Wekzeugwechsel\" MB_OK;
        _PrgOn;
      }
    }
  }
  else
  {
    Gettool %1;
  }
 if(%P > 2) {X %3; Y %4; MF; Z Zup; MF;}
";

/*--------------------------------------------------------------------*/
/* d-Befehle in Bohrdateien (d = Drill)                               */
/*--------------------------------------------------------------------*/
(MacroDef "d") =       
"local \"rSoll\";
 if(%P > 2)  {rSoll = %3;}  // Version mit r als Argument
 else        {rSoll =  r;}  // Version mit 'globalem' r

 Drill %1 %2 rSoll ToolRad; 
";

/*--------------------------------------------------------------------*/
/* Drill-Befehl in Bohrdateien                                        */
/*--------------------------------------------------------------------*/
(MacroDef "Drill") =
"local  \"z1\" \"z2\" \"dz\" \"rTool\" \"rc\";

 z1 = _fParm.Zdn0;
 z2 = _fParm.ZdnDrill[0];
 dz = _fParm.ZdnDrill[1];

 rTool = ToolRadEmu;
 if(%P > 3) {rTool = %4;}

 rc = %3 - rTool;                          /* Radius kompensieren     */
 if(rc < 0.0) {rc = 0.0;}                  /* ggf. �berbohren         */
 if(_fparm.ZdnDrillFlag == 0){rc = 0;}     /* Spiralbohren verhindern */  

 if(z < zup){Z Zup; mf;}                   /* freifahren              */
 x %1 + rc; y %2; mf;                      /* Punkt rechts anfahren   */

 DrawDrillCirc %1 %2 %3;                   // draw with expected diameter
 if(rc == 0.0){DrawDrillCirc %1 %2 rTool;} // redraw with actual diameter

 z z1; MF;                                 /* absenken                */

 if(rc <= 0.0)                             /* ohne Spiralbohren:      */
 {
   if(_fParm.Zdn0Flag) {z Z2; mld;} else {z Z2; ml;}
 }
 if(rc > 0.0)                              /* mit Spiralbohren:       */       
 {
   while \"z > z2 + 0.001\"
   {
     x %1 - rc; mc rc \"-LZ\";             /* einen Kreis fahren      */
     x %1 + rc; mc;
     z z - dz;                             /* weiter absenken         */
     if (z < z2) {z z2;}
     MLD;
   }
   x %1 - rc; mc rc \"-LZ\";               /* letzten Kreis fahren    */ 
   x %1 + rc; mc;
 }

 z zup; mf;                                /* auftauchen              */ 
";

/*--------------------------------------------------------------------*/
/* Pen-Befehle (Pen up, pen down)                                     */
/*--------------------------------------------------------------------*/
(MacroDef "PU") = "SetCrc 0; Z Zup; MF;";
(MacroDef "PD") = "Z Zdn0; mF; Z Zdn; MLD;";

(MacroDef "SetF") = "ML_F = %1; MvPar;";
(MacroDef "G11")  = "";
// (MacroDef "SetF") = ""; /* leer �berschreiben f�r Hella */

/*--------------------------------------------------------------------*/
/* Kreis-Befehle (Innen/Aussen; links/rechts)                         */
/*--------------------------------------------------------------------*/
(MacroDef "Circ") =
"local \"rB\" \"rT\";

 // %1 = 45: Vollkreis innen  links
 // %1 = 46: Vollkreis innen  rechts
 // %1 = 47: Vollkreis aussen links
 // %1 = 48: Vollkreis aussen rechts

 rT = Tools[_toolSelEmu].Rad;

 if(%1 <= 46) {rB = %4 - rT;} 
 if(%1 >= 47) {rB = %4 + rT;} 

 DrawDrillCirc %2 %3 %4 0xFFFFFFL;          // weiss mit Kreisradius

 if(%1 == 45)
 {
    X %2 + 0.707 * 0.5 * rB;                // Anfahrt
    Y %3 + 1.707 * 0.5 * rB;
    MF; Z Zdn0; MF; Z Zdn; MLD;
    
    X %2; Y %3 + rB; Mc rB \"-LZ\";         // Vollkreis
    X %2; Y %3 - rB; Mc rB \"-LZ\";
    X %2; Y %3 + rB; Mc rB \"-LZ\";

    // langsamer fahren

    X %2 - 0.707 * 0.5 * rB;                // Abfahrt 
    Y %3 + 1.707 * 0.5 * rB; Mc rB \"-Lz\";
 }

 if(%1 == 46)
 {
    X %2 - 0.707 * 0.5 * rB;                // Anfahrt
    Y %3 + 1.707 * 0.5 * rB;
    MF; Z Zdn0; MF; Z Zdn; MLD;
    
    X %2; Y %3 + rB; Mc rB \"-RZ\";         // Vollkreis
    X %2; Y %3 - rB; Mc rB \"-RZ\";
    X %2; Y %3 + rB; Mc rB \"-RZ\";

    // langsamer fahren

    X %2 + 0.707 * 0.5 * rB;                // Abfahrt
    Y %3 + 1.707 * 0.5 * rB; Mc rB \"-RZ\";
 }

 if(%1 == 47)
 {
    X %2 - rB; Y %3 + rT;                   // Anfahrt
    MF; Z Zdn0; MF; Z Zdn; MLD;

    X %2 - rB; Y %3; Ml;                    // Vollkreis
    X %2 + rB; Y %3; Mc rB \"-LZ\";
    X %2 - rB; Y %3; Mc rB \"-LZ\";
    
    // langsamer fahren

    X %2 - rB; Y %3 - rT; Ml;               // Abfahrt
 }

 if(%1 == 48)
 {
    X %2 - rB; Y %3 - rT;                   // Anfahrt
    MF; Z Zdn0; MF; Z Zdn; MLD;
    
    X %2 - rB; Y %3; Ml;                    // Vollkreis
    X %2 + rB; Y %3; Mc rB \"-RZ\";
    X %2 - rB; Y %3; Mc rB \"-RZ\";

    // langsamer fahren

    X %2 - rB; Y %3 + rT; Ml;               // Abfahrt
 }

 Z Zup; Mf;
";

/*--------------------------------------------------------------------*/
/* Radiuskorrektur                                                    */
/*--------------------------------------------------------------------*/
(MacroDef "SetCrc") = 
"
 if(%1 == 0) {CrcClose;} else {_Crcpar.m_isClosed = 0;}

 if(%1 == 1) {CrcOri =  1; CrcOpen;}
 if(%1 == 2) {CrcOri = -1; CrcOpen;}
";

