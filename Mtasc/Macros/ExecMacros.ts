/* ================================================================== */
/* Macros for program execution                                       */
/* ================================================================== */
/* ExecMacros: _ExecVci                                               */ 
/*             _ExecProject                                           */ 
/*             _MainCall                                              */ 
/*             _ThrdCall                                              */ 
/*             _PrjDrawExec                                           */ 
/*             pCall                                                  */ 
/*             _PrgDrawExecRepeat                                     */ 
/* ================================================================== */
/* _ExecVci: Projekt-Ausf�hrung f�r Teilkonturen.                     */
/* ------------------------------------------------------------------ */
/* This macro sets the VCI variables and calls _ExecProject           */
/* in graphic or execution mode.                                      */ 
/* Parameters: %1:   Full path and project name                       */
/*             %2=0: graphic call;  %2=1: execution                   */ 
/*             %3=0: VCI-Search;    %3=1: hook-search                 */ 
/*             %3=2: start->mark;   %3=3: mark->end;  %3=4: contour   */ 
/* Start, mark and end are set by VciForm                             */ 
/* ================================================================== */
(MacroDef "_ExecVci") =
"local \"CallOnExit\";
 CallOnExit = sprint \"@_drawpar[0] = %d; Thread_agi.vci.Mode = Thread_agi.g3.search.g3sMode = '0';\" _drawpar[0];

 Thread_agi.vci.switchToNc = %2;

 if((%3 == 2) AND (%2 == 0)) {_drawpar[0] = 0xFFFFL;} // ML-Farbe gelb

 if(%3 == 0) {VciSet 'p' 0 0;}                        // search path
 if(%3 == 1) {VciSet 'p' 0 0;}                        // search contour
 if(%3 == 2) {VciSet 'S' 0 Thread_agi.vci.iMark + 1;} // start to mark
 if(%3 == 3) {VciSet 'S' Thread_agi.vci.iMark -1;}    // mark  to end
 if(%3 == 4) {VciSet 'S';}                            // contour 

 if((%3 == 2) AND (%2 == 1)) {Thread_agi.vci.Mode = 'E';}

 //if(%3 <= 1) {MsgBox sprint \"g3sMode='%c'\" Thread_agi.g3.search.g3sMode;}

 if(0) //_IdeFlag)
 {
   VciShow \"Vorher:\" %2 %3;
   CallOnExit = callOnExit & \" VciShow \\\"nachher\\\";\";
 }

 if(%2 == 0)    
  {_ExecProject %1 mtagI 0 1 0;}             // Teilkontur nur Grafik

 if(%2 == 1)    
 {
   if(%3 == 2)
    {_ExecProject %1 mtagI 1 0 0;}           // Teilkontur Start->Mark
   else
    {_ExecProject %1 mtagI 3 0 0;}           // Teilkontur ab Mark
 }

 if(%3 == 1)                                 // gefundene Struktur anzeigen
  {Thread_agi.vci.Mode = 'S'; _ExecProject %1 mtagI 0 1 0;}
";

(MacroDef "VciSet") =
"Thread_agi.vci.Mode   = %1;
 if(%P > 1) {Thread_agi.vci.iStart = %2;}
 if(%P > 2) {Thread_agi.vci.iEnd   = %3;}
";

(MacroDef "VciShow") =
"local \"Txt\"; Txt = \"\";
 
 if(%P) {Txt = Txt & %1 & \"\\\r\\\n\";}

 if(%P >= 3) {txt = txt & sPrint \"Vci-Args: No.2=%d; No.3=%d\r\n\" %2 %3;}

 txt = txt & sPrint \"VciModes: Vci=%d, g3.search=%d\r\n\"
             Thread_agi.vci.Mode
             Thread_agi.g3.search.g3sMode;
 txt = Txt & Sprint \"Mark/Start/End/Curr:  %d %d %d %d\r\n\"
             Thread_agi.vci.iMark Thread_agi.vci.iStart
             Thread_agi.vci.iEnd  Thread_agi.vci.iCurr;

 msgbox Txt;
";

(MacroDef "VciCurr") =
"if(_IdeFlag) 
{
  // MsgBox \"IDE: VciCurr wurde unerwartet aufgerufen\";
  SendError \"ERR_ExecMacros_VciCurr\";
} 
0L";

/* ================================================================== */
/* _ExecProject: Execute project in AGI                               */ 
/* ------------------------------------------------------------------ */
/* %1:   Full project path and name                                   */
/* %2:   AGI to execute project                                       */ 
/* %3=0: Graphic                                                      */ 
/* %3=1: Execution with switches                                      */
/* %3=2: Execution without switches                                   */
/* %3=3: Execution with start in silent mode                          */
/* %4:   Graphic visible/invisible  (optional)                        */
/* %5:   Calculate path time values (optional)                        */
/* ================================================================== */
(MacroDef "_ExecProject") =
"if(0 == FileExists %1)
 {Exit \"Die Projektdatei wurde nicht gefunden: \r\n\r\n\" & %1;}

 local \"nFiles\";
 nFiles = scall GetIniString %1 \"Project\" \"Files\" \"0\";

 local \"OneLine\" \"Ok\" \"i\"; Ok = i = 0;
 for(nFiles)
 {
   OneLine = GetIniString %1 \"Files\" (sprint \"%d\" i = i + 1) \"\";
   if(OneLine == \"\"){Exit sprint \"Fehlende Projektzeile %d\" i;}
   if(0 < GetStkObj 0 scall OneLine) {Ok = 1;}
 }

 if(0 == nFiles)
 {
   OneLine = GetIniString %1 \"Project\" \"TempFile\" \"\";

   if(OneLine == \"\")
    {Exit GetIniTxt \"System\" \"ExecMissingFile\";}
   else
    {Ok = 1;}
 }

 if(0 == OK)
 {return;}    // Die Projektdatei enth�lt keine ausf�hrbaren Zeilen
 
 local \"Visible\"; Visible = 1; if(%P > 3) {Visible = %4;}
 local \"PtCount\"; PtCount = 1; if(%P > 4) {PtCount = %5;}

 if(0) // (_IdeFlag)
  {print sprint \"ExecProject vorher:  AGI=%d; Mode=%d; Vis=%d; PathTime=%d\" %2 %3 Visible PtCount;}


 if(%3 == 0) {MainCall %1 %2 %3 Visible PtCount;}  // Graphic
 if(%3 == 1) {ThrdCall %1 %2 %3 Visible PtCount;}  // Nc Execution
 if(%3 == 2) {ThrdCall %1 %2 %3 Visible PtCount;}  // Nc Execution
 if(%3 == 3) {ThrdCall %1 %2 %3       0 PtCount;}  // silent mode

 if(0) // (_IdeFlag)
  {print sprint \"ExecProject nachher: AGI=%d; Mode=%d; Vis=%d; PathTime=%d\" %2 %3 Visible PtCount;}
";

/* ================================================================== */
/* run Graphic in main thread                                         */
/* ------------------------------------------------------------------ */
/* %1: Project name                                                   */
/* %2: AGI for execution                                              */
/* %3, %4, %5: passed to _PrjDrawExec                                 */
/* ================================================================== */
(MacroDef "MainCall") = 
"local \"CallOnExit\"; CallOnExit = \"@mtag[mi].thGr = 0;\";
 local \"mi\"; mi = %2; 
 mtag[mi].fileGr = &(%1);                 // publish file name
 mtag[mi].thGr = ThreadId;
 _PrjDrawExec *mtag[mi].fileGr %3 %4 %5;  // execute
 if(mtag[mi].thNc == 0){mreset;}          // if no program is running: reset positions
";

/* ================================================================== */
/* run Nc Execution in new thread                                     */
/* ------------------------------------------------------------------ */
/* %1: Project name                                                   */
/* %2: AGI for execution                                              */
/* %3, %4, %5: passed to _PrjDrawExec                                 */
/* ================================================================== */
(MacroDef "ThrdCall") = 
"@if(mtag[%2].thNc) {exit \"Program is already running\";}
 mtag[%2].fileNc = &(%1);     // publish project name
 mtag[%2].locked = 1;
 mtag[%2].thNc = ThreadCall
  (sprint \"local \\\"mi\\\"; mi = %d;     // index for mtag
    local \\\"CallOnExit\\\";
    CallOnExit  = \\\"�while(mtag[mi].locked){__test;} (*mtag[mi].agi).reset;
                      mtag[mi].thNc = 0; mtag[mi].thAgi = nullptr;\\\";
    // attach thread to a copy of the AGI and to its FAG
    Thread_SetDrv drv;
    Thread_NewObjList;        // thread local variable list
    mtag[mi].thAgi = &(gcnew CMtAgi 0 *mtag[mi].agi);
    Thread_setAgi (*mtag[mi].thAgi) (*mtag[mi].agi).fag;
    Thread_agi.outCtrl.drv = 1;
    //Thread_agi.g3InitCopy (*mtag[mi].agi).hg3;
    Thread_agi.vci.Flag01 = '1';

    LocalThr \\\"mtagI\\\"; mtagI = mi;
    LocalThr \\\"_gFuncTable\\\"
             \\\"_gFuncGroup\\\"
             \\\"_gFuncFHandler\\\";

    fcall MtApp.path & \\\"/AgiInit.ts\\\"; /* initialize local variables */
    fcall MtApp.path & \\\"/AgiCopy.ts\\\"; /* copy thread locs from main */

    mreset;

    _PrjDrawExec *mtag[mi].fileNc %d %d %d;      // execute
    wait 0;
  \" %2 %3 %4 %5)
  1000L;
 mtag[%2].locked = 0;
";

/* ================================================================== */
/* Draw/Execute one project:  %1=project name with path;              */
/*                            %2=0: draw; 1/2: execute; 3:silent      */
/*                            %3: draw visible/invisible              */
/*                            %4: draw with PtaI-Counting             */
/* ================================================================== */
(macrodef "_PrjDrawExec") =
"local \"ToolFile\";
 ToolFile = GetIniString %1 \"Project\" \"ToolFile\" \"\";

 if(0 == Toolfile == \"\")
 {Mreset sprint \"Laden der Werkzeuge nicht fertig... \r\n\r\n%s\" ToolFile;}

 local \"PTool\";
 PTool = GetIniString %1 \"Project\" \"Tool\" \"0\";

 local \"PcallStack\"; PcallStack = 0;    // Variable to stop unwanted cascading
 
 local \"CallOnExit\";
 CallOnExit =
 \"@rmpMode 'Y'; _mode = '0';
   Thread_agi.OutCtrl.ncMode = 3;
   Thread_agi.OutCtrl.grMode = 0;
   Thread_agi.OutCtrl.drv = 1;
 \";

 if(%2 == 0)                              // Graphic call
 {
   local \"CallOnExit\";

   Thread_agi.OutCtrl.ncMode = 2;
   Thread_agi.OutCtrl.grMode = %3;        // switch to graphic mode
   Thread_agi.OutCtrl.grModePrv = 0;
   Thread_agi.OutCtrl.drv = 0;            // avoid any access to driver
   _mode = 'G';
   Thread_agi.vci.iCurr = 0;

   _ptr.Global.s = _ptr.WithTool.s = 0.0; // Reset accumulated path time values
   _ptr.Global.t = _ptr.WithTool.t = 0.0; 
   _ptr.ptaILevel = %4; _drvPtReset;      // jedoch nicht bei VCi!
   rmpMode 'A';   
   
   _octMinMax_stat  = 1;                  // calculate object dimensions
   _octMinMax_sflag = 3;        

   mtagC.ToolsUsedByFile = 0;       // reset before drawing
   pCall %1 _pParm;                       // create graphic

   Mtag[MtAgi].ObjMin = _OctMin;          // save thread dependent object      				
   Mtag[MtAgi].ObjMax = _OctMax;          // dimensions to global variables

   _ptr.ptaILevel = 0; _drvPtReset;       // Reset accumulated path time values

   (*InfoTab.lbl[0]) = \"Weg:\";
   (*InfoTab.lbl[1]) = \"Zeit:\";
   InfoTab.lblDty = 3;

   (*InfoTab.data[0]) = sprint \"%6.1lf\" _ptr.Global.s;
   (*InfoTab.data[1]) = sprint \"%6.1lf\" _ptr.Global.t;
   InfoTab.dataDty = 3;
   
   if(0) //(_IdeFlag)
   {
     print \"ExecMacros.ts: Weg/Zeit nach Grafik-Aufruf:\";
     print sprint \"Weg:  %6.1lf\" _ptr.Global.s;
     print sprint \"Zeit: %6.1lf\" _ptr.Global.t;
     print sprint \"Withtool-s:  %6.1lf\" _ptr.WithTool.s;
     print sprint \"Withtool-t:  %6.1lf\" _ptr.WithTool.t;
   }

   GetTool _ToolNcEmu;                    // reset tool
 }

 if(%2 > 0)                               // execution with/without switches
 {
   IF(_RefTest == 0) {Mreset \"Erst Achsen referenzieren!\";}

   local \"CallOnExit\" \"CallOnError\";
   CallOnExit = CallOnError = \"if(UsePathTime) {ToolAccu; UsePathTime = 0;}\";

   if(_ExecToPark != 0) {CallOnExit  = CallOnExit  & \" MCall  *_MGoTo.Park;\";}
   if(_ExecToPark == 2) {CallOnError = CallOnError & \" MCall  *_MGoTo.Park;\";}

   if((Thread_agi.fag.jog.Flag01S == '1') AND (Thread_agi.fag.jog.MoveOT == 'O'))
   {
     Thread_agi.fag.jog.Flag01S = 'S'; Thread_agi.fag.jogWrite; mreset;

     local \"Txt\"; Txt = \" Thread_agi.fag.jog.Flag01S = '1'; Thread_agi.fag.jogWrite;\";
     CallOnExit  = CallOnExit  & Txt;
     CallOnError = CallOnError & Txt;
   }

   if(%2 < 3)
   {
     Thread_agi.OutCtrl.ncMode = 2;       // execution mode
     Thread_agi.OutCtrl.grMode = 0;
     _mode = 'X';
   }
   else
   {
     Thread_agi.OutCtrl.ncMode = 0;       // silent mode
     Thread_agi.OutCtrl.grMode = 0;
     _mode = 'G';
   }
   Thread_agi.vci.iCurr = 0;

   _ptr.Global.s = _ptr.WithTool.s = 0.0; // Reset accumulated path time values
   _ptr.Global.t = _ptr.WithTool.t = 0.0; 
   _ptr.ptaILevel = 1; _drvPtReset;
   //UsePathTime = 1;

   _octMinMax_stat  = 0;                  // no calculation of object dimensions

   if(%2 == 1) {_PrgStart; _DspUpdate;}   // set switches

   pCall %1 _pParm;                       // Start execution
 
   if((%2 == 1) or (%2 == 3)) {_PrgEnd; _DspUpdate;} // reset switches

   if(UsePathTime) {ToolAccu; UsePathTime = 0;}
   _ptr.ptaILevel = 0; _drvPtReset;       // Reset accumulated path time values
 }
";

/* ================================================================== */
/* Draw/Execute project; %1=project name with path                    */ 
/* ================================================================== */
// %1: project file
// %2: _pParm from caller
(macrodef "pCall") =
"local \"CallOnExit\"; CallOnExit = \"@PcallStack = PcallStack - 1;\";

 PcallStack = PcallStack + 1;

 if(PcallStack < 4)
 {
   local \"_PParm\" \"_FParm\" \"fName\" \"nFiles\" \"i\" \"PrjPath\";
   
   _PParm = ObjDef CPParm;
   _FParm = ObjDef CFParm;

   LoadPParm %1 %2;

   i = 0;
   nFiles  = scall GetIniString %1 \"Project\" \"Files\"   \"0\";
   PrjPath =       GetIniString %1 \"Project\" \"PrjPath\" \"\";
   for(nFiles)
   {
     LoadFParm %1 i = i + 1;      
     if(_fParm.Enabled > 0)
     {
       fName = PrjPath & \"/\" & *_FParm.Name;
       _PrgDrawExecRepeat _FParm.Type fName;
     }
   }

   if(nFiles == 0)
   {
     LoadFParm %1 0;
     _FParm.Type = scall GetIniString %1 \"Project\" \"TempType\" \"0\";
     fName =             GetIniString %1 \"Project\" \"TempFile\" \"\";
     if(fName == \"\"){exit GetIniTxt \"System\" \"ExecMissingFile\";}
     _PrgDrawExecRepeat _FParm.Type fName;
   }
 }
";

/* ================================================================== */
// _PrgPrePost: customer specific stuff before / after calling programs
/* ------------------------------------------------------------------ */
// %1 = file type (1, 2, 3, 4, 10, >100)
// %2 = 0:before, 1:after
/* ================================================================== */
(MacroDef "_PrgPrePost") = "";

/* ================================================================== */
/* _PrgDrawExecRepeat: Draw/execute project line with Trans and Clone */
/* ------------------------------------------------------------------ */
/* %1 = file type (1, 2, 3, 4, 10, >100)                              */
/* %2 = file name and path                                            */
/* ================================================================== */
(MacroDef "_PrgDrawExecRepeat") =
"local \"_iRepX\" \"_iRepY\" \"_AnzRep\" \"_AnzPush\";

 if(_PParm.RepN[0] == 0)  {_PParm.RepN[0] = 1;}    /* Anzahl in x     */
 if(_PParm.RepN[1] == 0)  {_PParm.RepN[1] = 1;}    /* Anzahl in y     */
 _AnzRep = _PParm.RepN[0] * _PParm.RepN[1];        /* Anzahl gesamt   */
 if(0 == _PParm.Flag and 8) {_AnzRep = 1;}         /* no clones       */

 local \"LocalTool\"; LocalTool = _ToolNcEmu;

 if(_fParm.Tool > 0)                  /* load tool from file          */
  {LocalTool = _fParm.Tool;}

 if(_fParm.Tool == 0)                 /* no tool in file              */
  {if(_pParm.Tool > 0)                /* load tool from project       */
    {LocalTool = _pParm.Tool;}
  }

 if(_ToolMode == 2) {LocalTool = Tools[LocalTool].Ident;}
 Gettool LocalTool;                   /* set activation/deactivation  */ 

 local \"CallOnExit\" \"CallOnError\";        /* variables to recover */
 local \"HpglMerk\"; HpglMerk = _HpglPar;     /* Hpgl vals. to recover*/
 local \"CrcMerk\";  CrcMerk  = _CrcPar;      /* Crc  vals. to recover*/
 local \"CallPath\"; CallPath = FileGetFolder %2;

 _AnzPush = 0;
 CallOnError = CallOnExit =
             \"if(Thread_agi.OutCtrl.drv){mStop '1'; Thread_agi.Reset;}\"
             &         \" _HpglPar = HpglMerk;\"        /* old Hpgl   */ 
             &         \" _CrcPar  = CrcMerk; \"        /* old CRC    */
             &         \" _MTransOff _AnzPush;\"        /* MTrans off */
             & (sprint \"  Zup             = %.3lf;\"  Zup)
             & (sprint \"  Zdn             = %.3lf;\"  Zdn)  
             & (sprint \"  Zdn0            = %.3lf;\"  Zdn0)
             & (sprint \"  Zdn0flag        = %d;   \"  Zdn0flag)
             & (sprint \" _mtSTat.CrcState = %d;   \" _mtStat.CrcState)
             & (sprint \"  rmpMode '%c';           \"  rmpMode '?')
             & (sprint \" _SetOvr   %.0lf; _DspUpdate;\" mtag[mtagI].ovr[0] * 100.0)
             & (Sprint \"  MF_F = %lf; ML_F = %lf; MvPar;\" MF_F ML_F)
             & \"mtag[mtagI].coTrans = &(\\\"O\\\");\" // reset coord. display
             & \"_iWear = 0;\"                       // reset wear offset
             & (Sprint \"  SetMc2Oc %d;            \" _iMc2Oc)
 ;

 rmpMode 'A';                                /* activate look ahead   */

 local \"ZAnf\" \"ZEnd\" \"ZCur\" \"dz\";    /* Schicht-Daten laden   */

 ZAnf = _fParm.Zdn0;        
 ZEnd = _fParm.ZdnMill[0];
 dz   = _fParm.ZdnMill[1];                   /* Speziell Schichten    */
 if(dz <= 0.0) {dz = 0.1;}

 if(_mtStat.CrcState == 2) {CrcClose; _mtStat.CrcState = 1;}

 FParm2NonFParm;

 if(0)//_IdeFlag)
 {
   print (sprint \"_Hpglpar.CrcAuto=%d\\\r\\\n\"   _Hpglpar.CrcAuto   + 0L) 
       & (sprint \"_Crcpar.Ori =%d\\\r\\\n\"       _Crcpar.Ori        + 0L)
       & (sprint \"_Crcpar.m_isClosed=%d\\\r\\\n\" _Crcpar.m_isClosed + 0L) 
       & (sprint \"_Crcpar.BufSize=%d\\\r\\\n\"    _Crcpar.BufSize    + 0L) 
       & (sprint \"_Crcpar.nCmdOpen=%d\"           _Crcpar.nCmdOpen   + 0L); 
 }
 _iRepX   = _iRepY = 0;                      /* Startwerte            */
 _Anzpush = _MTransOn;                       // MTrans ggf. ein

 if(%1 >= 100)                                     /* SVP-Bearbeitung */
 {
   if(T_Undefined == (TypeOf \"VhfDrill\").type)   /* VhfDrill defin. */
    {fCall MtApp.path & \"/ProBoard/VhfDrill.ts\";}

   Tools[0].Rad = ToolRad;          /* Radius im Werkzeug 0 speichern */
   rCurr = -1.0;                    /* Solldurchmesser initialisieren */
 }
 
 for _AnzRep                                       /* Clone-Schleife  */
 { 
   if(_AnzRep > 1)
   {
     Mtrans \"PUSH\"; _AnzPush = _AnzPush + 1;     /* Clone setzen    */
     Mtrans \"OFS\" (_iRepx * _PParm.RepD[0]) (_iRepY * _PParm.RepD[1]);
   }

   ZCur = ZEnd + dz - 0.0001;
   if(_fParm.ZdnMillFlag) {ZCur = ZAnf - 0.0001;} 	  

   while \"ZCur > ZEnd\"                           /* Schichten:      */
   {
     local \"CallOnExit\";                         /* unwind stack    */
     CallOnExit = \"@While (\\\"_AnzPush < MTrans \\\\\\\"Level\\\\\\\"\\\")
                        {MTrans \\\"Pop\\\";}
                  \";

     ZCur = ZCur - dz; 
     if(ZCur < ZEnd) {ZCur = ZEnd;}
     Zdn = ZCur;

     if(_fParm.ZdnMillFlag)
     {local \"i\"; i = 0; for 9 {_Hpglpar.Zdn[i] = zCur; i = i + 1;}}
     else          {_Hpglpar.Zdn[0] = zdn;} // inherit zdn for PEN 0 from global zdn

     if((_mode == 'X') and mtag[mtagI].Step)     /* ggf. Step mode ein*/
      {Mstop 'E';}

     _PrgPrePost %1 0;        // customer specific stuff before calling
     if(%1 ==   1) {fCall    %2;}                /* Datei ausf�hren   */
     if(%1 ==   2) {dinCallX %2;}
     if(%1 ==   3) {hpglCall %2;}
     if(%1 ==   4) {iselCall %2;}
     if(%1 ==  10) {pCall    %2 _pParm;}
     if(%1 >= 100) {vhfcall  %2 (%1 - 100) Zup Zdn;}
     _PrgPrePost %1 1;        // customer specific stuff after calling
   }

/* mreset-sequenz falls state = 2 */

   if(_AnzRep > 1)                                 /* clone aus       */
   {
     _MTransOff 1;
     _AnzPush = _AnzPush - 1;
     if(1 AND _NcMode) {Wait 0; mreset;}           /* workaround      */
     Z Zup; mf;                                    /* auftauchen      */
   }        

   _iRepx = _iRepx + 1;                            /* Schleifenz�hler */
   if(_iRepX == _PParm.RepN[0])
    {_iRepX = 0; _iRepy = _iRepy + 1;}

   if(1 AND _NcMode) {Wait 0;}                     /* Bearb. abwarten */
 }
 
 _MTransOff _AnzPush; _AnzPush = 0;                /* MTrans aus      */
";
