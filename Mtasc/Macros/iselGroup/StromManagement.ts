////////////////////////////////////////////////////////////////////////////////////////////////
//
// -- xml Element anlegen falls nicht vorhanden und vorbelegen ------
//
////////////////////////////////////////////////////////////////////////////////////////////////
// Diverse Zeiten f�r die Hardware
CTimes = ObjDef T_classDscr
"CurrentToMiddle" T_long  0	// Zeit in 1 sec, nach dieser Zeit wird der Strom von HIGH auf MIDDLE abgesenkt
"CurrentToLow" 	  T_long  0	// Zeit in 1 sec, nach dieser Zeit wird der Strom von MIDDLE auf LOW abgesenkt
;  

Times = gcnew CTimes;
  
if(0 == MtApp.xml.Hardware._elExists "Times")
{
 Times.CurrentToMiddle = 20;	
	Times.CurrentToLow    = 20;			
	MtApp.xml.Hardware.!E!Times._Mt2Xml Times;
}

MtApp.xml.Hardware.!E!Times._Xml2Mt Times; // -- Werte aus der xml lesen --

/////////////////////////////////////////////////////////////////////////////
// Motorstrom-Einstellungen
CMotAmp = ObjDef T_classDscr
"Step"	   	T_long    0
"Strom"    T_double  0
"StromM"	  T_long    0
"StromM_P"	T_double  3
"intPol"   T_long    0
"DEDGE"    T_long    0
"Wid"      T_long    0
"Ind"      T_long    0
;

Anzahl_Achsen = 8 ;			// maximale Anzahl anlegen
MotAmp = gcnew CMotAmp Anzahl_Achsen;

if(0 == MtApp.xml.Hardware._elExists "MotAmp")
{
	local "i";
	i = 0; 
	for Anzahl_Achsen
	{
		MotAmp[i].Strom    		= 1.8;		// Strom in Ampere
		MotAmp[i].StromM   		= 1;		// Strom-Management: 0 = aus; 1 = an
		MotAmp[i].StromM_P[0]	= 33.0;		// Stromwert[%]: Low
		MotAmp[i].StromM_P[1]	= 66.0;		// Stromwert[%]: Middle
		MotAmp[i].StromM_P[2]	= 100.0;	// Stromwert[%]: High
		MotAmp[i].Step     		= 16 ;    	// 1/16 Schritt
		MotAmp[i].intPol   		=  1 ; 		// Interpolation ein
		MotAmp[i].DEDGE    		=  0 ; 		// eine Flanke aktiv
		MotAmp[i].Wid      		=  1 ; 		// Widerstand des Motors in Ohm
		MotAmp[i].Ind      		=  1 ; 		// Induktivit�t des Motors in mH
		i = i + 1;
	}
	MtApp.xml.Hardware.!E!MotAmp._Mt2Xml MotAmp;
}

MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --

////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definition diverser Makros f�r das StromManagement
//
////////////////////////////////////////////////////////////////////////////////////////////////
// Tabelle der Bewegungszust�nde f�r jede Achse i:
// trc.speedStateReg[i]:	0: Stromverwaltung deaktiviert
//   						1: Strom low 			        - ist
//   						2: Strom high 			       - angefordert
//   						3: Strom high 			       - ist
//   						4: Auf Strom low warten - angefordert
//   						5: Auf Strom low warten	- ist	
//							  6: Strom Middle			      - angefordert	
//							  7: Strom Middle 		      - ist	
//////////////////////////////////////////////////////////////////////////////////////////
// Diverse Variablen
//////////////////////////////////////////////////////////////////////////////////////////
// ThrCnt => 1 sec = 20*ThrCnt
ArrayLength        = 3 * trc.nAx;					      // L�nge eines Arrays
StromProzentTab    = array ArrayLength 0.0;	// Stromwerte[%] f�r jede Achse
SetCurrentToMiddle = array trc.nAx 0;       // Zeitpunkt, ab dem der Stromwert aller Achsen auf MIDDLE geschaltet wird
SetCurrentToLow    = 0;						               // Zeitpunkt, ab dem der Stromwert aller Achsen auf LOW geschaltet wird
MiddleRq           = array trc.nAx 0;
sLowRq             = 0;
sMiddle            = 0;
StromLevel         = array trc.nAx 0;
iC                 = 0;

// Zeiten vom StromManagement �berpr�fen
if ((Times.CurrentToMiddle < 0) or (Times.CurrentToLow < 0))	
{
  // MsgBox "ERROR: Times.CurrentToMiddle"; 
  SendError "ERR_EtInit_InvalidTimeValue";
  exit;
}

//MsgBox sprint "%.2lf" MotAmp[0].StromM_P[0];

local "i" "k"; i = 0;
for trc.nAx
{	
	// StromManagement-Einstellungen �berpr�fen
	if (((MotAmp[i].StromM != 0) and (MotAmp[i].StromM != 1)) or
     ((MotAmp[i].StromM_P[0] < 0) or (MotAmp[i].StromM_P[0] > 100)) or
     ((MotAmp[i].StromM_P[1] < 0) or (MotAmp[i].StromM_P[1] > 100)) or
     ((MotAmp[i].StromM_P[2] < 0) or (MotAmp[i].StromM_P[2] > 100))) 
 {
   // MsgBox "ERROR: MotAmp[i].StromM";	
   SendError "ERR_EtInit_InvalidCurrentMValue";
   exit;
 }

 if (0 == ((MotAmp[i].StromM_P[0] < MotAmp[i].StromM_P[1]) and (MotAmp[i].StromM_P[1] < MotAmp[i].StromM_P[2])))
 {
  // MsgBox "ERROR: MotAmp[i].StromM";	
  SendError "ERR_EtInit_InvalidCurrentMValue";
  exit;
 }
	
	// Werte aus der XML �bernehmen
	trc.speedStateReg[i] = 1;
	
 k = i * 3;
 
 if(MotAmp[i].StromM != 0)
 {
  StromProzentTab[k] 		  = MotAmp[i].StromM_P[0];
  StromProzentTab[k + 1] = MotAmp[i].StromM_P[1];
  StromProzentTab[k + 2] = MotAmp[i].StromM_P[2];
	}
 else
 {
  StromProzentTab[k] 		  = 100.0;
  StromProzentTab[k + 1] = 100.0;
  StromProzentTab[k + 2] = 100.0;
 }

	i = i + 1;
}

/////////////////////////////////////////////
// MacroDef "speedStateSet"
// %1: Achs-Index
// %2: 1=low, 2=middle, 3=high
(MacroDef "speedStateSet" '@') =
"
  if((%2 < 1) or (%2 > 3))
  {
    // MsgBox \"Interner Fehler speedStateSet\";
    SendError \"ERR_EtInit_InvalidSpeedState\";
    exit;
  }
  
  local \"vs\" \"k\"; 
  k = 3 * %1;
  k = k + %2 - 1;
  
  vs = StromProzentTab[k];
  SelectOneAxisPower vs %1;
  StromLevel[%1] = %2;
  LogWrite2 sprint \"trc.speedFlag = %d;  Strom Axis %d = %d;  StromLevel = %.1lf\" trc.speedFlag %1 %2 vs;
  
  out_WD_trigger; 
";

trc.speedStateCmd = &(
"
  iC = 0;
  for trc.nAx
  {
    // Motorstrom sofort erh�ht
    if (trc.SpeedStateReg[iC] == 2)
    {
      speedStateSet iC 3;
      trc.speedStateReg[iC] = 3;
    }
    
    iC = iC + 1;
  }
  
  iC = 0;
  for trc.nAx
  {
    if (trc.SpeedStateReg[iC] == 6)
    {
      if(StromLevel[iC] == 1)
      {
        speedStateSet iC 2;
        trc.speedStateReg[iC] = 7;
      }
      else // == 3
      {
       if (MiddleRq[iC] == 0)
       {
        MiddleRq[iC] = 1;
        SetCurrentToMiddle[iC] = ThrCnt + Times.CurrentToMiddle;
       }
      }
    }
    
    iC = iC + 1;
  }
");


////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definition von Makros, um diverse Standardwerte den Strom einzustellen
//
////////////////////////////////////////////////////////////////////////////////////////////////
if (CtType.IMC481)
{
  (MacroDef "SelectOneAxisPower") =
  "
    local \"Mot_Strom\";
    Mot_Strom = 0;
    
    if (MotAmp[%2].strom > 6.0) {MotAmp[%2].strom = 6.0;}
    Mot_Strom = ((MotAmp[%2].strom * 5) * %1 / 100.0) + 0.5;
    Set_Current %2 Mot_Strom;   
  "; 
}

else
{
  (MacroDef "SelectOneAxisPower") =
  "
    local \"Mot_Strom\";
    Mot_Strom = 0;
    
    Mot_Strom = (MotAmp[%2].strom  * %1 / 10); // Angabe effektiv Strom in 1/10 A
    Set_Current %2 Mot_Strom;   
  "; 
}

// Alle Achsen vollen Strom
(MacroDef "MaxPower") =
"
  local \"vs\" \"i\" \"k\";
  vs = 0;
  i = 0;

  for trc.nAx
  {
    k = 3 * i;
    k = k + 3 - 1;
    vs =	(StromProzentTab[k]);
    SelectOneAxisPower vs i;
    i = i + 1;
  }
  
  out_WD_trigger;
";

// Alle Achsen Strom absenken
(MacroDef "MinPower") =
"
  local \"vs\" \"i\" \"k\";
  vs = 0;
  i = 0;

  for trc.nAx
  {
    k = 3 * i;
    k = k + 1 - 1;
    vs =	(StromProzentTab[k]);
    SelectOneAxisPower vs i;
    i = i + 1;
  }
  
  out_WD_trigger;
";

// Alle Achsen auf 10% Strom absenken
(MacroDef "LowPower") =
"
  local \"vs\" \"k\";
  vs = 10;
  k = 0;

  for trc.nAx
  {
    SelectOneAxisPower vs k;
    k = k + 1;
  }
  
  out_WD_trigger;
";

// Die Stromwerte aller Achsen langsam ansteigen lassen
(MacroDef "SmoothPowerRaise") =
"
  local \"vs\" \"i\" \"k\";
  vs = 0;
  i = 0;
  k = 0;

  for 10
  {
    k = 0;
    i = i + 1;
    vs = i * 10;
    
    for trc.nAx
    {
      SelectOneAxisPower vs k;
      k = k + 1;
    }
    
    out_WD_trigger;
  }
";