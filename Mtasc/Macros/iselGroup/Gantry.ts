////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definition diverser Makros f�r das Koppeln bzw. Entkoppeln einzelner Achsen
//
////////////////////////////////////////////////////////////////////////////////////////////////

(MacroDef "Link2Axis") =
"
  local \"mpos\" \"Master\" \"Slave\";
  Master = %1;
  Slave  = %2;
  mpos = getmc;
  mpos[Master] = mpos[Slave] = 0.0;
  _ocNext = mpos;
  setmc;
	 DrvAxLink array 2 Master Slave;           
  _dspUpdate;
";

(MacroDef "Link3Axis") =
"
  local \"mpos\" \"Master\" \"Slave1\" \"Slave2\";
  Master = %1; 
  Slave1 = %2; Slave2 = %3;
  mpos = getmc;
  mpos[Master] = mpos[Slave1] = mpos[Slave2] = 0.0;
  _ocNext = mpos;
  setmc;
	 DrvAxLink array 4 Master Slave1 Master Slave2;           
  _dspUpdate;
";

(MacroDef "Link2x2Axis") =
"
  local \"mpos\" \"Master1\" \"Slave1\" \"Master2\" \"Slave2\";
  Master1 = %1; Slave1  = %2;
  Master2 = %3; Slave2  = %4;
  mpos = getmc;
  mpos[Master1] = mpos[Slave1] = 0.0;
  mpos[Master2] = mpos[Slave2] = 0.0;
  _ocNext = mpos;
  setmc;
	 DrvAxLink array 4 Master1 Slave1 Master2 Slave2;           
  _dspUpdate;
";

(MacroDef "Link3x2Axis") =
"
  local \"mpos\" \"Master1\" \"Slave1\" \"Master2\" \"Slave2\" \"Master3\" \"Slave3\";
  Master1 = %1; Slave1  = %2;
  Master2 = %3; Slave2  = %4;
  Master3 = %5; Slave3  = %6;
  mpos = getmc;
  mpos[Master1] = mpos[Slave1] = 0.0;
  mpos[Master2] = mpos[Slave2] = 0.0;
  mpos[Master3] = mpos[Slave3] = 0.0;
  _ocNext = mpos;
  setmc;
	 DrvAxLink array 6 Master1 Slave1 Master2 Slave2 Master3 Slave3;           
  _dspUpdate;
";

(MacroDef "Link2x3Axis") =
"
  local \"mpos\" \"Master1\" \"Slave11\" \"Slave12\" \"Master2\" \"Slave21\" \"Slave22\";
  Master1 = %1; Slave11  = %2; Slave12  = %3;
  Master2 = %4; Slave21  = %5; Slave22  = %6;
  mpos = getmc;
  mpos[Master1] = mpos[Slave11] = mpos[Slave12] = 0.0;
  mpos[Master2] = mpos[Slave21] = mpos[Slave22] = 0.0;
  _ocNext = mpos;
  setmc;
	 DrvAxLink array 8 Master1 Slave11 Master1 Slave12 Master2 Slave21 Master2 Slave22;           
  _dspUpdate;
";

(MacroDef "DeLinkAxis") =
"	
  DrvAxLink array 3 -1 -1 -1;           
  mreset;
  _dspUpdate;
";


//////////////////////////////////////////////////////
// noch alte Version der Kopplung der X und A Achse
(MacroDef "AxLink") =
"
  if(%1 == 0)
  {
    DrvAxLink array 4 -1 -1 -1 -1;           // X, A entkoppeln
    mreset;
    _oStat[4] = 1; _dspUpdate;
  }
  else
  {
    local \"mpos\";
    mpos = getmc;
    mpos[0] = mpos[3] = 0.0;
    _ocNext = mpos;
    setmc;
    DrvAxLink array 4 0 3 -1 -1;           // X, A koppeln
    _oStat[4] = 0; _dspUpdate;
  }
";