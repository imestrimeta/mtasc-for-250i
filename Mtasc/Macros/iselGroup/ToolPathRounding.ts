////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definition diverser Makros f�r das �berschleifen eines Punktes zwischen zwei Vektoren
//
////////////////////////////////////////////////////////////////////////////////////////////////
// Anlegen der Parameter in der XML-Datei
CTpr = ObjDef T_classDscr
"Mode" 	T_long  	0
"angMin"	T_double  	0
"angMax"	T_double  	0
"Radius"	T_double  	0
;

Tpr = gcnew CTpr;

if(0 == MtApp.xml.MtDrv.fag.v0._elExists "Tpr")
{
  Tpr.Mode = 0;
  Tpr.angMin = 10.0;
  Tpr.angMax = 180.0;
  Tpr.Radius = 1.0;	
		
  MtApp.xml.MtDrv.fag.v0.!E!Tpr._Mt2Xml Tpr;
}

MtApp.xml.MtDrv.fag.v0.!E!Tpr._Xml2Mt Tpr; // -- Werte aus der xml lesen --

////////////////////////////////////////////////////////////////////////////////////////////////
// Macrodefinitionen
(MacroDef "TPR_ON") = 
"
  local \"min\" \"max\" \"Radius\"; min = max = Radius = 0.0;
  min = %1;
  max = %2;
  Radius = %3;
  
  if ((min < 0) OR (max > 180) OR (Radius <= 0))
  {
    SendError \"ERR_TPR_InvalidParameter\";
    return 1;
  }

  mtIAg0.tpr.angMin = _d2r * min;
  mtIAg0.tpr.angMax = _d2r * max;
  mtIAg0.tpr.tprDist = Radius;
  mtIAg0.tpr.tprMode = 1;
  
  return 0;
";

(MacroDef "TPR_OFF") = "mtIAg0.tpr.tprMode = 0;";

(MacroDef "TPR_GetPara") =
"
  if (Tpr.Mode == 1)
  {
    TPR_ON Tpr.angMin Tpr.angMax Tpr.Radius;
  }
"

////////////////////////////////////////////////////////////////////////////////////////////////
// ToolPathRounding aktivieren
if (Tpr.Mode != 0) 
{
  TPR_ON Tpr.angMin Tpr.angMax Tpr.Radius;
}