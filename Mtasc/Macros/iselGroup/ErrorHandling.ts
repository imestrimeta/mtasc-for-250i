////////////////////////////////////////////////////////////////////////////////////////////////
//
// Definition diverser Makros f�r das ErrorHandling und weiter leiten einer Fehlernummer an Remote
//
////////////////////////////////////////////////////////////////////////////////////////////////

// ErrHandling
CErrCodes = ObjDef T_classDscr
"n"      T_long  0
"list"   T_object 0
"get"    T_function 0
;

ErrCodes = gcnew CErrCodes;
ErrCodes.n = 31;
ErrCodes.list = &(array ErrCodes.n
 &("NoError")                                  // 00 = Kein Error vorhanden
 &("ERR_EtInit_USBConnection")                 // 01 = Kein TRC2 USB-Ger�t gefunden
 &("ERR_EtInit_InvalidInteruptFrequenc")       // 02 = Ungueltige Interuptfrequenz f�r Taktgenerierung
 &("ERR_EtInit_mlPortTimeOutError")            // 03 = Timeout Error in mlPort
 &("ERR_EtInit_ErrRefInp")                     // 04 = Fehler bei Referenz-Input
 &("ERR_EtInit_NoIndexSearchSupport")          // 05 = Index-Suche wird nicht unterstuetzt
 &("ERR_EtInit_InvalidTimeValue")              // 06 = Ungueltiger Wert f�r Zeit-Variable
 &("ERR_EtInit_InvalidCurrentMValue")          // 07 = Ungueltiger Wert f�r StromMangement-Variable
 &("ERR_EtInit_InvalidSpeedState")             // 08 = Ungueltiger SpeedState
 &("ERR_EtInit_SpeedBufferOverflow")           // 09 = SpeedBufferOverflow
 &("ERR_MctlInit_InvalidMtVersion")            // 10 = Ungueltige MTasc.dll Version 
 &("ERR_Controller_InvalidNumberOfAxes")       // 11 = Ungueltige Anzahl an Endstufen fuer Achsen
 &("ERR_Controller_InvalidController")         // 12 = Ungueltige eingestellte Steuerung
 &("ERR_Controller_NoMainPower")               // 13 = Keine Hauptspannungsversorgung
 &("ERR_PLC_EmergencyStop")                    // 14 = Not-Aus oder Folgefehler
 &("ERR_System_ClosingTimeOutError")           // 15 = Timeout Error beim schlie�en
 &("ERR_System_Closing")                       // 16 = Interner Fehler beim schlie�en
 &("ERR_System_InternalSystemError")           // 17 = Interner System Fehler
 &("ERR_System_OneProgramIsRunning")           // 18 = Ein Programm l�uft bereits
 &("ERR_System_NoDinISOSupport")               // 19 = Din-Iso ist nicht implementiert
 &("ERR_System_NoCustomMacro")                 // 20 = Kein CustomMakro definiert
 &("ERR_ExecMacros_VciCurr")                   // 21 = IDE: VciCurr wurde unerwartet aufgerufen
 &("ERR_ExecUtils_MoveCommand_Origin")         // 22 = Fahrbefehl mit dem aktuellen Nullpunkt nicht erlaubt
 &("ERR_TPR_InvalidParameter")                 // 23 = Ungueltiger Parameter
 &("ERR_LoadSaveTools_MissingLine")            // 24 = Eingabe fehlt
 &("ERR_MctlIO_IllegalAnalogChannel")          // 25 = Falscher analoger Kanal
 &("ERR_MctlIO_MissingEntry")                  // 26 = Eingabe fehlt in XML
 &("ERR_PosLoad_MissingLine")                  // 27 = Eingabe fehlt
 &("ERR_RefMacros_General")                    // 28 = Fehler beim Referenzieren
 &("Code")                                     // 29 = 
 &("Code")                                     // 30 = 
);

(MacroDef "CErrCodes_get") =
"
  local \"i\"; i = 0;
  for %0.n
  {
    if(%1 == *(*(%0.list))[i]) {return i;}
    i = i + 1;
  }
  return -1;
";

(MacroDef "SendError") = 
"
  //MsgBox %1;
  Mds.errCode = ErrCodes.get %1;
";

//////////////////////////////////////////////
// !!Muss noch implementiert werden
/*if(0 == (MtApp.AppName == "EdiTasc"))
{
  
  
  // (MacroDef "_ErrHandler") = "MsgBox _sysMsg.errText \"MTASC message\" MB_OK;"; // MTasc.cpp - Zeile 1231
}*/
