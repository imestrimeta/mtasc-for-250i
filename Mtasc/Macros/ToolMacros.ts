/*====================================================================*/
/* Tool management variables and macros                               */
/*====================================================================*/
// Tool class definition see System.ts
/*--------------------------------------------------------------------*/

/* ================================================================== */
/* Data for  tool changing                                            */ 
/* ================================================================== */
CToolChange = ObjDef T_classDscr
"TongSwitch"   T_Short   0              /* with tongswitch message    */
"AllowT0"      T_Short   0              /* allow tool 0               */
"doBefore"     T_object  0              /* do before changing         */
"doAfter"      T_object  0              /* do after  changing         */
"doIfAborted"  T_object  0              // do if changing was aborted

"Prolog"       T_object  0              /* current point to ToolHome  */
"Epilog"       T_object  0              /* change  point to ToolHome  */
"LimitsOff"    T_object  0              /* suspend limit watching     */
"LimitsOn"     T_object  0              /* restore limit watching     */
"ManChange"    T_object  0              /* manual tool change         */
"DropFetch"    T_object  0              /* move to pickpos; drop/fetch*/
"ReadSwitch"   T_object  0              /* get tool length            */
"GotoPos"      T_object  0              /* move to position %1        */
"SaveToolNc"   T_object  0              /* save new tool to file etc. */
"SwitchState"  T_Short   0              /* Expected status of switch  */
;

_tlch = ObjDef CToolChange;             /* create CToolChange object  */

_tlch.TongSwitch  = 0;                  /* without tongswitch message */
_tlch.SwitchState = -1;                 /* unknown                    */
_tlch.AllowT0     =  1;                 // Tool 0 allowed (default)

_tlch.doBefore    = &("");
_tlch.doAfter     = &("");
_tlch.doIfAborted = &("");

/* ------------------------------------------------------------------ */
_tlch.Prolog = &(                       /* current point to tool home */
/* ------------------------------------------------------------------ */
"if(_iMc2Oc) {Wait 0; mReset \"Illegal call of Prolog in tool change\";}
 
 wait 0; _PrgStop; _dspUpdate;
 
 print GetIniTxt \"GetTool\" \"Prn000\";/* start toolchange           */
 
 mf_f = _ToolSpeedF;                    /* set toolchange speed       */
 ml_f = _ToolSpeedS; MvPar;             /* restore in calling program */
 if(_ToolAuto == 3)
   {MCall *_tlch.GoToPos McPos[3].Ofs;} /* move to tool home          */
 else
   {MCall *_tlch.GoToPos McPos[1].Ofs;} /* move to service position   */
");

/* ------------------------------------------------------------------ */
_tlch.ManChange = &(                    /* manual tool change         */
/* ------------------------------------------------------------------ */
"if(_tlch.TongSwitch > 0)
 {                                      /* show open message          */
   MsgBox (GetIniTxt \"GetTool\" \"Msg000\") 
          (GetIniTxt \"GetTool\" \"Msg002\");
 
   _ToolOpen;
 }
                                        /* ask for tool to replace    */
 local \"MsgText\";
 MsgText = sprint (GetIniTxt \"GetTool\" \"Msg001\") _toolSel;

 MsgBox (MsgText & \"\\n\\n\" & *Tools[_toolsel].Text)
        (GetIniTxt \"GetTool\" \"Msg002\");

 _ToolClose;                            /* close tong switch          */    
");

/* ------------------------------------------------------------------ */
_tlch.DropFetch = &(                    /* drop or fetch tool         */
/* ------------------------------------------------------------------ */
"
  if(_iMc2Oc)
   {Wait 0; mReset \"Unzulässiger Aufruf von DropFetch\";}
  if(Thread_agi.vdim < 3)
   {Wait 0; mReset \"Unzulässiger Aufruf von DropFetch\";}

  local \"CallOnExit\" \"CallOnError\";     /* variables to restore    */
  CallOnExit = sprint \"Wait 0; MtransFlag01 = '%c'; mreset;\" MtransFlag01;
  CallOnError = \"mstop '1'; mreset;\" & CallOnExit;

  Wait 0; MtransFlag01 = '0'; mreset;       /* disable MTrans          */

  if(_OcNext[2] <  _ToolZup1)               /* move up                 */
   {_OcNext[2] = _ToolZup1; MF;} 

  _OcNext[0] = PickPos[%1].Ofs[0];
  _OcNext[1] = PickPos[%1].Ofs[1]; MF;      /* approach XY             */

  if(%2 == 0) /* drop */
  {
    SetMc2Oc 0 0;
    print GetIniTxt \"GetTool\" \"Prn002\";

    _OcNext[2] = PickPos[%1].Ofs[2]; ML; wait 0;        /* move down   */
    _ToolOpen; wait 0.5;                                /* drop        */ 
  }

  if(%2 == 1) /* fetch */
  {
    print GetIniTxt \"GetTool\" \"Prn004\";
    wait 0; _ToolOpen;                                  /* open tong   */
  
    _OcNext[2] = PickPos[%1].Ofs[2]; ML; wait 0;        /* move down   */
    _ToolClose; wait 0.5;                               /* fetch       */
  }

  _OcNext[2] = _ToolZup1; MF; wait 0;                   /* move up     */
  if(%2 == 0) {_ToolClose;}
");

/* ------------------------------------------------------------------ */
_tlch.ReadSwitch = &(                   /* check tool length          */
/* ------------------------------------------------------------------ */
// %1 = 0: Hit not expected - error message if hit
// %1 = 1: Hit expected     - error message if not hit
// %1 = 2: check if tool inserted - return 1(yes) | 0(no)
/* ------------------------------------------------------------------ */
"if(_iMc2Oc)
  {Wait 0; mReset \"Unzulässiger Aufruf von ReadSwitch\";}
 if(Thread_agi.vdim < 3)
  {Wait 0; mReset \"Unzulässiger Aufruf von ReadSwitch\";}

 local \"CallOnExit\" \"CallOnError\";     /* variables to restore    */
 CallOnExit = (sprint \"MF_F = %.3lf; ML_F = %.3lf; MvPar;\" MF_F ML_F)
                          & \" Wait 0; MtransFlag01 = '1'; mreset;\";
 CallOnError = \"mstop '1'; mreset;\" & CallOnExit;

 MtransFlag01 = '0'; mreset;               /* disable MTrans          */

 _OcNext[0] = PickPos[0].Ofs[0];           /* approach X,Y            */
 _OcNext[1] = PickPos[0].Ofs[1]; MF;  
 wait 0;

 if(%1 == 0) {print GetIniTxt \"GetTool\" \"Prn003\";}
 if(%1 == 1) {print GetIniTxt \"GetTool\" \"PrnSearchZ\";}
 if(%1 == 2) {print GetIniTxt \"GetTool\" \"PrnChkTool\";}

 local \"ZSwitch\"; ZSwitch = PickPos[0].Ofs[2];
 local \"Ok\";      Ok      = _Tlch.SwitchState;

 _OcNext[2] = ZSwitch + _TPrbZUp; MF; Wait 0;     /* to top of range  */
 local \"Result\"; Result = GetOc[2];

 Mf_f = _ToolProbeF;                    /* set probing speed          */
 Ml_f = _ToolProbeS; MVPAR;     

 if(0 == _Test AND 1)                   /* move to switch and touch   */
 {
   if(Ok < 0) {Ok = 0 == in _InpDsc[_iTPrb];}

   if(Ok == 0 != in _InpDsc[_iTPrb])
    {mreset GetIniTxt \"GetTool\" \"ErrSwitch\";}

   _OcNext[2] = ZSwitch + _TPrbZDn; MF; // move down fast
   stoprequest _InpDsc[_iTPrb] Ok; wait 0.2 0;
   wait 0 3; mreset;  Result = GetOc[2];
 }
 else                                   /* simulation with message box*/
 {
   local \"SimOk\" \"Txt\"; wait 0;
   if(%1 == 0) {Txt = GetIniTxt \"GetTool\" \"MsgChange2\"; SimOk = 7;}
   if(%1 == 1) {Txt = GetIniTxt \"GetTool\" \"MsgChange3\"; SimOk = 6;}

   if(SimOk == MsgBox Txt \"\" MB_YESNO)   // simulation with _toolSel as 'offset'
    {Result = ZSwitch + _TPrbZDn + _toolSel + 5.0;}
 } 	

  local \"Found\"; Found = 0;

  if((Result > ZSwitch + _TPrbZDn + 0.1)  
  AND(Result < ZSwitch + _TPrbZUp - 1.0))
  {
    Found = 1;

    if((%1 == 1) AND (0 == _Test AND 1))
    {
      _OcNext[2] = _OcNext[2] + 5.0; MF;
      wait 0;
      stoprequest _InpDsc[_iTPrb] Ok;  // move down slowly and take probe
      _OcNext[2] = _OcNext[2] - 6.0; ML;
      wait 0 3; mreset; Result = GetOc[2];
    }
  }

  mf_f = _ToolSpeedF;                    /* reset to toolchange speed  */
  ml_f = _ToolSpeedS; MvPar; 

  _OcNext[2] = _ToolZup; MF;             /* move up                    */
  Wait 0; mreset;      

  if(%1 == 1)
  {
    if(Found == 1)
    { 
      Tools[_toolSel].ofs[2] = ZSwitch - Result;     // set tool offset
    }
  }
  return Found;
");

/* ------------------------------------------------------------------ */
_tlch.SaveToolNc = &(                   /* Save new tool to file etc. */
/* ------------------------------------------------------------------ */
"WriteIniString (_AGIPath & \"/EdiTascAGI.ini\")  \"ToolConfig\" \"CurrTool\"
                (sprint \"%d\" _toolNc + 0L);
 _dspupdate;                            /* update display             */
");

/* ------------------------------------------------------------------ */
_tlch.EpiLog = &(                       /* current point to tool home */
/* ------------------------------------------------------------------ */
"if(_tchToHome == 2)                    /* go to last position        */
 {
   print GetIniTxt \"GetTool\" \"PrnOldPos\";  /* print message       */
 }

 print GetIniTxt \"GetTool\" \"Prn001\";/* Tool change ready message  */ 
 _PrgCont;                              /* prepare to continue work   */ 
");

/* ------------------------------------------------------------------ */
_tlch.GoToPos = &(
// move to position %1, use current origin
// %1: destination array
/* ------------------------------------------------------------------ */
" local \"imo\" \"to\" \"CallOnExit\" \"CallOnError\";
  imo = _iMc2Oc;               // use this origin for destination
  to  = mtag[mtagI].tofsUsed;  // usage of tool offset
  CallOnExit  =
    (sprint \"Wait 0; MtransFlag01 = '%c'; mreset;\" MtransFlag01) &
            \" SetMc2Oc imo to;\";
  CallOnError = \"mstop '1'; mreset;\" & CallOnExit;

  Wait 0; MtransFlag01 = '0'; mreset;       /* disable MTrans          */

  if(Thread_agi.vdim > 2)
  {
    local \"v\"; v = gcnew CVct 0 Thread_agi.vdim;
    if(5.0 < VLen _oc - v._ = %1)
    {
      SetMc2Oc 0 0;
      _ocNext[2] = McPos[3].ofs[2]; MF;     // move up to safe Z level

      SetMc2Oc imo to;
      _ocNext[0] = %1[0]; 
      _ocNext[1] = %1[1]; MF;                 // move XY
      _ocNext    = %1;

      _ocNext[2] = _oc[2]; MF;              // move all axes except Z
      _ocNext[2] = %1[2];
    }
  }
  else
  {
    _ocNext    = %1;
  }
  MF; Wait 0;                             // move all+Z
");

/* ------------------------------------------------------------------ */
_tlch.LimitsOff = &(                    /* suspend limit watching     */
/* ------------------------------------------------------------------ */
"Local \"OldLim\"; OldLim = Mlimit '?'; /* get current state          */
 Mlimit '0'; MlimitIsSuspended = 1;     /* Limit watching off         */
// _Plc.DirtyFlags.Trans = 1;             /* redraw limits              */
 Oldlim                                 /* return state, no ';'!      */
");

/* ------------------------------------------------------------------ */
_tlch.LimitsOn = &(                     /* restore limit watching     */
/* ------------------------------------------------------------------ */
" wait 0; mreset;                       /* make position valid        */
  Mlimit %1;                            /* restore limit flag         */
  MlimitIsSuspended  = 0;
//  _Plc.DirtyFlags.Trans = 1;            /* redraw limits              */
");

/* ================================================================== */
/* ToolMacros:  _ToolOpen, _ToolClose, ToolRad, LoadTools, GetTool    */ 
/* ================================================================== */
(Macrodef "_ToolOpen")  = "";
(Macrodef "_ToolClose") = "";
(Macrodef "ToolRad")    = "Tools[_ToolSel].Rad;";
(Macrodef "ToolRadEmu") = "Tools[_ToolSelEmu].Rad;";

/* ================================================================== */
/* LoadTools: Load tools from file                                    */
/* ================================================================== */
// Parameters:
// %1 = tool file
// %2(optional) = 1: enforce loading
/* ------------------------------------------------------------------ */
(Macrodef "LoadTools") = 
"
  if(%P == 1) {fcallP (MtApp.path & \"/Macros/LoadSaveTools.ts\") 'L' %1;}
  else        {fcallP (MtApp.path & \"/Macros/LoadSaveTools.ts\") 'L' %1 %2;}
";

/* ================================================================== */
/* SaveTools: Save tools to file                                      */
/* ================================================================== */
// Parameter: %1 = tool file; [%2: tool (ignored)]
/* ------------------------------------------------------------------ */
(Macrodef "SaveTools") = 
"
  fcallP (MtApp.path & \"/Macros/LoadSaveTools.ts\") 'S' %1;
";

/* ================================================================== */
/* CheckTools: check tool file                                        */
/* ================================================================== */
(MacroDef "CheckTools") =
"if(0 == (_ToolFile == %1))
 {if(0 == (_ToolFileAsked == %1))
   {MsgBox ((GetIniTxt \"Dxf2Et\" \"ToolFileInfo0\") 
           & \"\n\n\" & %1 & \".\n\n\" 
           & (GetIniTxt \"Dxf2Et\" \"ToolFileInfo1\"))
           (\"Dxf2Et\");
     _ToolFileAsked = %1;
   }
 }
";

/* ================================================================== */
/* ToolAccu: accumulate path/time values                              */
/* ================================================================== */
(MacroDef "ToolAccu") =
"
  if(_ncMode AND 1)
  {
    Tools[_ToolNc].SPath = Tools[_ToolNc].SPath + _ptr.WithTool.s;
    Tools[_ToolNc].STime = Tools[_ToolNc].STime + _ptr.WithTool.t;
    SaveTools (_ToolFile) _ToolNc;
  }
  _ptr.WithTool.s = _ptr.WithTool.t = 0;
";

UsePathTime = 0;
/* ================================================================== */
/* GetTool: Get real tool from emulated tool and                      */
/*          Change real tool by calling file GetTool.ts               */
/* ================================================================== */
/* Parameter: %1 = new tool number -> _ToolSelEmu                     */ 
/*  optional: %2 = override active flag                               */ 
/*  optional: %3 = enforce tool change                                */ 
/* ------------------------------------------------------------------ */
(Macrodef "GetTool") = 
"
  local \"ToolNo\" \"ToolFound\";

  ToolNo = SetToolWear %1 _ToolMode; 

  ToolFound = NcFromEmu ToolNo _ToolMode; /* Get NC tool from EMU tool */

  if(ToolFound == -1)                               /* invalid tool    */
   {mreset sprint (GetIniTxt \"GetTool\" \"Err002\") ToolNo;}

  if(ToolFound == -2)                               /* invalid pointer */
   {mreset sprint (GetIniTxt \"GetTool\" \"Err006\") ToolNo;}

  if(0 < UsePathTime) {ToolAccu;}

  /*--------------------------------------------------------------------*/
  /* Check tool position for automatic tool change                      */
  /*--------------------------------------------------------------------*/
  if((_ToolAuto == 3) and (ToolFound >= mtag[mtagI].nPickPos))
  {                                      /* no tool location           */
   mreset sprint (GetIniTxt \"GetTool\" \"Err003\") ToolNo;
  }

  _ToolSelEmu = ToolNo;                  /* assign tool                */
  _ToolSel    = ToolFound;
  if(_toolMode == 2) {_ToolSelEmu = ToolFound;}

  /*--------------------------------------------------------------------*/
  /* Grafik aktivieren / deaktivieren aufgrund des Werkzeug-Modus       */
  /*--------------------------------------------------------------------*/
  local \"ToolEmuActive\";

  ToolEmuActive = 1;

  if(T_undefined != (TypeOf \"CFParm\").type)      /* if CFParm exists */ 
  {
    ToolEmuActive = ('1' == (*_Fparm.ToolActive)[_ToolselEmu]);
    if(0 == (mtagC.ToolsSelByUser and (1 << _ToolSelEmu)))
    {
      ToolEmuActive = 0;
    }
  }

  if(%P >= 2)       {ToolEmuActive = %2;}          // override active flag
  if(_ToolSelEmu == 0) {ToolEmuActive =  1;}       // override for tool=0
  /*--------------------------------------------------------------------*/
  /* Aktivieren / deaktivieren aufgrund des Werkzeug-Modus              */
  /*--------------------------------------------------------------------*/
  if(_Mode == 'G')                        /* Ausführungsmodus Grafik   */
  {
    _grMode = ToolEmuActive;             // Grafikmodus anpassen
    mtagC.ToolsUsedByFile = mtagC.ToolsUsedByFile or (1 << _ToolSelEmu);
  }

  if(_mode != 'G')
  {
    local \"CallOnExit\" \"CallOnError\" \"DoChange\";
    CallOnExit = (sprint \"Thread_agi.vci.iCurr = %dL; \" Thread_agi.vci.iCurr)
               & (sprint \" RmpMode   '%c'; \" RmpMode '?')
               & \"if(mtag[mtagI].Step){Mstop 'E'; Z Z + 0.001; mf;}\";
    CallOnError = CallOnExit & *_tlch.doIfAborted;

    DoChange = 0;
    if(%P == 3) {DoChange = %3;}        /* enforce tool change         */ 

    RmpMode 'Y';                        // no ramp look ahead in toolchange
    
    wait 0; MpLock '+';
    CallOnExit = CallOnExit & \" MpLock '-';\";
    CallOnError = CallOnExit & *_tlch.doIfAborted;

    If((0 == _ncMode AND 1)                  /* Werkzeug ist inaktiv   */  
    AND(1 == ToolEmuActive))                 /* aktivieren verlangt    */ 
    {
      _ncMode = _NcMode or 1;                /* aktivieren             */
      mreset;
      if((_ToolAuto > 0) AND (Thread_agi.vdim > 2))
      {
        if(_IdeFlag)
          {msgbox sprint \"Aktivieren: ToolEmuActive=%d; _NcMode=%d\" 
                                       ToolEmuActive     _ncMode;}

        //if(_OcNext[2] < McPos[3].ofs[2])     /* hochfahren             */
        //  {_OcNext[2] = McPos[3].ofs[2]; MF; Wait 0; mreset;}
      }
    }  
       
    If((1 == _ncMode AND 1)                  /* Werkzeug ist aktiv     */  
    AND(0 == ToolEmuActive))                 /* deaktivieren verlangt  */ 
    { 
      if((_ToolAuto > 0) AND (Thread_agi.vdim > 2))
      {
        if(_IdeFlag)
          {msgbox sprint \"Deaktivieren: ToolEmuActive=%d; _NcMode=%d\" 
                                         ToolEmuActive     _ncMode;}

        //if(_OcNext[2] < McPos[3].ofs[2])     /* hochfahren             */
        //  {_OcNext[2] = McPos[3].ofs[2]; MF; Wait 0; mreset;}
      }
      _ncMode = _NcMode and -2;              /* deaktivieren           */
    }

    If(1 == _ncMode AND 1)                   /* real tool change       */
    {
      if(ThreadFlags \"MtdrvIsSet\")
      {
        if((_toolSel != _toolNc) OR (DoChange > 0)) 
         {fcall MtApp.path & \"/Macros/GetTool.ts\"; wait 0;}
      }
      _toolNcEmu = _toolSelEmu;              /* update EMU-tool        */
      _Plc.DirtyFlags.Tool = 1;              /* signal EMU changes     */
    }
  }

  /*--------------------------------------------------------------------*/
  /* Final tasks                                                        */ 
  /*--------------------------------------------------------------------*/
  local \"ToolCol\";                              /* select tool color */
  ToolCol = Tools[_toolSelEMU].Color;             /* ...from tools     */ 

  if(T_undefined != (TypeOf \"CFParm\").type)     /* if CFParm exists  */ 
  {
    //local \"CM\"; CM = _fParm.CrcMode;            /* default = fParm   */
    //if(CM == 0) {CM = _pParm.CrcMode;}            /* fParm => pParm    */
    //if(CM == 5) {CM = Tools[_ToolNcEmu].CRC;}     /* pParm => tool     */

    //ModeToCrc CM; 

    if(_PParm.ColorMode)
    {                                                  /* tool color   */
      If(_fParm.MlColor) {ToolCol = _fParm.MlColor;}   /* from project */
    }
  }  

  _Crcpar.Rad = ToolRadEmu;                       /* set crc radius    */

  SetDrawPar 'L' ToolCol -1 -1 -1;                /* set color         */

  if(_ToolWidthVar)                               /* set width         */
  {
    local \"ToolWidth\";       
    ToolWidth = 0;                
    Toolwidth = 2.0 * _MM2Pix * ToolRadEmu;
    if(_mtStat.crcstate == 0) {SetDrawPar 'L' ToolCol -1 ToolWidth -1;}
    if(_mtStat.crcstate >  0) {SetDrawPar '0' ToolCol -1 ToolWidth -1;}
  }

  if(_SpeedFromTool > 0)
  {
    if(Tools[_toolSelEmu].Speed > 0)              // set speed
    {ML_F = Tools[_toolSelEmu].Speed; MvPar;}
    if(Tools[_toolSelEmu].SpeedZdn > 0)           // set submerge speed
    {mld_f = Tools[_toolSelEmu].SpeedZdn; MvPar;}
  }

  if(_RpmFromTool > 0)
  {
    mtag[mtagI].spdRpm = Tools[_toolSelEmu].Rpm;            /* set spindle speed */
    if(_ncMode AND 1){_oaSet mtag[mtagI].spdRpm; _dspUpdate;}
  }
";

/* ================================================================== */
/* Ask if the tool rack is empty at location _ToolNC                  */
/* ================================================================== */
(MacroDef "QueryToolOk") =
" local \"mb\" \"mname\";
  if(_ToolNC) {mname = \"QueryToolOK\";}
  else        {mname = \"QueryTool0\";}
  mb = MsgBox
       (sprint (scall \"\\\"\" & (GetIniTxt \"AutoStrt\" mname) & \"\\\"\") _ToolNC + 0)
       (sprint (GetIniTxt \"AutoStrt\" \"QueryToolCapt\") _ToolNC + 0L)
        MB_YESNO or MB_ICONEXCLAMATION;

  if(mb == 7)                               /* altes Tool herausnehmen */
  {
    if('1' == mlimit '?')
    {
      local \"CallOnExit\" \"CallOnError\";
      CallOnExit = CallOnError = sprint \"Setmc2Oc %d\" _iMc2Oc;
      SetMc2Oc 0 0;                           // Mc ohne Tool Offset verwenden

      if(Thread_agi.vdim > 2)
       {_OcNext[2] = McPos[1].Ofs[2]; MF;}    /* Z zur Serviceposition   */   

      _OcNext[0] = McPos[1].Ofs[0];
      _OcNext[1] = McPos[1].Ofs[1]; MF;       /* X,Y zur Serviceposition */
      Wait 0; mreset;
    }
    if(_ToolNC) {mname = \"InsertTool\";}
    else        {mname = \"RemoveTool\";}
    MsgBox (sprint (GetIniTxt \"AutoStrt\" mname) _ToolNC + 0L)
            \"AutoStart.ts\"  MB_OK or MB_ICONEXCLAMATION;
    wait 0.5;
  }
";

/* ================================================================== */
/* Ask if the tool is to be calibrated                                */
/* ================================================================== */
(MacroDef "QueryToolCalib") =
"Local \"Msg\"; 

  _ToolPrbState = 0;                             /* reset flag         */   

  if(0 == _Test AND 2)
  {
    local \"OldLim\"; OldLim = Mlimit '?';/* Save current limit flag */
    mcall *_tlch.LimitsOff;               /* suspend limit watching  */ 

    local \"CallOnExit\" \"CallOnError\";
    CallOnExit = (sprint \"MF_F = %.3lf; ML_F = %.3lf; MvPar;\" MF_F ML_F)
              & (sprint \"SetMc2Oc %d;\" _iMc2Oc);

    CallOnError = CallOnExit; 
    SetMc2Oc 0 0;
    Msg = GetIniTxt \"GetTool\" \"MsgKalib1\";   /* want calibration?  */
    if(6 == MsgBox (Scall \"\\\"\" & Msg & \"\\\"\") 
                   (GetIniTxt \"GetTool\" \"MsgKalib2\") MB_YESNO)
    { 
      if(_iTPrb == 0)
      {
      _Test = _Test OR 1;                  /* switch is not defined   */
       msgbox GetIniTxt \"GetTool\" \"MsgKalib3\";
      }
      _ToolPrbState = 2;                         /* calibration wanted */   
      if(_ToolAuto >= 2)
      {
        mf_f = _ToolSpeedF;                   /* set toolchange speed    */
        ml_f = _ToolSpeedS; MvPar;

        local \"sw\";
        sw = Mcall *_tlch.ReadSwitch 1;         // check if tool is inserted
        if(_ToolNC)
        {
          if(sw == 0)
          {
            Msg = GetIniTxt \"GetTool\" \"MsgKalib4\";  // no tool inserted
            MsgBox (Scall \"\\\"\" & Msg & \"\\\"\") 
                    (GetIniTxt \"GetTool\" \"MsgKalib2\") MB_OK;
            _ToolNc = _ToolSel = _ToolNcEmu = _ToolSelEmu = 0;
            _plc.dirtyFlags.Tool = 1;     // silent change to tool 0
          }
        }
        else
        {
          if(sw)
          {
            Msg = GetIniTxt \"GetTool\" \"MsgKalib5\";  // unexpected tool found
            MsgBox (Scall \"\\\"\" & Msg & \"\\\"\") 
                    (GetIniTxt \"GetTool\" \"MsgKalib2\") MB_OK;
          }
        }
        if(_TchToHome == 1)
        {MCall *_tlch.GoToPos McPos[3].Ofs;} /* return to tool home pos.*/
      }
    }
    mcall *_tlch.LimitsOn OldLim;            /* restore limit watching  */
  }
 } 
";

/* ================================================================== */
/* Get NC tool from EMU tool:                                         */  
/* ================================================================== */
/* Parameter: %1 = EMU tool, %2 = ToolMode (0, 1, 2)                  */  
/* ------------------------------------------------------------------ */
(Macrodef "NcFromEmu") =
"local \"i\" \"Result\";

 Result = %1;                           /* default = requested tool   */      

 if(%2 <= 1)                            /* check tool variable        */
 {
  if((%1 < 0) OR (%1 > _toolN)) {Result = -1;}     /* invalid tool    */
 }

 if((Result > 0) AND (%2 == 1))         /* get tool pointed to by EMU */
 {
   i = 0L + Tools[%1].EMU;
   if((i > 0) and (i <= _toolN)) {Result =  i;} 
   if((i < 0) or  (i >  _toolN)) {Result = -2;}    /* invalid pointer */
 }

 if((Result > 0) AND (%2 == 2))         /* search tool with Ident-No. */
 { 
   i = Result = 0;                      /* there is no default        */ 
   while \"i < _ToolN\"
   {
     if(Tools[i = i + 1].Ident == %1) {Result = i; i = _ToolN;}
   }
 }

 if((Result == 0) And (_tlch.AllowT0 == 0)) {Result = -1;}

Result";                                 /* return result (=tool)     */

/* ================================================================== */
/* SetToolWear: Change tool wear index if tool no. > 99               */
/*   "GetTool 0104;" = Get tool 1 with tool wear index = 4.           */
/* ================================================================== */
/* Parameter: %1 = new tool number -> _ToolSelEmu                     */ 
/*            %2 = tool mode                                          */ 
/* ------------------------------------------------------------------ */
(Macrodef "SetToolWear") = 
"local \"ToolNo\" \"iWear\";

 ToolNo = %1; iWear = 0L;
 if((%1 > 99) AND (%2 < 2))
 {
   ToolNo = %1 / 100;   
   iWear  = %1 - 100 * ToolNo;
   if(iWear >= _nWearOfs) {iWear = 0;}
 }

 _iWear = iWear; 

ToolNo";                                 /* return tool number        */
