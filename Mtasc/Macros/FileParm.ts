/* ================================================================== */
/* File and Project parameters                                        */
/* ================================================================== */
CFParm = ObjDef T_classDscr                /* File info class         */
"Enabled"      T_Short  0                  /* enable flag             */
"MirrFlag"     T_Short  0                  /* Mirroring flags         */
"CrcMode"      T_Short  0                  /* cutter compensation     */
"CrcHpgl"      T_Short  0                  /* CRC in HPGL file        */
"CrcBuffer"    T_Short  0                  /* buffer size             */
"CrcNOpen"     T_Short  0                  /* CRC open delay          */
"CmdSuv"       T_Short  0                  /* CmdSuv mode             */
"Tool"         T_Short  0                  /* File default tool       */
"Type"         T_Short  0                  /* file type (.t .NC etc.) */
"Cnv"          T_Short  0                  /* converted flag          */
"Name"         T_Object 0                  /* file name               */
"Text"         T_Object 0                  /* file description        */

"Zup"          T_Double 0                  /* Tool up value           */
"Zdn0"         T_Double 0                  /* Begin submerging        */
"Zdn0Flag"     T_Short  0                  /* Use different speed     */
"ZdnMill"      T_Double 2                  /* Layering info           */
"ZdnMillFlag"  T_Short  0                  /* Layering flag           */ 
"ZdnDrill"     T_Double 2                  /* Virtual drilling info   */
"ZdnDrillFlag" T_Short  0                  /* Virtual drilling flag   */ 

"HookFlag"     T_Short  0                  /* Hook flag               */
"HookSrc"      T_Double 3                  /* Hook source data        */   
"HookDst"      T_Double 3                  /* Hook destination data   */  
 
"MlColor"      T_Long   0                  /* tool color override     */
"ToolActive"   T_object 0                  /* Which tools are active  */

"RubValid"     T_Short  0                  /* rubout valid flag       */
"RubWc1"       T_Double 2                  /* rubout first point      */   
"RubWc2"       T_Double 2                  /* rubout second point     */   
"RubRad"       T_Double 0                  /* rubout radius           */  
;

/* ================================================================== */
/* Project lines, Project version 1.1                                 */
/* ================================================================== */
/* Load a project line                                                */
/* %1: file name; %2: line                                            */
/* ------------------------------------------------------------------ */
(macrodef "LoadFParm") = 
"Local \"OneLine\" \"Code\" \"PShort\"; PShort = 0;
 Local \"iLine\"; iLine = sprint \"%d\" %2;

 Code = 63; if(%P > 2) {Code = %3;}

 if(Code AND 1)
 {
   OneLine = GetIniString %1 \"Files\" iLine \"\";
   _fParm.Enabled = GetStkObj 0 scall OneLine;

   pShort = GetStkObj 1 scall OneLine;
   _fParm.MirrFlag = pShort - 6 * pShort / 10;/* umrechnen in Basis 4 */

   pShort = GetStkObj 2 scall OneLine;        /* Zerlegen in 2 Ziffern*/
   _fParm.CrcMode       = pShort - 10 * pShort / 10;
   _fParm.CrcHpgl       = pShort / 10;

   _fParm.CrcBuffer     = GetStkObj 3 scall OneLine;
   _fParm.CrcNOpen      = GetStkObj 4 scall OneLine;
   _fParm.Tool          = GetStkObj 5 scall OneLine;
   _fParm.Type          = GetStkObj 6 scall OneLine;
   _fParm.Cnv           = GetStkObj 7 scall OneLine;
   (*_fParm.Name)       = GetStkObj 8 scall OneLine;
   (*_fParm.Text)       = GetStkObj 9 scall OneLine;

   if(%2 == 0) 
   {
     _PParm.Tool    = _FParm.Tool;
     _PParm.CrcMode = _FParm.CrcMode;
   } 
 }

 if(Code AND 2)
 {
   OneLine = GetIniString %1 \"ZupDn\" iLine \"\";
   _fParm.Zup           = GetStkObj 0 scall OneLine;
   _fParm.Zdn0          = GetStkObj 1 scall OneLine;
   _fParm.Zdn0Flag      = GetStkObj 2 scall OneLine;
   _fParm.ZdnMill[0]    = GetStkObj 3 scall OneLine;
   _fParm.ZdnMill[1]    = GetStkObj 4 scall OneLine;
   _fParm.ZdnMillFlag   = GetStkObj 5 scall OneLine;
   _fParm.ZdnDrill[0]   = GetStkObj 6 scall OneLine;
   _fParm.ZdnDrill[1]   = GetStkObj 7 scall OneLine;
   _fParm.ZdnDrillFlag  = GetStkObj 8 scall OneLine;
 }

 if(Code AND 4)
 {
   OneLine = GetIniString %1 \"Hook\" iLine \"\";
   _fParm.HookFlag   = GetStkObj 0 scall OneLine;
   _fParm.HookSrc[0] = GetStkObj 1 scall OneLine;
   _fParm.HookSrc[1] = GetStkObj 2 scall OneLine;
   _fParm.HookSrc[2] = GetStkObj 3 scall OneLine;
   _fParm.HookDst[0] = GetStkObj 4 scall OneLine;
   _fParm.HookDst[1] = GetStkObj 5 scall OneLine;
   _fParm.HookDst[2] = GetStkObj 6 scall OneLine;
 }

 if(Code AND 8)
 {
   OneLine = GetIniString %1 \"ToolActive\" iLine \"\";

   if(_ToolN > 0)
   {
     for(1 + _ToolN / 10) {OneLine = OneLine & \"1111111111\";}
     if ((_ToolN - 1) < SizeOfArray oneline) {OneLine[_ToolN] = 0;}
   } 

   (*_fParm.ToolActive) = sprint \"0%s\" OneLine;
 }

 if(Code AND 16)
 {
   OneLine = GetIniString %1 \"Rubout\" iLine \"0 0.0 0.0 0.0 0.0 0.0 0.0 0.0\";
   _fParm.RubValid   = GetStkObj 0 scall OneLine;
   _fParm.RubWc1[0]  = GetStkObj 1 scall OneLine;
   _fParm.RubWc1[1]  = GetStkObj 2 scall OneLine;
   _fParm.RubWc2[0]  = GetStkObj 3 scall OneLine;
   _fParm.RubWc2[1]  = GetStkObj 4 scall OneLine;
   _fParm.RubRad     = GetStkObj 5 scall OneLine;
 }

 if(Code AND 32)
 {
   OneLine = GetIniString %1 \"Extended\" iLine \"0 0x000000L\";
   _fParm.CmdSuv  = GetStkObj 0 scall OneLine;
   _fParm.MlColor = GetStkObj 1 scall OneLine;
 }
";

/* ------------------------------------------------------------------ */
/* Save a project line                                                */
/* ------------------------------------------------------------------ */
(macrodef "SaveFParm") = 
"Local \"Txt\" \"Code\"; 
 Local \"iLine\"; iLine = sprint \"%d\" %2;

 Code = 63; if(%P > 2) {Code = %3;}
 if(Code AND 1)
 {
   Txt = (sprint \"'%2d  %d%d\"  _FParm.Enabled  (_FParm.MirrFlag / 4) _FParm.MirrFlag AND 3)
       & (sprint \" %2d%d\"      _FParm.CrcHpgl   _FParm.CrcMode)
       & (sprint \" %3d%4d\"     _FParm.CrcBuffer _FParm.CrcNOpen)
       & (sprint \" %4d%5d%4d\"  _FParm.Tool      _FParm.Type _FParm.Cnv)
       & (sprint \" %-23s\"      \" \\\"\" & (*_FParm.Name) & \"\\\"\")
       & (sprint \" %s\"         \" \\\"\" & (*_FParm.Text) & \"\\\"\")
   ;
   if(%2 <= 9) {Txt = \" \" & Txt;}

   WriteIniString %1 \"Files\" iLine Txt & \"'\";
 } 

 if(Code AND 2)
 {
   Txt = (sprint \" %7.3lf %7.3lf %d\"  _FParm.Zup         _FParm.Zdn0        _FParm.Zdn0Flag)
       & (sprint \" %7.3lf %7.3lf %d\"  _FParm.ZdnMill[0]  _FParm.ZdnMill[1]  _FParm.ZdnMillFlag)
       & (sprint \" %7.3lf %7.3lf %d\"  _FParm.ZdnDrill[0] _FParm.ZdnDrill[1] _FParm.ZdnDrillFlag)
   ;
   if(%2 <= 9) {Txt = \" \" & Txt;}

   WriteIniString %1 \"ZupDn\" iLine Txt;
 } 

 if(Code AND 4)
 {
   Txt = (sprint \" %3d\"  _FParm.HookFlag)
       & (sprint \" %7.3lf %7.3lf %7.3lf\" _FParm.HookSrc[0] _FParm.HookSrc[1] _FParm.HookSrc[2])
       & (sprint \" %7.3lf %7.3lf %7.3lf\" _FParm.HookDst[0] _FParm.HookDst[1] _FParm.HookDst[2])
   ;
   if(%2 <= 9) {Txt = \" \" & Txt;}

   WriteIniString %1 \"Hook\" iLine Txt;
 } 

 if(Code AND 8)
 {
   Txt = *_FParm.ToolActive; Txt[0] = 32;
   if(%2 <= 9) {Txt = \" \" & Txt;}
   WriteIniString %1 \"ToolActive\" iLine Txt;
 }

 if(Code AND 16)
 {
   Txt = (sprint \" %3d\"  _FParm.RubValid)
       & (sprint \" %7.3lf %7.3lf\" _FParm.RubWc1[0] _FParm.RubWc1[1])
       & (sprint \" %7.3lf %7.3lf\" _FParm.RubWc2[0] _FParm.RubWc2[1])
       & (sprint \" %7.3lf\"        _FParm.RubRad)
   ;
   if(%2 <= 9) {Txt = \" \" & Txt;}

   WriteIniString %1 \"Rubout\" iLine Txt;
 } 

 if(Code AND 32)
 {
   Txt = sprint \" %3d  0x%06XL\" _FParm.CmdSuv _FParm.MlColor;
   if(%2 <= 9) {Txt = \" \" & Txt;}

   WriteIniString %1 \"Extended\" iLine Txt;
 } 
";

/* ------------------------------------------------------------------ */
/* copy FParm values to non-FParm values                              */
/* ------------------------------------------------------------------ */
(macrodef "FParm2NonFParm") = 
"Zup      = _HpglPar.Zup = _FParm.Zup;
 Zdn      = _FParm.ZdnMill[0];
 Zdn0     = _FParm.Zdn0;
 Zdn0Flag = _FParm.Zdn0Flag;
       
 local \"CM\"; CM = _fParm.CrcMode;                /* default = fParm */
 if(CM == 0)  {CM = _pParm.CrcMode;}               /* fParm => pParm  */
 if(CM == 5)  {CM = Tools[_ToolNcEmu].CRC;}        /* pParm => tool   */

 ModeToCrc CM;                                     /* set crc mode    */

 _Hpglpar.CrcAuto    = _FParm.CRCHpgl;
 //_Hpglpar.ToolForPen = _PParm.HpglPen;
 //_Hpglpar.Zdn        = _PParm.HpglZdn;
 _Hpglpar.UnitX      = _PParm.HpUnitX;
 _Hpglpar.UnitY      = _PParm.HpUnitY;

 _CrcPar.BufSize     = _FParm.CrcBuffer;
 _CrcPar.nCmdOpen    = _FParm.CrcNOpen;
 _CrcPar.CheckLenMax = scall GetIniString (MtApp.path & \"/EdiTasc.ini\") 
                                          \"Param\" \"CheckLenMax\" \"2.0\";
";

(MacroDef "ModeToCrc") = 
"if(%1 == 0) {/* no action ... */} 
 if(%1 == 1) {_Crcpar.Ori = -1; _Crcpar.m_isClosed = 0;} 
 if(%1 == 2) {_Crcpar.Ori =  1; _Crcpar.m_isClosed = 0;} 
 if(%1 == 3) {_Crcpar.Ori = -1; _Crcpar.m_isClosed = 1;} 
 if(%1 == 4) {_Crcpar.Ori =  1; _Crcpar.m_isClosed = 1;} 
";

/* ================================================================== */
/* Project parameters                                                 */
/* ================================================================== */
CPParm = ObjDef T_classDscr                /* Project info class      */
"nLines"       T_Short  0                  /* number of project lines */
"ColorMode"    T_Short  0                  /* tool color override     */
"Tool"         T_Short  0                  /* project default tool    */
"ToolFile"     T_Object 0                  /* file name for tools     */
"CrcMode"      T_Short  0                  /* project CRC mode        */
"CrcCheckLen"  T_Double 0                  /* CRC max length to check */

"Ofs"  T_Double 3                          /* normal offset           */
"Fac"  T_Double 3                          /* normal factor           */
"Rot"  T_Double 3                          /* normal rotation         */
"RepN" T_Short  3                          /* clone repeater          */
"RepD" T_Double 3                          /* clone Offset            */
"Flag" T_Short  0           // Flags for mTrans
                            // Bit 0: use OFS
                            // Bit 1: use FAC
                            // Bit 2: use local mirror axis
                            // Bit 4: use global mirror axis
"mirrX" T_double 0          // local mirror axis in X

"HpUnitX" T_Double 0
"HpUnitY" T_Double 0
"HpUFlag" T_Short  0
//"HpglPen" T_Short  9
//"HpglZdn" T_Double 9
;

/* ------------------------------------------------------------------ */
/* Load project parameters from project file %1                       */  
/* ------------------------------------------------------------------ */
// %1: project file
// %2: _pParm from caller (optional)
(Macrodef "LoadPParm") =
"_PParm.nLines      = scall GetIniString %1 \"Project\" \"Files\"       \"0\";
 _PParm.ColorMode   = scall GetIniString %1 \"Project\" \"ColorMode\"   \"0\";
 _PParm.CrcCheckLen = scall GetIniString %1 \"Project\" \"CrcCheckLen\" \"2.0\";

 (*_PParm.ToolFile) = GetIniString %1 \"Project\" \"ToolFile\"  \"\";

 LoadHpgl  %1;
 local \"prvMirrX\";
 prvMirrX = mtag[mtagI].mirrX;      // global mirror axis
 //if(%P >= 2){if(%2.Flag and 16){prvMirrX = %2.mirrX;}}  // get from caller
 LoadTrans %1 \"MTrans\";
 if(_PParm.Flag and 16)
 {_PParm.mirrX = prvMirrX;}         // use global mirror axis
 else
 {_PParm.mirrX = scall GetIniString %1 \"Mtrans\" \"mirrX\" \"0.0\";}
";

/* ------------------------------------------------------------------ */
/* Save project parameters to project file %1                         */  
/* ------------------------------------------------------------------ */
(Macrodef "SavePParm") =
"WriteIniString %1 \"Project\" \"Files\"       sprint \"%d\"    _PParm.nLines;
 WriteIniString %1 \"Project\" \"ColorMode\"   Sprint \"%d\"    _PParm.ColorMode;
 WriteIniString %1 \"Project\" \"CrcCheckLen\" sprint \"%.2lf\" _PParm.CrcCheckLen;
 WriteIniString %1 \"Project\" \"ToolFile\"    *_PParm.ToolFile;

 SaveHpgl  %1;
 SaveTrans %1;
";

/* ------------------------------------------------------------------ */
/* Load Hpgl parameters from project file %1                          */  
/* ------------------------------------------------------------------ */
(Macrodef "LoadHpgl") = 
"_PParm.HpUnitX = scall GetIniString %1 \"HPGL\" \"HpglUnitX\" \"0.025\";
 _PParm.HpUnitY = scall GetIniString %1 \"HPGL\" \"HpglUnitY\" \"0.025\";
 _PParm.HpUFlag = scall GetIniString %1 \"HPGL\" \"HpglUFlag\" \"0\";
 //_PParm.HpglPen = ObjDef T_short 9 scall GetIniString %1 \"HPGL\" \"HpglPen\" \"0\"; 
 //_PParm.HpglZdn = ARRAY 9 sCall GetIniString %1 \"HPGL\" \"HpglZdn\" \"0.0\"; 
";

/* ------------------------------------------------------------------ */
/* Save Hpgl parameters to project file %1                            */  
/* ------------------------------------------------------------------ */
(Macrodef "SaveHpgl") =
"WriteIniString %1 \"HPGL\" \"HpglUnitX\" sprint \"%8.4lf\" _PParm.HpUnitX;
 WriteIniString %1 \"HPGL\" \"HpglUnitY\" Sprint \"%8.4lf\" _PParm.HpUnitX;
 WriteIniString %1 \"HPGL\" \"HpglUFlag\" sprint \"%d\"     _PParm.HpUFlag;

 local \"Txt1\" \"Txt2\" \"i\";
 Txt1 = Txt2 = \"\"; i = 0; 
 // for 9
 // { 
   // Txt1 = Txt1 & sprint \" %d\"     _PParm.HpglPen[i] + 0L;
   // Txt2 = Txt2 & sprint \" %8.3lf\" _PParm.HpglZdn[i] + 0L;
   // i = i + 1;
 // }
 //WriteIniString %1 \"HPGL\" \"HpglPen\" Txt1;
 //WriteIniString %1 \"HPGL\" \"HpglZdn\" Txt2;
";
