
// Resize arrays of axis group data
// if vDim has changed (vDimMc != vDimMcPrv)
// Parameters:
// %1: axis group index

// fag values
local "fag" "d";
fag => MtApp.xml.MtDrv.fag.(sprint "v%d" %1);
d = fag.vDimMc._Xml2Mt;

fag.laaMapping._MtResize d "extAutoInc";
fag.McLimMin._MtResize d "extLast";
fag.McLimMax._MtResize d "extLast";
fag.CheckSwLim._MtResize d "extLast";
fag.CheckHwLim._MtResize d "extLast";
fag.manVMin._MtResize d "extLast";
fag.manVMax._MtResize d "extLast";
fag.vMaxV._MtResize d "extLast";
fag.accMaxV._MtResize d "extLast";
fag.speedJerkTolV._MtResize d "extLast";
fag.vDimOc.!A?v0 fag.vDimMc.!A?v0;

// axis group values
local "aag";
aag => MtApp.xml.Application.(sprint "AxisGroup%d" %1);
aag.Ref.Dir._MtResize d "extLast";
aag.Ref.Dist._MtResize d "extLast";
aag.Ref.DistCorr._MtResize d "extLast";
aag.Ref.IInput._MtResize d "extAutoInc";
aag.Ref.SpeedFast._MtResize d "extLast";
aag.Ref.SpeedSlow._MtResize d "extLast";
local "axl";
axl = aag.!A?AxisLett;
if((SizeOfArray axl) < d + 1)
{
  axl = "YXZABCUV";      // set default axis letters
  aag.!A?AxisLett axl;
}
if((SizeOfArray aag.Ref.Sequence.!A?v0) < d + 1)
{
  axl[0] = (aag.!A?AxisLett)[2];   // set default Ref Sequence
  axl[2] = (aag.!A?AxisLett)[0];
  aag.Ref.Sequence.!A?v0 axl;
}
local "m";
m = aag.Ref.Mask._Xml2Mt;
aag.Ref.Mask._Mt2Xml (1 << d) or m;

aag.SpeedAccel.mp_f._MtResize d "extLast";
aag.SpeedAccel.mp_r._MtResize d "extLast";

MsgBox sprint "No. of fag axes successfully changed from %d to %d" (fag.vDimMcPrv._Xml2Mt) d;
fag.vDimMcPrv.!A?v0 fag.vDimMc.!A?v0;
