//
//  Definitionen f�r MtConfigDN.exe
//

// Define temporary variables
// %1: 'I' or 'O'
// %2: Index
// These variables are defined:
// Mtc_d:  IO Descriptor
// Mtc_dTmp: temporary copy of IO Descriptor
// Mtc_dBak: backup copy of IO Descriptor
// Mtc_index: Index
// Mtc_md: IO Mode
// Mtc_lt: pointer to IO Label Text
(MacroDef "Mtc_SelectIo") =
"
  Mtc_index = %2;
  if(%1 == 'I') {Mtc_d => _inpDsc[%2]; Mtc_md => _iMode[%2]; Mtc_lt => _iLabel[%2].Text;}
  else          {Mtc_d => _outDsc[%2]; Mtc_md => _oMode[%2]; Mtc_lt => _oLabel[%2].Text;}
  Mtc_dBak = Mtc_dTmp = Mtc_d;
  if(%1 == 'O') {Mtc_dTmp.mapFlags = Mtc_dTmp.mapFlags or 4;}  // read memory backup
";

// Get formatted string for IO Descriptor
// %1: 'I' or 'O'
// %2: Index
(MacroDef "Mtc_IoDef2Str") =
"local \"d\" \"md\" \"lt\";
  if(%1 == 'I') {d => _inpDsc[%2]; md => _iMode[%2]; lt => *_iLabel[%2].Text;}
  else          {d => _outDsc[%2]; md => _oMode[%2]; lt => *_oLabel[%2].Text;}
  return
  sprint \"%2d: %-7s  %.4X  %d    %.4X  %.4X   %d   %d    %s\" %2 *_drvPdTypes[d.type] d.adr d.size d.inv d.mask d.mapFlags md lt;
";

// Save IO Descriptor to xml
// %1: 'I' or 'O'
// %2: Index
(MacroDef "Mtc_Save2Xml") =
"local \"xml\";
  if(%1 == 'I')
  {
    xml => MtApp.xml.Hardware.Inputs.(sprint \"!E!v%d\" %2);
  }
  else
  {
    xml => MtApp.xml.Hardware.Outputs.(sprint \"!E!v%d\" %2);
  }
  xml.!A!Type *_drvPdTypes[Mtc_d.type];
  xml.!A!Address sprint \"0x%.4X\" Mtc_d.adr;
  xml.!A!Mask    sprint \"0x%.4X\" Mtc_d.mask;
  xml.!A!InvMask sprint \"0x%.4X\" Mtc_d.inv;
  xml.!A!ByteSize sprint \"%d\" Mtc_d.size;
  xml.!A!MapMode  sprint \"%d\" Mtc_d.mapFlags;
  xml.!A!IoMode  sprint \"%d\" Mtc_md;
  xml.!A!LabelText *Mtc_lt;
";

(MacroDef "Mtc_delete") =
"
  erase Mtc_d;
  erase Mtc_dTmp;
  erase Mtc_dBak;
  erase Mtc_index;
  erase Mtc_md;
  erase Mtc_lt;
  erase Mtc_xmlBak;
  erase (MacroDef \"Mtc_SelectIo\");
  erase (MacroDef \"Mtc_IoDef2Str\");
  erase (MacroDef \"Mtc_Save2Xml\");
  erase (MacroDef \"Mtc_delete\");  // delete this macro too
";
