/*====================================================================*/
/* Spiegeln w�hrend der Bearbeitung                                   */
/*====================================================================*/
MirrStack = 0;   /* Zum Zeitpunkt der Makro-Definition initialisieren */               

(MacroDef "SetMirror") = 
"
local \"OK\" \"MirrX\" \"MirrY\" \"FacX\" \"FacY\";
Ok = 0;

For MirrStack {MTrans \"POP\";}            /* alte Spiegelg. aufheben */
MirrStack = 0;

if((_Mode == 'G') AND (1 == %1 AND 1)){Ok = 1;} /* Spiegeln Grafik    */
if((_Mode != 'G') AND (2 == %1 AND 2)){Ok = 1;} /* Spiegeln Bearbeitg.*/

if(ok)
{
  MirrX = MirrY = 0.0;
  FacX  = FacY  = 1.0;
  if(%2 == 0) {MirrY = 0.001 * %3; FacY = -1.0;}/* horizontal         */
  if(%2 == 1) {MirrX = 0.001 * %3; FacX = -1.0;}/* vertikal           */
    
  Mtrans \"PUSH\"; MirrStack = MirrStack + 1;
  Mtrans \"OFS\" (-MirrX) (-MirrY) 0.0;         /* verschieben        */
     
  Mtrans \"PUSH\"; MirrStack = MirrStack + 1;
  Mtrans \"FAC\" FacX  FacY  1.0;               /* an Achse spiegeln  */ 
  Mtrans \"OFS\" MirrX MirrY 0.0;               /* zur�ckverschieben  */
}
";


