/*====================================================================*/
/* Konvertierung von Bahnobjekt-Sequenzen nach HPGL                   */
/*====================================================================*/
/* Eingabe: Datei-Variable "fileIn"                                   */
/* Ausgabe: Datei-Variable "fileOut"                                  */
/*--------------------------------------------------------------------*/

if(0 == FileExists FileIn) {mreset "Die Eingabedatei wurde nicht gefunden:\n\n" & FileIn;}
if(FileOut == "")          {mreset "Die Ausgabedatei 'FileOut' ist nicht definiert";}

local "CallOnExit" "dst";

CallOnExit = (sprint "@Zup = %lf;" Zup)   /* variables to be restored */
           & (sprint " Zdn = %lf;" Zdn)
           & (sprint " Thread_agi.OutCtrl.mtp    = %d;" Thread_agi.OutCtrl.mtp)
           & (sprint " Thread_agi.OutCtrl.drv    = %d;" Thread_agi.OutCtrl.drv)
           & (sprint " Thread_agi.OutCtrl.grMode = %d;" Thread_agi.OutCtrl.grMode + 0)
           & (sprint " Thread_agi.OutCtrl.ncMode = %d;" Thread_agi.OutCtrl.ncMode + 0)
           & (sprint " _Mode              = %d;" 0 + _Mode);
//           &         " DeleteDgiMtp; FileOut = \"\";";

zup = 10.0;                               /* initialization           */
zdn =  0.0;
dst = Array FagVDimMax 0.0;

Thread_agi.OutCtrl.mtp    = 1;
Thread_agi.OutCtrl.drv    = 0;
Thread_agi.OutCtrl.grMode = 0;
Thread_agi.OutCtrl.ncMode = 3;
_Mode              = 'X';
_crcRequest        = 0;                   // for dxf processing 

NewDgiMtp 'F' fileOut;

_mtp.text = &("IN; PU;\r\n");
_mtp_Add;

_mtp.linHandler = &("
 if((*_mtp.text)[0] != 'I')
 {
  dst = (*_mtp.mobj).<0>.pEnd._;
  if(dst[2] >= zup){_mtp.text = &(\"PU\");}
  if(dst[2] < zup) {_mtp.text = &(\"PD\");}
  _mtp.text = &((*_mtp.text) & sprint \"%.3lf %.3lf;\\r\\n\"
   (dst[0] / _HpglPar.UnitX) (dst[1] / _HpglPar.UnitY));
 }

 if((*_mtp.text)[0] == 'I')           /* ingnore first motion command */
  {_mtp.text = &(\"\");}
");

_mtp.arcAngRes = 5.0 * _d2r;          /* Arc resulution: max. angle   */
_mtp.arcMaxLen = 5.0;                 /* Arc resolution: max. length  */

if(0){ /* if(1): Normal,  if(0): B�gen in Linien Aufl�sen */
_mtp.arcHandler = &("
 local \"ori\";
 ori = -1 + 2 * 0.0 <= 
  ((*_mtp.mobj).dirX._[0] * (*_mtp.mobj).dirY._[1]) - ((*_mtp.mobj).dirX._[1] * (*_mtp.mobj).dirY._[0]);
 _mtp.text = &(sprint \"AA%.0lf %.0lf %.2lf\\r\\n\"
  ((*_mtp.mobj).cent._[0] / _HpglPar.UnitX) ((*_mtp.mobj).cent._[1] / _HpglPar.UnitY)
  ((ori * (*_mtp.mobj).a1 - (*_mtp.mobj).a0) / _d2r));
");
}

local "i"; i = 0;
for Thread_agi.vDim{ _oc[i] = _ocNext[i] = 0.0; i = i + 1;}

_oc = _ocNext = dst;
_oc[2] = _ocNext[2] = dst[2] = zup;              // initialize with zup status
x x + 1; mf;
fcall fileIn;
wait 0;
DeleteDgiMtp;
sleep 100;

// optimize HPGL paths
ShellOpen 0 (MtApp.path & "/Plt2Plt.exe") ("\"" & fileOut & "\" \"" & fileOut & "\"");
sleep 500;
fileOut = "";
