/* ================================================================== */
// Erzeugung einer Konturliste f�r PLT-Dateien
/* ================================================================== */

local "CallOnExit" "mlist" "mlistPrv";
CallOnExit = sprint "@Thread_agi.SetMObjList mlistPrv; Thread_agi.outCtrl.mObjList = %d;" Thread_agi.outCtrl.mObjList + 0;

Thread_agi.mact.listHandler = &(
"if(%P == 2)                                      /* Konturanfang     */
 {
   list_cnt = list_cnt + 1;
   z zup; mf;
   _ocNext[0] = %2._[0]; _ocNext[1] = %2._[1]; mf;
   _ocNext = %2._; ml;
   if(%1 != 2)
   {
     //if(_mode == 'G') {wait 0.2 0;}

     if(usecrc)                                  /* Radius-Korr. ein */ 
     {
       _Crcpar.m_isClosed  = 1;                   /* geschlossen      */
       _Crcpar.Ori         = 1;                   /* nach rechts      */  
       _crcpar.BufSize     = 4;                  /* Parameter setzen */
       _crcpar.nCmdOpen    = 2;
       _Crcpar.Rad         = 0.3;
       _Crcpar.CheckLenMax = 2.0;
       CrcOpen;                                   /* aktivieren       */
     }
     //z zdn; ml;                                   /* abarbeiten       */
     //_mtStat.xData[2] = _mtStat.xData[2] + 1;
     //if(_mtStat.xData[2] == 7){__test;}
   }

   if(%1 == 2)                           /* Kein CRC beim Wiederholen */
   {
     //if(_mode == 'G') {wait 0.2 0;}
MsgBox \"T ROT\";
     SetDrawPar 'L' 0xFFL -1 -1 -1;             /* Wiederholen in rot */
   }
 }

 if(%P == 1)                                    // Konturende
 {
   if(Thread_agi.vci.iMark > iMark)
   {
     list_iMark = list_cnt;
     iMark = Thread_agi.vci.iMark;
   }
   //if(_mtStat.xData[2] == 18){__test;}
   if(usecrc) {CrcClose;}                      /* Radius-Korr. aus   */ 

   z zup; mf;

   if(%1 == 2)                                  /* Farbe zur�cksetzen */
    {SetDrawPar 'L' Tools[_ToolSelEmu].Color -1 -1 -1;}
 }
");

Thread_agi.mact.lhExecCallLevel = 1;
Thread_agi.mact.ignorePStartForFastLines= 1;
Thread_agi.mact.warningState = 0;


mlist = gcnew CMObjList;
mlist.<0>.dim= 3;
mlist.addObjGrpMode = 1;
mlistPrv = Thread_agi.SetMObjList &mlist;

Thread_agi.outCtrl.mObjList = 1;

// fCall  %1;                // Datei ausf�hren
// dinCall %1;
_HpglPar.zup = 0;
hpglCall %1;

//mreset;

return mlist;
