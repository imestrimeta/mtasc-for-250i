/*====================================================================*/
// Read initializing data for LAA and FAGs
// Save in MtApp.xml
/*====================================================================*/
// These global data are created:
// classes: CLaaInit, CFag<i>Init
// laa, gmask, fag<i>
// with <i> for all bits set in gmask
/*--------------------------------------------------------------------*/

IniFileMt  = MtApp.path & "/Mtasc.ini";
IniFileDrv = MtApp.path & "/Mtdrv.ini";

/*--------------------------------------------------------------------*/
// Read an integer value from Mtasc.ini
/*--------------------------------------------------------------------*/
//(MacroDef "GetMtInt") =             // get integer from global entry
//"Local \"Txt\" \"Ind\";
//Txt = GetIniString IniFileMt \"MTASC\" %1 \"0\";
//Ind = 0L; if(%P == 2) {Ind = %2;}
//0L + GetStkObj Ind scall Txt & \" 0 0 0 0 0 0 0 0 \";";

//(MacroDef "GetIniInt") =            // get integer from AGI by argument
//"Local \"Txt\" \"Ind\" \"AgKey\";
//AgKey = sprint \"AGI%d\" %1;
//Txt = GetIniString IniFileMt AGKey %2 \"0\";
//Ind = 0L; if(%P == 3) {Ind = %3;}
//0L + GetStkObj Ind scall Txt & \" 0 0 0 0 0 0 0 0 \";";

/*--------------------------------------------------------------------*/
// Read a double value from Mtasc.ini
/*--------------------------------------------------------------------*/
(MacroDef "GetMtDbl") =             // get double from global entry 
"Local \"Txt\" \"Ind\";
Txt = GetIniString IniFileMt \"MTASC\" %1 \"0.0\";
Ind = 0L; if(%P == 2) {Ind = %2;}
0.0 + GetStkObj Ind scall Txt & \" 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 \";";

(MacroDef "GetAGIDbl") =            // get double from active AGI entry
"Local \"Txt\" \"Ind\" \"AgKey\";
AgKey = sprint \"AGI%d\" Thread_AGI.Fag.Address;
Txt = GetIniString IniFileMt AGKey %1 \"0.0\";
Ind = 0L; if(%P == 2) {Ind = %2;}
0.0 + GetStkObj Ind scall Txt & \" 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 \";";

(MacroDef "GetIniDbl") =            // get double from AGI by argument
"Local \"Txt\" \"Ind\" \"AgKey\";
AgKey = sprint \"AGI%d\" %1;
Txt = GetIniString IniFileMt AGKey %2 \"0.0\";
Ind = 0L; if(%P == 3) {Ind = %3;}
0.0 + GetStkObj Ind scall Txt & \" 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 \";";


local 30 "f"; f = IniFileDrv;
local "i";

////////////////////////////////////////////////////////////////////////
// LAA data
////////////////////////////////////////////////////////////////////////

erase laa;     // erase old data

local "vDim"; vDim = 0;
vDim = scall GetIniString f "LAA" "vDim" "0";

CLaaInit = ObjDef T_classDscr
"Ver"    T_long   0
"vDim"   T_long   0
"vDimPrv" T_long   0
"aMask"   T_long  0
"DrvEnable" T_long 0
"frqCorrFac" T_double 0
"PIDFrq"     T_long   0
"backgrFrq"  T_long   0
"backgrWait"  T_long   0

"AcToMcFac"   T_double vDim

"dAcdCorr"    T_long   vDim

"AdrIn"       T_long   vDim
"AdrOut"      T_long   vDim
"PdType"      T_object vDim
"PdId"        T_long   vDim
"PdCh"        T_long   vDim
"HwType"      T_object vDim
"PdReload"    T_long   0
"StopTime"    T_double 0
"IcFacUsed"   T_long   0
"IcFac"       T_double vDim

// Control Limits ----------------------- Tab 10 -------------------------
"posErrTestFlag" T_long   0
"speedMaxTol"    T_long   vDim
"speedMaxCut"    T_long   vDim
"speedStart"     T_long   vDim

// Control Parameters ------------------- optimize -----------------------
"dAcdrPFac"      T_double vDim
"dAcdrIFac"      T_double vDim
"dAcdrDFac"      T_double vDim
"dAcdrD2Fac"     T_double vDim
"AcdDeadTime"    T_double vDim
;

laa = gcnew CLaaInit;

local "s"; s = "LAA";
laa.Ver   = scall GetIniString f s  "Ver" "0";
laa.vDim  = laa.vDimPrv = scall GetIniString f s "vDim" "0";
laa.aMask = scall GetIniString f s "aMask" "0";
laa.DrvEnable = scall GetIniString f s "DrvEnable" "0";
laa.frqCorrFac = 0.0;
if((GetIniString f s "frqCorrFac" "!")[0] == '?')
{
  laa.frqCorrFac = -1.0;
}
else
{
  laa.frqCorrFac = scall GetIniString f s "frqCorrFac" "0";
}
laa.PIDFrq = scall GetIniString f s "PIDFrq" "0";
laa.backgrFrq = scall GetIniString f s "backgrFrq" "0";
laa.backgrWait = scall GetIniString f s "backgrWait" "0";
laa.AcToMcFac  = array laa.vDim scall GetIniString f s "AcToMcFac" "0";
laa.dAcdCorr   = array laa.vDim scall GetIniString f s "dAcdCorr" "0";
laa.AdrIn      = array laa.vDim scall GetIniString f s "AdrIn" "0";
laa.AdrOut     = array laa.vDim scall GetIniString f s "AdrOut" "0";
local "CC" "USB1_0" "P4C1" "P4C1_0" "P4C1_1";
CC = "CC"; USB1_0 = "USB1_0"; P4C1 = "P4C1"; P4C1_0 = "P4C1_0"; P4C1_1 = "P4C1_1";
i = 0; for laa.vDim
{
  laa.PdType[i] = &(GetStkObj i scall GetIniString f s "PdType" "");
  i = i + 1;
}
laa.PdId     = array laa.vDim scall GetIniString f s "PdId"   "0";
laa.PdCh     = array laa.vDim scall GetIniString f s "PdCh"   "0";
local "ClosedLoop"; ClosedLoop = "ClosedLoop";
i = 0; for laa.vDim
{
  laa.HwType[i] = &(GetStkObj i scall GetIniString f s "HwType" "0");
  i = i + 1;
}
laa.StopTime = scall GetIniString f s "StopTime" "0";
if((GetIniString f s "IcFac" "!")[0] == '!')
{
  laa.IcFacUsed = 0;
}
else
{
  laa.IcFacUsed = 1;
  i = 0; for laa.vDim
  {
    laa.IcFac[i] = GetStkObj i scall GetIniString f s "IcFac" "!");
    i = i + 1;
  }
}
laa.posErrTestFlag = scall GetIniString f s "posErrTestFlag" "0";
laa.speedMaxTol    = array laa.vDim scall GetIniString f s "speedMaxTol"  "0";
laa.speedMaxCut    = array laa.vDim scall GetIniString f s "speedMaxCut"  "0";
laa.speedStart     = array laa.vDim scall GetIniString f s "speedStart"   "0";
i = 0; for laa.vDim
{
  laa.dAcdrPFac[i]   = GetStkObj i scall GetIniString f s "dAcdrPFac"    "0";
  laa.dAcdrIFac[i]   = GetStkObj i scall GetIniString f s "dAcdrIFac"    "0";
  laa.dAcdrDFac[i]   = GetStkObj i scall GetIniString f s "dAcdrDFac"    "0";
  laa.dAcdrD2Fac[i]  = GetStkObj i scall GetIniString f s "dAcdrD2Fac"   "0";
  laa.AcdDeadTime[i] = GetStkObj i scall GetIniString f s "AcdDeadTime"  "0";
  i = i + 1;
}

MtApp.xml.!E!MtDrv.!E!laa._Mt2Xml laa;

////////////////////////////////////////////////////////////////////////
// FAG data
////////////////////////////////////////////////////////////////////////

local "ifag" "sfag" "nfag" "gmask" "vDimOc" "vDimMc";
gmask = scall GetIniString f "FAG" "fagmask" "0";
MtApp.xml.MtDrv.!E!fagMask._Mt2Xml gmask;
MtApp.xml.MtDrv.!E!fagMaskPrv._Mt2Xml gmask;
nfag = 0;  // calculate # of axix groups
for 8
{
  if(gmask and (-1 << nfag)) {nfag = nfag + 1;}
}
MtApp.xml.MtDrv.!E!nFag._Mt2Xml nfag;

ifag = 0; for nfag
{
  if(gmask and 1 << ifag)
  {
    sfag      = sprint "fag%d" ifag;
    vDimOc = vDimMc = 0;
    vDimOc = array 2 GetStkObj 0 scall GetIniString f sfag "vDimOcMc" " 3 3";
    vDimMc = array 2 GetStkObj 1 scall GetIniString f sfag "vDimOcMc" " 3 3";
    // create reference to global class name for sfag data
    (scall "C" & sfag & "Init") = ObjDef T_classDscr  // create global class CFag<i>Init
    "vDimOc"   T_long   0
    "vDimMc"   T_long   0
    "vDimMcPrv" T_long   0
    "laaMapping" T_long  vDimMc
    "cmdBufSize" T_long 2

    "S1_Vel"   T_double 0    // PTC members
    "T1_Vel"   T_double 0
    "S1_Acc"   T_double 0
    "T1_Acc"   T_double 0
    "McLimMin" T_double vDimMc
    "McLimMax" T_double vDimMc
    "CheckSwLim" T_long vDimMc
    "manVMin"  T_double vDimMc
    "manVMax"  T_double vDimMc
    "vMax"     T_double 0
    "vMaxV"    T_double vDimMc
    "accMax"   T_double 0
    "accMaxV"  T_double vDimMc
    "speedJerkTol"  T_double 0
    "speedJerkTolV" T_double vDimMc
    ;
    local "fag";
    fag => (scall sfag) = gcnew (scall "C" & sfag & "Init");   // create global object fag<i>
    fag.vDimOc = vDimOc;
    fag.vDimMc = fag.vDimMcPrv = vDimMc;
    fag.laaMapping = array vDimMc scall GetIniString f sfag "LaI" " 0 1 2 3 4 5 6 7";
    fag.cmdBufSize = array 2 scall GetIniString f sfag "cmdBufSize" "0 0";
    fag.S1_Vel = scall GetIniString f sfag "S1_Vel" "1";
    fag.T1_Vel = scall GetIniString f sfag "T1_Vel" "1";
    fag.S1_Acc = scall GetIniString f sfag "S1_Acc" "1";
    fag.T1_Acc = scall GetIniString f sfag "T1_Acc" "1";
    fag.vMax = scall GetIniString f sfag "vMax" "";
    fag.accMax = scall GetIniString f sfag "accMax" "";
    fag.SpeedJerkTol = scall GetIniString f sfag "SpeedJerkTol" "";
    fag.CheckSwLim = array vDimMc scall GetIniString f sfag "CheckSwLim" "3 3 3 3 3 3 3 3";
    i = 0; for vDimMc
    {
      fag.McLimMin[i] = GetStkObj i scall GetIniString f sfag "McLimMin" "";
      fag.McLimMax[i] = GetStkObj i scall GetIniString f sfag "McLimMax" "";
      fag.vMaxV[i] = GetStkObj i scall GetIniString f sfag "vMaxV" "";
      fag.accMaxV[i] = GetStkObj i scall GetIniString f sfag "accMaxV" "";
      fag.SpeedJerkTolV[i] = GetStkObj i scall GetIniString f sfag "SpeedJerkTolV" "";
      fag.manVMin[i] = GetStkObj i scall GetIniString f sfag "manVMin" "";
      fag.manVMax[i] = GetStkObj i scall GetIniString f sfag "manVMax" "";
      i = i + 1;
    }
    (MtApp.xml.MtDrv.("!E!" & sfag))._Mt2Xml scall sfag;
  }
  ifag = ifag + 1;
}

////////////////////////////////////////////////////////////////////////
// IO data
////////////////////////////////////////////////////////////////////////

f = IniFileMt;

local "io" "errMsg" "n" "vi" "li";
errMsg = "exit \"Missing Entry\"";

io => MtApp.xml.!E!Hardware.!E!Inputs;                 // Hardware.Inputs
s = "IoInputs";
n = scall GetIniString f s "n" errMsg;
io.!A!nData sprint "%d" n;
i = 1; for n
{
  li = GetIniString f s (sprint "%d" i) errMsg;
  vi = sprint "!E!v%d" i;
  (io.(vi)).!A!Type      GetStkObj 0 scall li;
  (io.(vi)).!A!Address  sprint "0x%.4X" GetStkObj 1 scall li;
  (io.(vi)).!A!Mask  sprint "0x%.4X" GetStkObj 2 scall li;
  (io.(vi)).!A!InvMask  sprint "0x%.4X" GetStkObj 3 scall li;
  (io.(vi)).!A!ByteSize sprint "%d" GetStkObj 4 scall li;
  (io.(vi)).!A!MapMode  sprint "%d" GetStkObj 5 scall li;
  (io.(vi)).!A!IoMode   sprint "%d" GetStkObj 6 scall li;
  (io.(vi)).!A!LabelText GetStkObj 7 scall li;
  i = i + 1;
}

io => MtApp.xml.Hardware.!E!Outputs;                 // Hardware.Outputs
s = "IoOutputs";
n = scall GetIniString f s "n" errMsg;
io.!A!nData sprint "%d" n;
i = 1; for n
{
  li = GetIniString f s (sprint "%d" i) errMsg;
  vi = sprint "!E!v%d" i;
  (io.(vi)).!A!Type      GetStkObj 0 scall li;
  (io.(vi)).!A!Address  sprint "0x%.4X" GetStkObj 1 scall li;
  (io.(vi)).!A!Mask  sprint "0x%.4X" GetStkObj 2 scall li;
  (io.(vi)).!A!InvMask  sprint "0x%.4X" GetStkObj 3 scall li;
  (io.(vi)).!A!ByteSize sprint "%d" GetStkObj 4 scall li;
  (io.(vi)).!A!MapMode  sprint "%d" GetStkObj 5 scall li;
  (io.(vi)).!A!IoMode   sprint "%d" GetStkObj 6 scall li;
  (io.(vi)).!A!LabelText GetStkObj 7 scall li;
  i = i + 1;
}

////////////////////////////////////////////////////////////////////////
// Application
////////////////////////////////////////////////////////////////////////

f = IniFileMt;

MtApp.xml.Application.!A!Version "2.0";
MtApp.xml.Application.!A!Language GetIniString f "Mtasc" "LangChar" "E";
MtApp.xml.Hardware.!A!AdrJog      GetIniString f "Mtasc" "AdrJog" "0x0";
MtApp.xml.Application.!A!MhelAng0To2PiFlag GetIniString f "Mtasc" "MhelAng0To2PiFlag" "0";
MtApp.xml.Application.!A!TrExtendedMax GetIniString f "Mtasc" "MtExtended" "0";
MtApp.xml.Application.!A!ExecSys0 GetIniString f "Mtasc" "ExecSys0" "";
MtApp.xml.Application.!A!ExecSys1 GetIniString f "Mtasc" "ExecSys1" "";

////////////////////////////////////////////////////////////////////////
// Application.AxisGroup<i>
////////////////////////////////////////////////////////////////////////

f = IniFileMt;

local "ag" "iag" "sag" "bd";

iag = 0; for nfag
{
  if(gmask and 1 << iag)
  {
    s   = sprint "AGI%d" iag;
    sag = sprint "!E!AxisGroup%d" iag;
    ag => MtApp.xml.Application.(sag);
    ag.!A!AxisLett GetIniString f s "AxisLett" "XYZA";        // generic data
    ag.!A!MaType   GetIniString f s "maType" "1";

    ag.!A!UnitIndex  GetIniString f s "UnitIndex" "0";
    ag.!A!UnitMM     GetIniString f s "UnitMM" "mm";
    ag.!A!UnitSec    GetIniString f s "UnitSec" "sec";
    ag.!A!UnitDeg    GetIniString f s "UnitDeg" "�";
    ag.!A!UnitVel    GetIniString f s "UnitVel" "mm/sec";
    ag.!A!UnitAcc    GetIniString f s "UnitAcc" "mm/sec�";

    ag.!E!mp_f._Mt2Xml array vDimMc scall GetIniString IniFileMt (sprint "AGI%d" iag) "SpeedPosi" "";
    ag.!E!mp_r._Mt2Xml array vDimMc scall GetIniString IniFileMt (sprint "AGI%d" iag) "AccelPosi" "";
    ag.!E!mf_f._Mt2Xml scall GetIniString f s "SpeedFast" "50.0";
    ag.!E!mf_r._Mt2Xml scall GetIniString f s "AccelFast" "50.0";
    ag.!E!ml_f._Mt2Xml scall GetIniString f s "SpeedSlow" "10.0";
    ag.!E!ml_r._Mt2Xml scall GetIniString f s "AccelSlow" "10.0";
    ag.!E!mld_f._Mt2Xml scall GetIniString f s "SpeedDown" "2.0";
    ag.!E!mld_r._Mt2Xml scall GetIniString f s "AccelDown" "2.0";

    ag.!A!MirrX    GetIniString f s "MirrX" "0.0";
    ag.!A!MirrMode GetIniString f s "MirrMode" "2";

    bd = array vDimMc 0.0;   // buffer for doubles
    // ----------------------------------------------------------- referencing
    i = 0; for vDimMc
    {
      bd[i] = GetStkObj i scall GetIniString f s "RefDir" errMsg;
      if((bd[i] != 1.0) and (bd[i] != -1.0)) {exit "Illegal Entry in RefDir";}
    }
    ag.!E!Ref.!E!Dir._Mt2Xml bd;
    i = 0; for vDimMc
    {
      bd[i] = GetStkObj i scall GetIniString f s "RefDist" errMsg;
    }
    ag.!E!Ref.!E!Dist._Mt2Xml bd;
    i = 0; for vDimMc
    {
      bd[i] = GetStkObj i scall GetIniString f s "RefDistCorr" errMsg;
      i = i + 1;
    }
    ag.!E!Ref.!E!DistCorr._Mt2Xml bd;
    ag.!E!Ref.!E!IInput._Mt2Xml array vDimMc scall GetIniString f s "iRefInput" "1 2 3 4 5 6 7 8";
    i = 0; for vDimMc
    {
      bd[i] = (array vDimMc scall GetIniString f s "RefSpeedFast" errMsg)[i];
      i = i + 1;
    }
    ag.!E!Ref.!E!SpeedFast._Mt2Xml bd;
    i = 0; for vDimMc
    {
      bd[i] = (array vDimMc scall GetIniString f s "RefSpeedSlow" errMsg)[i];
      i = i + 1;
    }
    ag.!E!Ref.!E!SpeedSlow._Mt2Xml bd;
    ag.!E!Ref.!E!Sequence.!A!v0 GetIniString f s "RefSequence" "XYZABCUV";
    ag.!E!Ref.!E!Mask._Mt2Xml scall GetIniString f s "RefMask" errMsg;;
    // ----------------------------------------------------------- referencing: search index
    i = 0; for vDimMc
    {
      bd[i] = GetStkObj i scall GetIniString f s "IdxDist0" errMsg;
      i = i + 1;
    }
    ag.!E!Ref.!E!IdxDist0._Mt2Xml bd;
    i = 0; for vDimMc
    {
      bd[i] = GetStkObj i scall GetIniString f s "IdxDist1" errMsg;
      i = i + 1;
    }
    ag.!E!Ref.!E!IdxDist1._Mt2Xml bd;
    ag.!E!Ref.!E!IIdxInput._Mt2Xml array vDimMc scall GetIniString f s "iIdxInput" "1 2 3 4 5 6 7 8";
    ag.!E!Ref.!E!IdxMask._Mt2Xml scall GetIniString f s "IdxMask" errMsg;;

    // ----------------------------------------------------------- GCode
    ag.!E!GCode.!A!IJKMode GetIniString f s "IJKMode" "R";
    ag.!E!GCode.!A!FFac    GetIniString f s "FFac" "1.0";
    ag.!E!GCode.!A!IJKTol  GetIniString f s "IJKTol" "0.001";
    ag.!E!GCode.!A!XYZFac    GetIniString f s "XYZFac" "1.0";
    ag.!E!GCode.!A!iMap17    GetIniString f s "iMap17" "0 1 2";
    ag.!E!GCode.!A!iMap18    GetIniString f s "iMap18" "0 2 1";
    ag.!E!GCode.!A!iMap19    GetIniString f s "iMap19" "1 2 0";
    ag.!E!GCode.!A!CrcUseFirstCmdAsEntry GetIniString f s "crc.useFirstCmdAsEntry" "0";
    ag.!E!GCode.!A!CrcCloseByG40 GetIniString f s "crcCloseByG40" "0";
    ag.!E!GCode.!A!HelixMode     GetIniString f s "HelixMode" "0";
    ag.!E!GCode.!A!BlockSkip     GetIniString f s "BlockSkip" "1";
    ag.!E!GCode.!A!subProgNMax   GetIniString f s "subProgNMax" "20";
    ag.!E!GCode.!A!subProgMarker GetIniString f s "subProgMarker" "O";  // Default: letter 'O', not zero
    ag.!E!GCode.!A!subProgCaller GetIniString f s "subProgCaller" "";   // not used?
  }
  iag = iag + 1;
}

////////////////////////////////////////////////////////////////////////
// Save data
////////////////////////////////////////////////////////////////////////

MtApp.xml._Save;

MsgBox "Driver Data imported from Mtdrv.ini and Mtasc.ini" "Ini2Xml.ts" MB_OK;
