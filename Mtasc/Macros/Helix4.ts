/*====================================================================*/
/* MCPA: Helix-Aufruf mit Bewegung der A-Achse.                       */
/* Das Makro erwartet folgende Parameter:                             */
/* 1. Anfangspunkt  (X1, Y1, Z1, A1)                                  */
/* 2. Zwischenpunkt (X2, Y2, Z2, A2)    [Z wird nicht verwendet]      */
/* 3. Endpunkt      (X3, Y3, Z3, A3)                                  */
/*====================================================================*/
(MacroDef "_RefTest") = "1;";

(Macrodef "AngFromMP") = 
"local \"Phi\" \"Dx\" \"Dy\";
 Dx = %2[0] - %1[0];
 Dy = %2[1] - %1[1];
 Phi = Acos Dx / Sqrt ((Dx * Dx) + (Dy * Dy));
 if (Dy < 0.0) {Phi = (2 * M_Pi) - Phi;}
 Phi / _d2r;";
 
(Macrodef "MCPA") = 
"local \"Det\" \"QF\" \"Cnt\" \"Hrad\" \"Ang1\" \"Ang2\" \"Ang3\";

 Det =       (%2[0] - %1[0]) * (%3[1] - %1[1]);
 Det = Det - (%3[0] - %1[0]) * (%2[1] - %1[1]); 
 
 if(Det != 0.0)                 
 {
  QF = Array 3 0.0;                   /* Zentrum berechnen            */
  QF[0] =         (%3[0] * %3[0]) - (%2[0] * %2[0]);  
  QF[0] = QF[0] + (%3[1] * %3[1]) - (%2[1] * %2[1]);  

  QF[1] =         (%1[0] * %1[0]) - (%3[0] * %3[0]);  
  QF[1] = QF[1] + (%1[1] * %1[1]) - (%3[1] * %3[1]);  

  QF[2] =         (%2[0] * %2[0]) - (%1[0] * %1[0]);  
  QF[2] = QF[2] + (%2[1] * %2[1]) - (%1[1] * %1[1]);  

  Cnt = Array 3 0.0;
  Cnt[0] = (%1[1] * Qf[0]) + (%2[1] * Qf[1]) + (%3[1] * Qf[2]);
  Cnt[1] = (%1[0] * Qf[0]) + (%2[0] * Qf[1]) + (%3[0] * Qf[2]);

  Cnt[0] =  0.5 * Cnt[0] / Det;
  Cnt[1] = -0.5 * Cnt[1] / Det;

  
  Mhel \"Cent\" (Cnt[0]) (Cnt[1]) 0;

  Hrad = (%1[0] - Cnt[0]) * (%1[0] - Cnt[0]);   /* Radius berechnen   */
  Hrad = Sqrt (Hrad + ((%1[1] - Cnt[1]) * (%1[1] - Cnt[1])));
  Mhel \"Rad\" Hrad Hrad;
 
  MDraw 2 (%1[0])  (%1[1])   0.0  0.5; 
  MDraw 2 (%2[0])  (%2[1])   0.0  0.5; 
  MDraw 2 (%3[0])  (%3[1])   0.0  0.5; 
  MDraw 2 (Cnt[0]) (Cnt[1])  0.0  0.5; 

  Ang1 = AngFromMP Cnt %1;            /* Anfangs-/Endwinkel berechnen */
  Ang2 = AngFromMP Cnt %2;
  Ang3 = AngFromMP Cnt %3;
  
  if(Ang2 < Ang1) {Ang2 = Ang2 + 360.0;} 
  if(Ang3 < Ang2) {Ang3 = Ang3 + 360.0;} 
  if(Ang3 < Ang2) {Ang3 = Ang3 + 360.0;} 
  if(Ang3 > Ang1 + 360.0) {Ang3 = Ang3 - 720.0;} 

  Mhel \"Ang\" Ang1 Ang3;
  Mhel \"Rot\" 0 0.0, 1 0.0;

  MhelAExec %1 %3;             /* 1. und 3. Punkt wegen Z,A �bergeben */
}
";

/* ================================================================== */
/* 3D Helix mit Z,A-Bewegung                                          */
/* ================================================================== */
(MacroDef "MhelAExec") =
"local  \"i\" \"m\" \"dx\" \"dy\" \"dz\" \"t\" \"p\";
 _mdDim = 4;
 dx = dy = dz = ARRAY _mdDim 0.0;
 dx[0] = 1.0;
 dy[1] = 1.0;
 dz[2] = %2[2] - %1[2];
 dz[3] = %2[3] - %1[3];

/*
? sprint \"A1%5.1lf A2=%5.1lf\" %1 %2;
*/

 m = GetM33R (MhelRotI[0]) (MhelRotA[0]);
 dx = M33Mul m dx;
 dy = M33Mul m dy;
 dz = M33Mul m dz;

 m = GetM33R (MhelRotI[1]) (MhelRotA[1]);
 hlxD.p0._ = M33Mul m dx;
 hlxD.dy._ = M33Mul m dy;
 hlxD.dz._ = M33Mul m dz;

/*
? sprint \"A1%5.1lf A2=%5.1lf A3=%5.1lf\" (hlxD.p0.c[0])  (hlxD.p0.c[1]) (hlxD.p0.c[2]);
? sprint \"A1%5.1lf A2=%5.1lf A3=%5.1lf\" (hlxD.p1.c[0])  (hlxD.p1.c[1]) (hlxD.p1.c[2]);
? sprint \"A1%5.1lf A2=%5.1lf A3=%5.1lf\" (hlxD.dy.c[0])  (hlxD.dy.c[1]) (hlxD.dy.c[2]);
? sprint \"A1%5.1lf A2=%5.1lf A3=%5.1lf\" (hlxD.dz.c[0])  (hlxD.dz.c[1]) (hlxD.dz.c[2]);
*/

 hlxD.ang0 = MhelAng[0];
 hlxD.ang1 = MhelAng[1];

/*
? sprint \"A0%8.3lf A1=%8.3lf\" hlxD.ang0 hlxD.ang1;
*/

 if(MhelAng[1] < MhelAng[0])
 {
  hlxD.dy._ = -1.0 * hlxD.dy._;
  hlxD.ang0 = -(MhelAng[0]);
  hlxD.ang1 = -(MhelAng[1]);
 }

 hlxD.cent._ = MhelCent;        /* cent               */
 hlxD.cent._[2] = %1[2];
 hlxD.cent._[3] = %1[3];
 hlxD.centFlag = 0;             /* centFlag: absolute */

/*
? sprint \"A1%5.1lf A2=%5.1lf A3=%5.1lf\" (MhelCent[0])  (MhelCent[1]) (MhelCent[2]);
*/

 hlxD.p0Flag = 1;               /* p0 is relative     */
 hlxD.p1Flag = 0;               /* Not Used           */
 hlxD.dyFlag = 1;               /* use dy as it is    */
 hlxD.dzFlag = 1;               /* use dz as it is    */
 
 hlxD.r0 = MhelRad[0];
 hlxD.r1 = MhelRad[1];
 hlxD.z0 = 0.0;
 hlxD.z1 = sqrt (dz[2] * dz[2]) + (dz[3] * dz[3]);
 
 _ocNext = hlxD.cent._;
 _ocNext = _ocNext + (hlxD.r0 * cos hlxD.ang0) * hlxD.p0._;
 _ocNext = _ocNext + (hlxD.r0 * sin hlxD.ang0) * hlxD.dy._;
 mf;
 _ocNext = hlxD.cent._ + hlxD.dz._;
 _ocNext = _ocNext + (hlxD.r0 * cos hlxD.ang1) * hlxD.p0._;
 _ocNext = _ocNext + (hlxD.r0 * sin hlxD.ang1) * hlxD.dy._;
 mh hlxD hlx;
";

