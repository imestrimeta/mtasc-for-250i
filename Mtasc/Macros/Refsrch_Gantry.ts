
/* ------------------------------------------------------------------ */
/* Search Reference Position                                          */
/* ------------------------------------------------------------------ */
/* Parameters for RefSearch macro:
   1: Axis number (0=X)
   2: Input Descriptor
   3: Inverting / Non inverting
   4: direction for driving to reference switch
   5: Offset from reference switch before searching zero pulse
   6: for LS driver only
*/
 
// JG 08/04/2005: nur zum Test ... 
// msgbox sprint "Refmask=%X" refmask;  

if(0 < Mtrans "Level")                /* erst Verschiebung abschalten */ 
{
  mreset GetIniTxt "RefSrch" "MsgMtransOff";
}    

local "CallOnExit" "RefSequence";                      

CallOnExit = "@mreset;"                  /* current values to restore */
   & (sprint " SetMc2Oc %d ;" _iMc2Oc)
   & (sprint " rmpMode '%c';" (rmpMode '?'))
   & (sprint " _Ovr  =  %d ; _dspUpdate;" _Ovr)
   & (sprint " _mode =  %d ;" _mode + 0)
   & "_drvHwLimWatch 1;"   // limit switch watching on after referencing
;

_mode = 'X';

DrvBeforeRef;                            /* defined in RefMacros.ts   */
 
if(0 == _Test AND 16)                    /* skip referencing          */
{                                        /* test mode(s)              */
  local "spf" "sps" "i" "j"
        "RefDir" "RefDist" "IdxDist0" "IdxDist1" "refi" "idxi";
  
  SetMc2Oc 0 0;                          // set machine coordinates without tool offset
  rmpMode 'Y';                           /* Look ahead off            */
  _ovr = 100; _dspUpdate;                /* set Override to 100%      */

  RefMask = RefMask and GetXmlAppGrp.Ref.Mask._Xml2Mt;
  RefDir   = GetXmlAppGrp.Ref.Dir._Xml2Mt;
  RefDist  = GetXmlAppGrp.Ref.Dist._Xml2Mt;
  refi     = GetXmlAppGrp.Ref.IInput._Xml2Mt;

  idxi     = GetXmlAppGrp.Ref.IIdxInput._Xml2Mt;
  IdxDist0 = GetXmlAppGrp.Ref.IdxDist0._Xml2Mt;
  IdxDist1 = GetXmlAppGrp.Ref.IdxDist1._Xml2Mt;

                                         /* get reference speed vector*/
  spf = GetXmlAppGrp.Ref.SpeedFast._Xml2Mt;
  sps = GetXmlAppGrp.Ref.SpeedSlow._Xml2Mt;

  i = 1; for(Thread_agi.vdim - 1)
  {
   if(spf[i] == 0) {spf[i] = spf[i - 1];}
   if(sps[i] == 0) {sps[i] = sps[i - 1];}
   RefDir[i] = (RefDir[i] >= 0) - (RefDir[i] < 0);  // must be +1 or -1
   i = i + 1;
  }

  RefSequence = (GetXmlAppGrp.Ref.Sequence.!A!v0) & "........";

  AxLink 1;           // X, A koppeln
  RefMask = 7;
  mlimit '0';                            /* limit watching off        */
  _drvHwLimWatch 0; 
  _mtStat.dirtyTrans = 1;                /* redraw limits             */

  i = 0; for(Thread_agi.vdim)                    
  {
    j = 0; for(Thread_agi.vdim)        /* referencing takes place here */
    {
      if((RefSequence[i] or 0x20) == ((Thread_agi.axislett[j]) or 0x20)) 
      {
        print sprint (GetIniTxt "RefSrch" "PrnRefSearch") Thread_agi.axislett[j];
        RefSearch j (_inpDsc[refi[j]]) (_inpDsc[idxi[j]])  (RefDir[j])
                    (IdxDist0[j])      (IdxDist1[j])       (RefDist[j]);
      }
      j = j + 1; 
    }
    print GetIniTxt "RefSrch" "PrnRefComplete";
    i = i + 1;
  }
   RefMask = 9;
   AxLink 0;           // X, A entkoppeln
   ? "X Referenzieren";
   j = 0;              // X alleine referenzieren
   RefSearch j (_inpDsc[refi[j]]) (_inpDsc[idxi[j]])  (RefDir[j])
                    (IdxDist0[j])      (IdxDist1[j])       (RefDist[j]);
   j = 3;              // A alleine referenzieren
   ? "A Referenzieren";
   RefSearch j (_inpDsc[refi[j]]) (_inpDsc[idxi[j]])  (RefDir[j])
                    (IdxDist0[j])      (IdxDist1[j])       (RefDist[j]);
   AxLink 1;           // X, A wieder koppeln
}

if(0 != _Test AND 16)                    /* instead of referencing    */
{ 
  RefDone = RefMask and drv.laa.amask;
  if(RefDone == 0) {RefDone = RefMask;}
  _ocNext = getmc;
  i = 0; for(Thread_agi.vdim)                 
  {
    if(RefDone and (ARRAY Thread_agi.vdim 1 2 4 8 16 32 64 128)[i])
     {_ocNext[i] = 0.0;}
    i = i + 1;
  }
  setmc;
}

mreset;

wait 0; DrvAfterRef;             // defined in RefMacros.ts

mlimit '1';                      // set limit watching
_mtStat.DirtyTrans = 1;          // signal limit change
