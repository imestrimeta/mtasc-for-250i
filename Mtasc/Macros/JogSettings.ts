/* ================================================================== */
/* Handrad/Joystick - Jog Wheel and Joystick settings                 */
/* ================================================================== */
(MacroDef "_jogTeach") = "0;";   /* Default macro: button not pressed */
(MacroDef "_JogAmask") = "0;";
(MacroDef "_JogFacI")  = "0;";

_AdrBsJog = scall MtApp.xml.Hardware.!A?AdrJog;   /* base addres; zero if not used     */
Thread_agi.fag.jog.Flag01S = '0'; 
_jogState = 0L;

if(0 != _AdrBsJog)
{  
  (MacroDef "_jogAmask") =
          "out (IoDscr \"\" (_AdrBsJog + 2) 0 0xFFL 1) 0xE4;
             (1 * 0 == in _iJogX)
           + (2 * 0 == in _iJogY)
           + (4 * 0 == in _iJogZ)
           + (8 * 0 == in _iJogA)";

  (MacroDef "_jogFacI") =
  "local \"i\"; i = 0;
   if (0 == in _iJog10)  {i = 1;}
   if (0 == in _iJog100) {i = 2;}
   i                  // return i
  ";

         /* nonzero if teach button is pressed */
  (MacroDef "_jogTeach") = "0 == in _iJogTeach;";  
  
         /* use data lines as inputs, Pins 14-17 as Vcc */
  out (IoDscr "" (_AdrBsJog + 2) 0 0xFFL 1) 0xE4;
  wait 0.1 0;
  in (IoDscr "" (_AdrBsJog + 0) 0 0xFFL 1);
  wait 0.1 0;
  // do 2. time, some LPT ports seem to need it...
  out (IoDscr "" (_AdrBsJog + 2) 0 0xFFL 1) 0xE4;

         /* Set all high because some ports do not support input mode */
  out (IoDscr "" (_AdrBsJog + 0) 0 0xFFL 1) 0xFF;

  local "i"; i = drv.laa.autoIo.n;
  drv.laa.autoIo.io[i].dscr = IoDscr "" (_AdrBsJog + 1) 0 0xFFL 1;
  drv.laa.autoIo.io[i].dscr.inv = 0x10;
  drv.laa.autoIo.n = drv.laa.autoIo.n + 1;
  drv.laa.autoIoWrite;

  i = drv.laa.counters.n = 0;                           // must be the 1. counter
  drv.laa.counters.c[i].flag = 2;                       // Encoder
  drv.laa.counters.c[i].iIo  = drv.laa.autoIo.n - 1;    // index to drv.laa.autoIo
  drv.laa.counters.c[i].IoMask[0] = 0x20L;
  drv.laa.counters.c[i].IoMask[1] = 0x10L;
  drv.laa.counters.c[i].cvDiv = 1;                      // divide counter
  drv.laa.counters.backgrFlag = 1;
  drv.laa.counters.n = drv.laa.counters.n + 1;
  drv.laa.countersWrite;

  _iJogX     = IoDscr "" (_AdrBsJog + 0) 0  0x01L 1;   
  _iJogY     = IoDscr "" (_AdrBsJog + 0) 0  0x02L 1;
  _iJogZ     = IoDscr "" (_AdrBsJog + 0) 0  0x04L 1;
  _iJogA     = IoDscr "" (_AdrBsJog + 0) 0  0x08L 1;
  _iJog1     = IoDscr "" (_AdrBsJog + 0) 0  0x10L 1;
  _iJog10    = IoDscr "" (_AdrBsJog + 0) 0  0x20L 1;
  _iJog100   = IoDscr "" (_AdrBsJog + 0) 0  0x40L 1;
  _iJogTeach = IoDscr "" (_AdrBsJog + 0) 0  0x80L 1;
}

/*--------------------------------------------------------------------*/
/* Joystick settings                                                  */
/*--------------------------------------------------------------------*/
CJoystick = ObjDef T_classDscr           /* Joystick class            */
"X"    T_Long  0                         /* x, y, z-values            */ 
"y"    T_Long  0    
"z"    T_Long  0    
"rX"   T_Long  0                         /* rotation values           */ 
"ry"   T_Long  0    
"rz"   T_Long  0    
"Sld"  T_Long  2                         /* slider values             */ 
"POV"  T_Long  4                         /* POV-values                */ 
"Btn"  T_Char 32                         /* Buttons '0' or '1'        */
"Stat" T_Long  0                         /* status                    */
"ax"   T_Long  FagVDimMax                  /* axis speed data           */
;

_Jst = ObjDef CJoystick;

(MacroDef "_JstTimer") = ""; /* called by timer when joystick is active */

