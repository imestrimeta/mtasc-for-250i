/* ------------------------------------------------------------------ */
/* Pumpenansteuerung initialisieren                                   */
/* ------------------------------------------------------------------ */

drv.laa.speedflag = 7; drv.laa.speedWrite;  /* nur xYZ f�r interpolierten Betrieb */

_oSpeedSet  = IoDscr "LAA" 0x0303  0 -1 4;  // type 3: speed, address 3: axis index

PmpFac = 0.0;  /* aktueller Pumpenfaktor */

/* ---------------------------------------------- */
/* Pumpenfaktor setzen                            */
/* ---------------------------------------------- */
/* %1: Faktor Bahngeschw. -> Wertebereich 0.0-1.0 */
/* 0.0: Pumpe steht                               */
/* 1.0 Pumpe dreht mit vMax von Pumpe.ini         */
(MacroDef "PmpSetFac") =
"PmpFac = %1 + 0.0;
 (drv.fag 0).tabxWrite 2 0 (PmpFac / (drv.fag 0).ptc.ivFac);
";


/* ------------------------------------------------------- */
/* Hilfsfunktion ini-Datei lesen                           */
/* ------------------------------------------------------- */
(MacroDef "PmpGetVal") =
"0L + scall GetIniString (MtApp.path & \"/Pumpe.ini\") \"Energy\" %1 \"\";";

(drv.fag 0).tabxWrite 1 0   0x10L;   /* table type */
(drv.fag 0).tabxWrite 1 1   0x10L;   /* tabiOfs */
(drv.fag 0).tabxWrite 1 2   0x10L;   /* tabiMin */
(drv.fag 0).tabxWrite 1 3   0x11L;   /* tabiMax */

/* ------------------------------------------------------- */
/* Wertetabelle initialisieren                             */
/* ------------------------------------------------------- */
(MacroDef "PmpInit") =
"
 local \"tab\";
 (drv.fag 0).tabxWrite 1 4 (PmpGetVal \"vMin\");    /* tabvMin */
 (drv.fag 0).tabxWrite 1 5 (PmpGetVal \"vMax\");    /* tabvMax */
 PmpSetFac PmpGetVal \"iFac\";                   /* tabiFac */
 (drv.fag 0).tabxWrite 2 1 (PmpGetVal \"vFac\");    /* tabvFac */
 (drv.fag 0).tabxWrite 2 2 (PmpGetVal \"vOfs\");    /* tabvOfs */
 (drv.fag 0).tabxWrite 2 0x10L (PmpGetVal \"vMin\") + 0.0;
 (drv.fag 0).tabxWrite 2 0x11L (PmpGetVal \"vMax\") + 0.0;
";

(drv.fag 0).outSpeed.flag01 = '0';
drv.laa.ioDTab.set 8 _oSpeedSet;     /* use driver's dsc table index 8 */
(drv.fag 0).outSpeed.adr = 8;
(drv.fag 0).outSpeedWrite;

/* ---------------------------------------------- */
/* Kopplung Pumpengeschw. - Bahngeschw.           */
/* ---------------------------------------------- */
/* %1: 1=EIN, 0=AUS                               */
(MacroDef "PmpStat") =
"if(%1)
 {
  (drv.fag 0).outSpeed.flag01 = '2'; (drv.fag 0).outSpeedWrite;
 }
 if(%1 == 0)
 {
  (drv.fag 0).outSpeed.flag01 = '0'; (drv.fag 0).outSpeedWrite;
  out _oSpeedSet 0;
 }
";

/* ---------------------------------------------- */
/* Pumpe zur�ckdrehen                             */
/* ---------------------------------------------- */
/* %1: Geschw.        */
/* %2: Zeit in sek    */
(MacroDef "PmpBack") =
"out _oSpeedSet -1 * %1;
 wait %2 0;
 out _oSpeedSet 0;
";

PmpInit;               /* initialisieren */
PmpStat 0;             /* Kopplung AUS   */

/* ------------------------------------------------- */
/* Pumpenmakros f�r PrimCAM                          */
/* _Ext0 val : Dosiernadel ausmessen mit prov.       */
/*             Werkzeugl�nge [um]                    */
/*                                                   */ 
/* _Ext1 0   : Dosierventil aus                      */
/*       1   : Dosierventil ein                      */
/*                                                   */ 
/* _Ext2 0   : Dosiermotor aus                       */
/*       1   : Dosiermotor ein konst.                */
/*       2   : Dosiermotor ein proport. v(Bahn)      */
/*       3   : Dosiermotor kurz vor/r�ckdrehen       */
/*                                                   */ 
/* _Ext3 val : Dosiermotor Drehzahl vorw�hlen [U/min]*/
/*                                                   */ 
/* _Ext4 val : Dosiermotor Zeit vorw�hlen [s]        */ 
/*                                                   */ 
/* _Ext5 val : Bahngeschwindigkeit f�r Pumpenfaktor- */
/*             berechnung angeben [mm/min]           */ 
/* ------------------------------------------------- */

/* Dosiermotor Drehzahl [dig. 0-5000; 100 entspricht 240 U/min = 4 U/s ] */
pmpspeed = 0;

pmptime = 0.8;

bahnfeed = 10.0;   /* Bahngeschwindigkeit [mm/s] */



(MacroDef "_Ext0") = 
"ToolLen = %1 / 1000.0; fcall MtApp.path & \"/ToolRefZ.ts\";";


(MacroDef "_Ext1") = 
"if(%1 != 0){outd (_outDsc[13]) -1;}
 if(%1 == 0){outd (_outDsc[13])  0;}
";

_Ext1 0;                /* Dosierventil aus */



(MacroDef "_Ext2") = 
"if(%1 == 0){PmpSetFac 0.0;PmpStat 0;}
 if(%1 == 1){PmpStat 0; out _oSpeedSet pmpspeed;}
 if(%1 == 2){PmpStat 1;PmpSetFac (pmpspeed * 0.0002 / bahnfeed)}               /* pmpfact = pmpspeed / (5000 * bahnfeed[mm/s])) */
 if(%1 == 3){PmpStat 0; PmpSetFac 1.0; PmpBack pmpspeed pmptime;}
";


(MacroDef "_Ext3") = "pmpspeed = %1 / 2.4;";

(MacroDef "_Ext4") = "pmptime = %1;";  /* Pumpenzeit [s] */


(MacroDef "_Ext5") = "bahnfeed = %1 / 60.0;";  /* Bahngeschwindigkeit f�r Berechnung */

