/* ================================================================== */
/* ShapeMacros: MCirc, MRect, DrawCross, DrawDrillCirc                */
/* MCirExec, Spline, Helix                                            */ 
/* ================================================================== */
(MacroDef "MCirc") =
"z zup; mf;
 x %1 + %3; y %2; mf; z zdn; mld;
 x %1 - %3; mc %3 \"+LZ\" %3;
 x %1 + %3; mc;
 z zup; mf;
";

(MacroDef "MRect") =
"z zup; mf;
 x %1 + %3 / 2.0; y %2 - %4 / 2.0; mf; z zdn; mld;
 y %2 + %4 / 2.0; ml;
 x %1 - %3 / 2.0; ml;
 y %2 - %4 / 2.0; ml;
 x %1 + %3 / 2.0; ml;
 z zup; mf;
";

(MacroDef "DrawCross") = 
"local \"d\" \"nc\" \"gr\"; d = %3 / 2.0;
 nc = _ncMode; _ncMode = 0;
 gr = _grMode; _grMode = 1;
 MtransFlag01 = '0'; mreset;
 x %1 - d; y %2;     mfHide;
 x %1 + d;           ml;
 x %1;     y %2 - d; mfHide;
 y %2 + d;           ml;
 _ncMode = nc; _grMode = gr;
 MtransFlag01 = '1'; mreset;
";

/* ------------------------------------------------------------------ */
/* Bohrkreis zeichnen                                                 */
/* ------------------------------------------------------------------ */
(MacroDef "DrawDrillCirc") = 
"if(_Mode == 'G')
 { local \"CallOnExit\";                   /* Werte wiederherstellen  */
   CallOnExit = (sprint \"@_ncMode    = %d;  \" _ncMode)
              & (sprint \" _grMode    = %d;  \" _grMode)
              & (sprint \" Thread_agi.g3.search.g3sFlag = %d;\" Thread_agi.g3.search.g3sFlag + 0)
              & (       \" _Oc = OldOc; _OcNext = OldOn;\")
   ;

   if(%P > 3)                              /* ggf. Farbe �ndern       */
   {  
     SetDrawPar 'L' %4 -1 -1 -1; MvPar;
     CallOnExit = CallOnExit & \" SetDrawPar 'L' Tools[_toolSelEmu].Color -1 -1 -1; MvPar;\";
   }

   _ncMode = _ncMode AND -3;               /* Zeitz�hlung abschalten  */
   _grMode = 1;
   Thread_agi.g3.search.g3sFlag = -1;             // cmd counting off

   local \"OldOc\"; OldOc = _Oc;
   local \"OldON\"; OldON = _OcNext;
   x %1 + %3; y %2; MfHide;                /* unsichtbar zum Rand     */
   x %1 - %3; MC %3 \"+LZ\";               /* 2 Halbkreise            */
   x %1 + %3; MC %3 \"+LZ\";
   _OcNext = OldOc; MfHide;                /* zur�ck zum Ausgangspkt. */
 }
";       

/* ------------------------------------------------------------------ */
/* Circle macros                                                      */
/* ------------------------------------------------------------------ */
(MacroDef "McirExec") = 
"local \"_t\" \"_p\";
 McirInterpol;
 _t = 0.0;
 if(_MOpen){MCrv.carry = %1;}
 _p = McirGet _t MCrv.carry;

 if(_MOpen){
  wait 0; X _p[0]; Y _p[1]; Z _p[2]; A _p[3]; mf;
  _MOpen = 0;
 }

 while \"_t < 1.0\" {
  _p = McirGet _t %1;
  X _p[0]; Y _p[1]; Z _p[2]; A _p[3]; ml;

  if(_mode == 'G') {
   _g3Push 0x808080L;
   MspDrPt 0.3;
   _g3Pop;
  }
 }

 if(_MClose){
  _p = McirGet _t;
  X _p[0]; Y _p[1]; Z _p[2]; A _p[3]; ml; wait 0;
  _MClose = 0;
 }
 if(_mode == 'G') {
  _g3Push 0xFF0000L;
  x MCrv.c0[0]; y MCrv.c0[1]; MspDrPt 0.4;
  x MCrv.c1[0]; y MCrv.c1[1]; MspDrPt 0.4;
  x MCrv.c2[0]; y MCrv.c2[1]; MspDrPt 0.4;
  _g3Pop;
 }
";

/* ------------------------------------------------------------------ */
/* Spline macros                                                      */
/* ------------------------------------------------------------------ */
MCrv.dim = 3;
_MOpen   = _MClose = 0;
MspDist  = 2.5;

(MacroDef "MspInit") = "MCrv.n  = 0;";
(MacroDef "MOpen")   = "_MOpen  = 1;";       /* "Neue Kurve beginnen" */
(MacroDef "MClose")  = "_MClose = 1;";       /* "Kurve abschlie�en"   */

(MacroDef "MspAdd")  =                       /* "Punkt hinzuf�gen"    */  
"local \"i\"; i = 0;
 MCrv.c0[i + MCrv.dim * MCrv.n] = %1; i = i + 1;
 MCrv.c0[i + MCrv.dim * MCrv.n] = %2; i = i + 1;
 if(i < MCrv.dim){MCrv.c0[i + MCrv.dim * MCrv.n] = 0.0;}
 if(i < %P){
  MCrv.c0[i + MCrv.dim * MCrv.n] = %3; i = i + 1;
 }
 if(i < MCrv.dim){MCrv.c0[i + MCrv.dim * MCrv.n] = 0.0;}
 if(i < %P){
  MCrv.c0[i + MCrv.dim * MCrv.n] = %4; i = i + 1;
 }
 if(i < MCrv.dim){MCrv.c0[i + MCrv.dim * MCrv.n] = 0.0;}
 if(i < %P){
  MCrv.c0[i + MCrv.dim * MCrv.n] = %5; i = i + 1;
 }
 if(i < MCrv.dim){MCrv.c0[i + MCrv.dim * MCrv.n] = 0.0;}
 if(i < %P){
  MCrv.c0[i + MCrv.dim * MCrv.n] = %6; i = i + 1;
 }
 MCrv.n = MCrv.n + 1;
";

(MacroDef "MspExec") =                       /* ausf�hren             */
"local \"_t\" \"_p\";
 MspInterpol;
 _t = 0.0;
 if(_MOpen){MCrv.carry = %1;}
 _p = MspGet _t MCrv.carry;

 if(_MOpen) {wait 0; _ocNext = _p; mf; _Mopen = 0;}

 while \"_t < MCrv.n - 1\" 
 {
   _p = MspGet _t %1;
   _ocNext = _p; ml;

   if(_mode == 'G') 
   {
    _g3Push 0x808080L;
    MspDrPt 0.3;
    _g3Pop;
  }
 }

 if(_MClose) {_p = MspGet _t; _ocNext = _p; ml; wait 0; _MClose = 0;}

 if(_mode == 'G')
 {
   _t = 0.0;
   _g3Push 0xFF0000L;
   for(MCrv.n) 
   {
    _p = MspGet _t;
    _ocNext = _p; MspDrPt 0.4;
    _t = _t + 1;
   }
   _g3Pop;
 }
 ";

(MacroDef "MspDrPt") = 
"_grMode = 0; x x + %1; ml;
 _grMode = 1; x x - 2 * %1;  mc %1 \"+RZ\";
 x  x + 2 * %1;  mc;
";

(MacroDef "Goto") = "X %1; Y %2; mf;";

/* ------------------------------------------------------------------ */
/* 3D Helix macros   dimension see AgiInit.ts                         */
/* ------------------------------------------------------------------ */

CMhel = ObjDef T_classDscr               /* helix data                */
"cent"  T_double   3
"ang"   T_double   2
"rad"   T_double   2
"dz"    T_double   0
"rotI"  T_short    2
"rotA"  T_double   2
"hd"    CHelixDat  0
"h"     T_object   0                     /* for CMObjHelix            */
;

(MacroDef "Mhel") =
"if(%1 == \"Cent\"){MhelD.Cent[0] = %2; MhelD.Cent[1] = %3; MhelD.Cent[2] = %4;}
 if(%1 == \"Ang\") {MhelD.Ang[0]  = %2 * _d2r; MhelD.Ang[1] = %3 * _d2r;}
 if(%1 == \"Rad\") {MhelD.Rad[0]  = %2; MhelD.Rad[1] = %3;}
 if(%1 == \"Dz\"){MhelD.Dz = %2;}
 if(%1 == \"Rot\")
 {MhelD.RotI[0] = %2; MhelD.RotA[0] = %3 * _d2r;
  MhelD.RotI[1] = %4; MhelD.RotA[1] = %5 * _d2r;}
";

(MacroDef "MOpen")  = "_MOpen  = 1;";        /* "Neue Kurve beginnen" */
(MacroDef "MClose") = "_MClose = 1;";        /* "Kurve abschlie�en"   */

(MacroDef "MhelExec") =
"local  \"i\" \"m\" \"dx\" \"dy\" \"dz\" \"t\" \"p\";
 dx = dy = dz = ARRAY MhelD.hd.dim 0.0;
 dx[0] = 1.0;
 dy[1] = 1.0;
 dz[2] = 1.0;

 m = GetM33R (MhelD.RotI[0]) (MhelD.RotA[0]);
 dx = M33Mul m dx;
 dy = M33Mul m dy;
 dz = M33Mul m dz;

 m = GetM33R (MhelD.RotI[1]) (MhelD.RotA[1]);
 MhelD.hd.p0._ = M33Mul m dx;
 MhelD.hd.dy._ = M33Mul m dy;
 MhelD.hd.dz._ = M33Mul m dz;

 MhelD.hd.ang0 = MhelD.Ang[0];
 MhelD.hd.ang1 = MhelD.Ang[1];
 if(MhelD.Ang[1] < MhelD.Ang[0])
 {
  MhelD.hd.dy._ = -1.0 * MhelD.hd.dy._;
  MhelD.hd.ang1 = -(MhelD.Ang[1]);
 }

 MhelD.hd.p0Flag = 1;		/* p0 is relative     */
 MhelD.hd.cent._ = MhelD.Cent;	/* cent               */
 MhelD.hd.centFlag = 0;		/* centFlag: absolute */
 MhelD.hd.p1Flag = 0;		/* Not Used           */
 MhelD.hd.dyFlag = 1;		/* use dy as it is    */
 MhelD.hd.dzFlag = 2;		/* get dz from z0, z1 */
 MhelD.hd.r0 = MhelD.Rad[0];
 MhelD.hd.r1 = MhelD.Rad[1];
 MhelD.hd.z0 = 0.0;
 MhelD.hd.z1 = (MhelD.Dz * (MhelD.hd.ang1 - MhelD.hd.ang0)) / (2 * M_PI);
 (*MhelD.h).getFrom MhelD.hd;
 _ocNext =  (*MhelD.h).<0>.getP 0.0; mf;
 Thread_agi.mact.mfl.mType = 1;
 Thread_agi.mact.exec  *MhelD.h;
";
