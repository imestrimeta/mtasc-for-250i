/*====================================================================*/
/* Get tool no. _ToolSel                                              */
/* (Mode "_ToolAuto" must be set from EdiTasc.Ini)                    */
/*====================================================================*/
if(0 == _ncMode AND 1)                     /* invalid call            */
 {mreset GetIniTxt "GetTool" "Err007";}

if((0 == _RefTest) AND (_ToolAuto > 0))
 {mreset GetIniTxt "GetTool" "Err004";}    /* do referencing first    */

if((_ToolNc == 0) and (_tlch.AllowT0 == 0))/* Invalid tool number     */
 {mreset sprint (GetIniTxt "GetTool" "Err002") 0L;}

/*--------------------------------------------------------------------*/
/* Local changes                                                      */
/*--------------------------------------------------------------------*/
local "imo" "to" "CallOnExit" "CallOnError";
imo = _iMc2Oc;               // use this origin for destination
to  = mtag[mtagI].tofsUsed;  // usage of tool offset
CallOnExit = (sprint "@MF_F = %lf; ML_F = %lf; MvPar; _Plc.DirtyFlags.Tool = 1;" MF_F ML_F);
CallOnError = CallOnExit & "SetMc2Oc imo to;";

local "OldLimit" "OldPos"; OldLimit = Mlimit '?';   /* Save current limit flag */
OldPos = GetOc;
SetMc2Oc 0 0;         // set machine coordinates without tool offset

local "AutoCode"; AutoCode = _ToolAuto;    /* Skip parts in test mode */
if((_ToolAuto == 2) AND (0 != _Test AND 2)) {AutoCode = 1;}
if((_ToolAuto == 3) AND (0 != _Test AND 4)) {AutoCode = 2;}
if((_ToolAuto >= 1) AND (0 != _Test AND 8)) {AutoCode = 0;}

/*====================================================================*/
/* ToolAuto = 0: Ignore tool change                                   */
/*====================================================================*/
if(AutoCode == 0) 
 {_toolNc = _toolSel; mcall *_tlch.SaveToolNc;} // save new tool to file

/*====================================================================*/
/* ToolAuto = 1: Change tool manually                                 */
/*====================================================================*/
if(AutoCode == 1) 
{
  mcall *_tlch.Prolog;                     /* go to tool home         */ 
  mcall *_tlch.ManChange;                  /* perform manual change   */ 
  _toolNc = _toolSel;
  mcall *_tlch.SaveToolNc;                 /* save new tool to file...*/ 
  mcall *_tlch.Epilog;                     /* return to tool home     */ 
}

/*====================================================================*/
/* ToolAuto = 2: Change tool manually with switch                     */
/*====================================================================*/
if(AutoCode == 2)
{                    
  mcall *_tlch.Prolog;                     /* go to tool home         */ 
  mcall *_tlch.ManChange;                  /* perform manual change   */ 
  _toolNc = _toolSel;
  mcall *_tlch.SaveToolNc;                 /* save new tool to file...*/ 

  OldLimit = mcall *_tlch.LimitsOff;       /* suspend limit watching  */ 

  if(_ToolPrbState >= 2)                   /* tool calibration        */
   {Mcall *_tlch.ReadSwitch 1;}            /* cancel if not hit       */

  if(_TchToHome == 1)
   {MCall *_tlch.GoToPos McPos[3].Ofs;}    /* return to tool home pos.*/ 

  mcall *_tlch.LimitsOn OldLimit;          /* restore limit watching  */ 
  mcall *_tlch.Epilog;                     /* return to tool home     */ 
}

/*====================================================================*/
/* ToolAuto = 3: Change tool automatically                            */
/*====================================================================*/
if(AutoCode == 3) 
{
  mcall *_tlch.Prolog;                     /* go to tool home         */
  OldLimit = mcall *_tlch.LimitsOff;       /* suspend limit watching  */
  CallOnExit = CallOnExit & "mcall *_tlch.LimitsOn OldLimit";  // restore limit watching
  
  if(_toolNc) {mcall (*_tlch.DropFetch) (_ToolNc) 0;}  // drop tool
  if((_ChkToolDrop > 0) AND (0 == _Test AND 2))
  {
    if(mcall *_tlch.ReadSwitch 0)
    {
      MsgBox sprint (GetIniTxt "GetTool" "MsgNotDropped") _ToolNc;
      _toolNc = _toolSel = _toolNcEmu = _toolSelEmu = 0;
      mcall *_tlch.SaveToolNc;             // save _toolNc=0 to file 
      mreset "";                         // abort
    }
  }
  _toolNc = 0;                             // now we have no tool
  mcall *_tlch.SaveToolNc;                 // save _toolNc to file

  if(_toolSel)
  {
    mcall (*_tlch.DropFetch) (_ToolSel) 1;   // fetch new tool

    if(_ToolPrbState >= 2)                   // measure tool length
    {
      if(0 == mcall *_tlch.ReadSwitch 1)
      {
        MsgBox sprint (GetIniTxt "GetTool" "MsgNoSignal") _ToolNc;
        _toolNc = _toolSel = _toolNcEmu = _toolSelEmu = 0;
        mcall *_tlch.SaveToolNc;             // save _toolNc=0 to file 
        mreset "";                           // abort
      }
    }
    _toolNc = _toolSel;
    mcall *_tlch.SaveToolNc;                 // save _toolNc to file 
  }
  if(_TchToHome == 1)
   {MCall *_tlch.GoToPos McPos[3].Ofs;}    /* return to tool home pos.*/ 

  mcall *_tlch.Epilog;                     /* return to tool home     */ 
}
SetMc2Oc imo to;                           // restore origin
mcall *_tlch.GotoPos OldPos;
