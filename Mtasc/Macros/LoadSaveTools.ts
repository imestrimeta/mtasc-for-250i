/* ================================================================== */
// Load/Save Tool Data
/* ================================================================== */
// Parameters:
// %1: 'L' = load, 'S' = save
// %2 = tool file
// %3(optional) = 1: enforce loading
/* ------------------------------------------------------------------ */

local "xml";

if(%1 == 'L')                                      // LOAD
{
  local "DoLoad"; DoLoad = 0;

  if(%P > 2) {if(%3) {DoLoad = 1;}}                // enforce loading
  if(0 == (_ToolFile == %2)) {DoLoad = 1;}         // new file to load

  if(DoLoad == 0) {return;}                         // nothing to do
  _ToolFile = %2;

  local "sl" "ff"; ff = 0;   // file format 0=undef, 1=etl (old), 2=xml
  sl = (SizeOfArray %2) - 1; // string length of file name
  if(%2[sl - 3] == 'x') {ff = 2;}
  if(%2[sl - 3] == 'e') {ff = 1;}
  if(ff == 0) {mreset "Illegal Tool File";}

  if(ff == 1)
   {MsgBox "Obsolete Tool File Format: " & %2; return;}
  local "fini";
  if(0 == FileExists %2)   // xml file not found (ff == 2) - try to read data from old ini file
  {
    fini = %2;
    fini[sl - 3] = 'e';  // set extension to etl
    fini[sl - 2] = 't';
    fini[sl - 1] = 'l';
    if(0 == FileExists fini) {mreset "Missing Tool File";}
    local "n" "i" "lFDnR" "TmpTxt";
    Local "OneLine" "Version" "DoLoad";

    Local "ZeroTool"; ZeroTool = Tools[0];

    Version = scall GetIniString fini "Tools" "Version" "0";

    n = 0L + scall GetIniString fini "Tools" "n" "0";
    if(n > 0)
    {
      _ToolN   = n;                                     
      Tools    = ObjDef CToolData _ToolN + 1;
      Tools[0] = ZeroTool;
    }

    TmpTxt = ObjDef T_Char n + 1;
    i = 0;
    for(n)
    { TmpTxt[i] = '1'; 
      LfdNr = sprint "%d" i = i + 1;
      OneLine = GetIniString fini "Tools" LfdnR "";

      if(OneLine == "")
      {
        // msgbox "Missing line in tool file section!";
        SendError \"ERR_LoadSaveTools_MissingLine\";
      }

      Tools[i].CRC     = GetStkObj 0 scall OneLine;
      Tools[i].EMU     = GetStkObj 1 scall OneLine;
      Tools[i].Ident   = GetStkObj 2 scall OneLine;
      Tools[i].Rad     = GetStkObj 3 scall OneLine;
      Tools[i].Len     = GetStkObj 4 scall OneLine;
      Tools[i].Speed   = GetStkObj 5 scall OneLine;
      Tools[i].Rpm     = GetStkObj 6 scall OneLine;
      Tools[i].Out     = GetStkObj 7 scall OneLine;
      Tools[i].Color   = GetStkObj 8 scall OneLine;
      (*Tools[i].Text) = "Tool Text";
      
      if(Version >= 1) {(*Tools[i].Text) = GetStkObj 9 scall OneLine;}

      Tools[i].Ofs = ARRAY _nAx scall GetIniString fini "Offset" LfdNr "";

      OneLine = GetIniString fini "Shape" LfdNr "0 \"Default\"";
      Tools[i].Flags    = GetStkObj 0 scall OneLine;
      (*Tools[i].File)  = GetStkObj 1 scall Oneline;

      OneLine = GetIniString fini "PathTime" LfdNr "0.0 0.0 0.0 0.0 0.0 0.0";
      Tools[i].FPath    = GetStkObj 0 scall OneLine;
      Tools[i].FTime    = GetStkObj 1 scall OneLine;
      Tools[i].SPath    = GetStkObj 2 scall OneLine;
      Tools[i].STime    = GetStkObj 3 scall OneLine;
      Tools[i].DPath    = GetStkObj 4 scall OneLine;
      Tools[i].DTime    = GetStkObj 5 scall OneLine;
    }

    _ToolMode = scall GetIniString fini "Tools" "ToolMode" "0";

    xml = gcnew CXml 0 _ToolFile;           // write data in new xml format
    xml.!E!Tools._Mt2Xml Tools;

    xml.Tools.!A!ToolMode sprint "%d" _ToolMode;

    xml.Tools.!E!UserVisibility.!A!Crc "1";
    xml.Tools.!E!UserVisibility.!A!EMU "1";
    xml.Tools.!E!UserVisibility.!A!Ident "1";
    xml.Tools.!E!UserVisibility.!A!HpglPen "1";
    xml.Tools.!E!UserVisibility.!A!Ofs "-1";  // bit mask
    xml.Tools.!E!UserVisibility.!A!Rad "1";
    xml.Tools.!E!UserVisibility.!A!Len "1";
    xml.Tools.!E!UserVisibility.!A!Speed "1";
    xml.Tools.!E!UserVisibility.!A!Rpm "1";
    xml.Tools.!E!UserVisibility.!A!Out "1";
    xml.Tools.!E!UserVisibility.!A!FPath "1";
    xml.Tools.!E!UserVisibility.!A!FTime "1";
    xml.Tools.!E!UserVisibility.!A!SPath "1";
    xml.Tools.!E!UserVisibility.!A!STime "1";
    xml.Tools.!E!UserVisibility.!A!DPath "1";
    xml.Tools.!E!UserVisibility.!A!DTime "1";
    xml.Tools.!E!UserVisibility.!A!Flags "1";
    xml.Tools.!E!UserVisibility.!A!File "1";
    xml.Tools.!E!UserVisibility.!A!Text "1";
    xml.Tools.!E!UserVisibility.!A!Color "1";
    xml._Save ;
  }
  else
  {
    xml = gcnew CXml 0 %2;
    Tools = xml.Tools._Xml2Mt;
    _ToolN = (SizeOfArray Tools) - 1;
    _ToolMode = scall xml.Tools.!A?ToolMode;
  }
}

if(%1 == 'S')                                      // SAVE
{
  xml = gcnew CXml 0 %2;
  xml.Tools._Mt2Xml Tools;
  xml.Tools.!A!ToolMode sprint "%d" _ToolMode;
  xml._Save;
}

local "i";
i = 0; for SizeOfArray _HpglPar.ToolForPen    // update HPGL settings from tool data
{
  _HpglPar.ToolForPen[i] = 1;   // default tool for all pens including pen 0
  i = i + 1;
} 
i = 1; for _ToolN
{
  if(Tools[i].HpglPen)
  {
    _HpglPar.ToolForPen[Tools[i].HpglPen] = i;
    _HpglPar.zdn[Tools[i].HpglPen] = Tools[i].HpglZdn;
  }
  i = i + 1;
}
