/* ================================================================== */
/* Handler f�r Rubout-Bereiche                                        */
/* ================================================================== */

pbo.mactHandler =
"if(%1 == 1){
  z zup; mf;
  _ocNext = %2._; mf;
  z zdn; ml;
 }
 if(%1 == 2){
  _ocNext = %2._; ml;
 }
 if(%1 == 3){
  z zup; mf;
 }
;";

                                          /* Rechteck initialisieren  */
pbo.RubRgInit _RubLine _RubX1 _RubY1 _RubX2 _RubY2 (_RubRad * 2.0);

if(_grMode == 0)
{
  local "CallOnExit";
  CallOnExit = sprint "@_grMode = %d;" (_grMode + 0);
  _grmode = 1;                              // Ergebnis grafisch anzeigen
  pbo.RubRgDoHatch   _RubLine;              // Schraffurlinien erzeugen
  pbo.RubRgDoBorders _RubLine;              // Umriss nachfahren
}
