/*====================================================================*/
/* Macros for processing of Gerber-converted output                   */
/*====================================================================*/
/* Todo:                                                              */
/* Rotationszentrum muss experimentell ermittelt werden!              */
/*--------------------------------------------------------------------*/

CGbxData = ObjDef T_classDscr
"flPboIn"   T_object 0   // input file for Proboard (gerber data converted to Mtasc)
"flType"    T_long   0   // file type (1=Mtasc)
"flBorder"  T_object 0   // additional border line (Mtasc)
"mode"      T_long   0   // mode 0=called for graphic/execution, 1=called for collecting paths in mlist
"grid"      T_double 0
"scale"     T_double 0
"arcCent"   CVct     0
"shapeMode" T_long   0   // 1=rect, 2=oval
"lfdNr"     T_long   0
"step"      T_long   0
"wtime"     T_double 0
"crc"       T_long   0   // for CRC in GetOutline.ts
"bufSize"   T_long   0
"nOpen"     T_long   0
"rad"       T_double 0
"chkLenMax" T_double 0
"init"      T_function 0
;
gbx = gcnew CGbxData;

gbx.flType = 1;
gbx.mode = 0;
gbx.grid  = 0.1;
gbx.scale = 1.0;

gbx.crc     = 0;
gbx.bufSize = 5;
gbx.nOpen   = 3;
gbx.rad     = 0.1;
gbx.chkLenMax  = 2.0;

gbx.arcCent = ObjDef CVct;

gbx.lfdNr   = 0;
gbx.step    = 0;
gbx.wtime    = 0.0;

/*--------------------------------------------------------------------*/
/* Diese Variablen m�ssen vor jedem Aufruf neu initialisiert werden.  */
/*--------------------------------------------------------------------*/
(MacroDef "CGbxData_init") =
"
 Oldtool      = 0;

 RotWasTold   = 0;
 HoleWasTold  = 0;
 AngleWasTold = 0;

 FillStart    = 0;
 FillMsgOk    = 0;

 MsgNewRad    = 0;
 rDraw = rOld = 0.0; 
 ArcMode = 74;

 %0.flBorder = nullptr;
 %0.scale = 1.0;
 %0.arcCent = gcnew CVct 0 2;
 %0.shapeMode = 2;
 InvertMode = 0;
 GetXTool 1;
";
(MacroDef "GbxInit") = "gbx.init";   // for compatibility

/*====================================================================*/
/* Standard G-Commands (MoveTo, LineTo, ArcTo)                        */
/*====================================================================*/
(MacroDef "MoveTo") = "X %2 * gbx.scale; Y %3 * gbx.scale; MF;";

/*--------------------------------------------------------------------*/
/* Linear interpolation with aperture if rDraw > 0                    */
/*--------------------------------------------------------------------*/
(MacroDef "LineTo") =
"local \"X0\" \"Y0\" \"X1\" \"Y1\";

 X0 = x; X1 = %2 * gbx.scale;
 Y0 = y; Y1 = %3 * gbx.scale;

 if(gbx.lfdNr) {WriteNr %1 X0 Y0;}
 getxtool 6; 

 if(rDraw != rOld)
 {
   if(MsgNewRad > 0) {msgbox sprint \"LineTo mit r = %lf\" rDraw;} 
   rOld = rDraw;
 }

 if(rDraw == 0) {X X1; Y Y1; ML;}            /* Line without aperture */

 if(rDraw > 0)                               /* Line with aperture    */
 {
   local \"l\" \"dx\" \"dy\";

   l  = Sqrt(((X1 - X0) * (X1 - X0)) + ((Y1 - Y0) * (Y1 - Y0)));

   if(l < 0.001)
   {
     if(gbx.shapeMode == 2)
     {
       X X0 + rDraw; Y Y0; MF;
       X X0 - rDraw;       MC rDraw \"+LZ\";
       X X0 + rDraw;       MC rDraw \"+LZ\";
     }
     else
     {
       x X0 - rDraw; y Y0 - rDraw; mf;
       x X0 + rDraw; y Y0 - rDraw; ml;
       x X0 + rDraw; y Y0 + rDraw; ml;
       x X0 - rDraw; y Y0 + rDraw; ml;
       x X0 - rDraw; y Y0 - rDraw; ml;
     }
   }
   else
   {
     dx = rDraw * (X1 - X0) / l;     
     dy = rDraw * (Y1 - Y0) / l;     
     if(gbx.shapeMode == 2)
     {
       x X0 + dy; y Y0 - dx; MF;
       x X1 + dy; y Y1 - dx; ML;
       x X1 - dy; y Y1 + dx; MC rDraw \"-LZ\";

       x X0 - dy; y Y0 + dx; ML;
       x X0 + dy; y Y0 - dx; MC rDraw \"-LZ\";
     }
     else // rectangle
     {
       x X0 + -dx +  dy; y Y0 + -dx + -dy; mf;
       x X1 +  dx +  dy; y Y1 + -dx +  dy; ml;
       x X1 +  dx + -dy; y Y1 +  dx +  dy; ml;
       x X0 + -dx + -dy; y Y0 +  dx + -dy; ml;
       x X0 + -dx +  dy; y Y0 + -dx + -dy; ml;
     }
   }
  
   X X1; Y Y1; MF;
 }
";

/*--------------------------------------------------------------------*/
/* ARC-interpolation with or without aperture                         */
/*--------------------------------------------------------------------*/

(MacroDef "ArcTo")  = /* LfdNr, X, Y, LR, I, J */
"local \"r\" \"X0\" \"Y0\" \"X1\" \"Y1\" \"dX\" \"dY\";
 local \"Det\" \"CMD\" \"Gap\" \"c\" \"d0\" \"d1\";

 X0 = x; X1 = %2 * gbx.scale;
 Y0 = y; Y1 = %3 * gbx.scale;
 if(%P > 4)
 {
  dX = %5 * gbx.scale;
  dy = %6 * gbx.scale;
  r   = Sqrt((dx * dx) + (dy * dy)); 	
  if(ArcMode == 74)
  {
   local \"r1\";
   d1 = ObjDef CVct;
   d1.dim= 2;
   d1[0] = X0 + dx - x1;
   d1[1] = Y0 + dy - y1;
   r1 = d1.l;
   if(((r - r1) > 0.001) or ((r - r1) < -0.001))
   {
    dx = -dx;
    d1[0] = X0 + dx - x1;
    d1[1] = Y0 + dy - y1;
    r1 = d1.l;
    if(((r - r1) > 0.001) or ((r - r1) < -0.001))
    {
     dx = -dx; dy = -dy;
     d1[0] = X0 + dx - x1;
     d1[1] = Y0 + dy - y1;
     r1 = d1.l;
     if(((r - r1) > 0.001) or ((r - r1) < -0.001))
     {
      dx = -dx;
      d1[0] = X0 + dx - x1;
      d1[1] = Y0 + dy - y1;
      r1 = d1.l;
      if(((r - r1) > 0.001) or ((r - r1) < -0.001))
      {
       mreset sprint \"Ung�ltige Mittelpunkt-Koordinaten I,J, Befehl Nr. %d\" %1;
      }
     }
    }
   }
  }
  gbx.arcCent[0] = X0 + dx;
  gbx.arcCent[1] = Y0 + dy;
 }
 else
 {
  dx = gbx.arcCent[0] - X0;
  dy = gbx.arcCent[1] - Y0;
  r   = Sqrt((dx * dx) + (dy * dy)); 	
 }

 Gap =       (X1 - X0) * (X1 - X0);
 Gap = sqrt Gap + (Y1 - Y0) * (Y1 - Y0);

 if(gbx.lfdNr) {WriteNr %1 X0 Y0;}

 getxtool 6;
 
 if(rDraw != rOld)
 {
   if(MsgNewRad > 0) {msgbox sprint \"ArcTo mit r = %lf\" rDraw;} 
   rOld = rDraw;
 }

 if(rDraw == 0)                               /* Arc without aperture */
 {
   if(Gap <= 0.01) {X x1; Y y1; ML;}

   if(Gap >  0.01)
   {
     Det =       (x1 * dy) - (y1 * dx); 
     Det = Det + (y0 * dx) - (x0 * dy); 

     CMD = \"+LZ\";
     if(%4 == 1) {CMD[1] = 'R';}
     if(Det < 0) {CMD[0] = '-';} 

     X x1; Y y1; MC r CMD;
   }
 }

 if(rDraw > 0)                                /* Arc with aperture    */
 {
   if(Gap < rDraw / 2.0)                      /* concentric circles   */
   {
     X x1 + dx + (r + rDraw); Y y1 + dy; MF;
     X X1 + dx - (r + rDraw);            MC (r + rDraw) \"+LZ\"; 
     X X1 + dx + (r + rDraw);            MC (r + rDraw) \"+LZ\"; 
   }

   if(Gap >= rDraw / 2.0)                     // KrummSchlauch
   {
//? sprint \"%.3lf %.3lf\" X0 Y0;
//? sprint \"%.3lf %.3lf\" X1 Y1;
     local \"p\";
     c = ObjDef CVct;
     c.dim= 2;
     d0 = c;
     d1 = c;
     p = c;
     d0[0] = -dx;
     d0[1] = -dy;
     c[0] = x0 + dx;
     c[1] = y0 + dy;
     d1[0] = x1 - c[0];
     d1[1] = y1 - c[1];
     d0._ = (1.0 / d0.l) * d0._;     // make unit vectors
     d1._ = (1.0 / d1.l) * d1._;
     p._ = c._ + (r - rDraw) * d0._;       // start point = beginning of 1. small arc
     x p[0]; y p[1]; mf;
     CMD = \"+LZ\";
     Det = (d0[0] * d1[1]) - (d1[0] * d0[1]);
     if(%4 == 0) {CMD[1] = 'L';} else {CMD[1] = 'R';}
     if(Det < 0) {CMD[0] = '-';} else {CMD[0] = '+';}
//? CMD;
     p._ = c._ + (r + rDraw) * d0._;       // end of 1. small arc
     x p[0]; y p[1]; mc rDraw CMD;
     p._ = c._ + (r + rDraw) * d1._;       // end of outer big arc
     x p[0]; y p[1]; mc (r + rDraw) CMD;
     p._ = c._ + (r - rDraw) * d1._;       // end of 2. small arc
     x p[0]; y p[1]; mc rDraw CMD;
     p._ = c._ + (r - rDraw) * d0._;       // end of inner big arc = start point
     if(%4 == 0) {CMD[1] = 'R';} else {CMD[1] = 'L';}
     if(Det < 0) {CMD[0] = '+';} else {CMD[0] = '-';}
     x p[0]; y p[1]; mc (r - rDraw) CMD;
   }

   X X1; Y Y1; MF;
 }
";

/*====================================================================*/
/* Standard Apertures (circle, rectangle, oval, polygon)              */
/*====================================================================*/
/* Extended Gerber Circle (Mx, My, D, d or dx, dy)                    */
/*--------------------------------------------------------------------*/
(MacroDef "GbxCirc") =
"local \"Mx\" \"MY\" \"R\";

 R  = 0.5 * %4 * gbx.scale;

 if(%1 > 0)
 {
   MX = %2 * gbx.scale; MY = %3 * gbx.scale;

   if(gbx.lfdNr) {WriteNr %1 Mx My;}

   getxtool 4;
   HoleWarn %1 %5 + %6;

   X Mx + R; Y My; MF;
   X Mx - R;       MC R \"+LZ\";
   X Mx + R;       MC R \"+LZ\";

   X Mx; Y My; MF;                                     /* go to center*/	
 } 

 if(%1 == 0) {rDraw = R;} 
";

/*--------------------------------------------------------------------*/
/* Extended Gerber Rectangle (Mx, My, Dx, Dy, r or dx, dy)            */
/*--------------------------------------------------------------------*/
(MacroDef "GbxRect") = 
"local \"Mx\" \"MY\" \"dx\" \"dy\";

 MX = %2 * gbx.scale; dx = %4 * gbx.scale;
 My = %3 * gbx.scale; dy = %5 * gbx.scale;

 if(%1 > 0)
 {
   if(gbx.lfdNr) {WriteNr %1 Mx My;}

   getxtool 4;
   HoleWarn %1 %6 + %7;
  
   MRect Mx My dx dy;

   X Mx; Y My; MF;                                     /* go to center*/	
 }

 if(%1 == 0)
 {
   gbx.shapeMode = 1;   // rectangle
   rDraw = 0.5 * dx;
   if(dy > dx) {rDraw = 0.5 * dy;}
 } 
";

/*--------------------------------------------------------------------*/
/* Extended Gerber Oval (obround)                                     */
/*--------------------------------------------------------------------*/
(MacroDef "GbxOval") = 
"local \"Mx\" \"MY\" \"dx\" \"dy\";

 MX = %2 * gbx.scale; dx = %4 * gbx.scale;
 My = %3 * gbx.scale; dy = %5 * gbx.scale;

 if(%1 > 0)
 {
   if(gbx.lfdNr) {WriteNr %1 Mx My;}
   HoleWarn %1 %6 + %7;
   getxtool 4;
  
   if(dx >= dy)
   {
      X Mx - 0.5 * (dx - dy); Y My - 0.5 * dy; MF;
      X Mx + 0.5 * (dx - dy);                  ML;
                              Y My + 0.5 * dy; MC (0.5 * dy) \"+LZ\";
      X Mx - 0.5 * (dx - dy);                  ML;
                              Y My - 0.5 * dy; MC (0.5 * dy) \"+LZ\";
   }

   if(dx < dy)
   {
      X Mx - 0.5 * dx; Y My - 0.5 * (dy - dx); MF;
                       Y My + 0.5 * (dy - dx); ML;
      X Mx + 0.5 * dx;                         MC (0.5 * dx) \"+RZ\";
                       Y My - 0.5 * (dy - dx); ML;
      X Mx - 0.5 * dx;                         MC (0.5 * dx) \"+RZ\";
   }

   X Mx; Y My; MF;                                     /* go to center*/	
 }

 if(%1 == 0)
 {
   gbx.shapeMode = 2;   // oval
   rDraw = 0.5 * dx;
   if(dy > dx) {rDraw = 0.5 * dy;}
 } 
";

/*--------------------------------------------------------------------*/
/* Extended Gerber Polygon                                            */
/*--------------------------------------------------------------------*/
(MacroDef "GbxPoly") = 
"local \"Text\";
 Text = \"Gerber-Polygon-Macro muss noch programmiert werden\";
 MsgBox Text sprint \"Warnung Teil %d:\" %1;
";

/*====================================================================*/
/* Aperture-Primitives (GCirc, VLine, CLine, LLine, Shape, Poly)      */
/*====================================================================*/
/* No. 0 GCirc: Circle (%2,%3=offset; %4=Exp.)                        */
/* %5=Diameter; %6,%7=CenterXY;                                       */
/*--------------------------------------------------------------------*/
(MacroDef "GCirc") =
"local \"Mx\" \"MY\" \"R\";

 if(%1 > 0)
 {
   Mx = (%6 + %2) * gbx.scale;
   My = (%7 + %3) * gbx.scale;

   if(gbx.lfdNr) {WriteNr  %1 Mx MY;}

   getxtool 2;
   if(%4 != 1) {ExpError %1 %4;} 

   R  = 0.5 * %5 * gbx.scale;

   X Mx + R; Y My; MF;
   X Mx - R;       MC R \"-LZ\";
   X Mx + R;       MC R \"-LZ\";

   x Mx; y My; MF;                                     /* go to center*/	
 }

 if(%1 == 0) {rDraw = 0.0;} 
";

/*--------------------------------------------------------------------*/
/* No. 1 VLine: Vector line (%2,%3=Offset; %4=Exp.)                   */
/* %5=Width; %6,%7=StartXY; %8,%9=EndXY; %10=Angle                    */
/*--------------------------------------------------------------------*/
(MacroDef "VLine") =
"local \"X0\" \"Y0\" \"X1\" \"Y1\";

 if(%1 > 0)
 {
   local \"w\" \"dx\" \"dy\" \"l\";
   X0 = (%6 + %2) * gbx.scale;
   Y0 = (%7 + %3) * gbx.scale;

   if(gbx.lfdNr) {WriteNr %1 X0 Y0;}

   X1 = (%8 + %2) * gbx.scale;
   Y1 = (%9 + %3) * gbx.scale;
   w = 0.5 * %5 * gbx.scale;
   dy = -(x1 - x0);              // dir vector + 90�
   dx =   y1 - y0;
   l = sqrt (dx * dx) + (dy * dy);
   dx = w * dx / l;
   dy = w * dy / l;

   getxtool 3;
   if(%4 != 1) {ExpError %1 %4;} 
   if(%P > 9)  {RotWarn %1 %10;}

   x X0 - dx; y Y0 - dy; MF; // go to start
   x X1 - dx; y Y1 - dy; ML; // 4 lines
   x X1 + dx; y Y1 + dy; ML;
   x X0 + dx; y Y0 + dy; ML;
   x X0 - dx; y Y0 - dy; ML;
 }

 if(%1 == 0) {rDraw = 0.0;} 
";

/*--------------------------------------------------------------------*/
/* No. 2 "CLine": Center line (rotation around center)                */
/* %2,%3=Offset; %4=Exp. %5,%6=Width/Height; %7,%8=CenterXY; %9=Angle */
/*--------------------------------------------------------------------*/
(MacroDef "CLine") =
"if(%1 > 0)
 {
   local \"dx1\" \"dy1\" \"dx2\" \"dy2\" \"Phi\";
   local \"Mx\" \"MY\";

   Mx  = gbx.scale * (%7 + %2); 
   MY  = gbx.scale * (%8 + %3); 
   
   if(gbx.lfdNr) {WriteNr %1 Mx MY;}

   getxtool 3;
   if(%4 != 1) {ExpError %1 %4;} 

   Phi = %9 * 3.14159 / 180.0;

   dx1 = gbx.scale * 0.5 * ((cos phi) * %5) - (sin Phi) * %6; 
   dy1 = gbx.scale * 0.5 * ((cos phi) * %6) + (sin Phi) * %5; 
   
   dx2 = gbx.scale * 0.5 * ((cos phi) * %5) + (sin Phi) * %6; 
   dy2 = gbx.scale * 0.5 * ((cos phi) * %6) - (sin Phi) * %5; 

   x Mx - dx1; y My - dy1; MF;                         /* go to start */	

   x Mx + dx2; y My - dy2; ML;                         /* 4 lines     */	
   x Mx + dx1; y My + dy1; ML;
   x Mx - dx2; y My + dy2; ML;
   x Mx - dx1; y My - dy1; ML;
 }

 if(%1 == 0) {rDraw = 0.0;} 
";

/*--------------------------------------------------------------------*/
/* No. 3 "LLine": Lower left line (%2,%3=Offset; %4=Exp.)             */
/* %5,%6=Width/Height; %7,%8=LowerLeftXY; %9=Angle                    */
/*--------------------------------------------------------------------*/
(MacroDef "LLine") =
"
 if(%1 > 0)
 {
   local \"x0\" \"Y0\";

   x0 = gbx.scale * (%7 + %2); 
   Y0 = gbx.scale * (%8 + %3); 
   
   if(gbx.lfdNr) {WriteNr %1 X0 Y0;}

   getxtool 3;
   if(%4 != 1) {ExpError %1 %4;} 
   if(%P > 8)  {RotWarn  %1 %9;}

   x X0;                  y Y0;                  MF;   /* go to start */	
   x X0 + %5 * gbx.scale;                        ML;   /* 4 lines     */	
                          y Y0 + %6 * gbx.scale; ML;
   x X0;                                         ML;  
                          y Y0;                  ML;
 }

 if(%1 == 0) {rDraw = 0.0;} 
";

/*--------------------------------------------------------------------*/
/* No. 4 "Shape":                                                     */
/* %2,%3=Offset; %5=nPoints; %6,%7=StartXY; %8=Angle;                 */
/* Local array "Data" contains 2*nPoints double precision values      */
/*--------------------------------------------------------------------*/
(MacroDef "Shape") =
"
 if(%1 > 0)
 {
   local \"x0\" \"Y0\" \"dx\" \"dY\" \"i\" \"Phi\";

   Phi = 0.0;
   if(%P > 7) {Phi = %8 * 3.14159 / 180.0;} 

   x0 = gbx.scale * (%2 + ((cos phi) * %6) - (sin Phi) * %7); 
   Y0 = gbx.scale * (%3 + ((cos phi) * %7) + (sin Phi) * %6); 

   if(gbx.lfdNr) {WriteNr %1 X0 Y0;}

   getxtool 5;    
   if(%4 != 1) {ExpError %1 %4;} 

   x X0; y Y0; MF;                                     /* go to start */	

   i = 0;
   for %5
   {
     dX = Data[2 * i];
     dy = Data[1 + 2 * i];

     X gbx.scale * (%2 + ((cos phi) * dx) - (sin Phi) * dy);
     y gbx.scale * (%3 + ((cos phi) * dy) + (sin Phi) * dx); ML;

     i = i + 1;
   }
 }

 if(%1 == 0) {rDraw = 0.0;} 
}
";

/*--------------------------------------------------------------------*/
/* No. 5 "Poly": Regular polygon (%2,%3=Offset; %4=Exp.)              */
/* %5=nPoints; %6,%7=CenterXY; %8=Diameter; %9=Angle                  */
/*--------------------------------------------------------------------*/
(MacroDef "Poly") =
"
 if(%1 > 0)
 {
   local \"x0\" \"Y0\" \"r\" \"Phi\";

   Phi = 0.0;
   if(%P > 8) {Phi = %9 * 3.14159 / 180.0;}

   r = 0.5 * %8;

   X0 = gbx.scale * (%2 + %6 + r * (cos phi));
   y0 = gbx.scale * (%3 + %7 + r * (sin Phi));

   if(gbx.lfdNr) {WriteNr %1 X0 Y0;}

   getxtool 7;    
   if(%4 != 1) {ExpError %1 %4;} 

   x X0; y Y0; MF;                                     /* go to start */	

   for(%5 - 1)
   {
     Phi = Phi + 2 * 3.14159 / %5;
     X gbx.scale * (%2 + %6 + r * (cos phi));
     y gbx.scale * (%3 + %7 + r * (sin Phi)); ML;
   }

   x X0; y Y0; ML;                                     /* close poly  */

   x gbx.scale * (%2 + %6);                            /* go to center*/	
   y gbx.scale * (%3 + %7); MF;
 }

 if(%1 == 0) {rDraw = 0.0;} 
";

/*--------------------------------------------------------------------*/
/* Exposure error                                                     */
/*--------------------------------------------------------------------*/
(MacroDef "ExpError") =
"local \"Text\";
 Text = sprint \"Ung�ltiger Wert f�r Exposure: %d\" %2;
 MsgBox Text sprint \"Warnung Teil %d:\" %1;
 GetXTool 8;
";

/*--------------------------------------------------------------------*/
/* Rotation warning                                                   */
/*--------------------------------------------------------------------*/
(MacroDef "RotWarn") =
"if(%2 != 0.0) {GetxTool 8;} 

 if((%2 != 0.0) AND (RotWasTold == 0)) 
 {
   local \"Text\";
   Text = sprint \"Rotation %lf ignored - edit macro definition!\" %2;
   MsgBox Text sprint \"Warnung Teil %d:\" %1;
   RotWasTold = 1;
 }
";

/*--------------------------------------------------------------------*/
/* Hole warning                                                       */
/*--------------------------------------------------------------------*/
(MacroDef "HoleWarn") =
"if(%2 > 0.0) {GetxTool 8;} 

 if((%2 > 0.0) AND (HoleWasTold == 0)) 
 {
  local \"Text\";
  Text = \"Holes not yet supported - edit macro definition!\";
  msgbox Text sprint \"Warnung Teil %d:\" %1;
  HoleWasTold = 1;
 }
";

/*--------------------------------------------------------------------*/
/* Extended GetTool for test                                          */
/*--------------------------------------------------------------------*/
(MacroDef "GetXTool") =
"
 if(OldTool != %1)
 {
  OldTool = %1;
 }
 // InvertMode (%LPDC%) is visualized by a blue component
 SetDrawPar 'L' ((0xF00000 * InvertMode) + (0x20 * %1) + (0x2000 * (8 - %1))) PS_SOLID 0 R2_COPYPEN;
 Thread_agi.mact.mfl.tool = 0x100 * InvertMode;
";

/*--------------------------------------------------------------------*/
/* Write current number                                               */
/*--------------------------------------------------------------------*/
(MacroDef "WriteNr") =
"
 local \"XOld\" \"Yold\";

 if(_Mode == 'G')
 {
   Wait gbx.wtime 0;	
   if((FillSTart == 0) or (FillMsgOk == 2))
   { 
     FillMsgOk = gbx.lfdNr;
     Xold = x; Yold = y;
     WrX0[0] = %2; WrXfac[0] = 0.2;
     wrY0[0] = %3; WrYfac[0] = 0.2;
     wrText0 = sprint \"%d\" %1; wrExec 0;
     X Xold; Y Yold; mf;
   }
 } 
";

/*--------------------------------------------------------------------*/
/* Define contour radius                                              */
/*--------------------------------------------------------------------*/
(MacroDef "DefineR") = "scall %1 & \" 0 0.0 0.0;\";";

/*--------------------------------------------------------------------*/
/* Start of filled contour                                            */
/*--------------------------------------------------------------------*/
(MacroDef "GbxFillStart") =
"FillStart = 1; FillMsgOk = 2;
 if(rDraw > 0) {gettool _ToolSel or 2;}
 rDraw = 0.0;
";

/*--------------------------------------------------------------------*/
/* End of filled contour                                              */
/*--------------------------------------------------------------------*/
(MacroDef "GbxFillStop") = "FillStart = 0;";

/*--------------------------------------------------------------------*/
/* Set invert mode                                                    */
/*--------------------------------------------------------------------*/
(MacroDef "GbxInvert") = "InvertMode = %1;";

/*--------------------------------------------------------------------*/
/* Set scale                                                          */
/*--------------------------------------------------------------------*/
(MacroDef "GbxScale") =
"gbx.scale = 1.0; if(%1 == 1) {gbx.scale = 25.4;}
";
