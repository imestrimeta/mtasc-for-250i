
// Resize arrays of laa data
// if vDim has changed (vDim != vDimPrv)
// Parameters:
// %1: xml data for laa

local "d"; d = %1.vDim._Xml2Mt;

%1.AcToMcFac._MtResize d "extLast";
%1.dAcdCorr._MtResize d "extLast";
%1.AdrIn._MtResize d "extLast";
%1.AdrOut._MtResize d "extLast";
%1.PdType._MtResize d "extLast";
%1.PdId._MtResize d "extLast";
%1.PdCh._MtResize d "extAutoInc";
%1.HwType._MtResize d "extLast";
%1.speedMaxTol._MtResize d "extLast";
%1.speedMaxCut._MtResize d "extLast";
%1.speedStart._MtResize d "extLast";
%1.dAcdrPFac._MtResize d "extLast";
%1.dAcdrIFac._MtResize d "extLast";
%1.dAcdrDFac._MtResize d "extLast";
%1.dAcdrD2Fac._MtResize d "extLast";
%1.AcdDeadTime._MtResize d "extLast";
%1.IcFac._MtResize d "extLast";

MsgBox sprint "No. of laa axes successfully changed from %d to %d" (%1.vDimPrv._Xml2Mt) d;
%1.vDimPrv.!A?v0 %1.vDim.!A?v0;
