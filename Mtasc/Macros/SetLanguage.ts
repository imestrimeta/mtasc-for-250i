local "lang";

if(FileExists MtApp.path & "/lang.ts")    // look for language definition file
{
  lang = fcall MtApp.path & "/lang.ts";
  if(lang == "en") {MtApp.lang = 'E'; MtApp.xml.Application.!A?Language "E";}
  if(lang == "ge") {MtApp.lang = 'G'; MtApp.xml.Application.!A?Language "G";}
}
