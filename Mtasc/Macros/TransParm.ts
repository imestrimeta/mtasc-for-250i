/* ================================================================== */
/* Transformation parameters and macros                               */
/* ================================================================== */
_AnzRep = 0; _CloneCount = 0;

/* ================================================================== */
/* Load transformation parameters from project file %1                */
/*      The key %2 is different for project versions 1.0/1.1          */
/* ================================================================== */
(Macrodef "LoadTrans") = 
"_PParm.Flag =         scall GetIniString %1 %2 \"MtFlag\" \"0\";
 _PParm.Ofs  = Array 3 scall GetIniString %1 %2 \"MtOfs\"  \"0.0 0.0 0.0\";
 _PParm.Fac  = Array 3 scall GetIniString %1 %2 \"MtFac\"  \"1.0 1.0 1.0\";
 _PParm.Rot  = Array 3 scall GetIniString %1 %2 \"MtRot\"  \"0.0 0.0 0.0\";
 _PParm.RepN = ObjDef T_short 3 scall GetIniString %1 %2 \"MtRep\"  \"1   1   1  \";
 _PParm.RepD = Array 3 scall GetIniString %1 %2 \"MtDst\"  \"0.0 0.0 0.0\";
 _PParm.mirrX = scall GetIniString %1 %2 \"mirrX\"  \"0.0\";
";

/* ================================================================== */
/* Save transformation parameters to project file %1                  */
/* ================================================================== */
(Macrodef "SaveTrans") = 
"local \"Fmt\";

 WriteIniString %1 \"MTrans\" \"MtFlag\" sprint \"%hd\" _PParm.Flag;

 Fmt = \"%8.3lf %8.3lf %8.3lf\";
 WriteIniString %1 \"MTrans\" \"MtOfs\" sprint Fmt _PParm.Ofs;
 WriteIniString %1 \"MTrans\" \"MtFac\" sprint Fmt _PParm.Fac;
 WriteIniString %1 \"MTrans\" \"MtRot\" sprint Fmt _PParm.Rot;
 WriteIniString %1 \"MTrans\" \"MtDst\" sprint Fmt _PParm.RepD;

 Fmt = \"%4d %8d %8d\";
 WriteIniString %1 \"MTrans\" \"MtRep\" sprint Fmt _PParm.RepN[0] _PParm.RepN[1] _PParm.RepN[2];
 WriteIniString %1 \"MTrans\" \"mirrX\" _fmtCoord _PParm.mirrX;
";

/* ================================================================== */
/* Set/reset transformation parameters according to _tParm values     */
/* ================================================================== */
(MacroDef "_MTransOn") =
"local \"i\" \"Pushes\"; Pushes = 0;
   
 if(_PParm.Flag and 4)                     /* Rotation                */
 {
   i = 0;
   for(3)
   {
     if(0.0 != _PParm.Rot[i])
     {
       Mtrans \"PUSH\"; Pushes = Pushes + 1;
       Mtrans \"MAT\" GetM33R i _PParm.Rot[i] * _d2r;
     }
     i = i + 1;
   }
 }
                                           /* Factor and offset       */
 if(_PParm.Flag AND 3) {Mtrans \"PUSH\"; Pushes = Pushes + 1;}
 if(_PParm.Flag and 2) {Mtrans \"FAC\" (_PParm.Fac[0]) (_PParm.Fac[1]) (_PParm.Fac[2]);}
 if(_PParm.Flag and 1) {Mtrans \"OFS\" (_PParm.Ofs[0]) (_PParm.Ofs[1]) (_PParm.Ofs[2]);}

 if(_fParm.HookFlag)                       /* Projektzeilen-Offset    */
 {
   Mtrans \"PUSH\"; Pushes = Pushes + 1;
 
   Mtrans \"OFS\" (_fParm.HookDst[0] - _fParm.HookSrc[0])
                  (_fParm.HookDst[1] - _fParm.HookSrc[1]);
 }

 local \"mirrMask\";
 if(_mode == 'G'){mirrMask = _fParm.MirrFlag and 3;}
 else            {mirrMask = (_fParm.MirrFlag and 12) / 4;}
 if(mirrMask)           // project ofs/mirr
 {
   Mtrans \"PUSH\"; Pushes = Pushes + 1;
 
   if(mirrMask and 1)        // project mirror offset
   {
     local \"mirrX\";
     mirrX = _pParm.mirrX;
     if(0 == mtag[mtagI].mirrMode and 1){mirrX = mirrX - Mc2Oc[_iMc2Oc].Ofs[0];}
     Mtrans \"OFS\" ARRAY 3 (2.0 * mirrX) 0.0 0.0;
    if(_IdeFlag) {print \"verschoben\";}
    }

   if(mirrMask and 2)       // project mirror factor -1
   {
     Mtrans \"FAC\" -1.0 1.0 1.0;
     if(_IdeFlag) {print \"gespiegelt\";}
     _Crcpar.Ori = -1 * _Crcpar.Ori;
   } 
 }

 Pushes;";                                 /* return number of pushes */

(MacroDef "_MTransOff") =                  /* clear Mtrans stack      */
"for(%1) {Mtrans \"POP\";}";
