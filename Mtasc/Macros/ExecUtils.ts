/* ================================================================== */
/* Utilities for program execution                                    */
/* ================================================================== */
/* Start/Stop: _PrgOn,    _PrgOff,  _PrgAbort, _PrgCont               */ 
/*             _PrgStart, _PrgStop, _PrgEnd                           */
/*                                                                    */ 
/* Selecting:  _vciGotoStart,  _vciLeaveEnd                           */
/*             _vciGotoStartG, _vciLeaveEndG                          */ 
/*                                                                    */ 
/* Other:      _MouseMoveXY, _SetOcNull, _SetOcCurr                   */
/* ================================================================== */
/* _PrgOn: Schalter aktivieren                                        */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgOn") = 
"local \"i\"; i = 1;
 for _nOutputs
 {
   if((0 != 4 and _oMode[i]) and (0 != _oStatPrv[i])) {_oSet i 1;}
   i = i + 1;
 }
 _DspUpdate;
";

/* ------------------------------------------------------------------ */
/* _PrgOff: Schalter zurücksetzen                                     */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgOff") =
"local \"i\"; i = 1;
 for  _nOutputs
 { 
   if(4 and _oMode[i]) {_oSet i 0;}
   i = i + 1;
 }
 _DspUpdate;
";

/* ------------------------------------------------------------------ */
/* _PrgStart:                                                         */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgStart") = 
"if(MlimitIsSuspended) {fcall MtApp.path & \"/MlimitCheck.ts\";}
 fcall _AGIPath & \"/DspIo.ts\";
 _PrgOn;
";

/* ------------------------------------------------------------------ */
/* _PrgEnd:                                                           */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgEnd") = 
"_PrgOff; MtransFlag01 = Thread_agi.vci.Flag01 = '1';
 Thread_agi.g3.search.g3sFlag = 0;
";

/* ------------------------------------------------------------------ */
/* _PrgStop:                                                          */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgStop") = 
"local \"CallOnExit\"; CallOnExit = \"@mtag[mtagI].locked = 0;\";
 _oStatPrv = _oStat; _PrgOff;
 mtag[mtagI].locked = 1; if(mtag[mtagI].thAgi)
 {
   mtag[mtagI].flStopped = mtag[mtagI].fileCurr;
   mtag[mtagI].vciStopped  = 1 + (*mtag[mtagI].thAgi).vci.iCurr - Mstat;
   local \"Text\" \"CmtOn\" \"CmtOff\";
   CmtOn  = \"/\" & \"*\";
   CmtOff = \"*\" & \"/\n\";
   Text   = CmtOn & \"------------------------------------------\" & CmtOff
          & CmtOn & \" Generated when EdiTasc is stopped by F9  \" & CmtOff
          & CmtOn & \" Used by VciSrch and system.ts on startup \" & CmtOff
          & CmtOn & \"------------------------------------------\" & CmtOff
          & (sprint \"mtag[%d].flStopped = &(\\\"%s\\\"); \\\n\" mtagI *mtag[mtagI].flStopped)
          & (sprint \"mtag[%d].vciStopped = %dL;\\\n\\\n\" mtagI mtag[mtagI].vciStopped);

   fwrites (_AGIPath & \"/FileStopped.ts\") Text;
 }
";

/* ------------------------------------------------------------------ */
/* _PrgCont:                                                          */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgCont") = "_PrgOn;";

/* ------------------------------------------------------------------ */
/* _PrgAbort:                                                         */
/* ------------------------------------------------------------------ */
(MacroDef "_PrgAbort") = 
"Thread_agi.vci.iCurr = Thread_agi.vci.iCurr - Mstat;
 //mreset; Mstop '0';
 _PrgEnd; _ptr.ptaILevel = 0;

 if(mtag[mtagI].thNc)
  {ThreadKill mtag[mtagI].thNc;}
 else
  {
   Thread_agi.outCtrl.drv = 1;
   if(%1 == 2) {mreset \"\";}
   else        {mreset 1;}
  }
";

/* ------------------------------------------------------------------ */
/* This macro is called from within the scope of PrgDrawExecRepeat.   */
/* It is called after the mode has just switched from graphic to NC.  */
/* It rearranges the _Mtrans stack from graphic to exec mode          */
/* and moves to the start point of the selected command.              */
/* ------------------------------------------------------------------ */
(MacroDef "_vciGotoStart") = 
"if(MlimitIsSuspended) {fcall MtApp.path & \"/MlimitCheck.ts\";}
 
 _MTransOff _AnzPush;                 /* Clear mTrans stack (graphic) */
 _GXmask = 12;
 _AnzPush = _MTransOn _GXmask;        /* remake mTrans stack (exec)   */

 if(_AnzRep > 1)                      /* add clone status             */
 {
   Mtrans \"PUSH\"; _AnzPush = _AnzPush + 1;
   Mtrans \"OFS\" (_iRepx * _PParm.RepD[0]) (_iRepY * _PParm.RepD[1]);
 }

 _mode = 'X';
 {
   local \"_ToolMode\"; _ToolMode = 0L; /* no tool search via ident-Nr. */
   Gettool _toolSel;                    /* get virtual tool as real tool*/
 }

 if(_ocxNext[2] < zup) {_ocxNext[2] = zup; mxf;}
 _ocxNext[0] = Thread_agi.vci.OcStart[0];
 _ocxNext[1] = Thread_agi.vci.OcStart[1]; mxf; wait 0;

 _PrgCont; _dspUpdate;                /* set outputs                  */  

 _ocxNext = Thread_agi.vci.OcStart._; mxl;

 wait 0;                   /* _drvPtReset needs empty command buffer  */
 _drvPtReset;              /* reset current path / time values        */
 _ptr.ptaILevel = 1;       /* start counting current path / time data */
";

/* ------------------------------------------------------------------ */
/* This macro is called after the selected command has been found     */
/* The mode is graphic, only the color changes                        */
/* ------------------------------------------------------------------ */
(MacroDef "_vciGotoStartG") = 
"wait 0;
 _ptr.ptaILevel = 1;         /* start counting current path/time data */
 _ptr.Global.s = 0.0; _ptr.Global.t = 0.0;  /* reset path/time values */
";

/* ------------------------------------------------------------------ */
/* This macro is called in NC mode at the end point of the selected   */
/* command sequence. It does some reasonable terminating motion       */
/* right before parsing of the data stream (NC data) will be aborted  */
/* ------------------------------------------------------------------ */
(MacroDef "_vciLeaveEnd") = 
"wait 0;
 _ptr.ptaILevel = 0;              /* stop cumulating path / time data */
 mreset;
 _ocxNext[2] = zup; mxf;
 wait 0; mreset;
 _PrgEnd; _dspUpdate;
";

/* ------------------------------------------------------------------ */
/* This macro is called in graphic mode at the end of the selected    */
/* command sequence. It is called right before parsing of the data    */
/* stream will be aborted                                             */
/* ------------------------------------------------------------------ */
(MacroDef "_vciLeaveEndG") = 
"wait 0;
 _ptr.ptaILevel = 0;              /* stop cumulating path / time data */
";

/* ------------------------------------------------------------------ */
/* This macro is called when the user selects 'Continue after stop'   */
/* ------------------------------------------------------------------ */
(MacroDef "_ContAfterStop") = "";

/* ------------------------------------------------------------------ */
/* This macro moves axes %1, %3 to the mouse position given by %2, %4 */
/* ------------------------------------------------------------------ */
(MacroDef "_MouseMoveXY") = 
"MtransFlag01 = '0'; mreset;

 if(Thread_agi.vdim > 2)
 {
   if((%1 < 2) AND (%3 < 2) AND (getoc[2] < zup)) {z zup; mf;} 
 }

 _ocNext[%1] = %2; _ocNext[%3] = %4; mf; wait 0;
 MtransFlag01 = '1'; mreset; 
";

/* ------------------------------------------------------------------ */
/* Set the current origin (axes %1, %3) to the mouse position (%2, %4)*/
/* ------------------------------------------------------------------ */
(MacroDef "_SetOcMouse") = 
"if(_iMc2Oc > 0)
 { 
   Mc2Oc[_iMc2Oc].ofs[%1] = Mc2Oc[_iMc2Oc].ofs[%1] + %2; 
   Mc2Oc[_iMc2Oc].ofs[%3] = Mc2Oc[_iMc2Oc].ofs[%3] + %4; 
   SetMc2Oc _iMc2Oc;                           /* apply new offset  */
 }
";

/* ------------------------------------------------------------------ */
/* Set the current position to 0                                      */
/* ------------------------------------------------------------------ */
(MacroDef "_SetOcNull") = 
"if(_iMc2Oc > 0)
 { 
   if(%1 < 0)                                /* set all axes          */
   {
     Mc2Oc[_iMc2Oc].ofs = Vn0._;           /* set Oc offset = 0     */
     SetMc2Oc _iMc2Oc;                     /* apply                 */
     Mc2Oc[_iMc2Oc].ofs = getOc;           /* compensate tool[0]    */
     SetMc2Oc _iMc2Oc;                     /* apply new origin      */
   }

   if(%1 >= 0)                               /* set one axis          */ 
   {
     Mc2Oc[_iMc2Oc].ofs[%1] = 0.0;         /* set Oc offset = 0     */
     SetMc2Oc _iMc2Oc;                     /* apply                 */
     Mc2Oc[_iMc2Oc].ofs[%1] = getOc[%1];   /* compensate tool[0]    */
     SetMc2Oc _iMc2Oc;                     /* apply new origin      */
   }
 }
";

/* ------------------------------------------------------------------ */
/* Set the current toolpos (axes %1,%3) to the mouse position (%2,%4) */
/* ------------------------------------------------------------------ */
(MacroDef "_SetOcCurr") = 
"if(_iMc2Oc > 0)
 { 
   Mc2Oc[_iMc2Oc].ofs[%1] = Mc2Oc[_iMc2Oc].ofs[%1] + getoc[%1] - %2; 
   Mc2Oc[_iMc2Oc].ofs[%3] = Mc2Oc[_iMc2Oc].ofs[%3] + getoc[%3] - %4; 
   SetMc2Oc _iMc2Oc;                           /* apply new offset  */
 }
";

/* ================================================================== */
/* Goto-Macros                                                        */
/* ================================================================== */
C_MGoto = ObjDef T_classDscr            /* Goto macros:               */
"Pos"       T_object 0                  /* go to position %1          */
"Origin"    T_object 0                  /* go to origin               */
"OriginXY"  T_object 0                  /* go to origin X,Y only      */
"Surface"   T_object 0                  /* go to surface              */
"Switch"    T_object 0                  /* go to tool switch          */
"Service"   T_object 0                  /* go to service position     */
"Park"      T_object 0                  /* go to park position        */
"ToolHome"  T_object 0                  /* go to tool home            */
"McPos"     T_object 0                  /* go to machine position     */
"OcPos"     T_object 0                  /* go to object origin        */
"UserDef"   T_object 0                  /* user definded              */
"GetZMax"   T_object 0                  /* get maximum z value        */
"ZMaxVal"   T_double 0;                 /* maximum z value            */

_MGoto = ObjDef C_MGoto;                /* create an instance         */
_MGoto.ZMaxVal = -2.0;

/* ------------------------------------------------------------------ */
_MGoto.Pos = &(                                /* GOTO position %1    */       
/* ------------------------------------------------------------------ */
"local \"CallOnExit\" \"CallOnError\";
 CallOnExit = CallOnError = (sprint \"SetMc2Oc %d;\" _iMc2Oc)
            & \" Wait 0; MtransFlag01 = '1'; mreset;\";

 Wait 0; SetMc2Oc 0 1;                        // use Mc with tool offset
 MtransFlag01 = '0'; mreset;

 if(Thread_agi.vdim > 2)
  {_ocNext[2] = McPos[3].ofs[2]; MF;}          /* move tool up        */

 _ocNext[0] = %1[0]; 
 _ocNext[1] = %1[1]; MF;                       /* move XY             */ 

 if(Thread_agi.vdim > 2)
 {
   _OcNext    = %1;
   _OcNext[2] = McPos[3].ofs[2]; MF;           /* move all except Z   */ 
   _ocNext[2] = %1[2]; MF;                     /* move Z              */ 
 }
");

/* ------------------------------------------------------------------ */
_MGoto.Origin = &(                             /* GOTO ORIGIN:        */       
/* ------------------------------------------------------------------ */
"if(Mc2Oc[_iMc2Oc].iMt > 0)
  {
    // Msgbox \"Fahrbefehl mit dem aktuellen Nullpunkt nicht erlaubt\";
    SendError \"ERR_ExecUtils_MoveCommand_Origin\";
  }
 else
  {mcall *_MGoto.Pos Mc2Oc[_iMc2Oc].Ofs;}
");


/* ------------------------------------------------------------------ */
_MGoto.OriginXY = &(                           /* GOTO ORIGIN XY:     */       
/* ------------------------------------------------------------------ */
"local \"OldOc\" \"Dest\" \"CallOnExit\";

 if(Mc2Oc[_iMc2Oc].iMt > 0)
  {
   // Msgbox \"Fahrbefehl mit dem aktuellen Nullpunkt nicht erlaubt\";
   SendError \"ExecUtils_MoveCommand_Origin\";
  }
 else
 {
   CallOnExit = \"@SetMc2Oc OldOc;\";          /* restore old origin  */
   OldOc      = _iMc2Oc; SetMc2Oc 0;         /* use machine origin  */

   Wait 0; Dest = GetOc;
   Dest[0] = Mc2Oc[OldOc].Ofs[0];
   Dest[1] = Mc2Oc[OldOc].Ofs[1];

   mcall *_MGoto.Pos Dest;
 }
");

/* ------------------------------------------------------------------ */
_MGoto.Surface = &(                            /* GOTO SURFACE + Zup  */
/* ------------------------------------------------------------------ */
"local \"OldOc\" \"Dest\" \"CallOnExit\";

 if(Mc2Oc[_iMc2Oc].iMt > 0)
  {
   // Msgbox \"Fahrbefehl mit dem aktuellen Nullpunkt nicht erlaubt\";
   SendError \"ExecUtils_MoveCommand_Origin\";
  }
 else
 {
   CallOnExit = \"@SetMc2Oc OldOc;\";          /* restore old origin  */
   OldOc      = _iMc2Oc; SetMc2Oc 0;         /* use machine origin  */

   if(Thread_agi.vdim > 2)
   { 
     Wait 0; Dest = GetOc;
     Dest[2] = Mc2Oc[OldOc].Ofs[2] + zup;

     mcall *_MGoto.Pos Dest;
   }
 }
");

/* ------------------------------------------------------------------ */
_MGoto.Switch   = &(                       /* GOTO SWITCH + _TPprbZup */   
/* ------------------------------------------------------------------ */
"local \"Dest\"; Dest = PickPos[0].ofs;
 if(Thread_agi.vdim > 2)
  {Dest[2] = Dest[2] + _TPrbZup;}
 MCall *_MGoto.Pos Dest;
");

/* ------------------------------------------------------------------ */
_MGoto.GetZMax = &(                        /* maximize Z value        */   
/* ------------------------------------------------------------------ */
"local \"ZMax\"; ZMax = %1;
 if(ZMax > Tools[_ToolNc].Ofs[2] + _MGoto.ZMaxVal)
   {ZMax = Tools[_ToolNc].Ofs[2] + _MGoto.ZMaxVal;}
 ZMax                  // return result
");

/* ------------------------------------------------------------------ */
/* Additional Goto-Macros                                             */
/* ------------------------------------------------------------------ */
_MGoto.Service  = &("MCall *_MGoto.Pos McPos[1].ofs;");
_MGoto.Park     = &("MCall *_MGoto.Pos McPos[2].ofs;");
_MGoto.ToolHome = &("MCall *_MGoto.Pos McPos[3].ofs;");
_MGoto.McPos    = &("MCall *_MGoto.Pos McPos[%1].ofs;");
_MGoto.OcPos    = &("MCall *_MGoto.Pos OcPos[%1].ofs;");

/* ------------------------------------------------------------------ */
_MGoto.UserDef = &(                            /* GOTO user defined   */ 
/* ------------------------------------------------------------------ */
"local \"Text\";           
 Text = \"Redefine the _MGoto.UserDef macro in your EtInit_XX.ts file.\n\";
 MsgBox (Text & \"Use example from ExecMacros.ts file.\") \"Todo:\";  
");
