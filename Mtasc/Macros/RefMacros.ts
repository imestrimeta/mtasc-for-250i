/* ================================================================== */
/* Support searching of Reference Point                               */
/* ================================================================== */
 
(MacroDef "_RefTest") = "('1' == Mlimit '?') + ('1' == Mtest '?');";
RefDone = 0L;        /* bit mask of axes with reference run completed */

/*--------------------------------------------------------------------*/
/* Driver dependent macros, may be redefined in EtInit_XX.ts          */
/*--------------------------------------------------------------------*/
(MacroDef "DrvBeforeRef") = "";

(MacroDef "DrvAfterRef")  = 
"If(_ToolAuto >= 2) {QueryToolCalib;}    /* Ask for tool calibration  */
";

/* ================================================================== */
/* RefSearch macro (called ONLY by Refsrch.ts)                        */
/* ------------------------------------------------------------------ */
/* Parameters:                                                        */
/*   %1: Axis number (0=X)                                            */
/*   %2: Input Descriptor reference switch                            */
/*   %3: Input Descriptor index signal                                */
/*   %4: direction for driving to reference switch                    */
/*   %5: if searching index signal: Offset before searching           */
/*   %6: maximum distance for searching the index signal              */
/*   %7: Offset after referencing (with or without index signal)      */
/*   global: speed vectors spf (fast) and sps (slow)                  */
/*   update: refdone (bit vector)                                     */
/* ================================================================== */
(MacroDef "RefSearch") =
"Local \"AxisBitVal\" \"IdxMask\";
 AxisBitVal = (ARRAY 8 1 2 4 8 16 32 64 128)[%1];

 IdxMask = GetXmlAppGrp.Ref.IdxMask._Xml2Mt;
 IdxMask = IdxMask AND RefMask AND drv.laa.amask;

 if(RefMask and drv.laa.amask and AxisBitVal)
 { if('0' != mlimit '?')                /* Limit watching must be off */
    {mreset GetIniTxt \"System\" \"ErrLimits\";}

   local \"CallOnExit\" \"errMsg\";
   CallOnExit = sprint \"@MF_F = %.3lf; ML_F = %.3lf; MvPar;\"
                MF_F ML_F;                   /* restore speed setting */

   MF_f = spf[%1]; ML_f = sps[%1]; MVPAR;    /* set special speed     */

   for 3      // retry up to 3 times
   {
     mreset;
     errMsg = \"\";
     if(0 == (0 != In %2))                     /* move off from switch  */
     {
       stoprequest %2 1;
       _OcNext[%1] = _OcNext[%1] - 100 * %4; mf; wait 0 3;
       if(Mstat 'E')
         {errMsg = GetIniTxt \"System\" \"ErrFollow\"; continue;}
       if('I' != Mstop)
         {errMsg = GetIniTxt \"System\" \"ErrRefInp\"; continue;}
       mreset;
     }

     stoprequest %2 0;                         /* move towards switch   */
     _OcNext[%1] = _OcNext[%1] + 10000 * %4; mf; wait 0 3; 
     if(Mstat 'E')
       {errMsg = GetIniTxt \"System\" \"ErrFollow\"; continue;}
     if('I' != Mstop)
       {errMsg = GetIniTxt \"System\" \"ErrRefInp\"; continue;}
     mreset;

     stoprequest %2 1;                         /* leave switch slowly   */
     _OcNext[%1] = _OcNext[%1] - 10 * %4; ml; wait 0 3;
     if(Mstat 'E')
       {errMsg = GetIniTxt \"System\" \"ErrFollow\"; continue;}
     if('I' != Mstop)
       {errMsg = GetIniTxt \"System\" \"ErrRefInp\"; continue;}
     mreset;

     if(IdxMask AND AxisBitVal)                /* index bit is set      */
     {
       in %3;                                  // reset input latch for index
       _OcNext[%1] = _OcNext[%1] - %5 * %4; mf;
       local \"ok\" \"p0\"; ok = 0;
       p0 = getoc;
       while \"ok == 0\" 
       {
         Wait 0;
         ? sprint \"Searching Index %c\" Thread_agi.axislett[%1];
         in %3;                                // reset input latch for index
         stoprequest %3 1;                     /* search index signal   */
         _OcNext[%1] = _OcNext[%1] - %6 * %4; ml; wait 0 3;
         if(Mstat 'E')
           {errMsg = GetIniTxt \"System\" \"ErrFollow\"; continue;}
         if('I' == Mstop)
           {ok = 1;}
         else
           {mreset; _ocNext = p0; mf; wait 0;} // go back and try again
           //{mreset GetIniTxt \"System\" \"ErrIdxInp\";}
       }
       mreset;
     }

         _OcNext[%1] = _OcNext[%1] - %7 * %4; mf; wait 0;
         wait 0.2; _ocNext = getmc; _OcNext[%1] = 0; setmc;

     RefDone = RefDone or RefMask and drv.laa.amask and AxisBitVal;
     return;
   }
   // MsgBox errMsg;
   SendError \"ERR_RefMacros_General\";
   mreset \"\";
 }
";

/* ================================================================== */
// RefInput: move until input signal and stop there
/* ------------------------------------------------------------------ */
/* Parameters:  
//   %1: Axis Index
//   %2: Input Descriptor signal
//   %3: direction for searching
//   %4: maximum distance for searching
//   %5: speed
/* ================================================================== */
(MacroDef "RefInput") =
"
  local \"CallOnExit\"; CallOnExit = sprint \"@ml_f = %.3lf; MvPar;\" ml_f;
  ml_f = %5; MvPar;
  Wait 0;
  ? sprint \"Searching Index %c\" Thread_agi.axislett[%1];
  in %2;                                  // reset input latch for P4C1
  if(in %2)
      {mreset GetIniTxt \"System\" \"ErrInp\";}
  stoprequest %2 1;                     /* search index signal   */
  _OcNext[%1] = _OcNext[%1] - %4 * %3; ml;
  rmpFlush 0;
  wait 0 3;
  if(Mstat 'E')
      {mreset GetIniTxt \"System\" \"ErrFollow\";}
  if('I' == Mstop)
      {mreset;}     // OK
  else
      {mreset GetIniTxt \"System\" \"ErrIdxInp\";}
";

