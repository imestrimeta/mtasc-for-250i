/*====================================================================*/
/* Macros for processing of DXF-converted output                      */
/*====================================================================*/
/* neu: Anfahren mit alter H�he, falls >= zup                         */
/*      seit 24.09.2004 auch f�r Bohren                               */
/*      seit 02.12.2004 mit Taschen-Macros                            */
/*      seit 02.02.2005 angepasst auf EdiTasc 8.8                     */
/*      seit 17.04.2005 mit �berarbeitetem Konturbeginn               */
/*      seit 16.11.2006 mit Maximall�nge von Strecken und B�gen       */
/*      seit 22.11.2006 mit MLD-Befehl ab Zdn0                        */
/*--------------------------------------------------------------------*/
/* general settings and macros                                        */
/*--------------------------------------------------------------------*/
_crcPar.m_autozup  = 1;
_crcPar.m_Zdn0Flag = 1;

_GapTol   = 0.5;                          /* Setting for finding gaps */
_ShowCont = 0;                            /* show contour begin/end   */ 

_ContState =   0;                         /* Initial values           */
_ContOn    =   0;
_MaxLen    = 0.0;                         /* Init=0.0: no MaxLen      */

/* Look-Ahead-Puffer f�r die Radiuskorrektur _crcpar.BufSize:         */
/* �ber Men� Maschine / Maschinen-Parameter einstellen!               */

/****** Eventuell alten Crc STatus zum Wiederherstellen merken ********/

                                     /* Macro f�r Anschlusstest       */
(MacroDef "Gap") = "(_GapTol < %1 - %2) or (_GapTol < %2 - %1);";

//(MacroDef "MToolNew") =              /* Aktionen bei Werkzeugwechsel  */
//"if (%1 > 0) {Gettool %1;}";

CGroup = ObjDef T_classDscr              /* Group class =             */
"Process" T_short  0                     /* process type              */
"Cont"    T_short  0                     /* Continuous flag           */
"Tool"    T_short  0                     /* Tool identifier (EMU)     */
"Visible" T_short  0                     /* Visible flag              */
"Name"    T_object 0;                    /* Group name                */

_GroupState = ObjDef CGroup 10;          /* 10 empty instances        */
_GroupIndex = 0;

_GroupState[0].Process = 0;
_GroupState[0].Cont    = 0;
_GroupState[0].Tool    = 0;
_GroupState[0].Visible = 0;
(*_GroupState[0].Name) = "EdiTasc";

/*====================================================================*/
/* Macro zum Setzen des Bearbeitungsstatus                            */
/*--------------------------------------------------------------------*/
/* Das Macro erwartet 5 Parameter:                                    */
// %1: Draw|Mill / CRC: 0 = draw only
//                      1 = mill without CRC
//                      2 = mill with CRC right
//                      3 = mill with CRC left
//                      4 = drill
// %2: Contour type:    0=disconnected, 1=open,  2=closed
// %3: Tool (EMU)
// %4: Visible: 0=Hidden, 1=Visible
// %5: Group name
/*====================================================================*/
(MacroDef "MGroupBegin") =  
"SetStatus %1 %2 %3 %4 %5;  
 
 if(_GroupIndex < 9)
 { _GroupIndex = _GroupIndex + 1;
   _GroupState[_GroupIndex].Process = %1;
   _GroupState[_GroupIndex].Cont    = %2;
   _GroupState[_GroupIndex].Tool    = %3;
   _GroupState[_GroupIndex].Visible = %4;
   (*_GroupState[_GroupIndex].Name) = %5;
 }
";

(MacroDef "MGroupEnd") = 
"if(_GroupIndex > 0) {_GroupIndex = _GroupIndex - 1;}
 SetStatus _GroupState[_GroupIndex].Process 
           _GroupState[_GroupIndex].Cont
           _GroupState[_GroupIndex].Tool
           _GroupState[_GroupIndex].Visible
           *_GroupState[_GroupIndex].Name;
";

(MacroDef "SetStatus") = 
"if(_mtStat.crcState == 2) {CrcClose; _mtStat.crcState = 1; z zup; mf;}
 if(_ContState       == 1) {Contour 0; z zup; mf;}    /* Kontur Ende */  

 _Crcpar.m_isClosed = 0; if(%2 == 2) {_Crcpar.m_isClosed = 1;}

 if(%1 == 0) {_Crcpar.Ori =  0; _mtStat.crcState = 0;} /* draw        */
 if(%1 == 1) {_Crcpar.Ori =  0; _mtStat.crcState = 0;} /* mill        */
 if(%1 == 2) {_Crcpar.Ori = -1; _mtStat.crcState = 1;} /* mill  ->    */
 if(%1 == 3) {_Crcpar.Ori =  1; _mtStat.crcState = 1;} /* mill  <-    */
 if(%1 == 4) {_Crcpar.Ori =  0; _mtStat.crcState = 0;} /* drill       */
";

/*--------------------------------------------------------------------*/
/* parameter conversion from string to integer                        */
/*--------------------------------------------------------------------*/
(MacroDef "MObjType") =
"local \"ObjTest\" \"Result\";
 ObjTest = %1;
 Result  = 0; 

 if(T_Char != (Typeof \"ObjTest\").Type) {Result = %1;}

 if(T_Char == (Typeof \"ObjTest\").Type)
 {
   if(0 < ObjTest == \"LINE\") {Result = 1;}
   if(0 < ObjTest == \"CIRC\") {Result = 2;}
   if(0 < ObjTest == \"ARC\")  {Result = 3;}
   if(0 < ObjTest == \"RPOC\") {Result = 4;}
   if(0 < ObjTest == \"CPOC\") {Result = 5;}
   if(Result == 0) {? \"DXF-Object not found: <\" & ObjTest & \">\";}
 }
Result";

/*--------------------------------------------------------------------*/
/* drawing macros                                                     */
/*--------------------------------------------------------------------*/
(MacroDef "MDraw") =
"local \"px\" \"py\";
 if((_GroupState[_GroupIndex].Tool > 0) and (_ToolSel != _GroupState[_GroupIndex].Tool))
  {Gettool _GroupState[_GroupIndex].Tool;}
 if((1 == MObjType %1) AND (_Mode == 'G'))            /* LINE         */
  {x %2; y %3; MF; z %4; ML;                          /* anfahren     */
   x %5; y %6; z %7; ML;                              /* arbeiten     */
  }

 if((2 == MObjType %1) AND (_Mode == 'G'))            /* CIRCLE       */
  {x %2; y %3; MF; z %4; ML;                          /* M anfahren   */ 
   _GrMode = 0; x %2 + %5; MF; _GrMode = 1;           /* zum Rand     */
   x %2 - %5; MC %5 \"+LZ\";                          /* 2 Halbkreise */
   x %2 + %5; MC %5 \"+LZ\";
   _GrMode = 0; x %2; MF; _GrMode = 1;                /* zur�ck zu M  */
  }

 if((3 == MObjType %1) AND (_Mode == 'G'))            /* ARC          */
  {Px = %2 + %5 * cos %6 + 0.0;                       /* Startpkt     */
   Py = %3 + %5 * sin %6 + 0.0;
   x Px; y Py; MF; z %4; ML;                          /* anfahren     */
 
   Px = %2 + %5 * cos %7 + 0.0;                       /* Endpunkt     */
   Py = %3 + %5 * sin %7 + 0.0;

   if(%6 < %7)                                        /* Linksb�gen   */
    {if(M_PI >= %7 - %6){x Px; y Py; MC %5 \"+LZ\";}
     if(M_PI <  %7 - %6){x Px; y Py; MC %5 \"-LZ\";}
    }
    if(%6 > %7)                                       /* Rechtsb�gen  */
    {if(M_PI >= %6 - %7){x Px; y Py; MC %5 \"-RZ\";}
     if(M_PI <  %6 - %7){x Px; y Py; MC %5 \"+RZ\";}
    }
  }

 if((4 == MObjType %1) AND (_Mode == 'G'))            /* RPOCKET      */
  {x %2; y %6; z %4; MF;                              /* approach     */

                           y 0.25 * (%3 + 3 * %6); MF;/* diag. lines  */
   x 0.25 * (%5 + 3 * %2); y %6;                   ML;
   x 0.5 *  (%2 + %5);                             MF;
   x %2;                   y 0.5  * (%3 +     %6); ML;
                           y 0.25 * (%6 + 3 * %3); MF;
   x 0.25 * (%2 + 3 * %5); y %6;                   ML;

   x %5; MF; x %2; y %3; ML;                          /* main diag.   */

   x 0.25 * (%5 + 3 * %2);                         MF;/* diag. lines  */
   x %5;                   y 0.25 * (%3 + 3 * %6); ML;
                           y 0.5  * (%3 +     %6); MF;
   x 0.5  * (%2 +     %5); y %3;                   ML;
   x 0.25 * (%2 + 3 * %5);                         MF;
   x %5;                   y 0.25 * (%6 + 3 * %3); ML;
                           Y %3;                   MF;

   x %2; ML; y %6; ML; x %5; ML; y %3; ML;            /* Frame        */
  }

 if((5 == MObjType %1) AND (_Mode == 'G'))            /* CPOCKET      */
  {x %2; y %3; MF; x %2 - %5; MF; z %4; ML;           /* anfahren     */

   x %2; y %3 + %5; ML;                               /* inner lines  */
   x %2 + 0.707 * %5; y %3 + 0.707 * %5; MF;
   x %2 - 0.707 * %5; y %3 - 0.707 * %5; ML;
   x %2; y %3 - %5; MF;
   x %2 + %5; y %3; ML;       

   x %2 - %5; MC %5 \"+LZ\";                          /* 2 Halbkreise */
   x %2 + %5; MC %5 \"+LZ\";
   x %2; MF;                                          /* abfahren     */
  }
";
	
/*====================================================================*/
/* milling macros                                                     */
/*====================================================================*/
(MacroDef "MMill") =
"local \"Rad\" \"ClsMerk\" \"CircOri\" \"Zup1\" \"Zdn1\" \"Zdn2\" \"ZdnX\";
 local \"px\" \"py\"; 

 if((_GroupState[_GroupIndex].Tool > 0) and (_ToolSel != _GroupState[_GroupIndex].Tool))
  {Gettool _GroupState[_GroupIndex].Tool;}

 Zup1 = z;    if(Zup1 <  zup) {Zup1 =  Zup;}  	
 Zdn1 = %4;   if(Zdn1 == 0  ) {Zdn1 =  zdn;} 
 ZdnX = Zdn0; if(ZdnX > Zup1) {ZdnX = Zup1;}
              if(ZdnX < Zdn1) {ZdnX = Zdn1;}
 
/*--------------------------------------------------------------------*/
/* Linie                                                              */
/*--------------------------------------------------------------------*/
 if(1 == MObjType %1)                                 /* LINE         */
 { 
   if((Gap (X) %2) or (Gap (Y) %3) or (Gap (z) Zdn1))
   {
     if(_ContState       == 1){Contour 0;}  
     if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}
     z zup1; mf; x %2; y %3; mf; z zup; mf;           /* anfahren     */
   }
  
   if(_mtStat.crcState == 1)
    {crcOpen; _mtStat.crcState = 2;}                  /* RK ein       */
 
   if(Gap (Z) Zdn1) {z ZdnX; ml; z Zdn1; mld;}        /* absenken     */
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   if(_ContState == 0) {Contour 1;}

   Zdn2 = %7; if(Zdn2 == 0) {Zdn2 = zdn;} 

   local \"LineLen\" \"i\" \"n\";
   LineLen = SQRT(((%2 - %5) * (%2 - %5)) + ((%3 - %6) * (%3 - %6)));

   n = 1;
   if(_MaxLen > 0) {n = 1 + LineLen / _MaxLen;}

   i = 0;
   for n
   {
     if(n > i = i + 1)                                /* Teilst�ck    */
     {
       x %2 + i * (%5 - %2) / n;
       y %3 + i * (%6 - %3) / n;
       z Zdn1 + i * (Zdn2 - Zdn1) / n; ml;
     }

     if(i == n) {x %5; y %6; z Zdn2; ml;}             /* Restst�ck    */
   }
 }
 
/*--------------------------------------------------------------------*/
/* Kreis                                                              */
/*--------------------------------------------------------------------*/
 if(2 == MObjType %1)                                 /* CIRCLE       */
  {if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}

   if(_ContState == 1) {Contour 0;}  
   CircOri = 1; if(%P >= 7){CircOri = %7;}
   ClsMerk = _Crcpar.m_isClosed; _Crcpar.m_isClosed = 0; 
 
   z zup1; mf; x %2 + %5; y %3; mf;  z zup; mf;       /* anfahren     */
  
   if(_mtStat.crcState == 1)
    {crcOpen; _mtStat.crcState = 2;}                  /* RK ein       */

   if(Gap (Z) Zdn1) {z ZdnX; ml; z Zdn1; mld;}        /* absenken     */
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   if(_ContState == 0) {Contour 1;}

   local  \"i\" \"n\" \"Phi\" \"Cmd\";

   Cmd = \"+LZ\"; if(CircOri < 0) {Cmd = \"-RZ\";}

   n = 2;                                             /* Anzahl St�cke*/
   if(_MaxLen > 0)
   {
     if(n < %5 * 2 * M_PI / _MaxLen){n = %5 * 2 * M_PI / _MaxLen;}
   } 

   i = 1;   
   for(n)                                             /* ausf�hren    */
   {
     Phi = CircOri * i * 2.0 * M_PI / (n + 0.0);      /* Winkel       */
     if(i == n) {Phi = 0;}
     X %2 + %5 * cos Phi;                             /* Punkt        */  
     y %3 + %5 * sin Phi; 
     mc %5 Cmd;                                       /* anfahren     */
     i = i + 1;
   }

   if(_mtStat.crcState == 2)
    {crcClose; _mtStat.crcState = 1;}                 /* RK aus       */
   _Crcpar.m_isClosed = ClsMerk;                      /* alter Wert   */
  }

/*--------------------------------------------------------------------*/
/* Kreisbogen                                                         */
/*--------------------------------------------------------------------*/
 if(3 == MObjType %1)                                 /* ARC          */
  {Px = %2 + %5 * cos %6 + 0.0;                       /* Startpkt     */
   Py = %3 + %5 * sin %6 + 0.0;
   if((Gap (X) Px) or (Gap (Y) Py) or (Gap (Z) Zdn1))
    {
     if(_ContState == 1)      {Contour 0;}  
     if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}
     z zup1; mf; x Px; y Py; mf;  z zup; mf;          /* anfahren     */
    }

   if(_mtStat.crcState == 1)
    {crcOpen; _mtStat.crcState = 2;}                  /* RK ein       */

   if(Gap (Z) Zdn1) {z ZdnX; ml; z Zdn1; mld;}        /* absenken     */
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   if(_ContState == 0) {Contour 1;}

   local  \"i\" \"n\" \"Phi\" \"Cmd\";

   Cmd = \"+LZ\"; if(%6 > %7) {Cmd = \"-RZ\";}        /* links/rechts */

   n = 1;                                             /* Anzahl St�cke*/
   if(M_PI < %7 - %6) {n = 2;} 
   if(M_PI < %6 - %7) {n = 2;} 

   if(_MaxLen > 0)                                    /* zerlegen     */
   {
     if(n < %5 * (%7 - %6) / _MaxLen){n = %5 * (%7 - %6) / _MaxLen;}
     if(n < %5 * (%6 - %7) / _MaxLen){n = %5 * (%6 - %7) / _MaxLen;}
   } 

   i = 1;   
   for(n)                                             /* ausf�hren    */
   {
     Phi = %6 + i * (%7 - %6) / (n + 0.0);            /* Winkel       */
     if(i == n) {Phi = %7;}
     X %2 + %5 * cos Phi;                             /* Punkt        */  
     y %3 + %5 * sin Phi; 
     mc %5 Cmd;                                       /* anfahren     */
     i = i + 1;
   }
  }

/*--------------------------------------------------------------------*/
/* Rechteck-Tasche                                                    */
/*--------------------------------------------------------------------*/
 if(4 == MObjType %1)                                 /* RPOCKET      */
  {if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}
   z zup1; mf;                                        /* Z einstellen */

   Rad = 0.0 + Tools[_toolSel].Rad;                   /* Startposition*/
   x %2 + 1.1 * Rad;  if(%5 < %2) {x %5 + 1.1 * Rad;}
   y %3 + 1.1 * Rad;  if(%6 < %3) {y %6 + 1.1 * Rad;}
   mf;  z zup; mf;                                    /* anfahren     */
  
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   RPocket %2 %3 %4 %5 %6 %7 %8 %9 %10;               /* fr�sen       */

   if(_Mode == 'G')                                   /* Rahmen       */
    {SetDrawPar 'L' 0xFFFFFFL -1 -1 -1;               /* set color    */

     x %2; if(%5 < %2) {x %5;}
     y %3; if(%6 < %3) {y %6;}
     z %4; if(%7 > %4) {z %7;} MF;

     x %2; if(%5 > %2) {x %5;}
     y %3; if(%6 < %3) {y %6;} ML;

     x %2; if(%5 > %2) {x %5;}
     y %3; if(%6 > %3) {y %6;} ML;

     x %2; if(%5 < %2) {x %5;}
     y %3; if(%6 > %3) {y %6;} ML;

     x %2; if(%5 < %2) {x %5;}
     y %3; if(%6 < %3) {y %6;} ML;

     GetTool _ToolSel;                                /* reset color  */
    }
  }

/*--------------------------------------------------------------------*/
/* Kreis-Tasche                                                       */
/*--------------------------------------------------------------------*/
 if(5 == MObjType %1)                                 /* CPOCKET      */
  {if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}
   z zup1; mf; x %2 + %5; y %3; mf;  z zup; mf;       /* anfahren     */
  
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   CPocket %2 %3 %4 %5 %6 %7 %8 %9;                   /* fr�sen       */

   if(_Mode == 'G')                                   /* Zusatzgrafik */
    {SetDrawPar 'L' 0xFFFFFFL -1 -1 -1;               /* set color    */
   
     x %2 + %5; y %3; Z %4; mf;                       /* anfahren     */
     x %2 - %5; MC %5 \"+LZ\";                        /* 2 Halbkreise */
     x %2 + %5; MC %5 \"+LZ\";
     x %2; mf;                                        /* abfahren     */

     GetTool _ToolSel;                                /* reset color  */
    }
  }
";

/*--------------------------------------------------------------------*/
/* drilling macros                                                    */
/*--------------------------------------------------------------------*/
(MacroDef "MDrill") = 
"local \"Zup1\" \"Zdn1\" \"Zdn2\";
 Zup1 = z; if(Zup1 < zup) {Zup1 = Zup;}  	
 Zdn1 = %4; if(Zdn1 == 0)  {Zdn1 = zdn;} 

 if(2 == MObjType %1)                       /*CIRCLE => senkrecht     */
  {if(_ContState == 1) {Contour 0;}  
   z zup1; mf; x %2; y %3; mf; z zup; mf;             /* anfahren     */
   
   if(_ContOn) {Contour 1; _ContOn = 0;}              /* Konturstart  */
   if(_ContState == 0) {Contour 1;}

   z zdn1; mlD;                                       /* bohren       */
   z zup; mf;                                         /* abfahren     */

   if(_Mode == 'G')                                   /* CIRCLE       */
    {x %2; y %3; MF; z %4; ML;                        /* M anfahren   */ 
     x %2 + %5; MfHide;                               /* zum Rand     */
     x %2 - %5; MC %5 \"+LZ\";                        /* 2 Halbkreise */
     x %2 + %5; MC %5 \"+LZ\";
     x %2; MfHide;                                    /* zur�ck zu M  */
    }
  }
";

/*--------------------------------------------------------------------*/
/* MAnf/MEnd macros called at beginning/end of contours               */
/*--------------------------------------------------------------------*/
_ContOn = 0;
(MacroDef "Default") = "";

(MacroDef "ContStart") =          /* Macro am Anfang eines Linienzugs */
"_ContOn = 1;";                   /* vor Beginn der Leerfahrt         */

(MacroDef "ContStop") =           /* Macro am Ende eines Linienzugs   */
"if(_mtStat.crcState == 2){crcClose; _mtStat.crcState = 1;}
 Contour 0; 
 if(zup > Z) {Z Zup; MF;}         /* go up if necessary               */ 
";

(MacroDef "Contour") =            /* Macro am Anfang eines Linienzugs */
"Local \"_XM\" \"_YM\" \"dx\";
 wait 0;  
 
 dx = 1.0; if(%1 == 0) {dx = -1.0;}
 if(_Mode == 'G')                 /* direkt vor dem ersten Fahrbefehl */
 {
   if(_ShowCont)                  /* Kontur mit <> markieren          */ 
   {
     _XM = X; _YM = Y;  
     _GrMode = 0; X _Xm + dx; y _YM + 0.5 * dx; ML; 
     _GrMode = 1; X _XM;      y _ym;            ML;
     _GrMode = 0; X _Xm + dx; y _YM - 0.5 * dx; ML; 
     _GrMode = 1; X _XM;      y _ym;            ML;
   }
 }
 
 if(%1 != 0)                      /* call macros to begin a contour   */
 {
   if(_Mode == 'G') {ContBeginG;}
   if(_Mode != 'G') {ContBeginX;}
 }

 if(%1 == 0)                      /* call macros to end a contour     */
 {
   if(_Mode == 'G') {ContEndG;}
   if(_Mode != 'G') {ContEndX;}
 }

 _ContState = %1;                 /* set flag                         */
";                     

(MacroDef "ContBeginG") = "";     /* New Macros when contour begins   */
(MacroDef "ContBeginX") = "";     /* G=Graphic, X=Execution           */
(MacroDef "ContEndG")   = "";     /* New Macro when contour ends      */
(MacroDef "ContEndX")   = "";     /* G=Graphic, X=Execution           */

/*====================================================================*/
/* Macro zum Fr�sen einer Kreistasche                                 */
/*--------------------------------------------------------------------*/
/* Das Macro erwartet 8 Parameter:                                    */
/* %1: X-Koordinate des Mittelpunktes                                 */
/* %2: y-Koordinate des Mittelpunktes                                 */
/* %3: Z-Koordinate des Mittelpunktes                                 */
/* %4: Radius des Kreises                                             */
/* %5: Tiefe der Tasche                                               */
/* %6: Dicke einer Schicht                                            */
/* %7: Schlichten einzeln (%8 == 1)/gesamt (%8 == 2)                  */
/* %8: Rand f�r Schruppen/Schlichten                                  */
/*--------------------------------------------------------------------*/
/* Fr�stiefe von Z=Kreismittelpukt bis Taschentiefe                   */
/*====================================================================*/
(MacroDef "CPocket") =                       /* Definition des Macros */
"local \"zCur\" \"zEnd\" \"Rad\" \"dZ\";
 local \"Skz\"  \"Rand\";

 ZEnd = %3 - %5; 
 dZ = 0.0 + %6; if(dZ < 0.1) {dZ = 0.1;} 

 Skz = %7;        if(%8  <= 0) {Skz  = 0;}   /* Schlicht-Kennzeichen  */ 
 Rand = 0.0 + %8; if(Skz == 0) {Rand = 0;} 

 Rad = 0.0 + Tools[_toolSel].Rad;            /* Werkzeugradius        */ 

 if (Rad == 0)        {Mreset \"Werkzeugradius ist null!\";}
 if (Rad > %4 - Rand) {Mreset \"Werkzeugradius ist zu gross!\";}
 
 z zup; mf;                                  /* Werkzeug anheben      */ 
 x %1 + (%4 - Rand) - Rad; y %2; mf;         /* Punkt rechts anfahren */

 zCur = 0.0 + %3;

 while \"zCur > ZEnd + 0.001\"               /* Schleife f�r Tiefe    */
 {
   zCur = zCur - dZ;                         /* Tiefe anpassen        */
   if (zCur < ZEnd) {zCur = ZEnd;} 

   cPocketLvl %1 %2 zCur (%4 - Rand) Rad;    /* eine Schicht fr�sen   */

   if(Skz == 1)                              /* einzeln schlichten    */
   { x %1 + (%4 - Rad); y %2; ml;                      /* anfahren    */
     x %1 - (%4 - Rad); MC (%4 - Rad) \"+LZ\";         /* 2 Halbkreise*/
     x %1 + (%4 - Rad); MC (%4 - Rad) \"+LZ\";
   }
 } 

 if(Skz == 2)                                /* gesamt schlichten     */ 
 { x %1 + (%4 - Rad); y %2; ml;                        /* anfahren    */
   x %1 - (%4 - Rad); MC (%4 - Rad) \"+LZ\";           /* 2 Halbkreise*/
   x %1 + (%4 - Rad); MC (%4 - Rad) \"+LZ\";
 }
 z zup; mf;                                  /* auftauchen            */ 
";

/*====================================================================*/
/*  create one level for a circle pocket                              */
/*--------------------------------------------------------------------*/
/* Das Macro erwartet 5 Parameter:                                    */
/* %1: X-Koordinate des Mittelpunktes                                 */
/* %2: y-Koordinate des Mittelpunktes                                 */
/* %3: Z-Koordinate des Mittelpunktes                                 */
/* %4: Radius des Kreises                                             */
/* %5: Radius des Werkzeugs                                           */
/*--------------------------------------------------------------------*/
/* Fr�stiefe ist z = %3                                               */
/*--------------------------------------------------------------------*/
(MacroDef "CPocketLvl") =                    /* Definition des Macros */
"local \"n\" \"i\" \"dBahn\" \"Cover\" \"dbl\";

 Cover = 0.9;                                /* �berdeckung           */

 dbl   = 0.0 + %4 + %5 * ((2 * Cover) - 1);  /* Zwischenwert          */
 n = 0; n = dbl / (2 * %5);                  /* Anzahl Ringe          */
 dBahn = 2.0 * (%4 - %5) / ((2 * n) - 1);    /* Bahn-Abstand          */

 dbl = %4 - (%5 + (n - 1) * dBahn);          /* Bahn-Punkt = Radius   */
 X %1 - dbl; y %2; Z %3; ML;                 /* Anfahrt               */
 X %1 + dbl; Mc dbl \"+LZ\";                 /* erster Halbkreis      */

 i = 1; for (n - 1)
 {
   dbl = %4 - (%5 + (n - i) * dBahn);        /* Bahn-Radius           */
   X %1 - dbl; Mc dbl \"+LZ\";               /* zwei Halbkreise       */
   X %1 + dbl + dBahn; Mc (dbl + 0.5 * dBahn) \"+LZ\";   
   i = i + 1;
 }

 dbl = %4 - %5;                              /* Bahn-Radius           */
 X %1 - dbl; Mc dbl \"+LZ\";                 /* letzter Kreis         */
 X %1 + dbl; Mc dbl \"+LZ\";              
";

/*====================================================================*/
/* Macro zum Fr�sen einer Rechtecktasche                              */
/*--------------------------------------------------------------------*/
/* Das Macro erwartet 9 Parameter:                                    */
/* %1: X-Koordinate von Eckpunkt 1                                    */
/* %2: y-Koordinate von Eckpunkt 1                                    */
/* %3: Z-Koordinate von Eckpunkt 1                                    */
/* %4: X-Koordinate von Eckpunkt 2                                    */
/* %5: y-Koordinate von Eckpunkt 2                                    */
/* %6: Z-Koordinate von Eckpunkt 2                                    */
/* %7: Dicke einer Schicht                                            */
/* %8: Schlichten einzeln (%8 == 1)/gesamt (%8 == 2)                  */
/* %9: Rand f�r Schruppen/Schlichten                                  */
/*--------------------------------------------------------------------*/
/* Fr�stiefe von Z=Eckpunkt1.Z bis Z=Eckpunkt2.Z                      */
/*====================================================================*/
(MacroDef "RPocket") =                       /* Definition des Macros */
"local \"Lft\"  \"Rgt\"  \"Bot\"  \"Top\"; 
 local \"zCur\" \"zEnd\" \"Rad\" \"dx\" \"dy\" \"dZ\";
 local \"SKz\" \"Rand\";

 Lft  = 0.0 + %1; if(Lft  >  %4) {Lft  = %4;}/* Parameter-Plausi      */
 Bot  = 0.0 + %2; if(Bot  >  %5) {Bot  = %5;}
 zCur = 0.0 + %3; if(ZCur <  %6) {ZCur = %6;}
 Rgt  = 0.0 + %4; if(Rgt  <  %1) {Rgt  = %1;}
 Top  = 0.0 + %5; if(Top  <  %2) {Top  = %2;}
 zEnd = 0.0 + %6; if(ZEnd >  %3) {ZEnd = %3;}

 dx   = 0.5 * (Rgt - Lft);
 dy   = 0.5 * (Top - Bot);
 dZ   = 0.0 + %7; if(dZ   < 0.1) {dZ   = 0.1;} 

 Skz = %8;        if(%9  <= 0) {Skz  = 0;}   /* Schlicht-Kennzeichen  */ 
 Rand = 0.0 + %9; if(Skz == 0) {Rand = 0;} 

 Rad = 0.0 + Tools[_toolSel].Rad;            /* Werkzeugradius        */ 
 
 if(Rad == 0)        {Mreset \"Werkzeugradius ist null!\";}
 if(Rad > dx - Rand) {Mreset \"Werkzeugradius ist zu gross!\";}
 if(Rad > dy - Rand) {Mreset \"Werkzeugradius ist zu gross!\";}

 z zup; mf;                                  /* Werkzeug anheben      */ 
 x Lft + 1.1 * Rad + Rand;
 y Bot + 1.1 * Rad + Rand; mf;               /* Punkt links unten     */

 //zCur = zCur + dz; 
 while \"zCur > ZEnd + 0.001\"               /* Schleife f�r Tiefe    */
 {
   zCur = zCur - dZ;                         /* Tiefe anpassen        */
   if (zCur < ZEnd) {zCur = ZEnd;} 

   RPocketLvl Lft Bot Rgt Top zCur Rad Rand; /* eine Schicht fr�sen   */

   if(Skz == 1)                              /* einzeln schlichten    */
   { x Lft + Rad; y Bot + Rad; ml;
     x Rgt - Rad; ml;
     y Top - Rad; ml;
     x Lft + Rad; ml;
     y Bot + Rad; ml;
   }
 } 

 if(Skz == 2)                                /* gesamt schlichten     */ 
 { x Lft + Rad; y Bot + Rad; ml;
   x Rgt - Rad; ml;
   y Top - Rad; ml;
   x Lft + Rad; ml;
   y Bot + Rad; ml;
 }
 z zup; mf;                                  /* auftauchen            */ 
";

/*====================================================================*/
/*  create one level for a rectangle pocket                           */
/*--------------------------------------------------------------------*/
/* Das Macro erwartet 7 Parameter:                                    */
/* %1: X-Koordinate von Eckpunkt 1                                    */
/* %2: y-Koordinate von Eckpunkt 1                                    */
/* %3: X-Koordinate von Eckpunkt 2                                    */
/* %4: y-Koordinate von Eckpunkt 2                                    */
/* %5: Z-Koordinate der Fr�stiefe                                     */
/* %6: Radius des Werkzeugs                                           */
/* %7: Rand f�r Schruppen/Schlichten                                  */
/*--------------------------------------------------------------------*/
/* Gefr�st wird mit z = %5                                            */ 
/* "Cover" (<=1) ist der Abstand der Bahnen in Werkzeugdurchmessern.  */
/*--------------------------------------------------------------------*/
(MacroDef "RPocketLvl") =                    /* Definition des Macros */
"local \"n\" \"i\" \"dbl\";
 local \"dBahn\" \"rTool\" \"dRect\" \"Cover\" \"Rand\";
 local \"Lft\" \"Rgt\" \"Bot\" \"Top\"; 
 Cover = 0.9;                                /* �berdeckung           */
 rTool = 0.0 + %6;  
 Rand  = 0.0 + %7;

 Lft = 0.0 + %1 + Rand; Bot = 0.0 + %2 + Rand;
 Rgt = 0.0 + %3 - Rand; Top = 0.0 + %4 - Rand;

 dRect = Rgt - Lft;
 if(dRect > Top - Bot) {dRect = Top - Bot;}

 dbl = dRect + (((2 * Cover) - 1.0) * 2 * rTool);
 n = 0; n = 0.5 + dbl / (4 * Cover * rTool);
 dBahn = (dRect - 2 * rTool) / ((2 * n) - 1);

 If((Rgt - Lft) >= (Top - Bot))
 { 
   X Lft + rTool * (2 - Cover);              /* Anfahrt               */
   Y Bot + (rTool + (n - 1) * dBahn); Mf;

   X Lft + n * rTool; Z %5; ML;              /* absenken              */   

   X Rgt - n * rTool; ML;                    /* erste Fahrt           */
   Y Top - rTool + (n - 1) * dBahn; ML;      /* zweite Fahrt          */
    
   i = 2; For(n - 1)
   {
     X Lft + (rTool + (n - i) * rTool); ML;  /* Kasten unten          */
     Y Bot + (rTool + (n - i) * dBahn); ML;  /* Kasten rechts         */ 
     X Rgt - (rTool + (n - i) * rTool); ML;  /* Kasten oben           */
     Y Top - (rTool + (n - i) * dBahn); ML;  /* Kasten links          */
     i = i + 1;
   }
   
   X Lft + rTool; ML;                        /* vorletzte Fahrt       */
   Y Bot + (rTool + (n - 1) * dBahn); ML;    /* letzte Fahrt          */
   X Lft + rTool * (2 - Cover); ML;          /* zum Startpunkt        */
 }

 If((Rgt - Lft) < (Top - Bot))
 { 
   X Rgt - (rTool + (n - 1) * dBahn);        /* Anfahrt               */
   Y Bot + rTool * (2 - Cover); MF;
    
   Y Bot + n * rTool; Z %5; ML;              /* absenken              */   

   Y Top - n * rTool; ML;                    /* erste Fahrt           */
   X Lft + (rTool + (n - 1) * dBahn); ML;    /* zweite Fahrt          */
   
   i = 2; For(n - 1)
   {
     Y Bot + (rTool + (n - i) * rTool); ML;  /* Kasten rechts         */
     X Rgt - (rTool + (n - i) * dBahn); ML;  /* Kasten oben           */ 
     Y Top - (rTool + (n - i) * rTool); ML;  /* Kasten links          */
     X Lft + (rTool + (n - i) * dBahn); ML;  /* Kasten unten          */
     i = i + 1;
   }
   
   Y Bot + rTool; ML;                        /* vorletzte Fahrt       */
   X Rgt - rTool + (n - 1) * dBahn; ML;      /* letzte Fahrt          */
   Y Bot + rTool * (2 - Cover); ML;          /* zum Startpunkt        */
 }
";
