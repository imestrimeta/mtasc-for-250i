/* ================================================================== */
/* SetOrigin.ts: Origin setting                                       */ 
/* ================================================================== */
/* ZZeroIni: Set origin offsets for switch and laser                  */
/*--------------------------------------------------------------------*/
(MacroDef "ZZeroIni") =
"Local \"ParmFile\"; ParmFile = MtApp.path & \"/ParamSet.ini\";

 If(%1 == 0)                           // Werkzeug auf Referenzfl�che
 {
   WriteIniString ParmFile \"ZZero\" \"ZZeroToolRef\"
                  sprint \"%9.3lf\" GetMc[2] + Tools[_ToolNC].Ofs[2];
   Z _Z + 25; mf;
 }
    
 If(%1 == 1)                           // Taster auf Referenzfl�che
 {
   ZZeroRef;
   WriteIniString ParmFile \"ZZero\" \"ZZeroSurface\"
                  sprint \"%9.3lf\" GetMc[2];
   Z _Z + 10; mf;
 }
    
 If(%1 == 2)                           // X und Y mit Werkzeug
 {
   WriteIniString ParmFile \"ZZero\" \"ZZeroToolPos\"
                  sPrint \"%9.3lf %9.3lf\" GetMc[0] GetMc[1];
 }

 If(%1 == 3)                           // X und Y mit Laser
 {
   WriteIniString ParmFile \"ZZero\" \"ZZeroLaser\"
                  sPrint \"%8.3lf %9.3lf\" GetMc[0] GetMc[1];
 }
";

/*--------------------------------------------------------------------*/
/* Use ZZero offsets to move the current origin                       */
/*--------------------------------------------------------------------*/
(MacroDef "ZZeroUse") =
"if(_iMc2Oc == 0)
  {mreset sprint (GetIniTxt \"SetOrigin\" \"NoChange\") 0L;} 

 Local \"ParmFile\" \"ToolRef\" \"Surface\" \"Object\";    
 ParmFile = MtApp.path & \"/ParamSet.ini\";

 if(%1 == 0)
 {
   ToolRef = Scall GetIniString ParmFile \"ZZero\" \"ZZeroToolRef\" \"0.0\";
   Surface = Scall GetIniString ParmFile \"ZZero\" \"ZZeroSurface\" \"0.0\";

   ZZeroRef;

   PrinT sprint \"Mc=%.2lf; Surface=%.2lf; Toolref=%.2lf; Pick=%.2lf; TO=%.2lf\"
                  GetMc[2]  Surface  ToolRef  PickPos[0].Ofs[2]  Tools[_ToolNC].Ofs[2];

   Mc2Oc[_iMc2Oc].ofs[2] = (Surface - ToolRef) + Tools[_ToolNC].Ofs[2];
   SetMc2Oc _iMc2Oc;

   Mc2Oc[_iMc2Oc].ofs[2] = GetOc[2];
   SetMc2Oc _iMc2Oc;

   Z _Z + 20; mf;
 }
 else
 { 
   Local \"Txt1\" \"Txt2\" \"OfsX\" \"OfsY\";

   Txt1 = GetIniString ParmFile \"ZZero\" \"ZZeroToolPos\" \"0.0 0.0\";
   Txt2 = GetIniString ParmFile \"ZZero\" \"ZZeroLaser\"   \"0.0 0.0\";
   
   OfsX = (GetStkObj 0 scall Txt2) - GetStkObj 0 scall Txt1; 
   OfsY = (GetStkObj 1 scall Txt2) - GetStkObj 1 scall Txt1; 

   if(%1 AND 1) {Mc2Oc[_iMc2Oc].ofs[0] = OfsX;}
   if(%1 AND 2) {Mc2Oc[_iMc2Oc].ofs[1] = OfsY;}
   SetMc2Oc _iMc2Oc;

   if(%1 AND 1) {Mc2Oc[_iMc2Oc].ofs[0] = GetOc[0];}
   if(%1 AND 2) {Mc2Oc[_iMc2Oc].ofs[1] = GetOc[1];}
   SetMc2Oc _iMc2Oc;
 }
";

/*--------------------------------------------------------------------*/
/* Get Z level and set current origin                                 */
/* Parameters: %1=0: use _iTPrb; %1=1: use _iZero                     */
/*--------------------------------------------------------------------*/
(MacroDef "ZZeroRef") =
"if(0 == _ncMode AND 1)                  /* unzul�ssiger Aufruf       */
  {mreset GetIniTxt \"GetTool\" \"Err007\";}

 if(0 == _RefTest)                       /* erst Achsen referenzieren */
  {mreset GetIniTxt \"GetTool\" \"Err004\";}

 if(_ToolNc == 0)                        /* Ung�ltige Werkzeugnummer  */
  {mreset sprint (GetIniTxt \"GetTool\" \"Err002\") 0L;} 

 local \"CallOnExit\";                   /* variables to restore      */

 CallOnExit = (sprint \"@MF_F = %.3lf; ML_F = %.3lf; MvPar;\" MF_F ML_F)
            & (sprint \" MtransFlag01 = '1'; mreset;\");

 ml_f = _ToolSpeedS;                     /* Anfahrgeschwindigkeit     */
 mf_f = _ToolSpeedF; MvPar;

 wait 0; MtransFlag01 = '0'; mreset;
 print GetIniTxt \"GetTool\" \"PrnSearchZ\";
 wait 0.3; 

 local \"Ok\"; Ok = 0;
 
 local \"ZDown\";
 ZDown = 0.0 + scall GetIniString ParmFile \"ZZero\" \"ZDown\" \"30\";
 if(ZDown > 0) {ZDown = 0 - ZDown;}

 local \"ZSwitch\"; ZSwitch = GetOc[2]; 
 local \"Dsc\"; Dsc = _InpDsc[_iZero];  /* get input descriptor       */

 Mf_f = _ToolProbeF;                    /* set probing speed          */
 Ml_f = _ToolProbeS; MVPAR;

 if(0 == _Test AND 1)                   /* move to switch and touch   */
 {
   if(0 == in Dsc) {Ok = 1;}
   Z ZSwitch + ZDown; MF;
   stoprequest Dsc Ok; wait 0.2 0;
   wait 0 3; mreset;
 }
 else
 {
   local \"Txt\"; wait 0;
   Txt = GetIniTxt \"GetTool\" \"MsgChange3\";    

   if(6 == MsgBox Txt \"\" MB_YESNO)    /* simulation using _ToolNC   */
    {Z _Z - 5.0; MF;}
 } 	
  
 local \"Result\"; wait 0; Result = GetOc[2];

 if((Result > ZSwitch + ZDown + 0.1)  
 AND(Result < ZSwitch - 1.0))
 { 
   if(0 == _Test AND 1)
   {
     stoprequest Dsc (Ok == 0);         /* move up and take probe     */ 
     Z _Z + 5.0; ML;
     wait 0 3; mreset;
   }
 }
 else                                   /* switch is not hit          */
 {
   mf_f = _ToolSpeedF; MvPar;           /* reset to toolchange speed  */
   Z ZSwitch; MF; wait 0;               /* move up                    */
   mreset GetIniTxt \"GetTool\" \"MsgNoSignal\";
 }
";
