// ===========================================================================
// **  Motor Amplifier Properties *****************************************
// -------------------------------------------------------------
// ====== Schrittaul�sung f�r Endstufen festlegen ============================
// folgende Werte sind m�glich :
//	0 -> 1/256
//	1 -> 1/128
//	2 -> 1/64
//	3 -> 1/32
//	4 -> 1/16
//	5 -> 1/8
//	6 -> 1/4
//	7 -> 1/2
//	8 -> 1/1
//-----------------------------------------------------------------------------
// ----  Interpolation und doppelte Flanken ein/aus ---------------------------
// intPol = 1 -> Interpolation ein, = 0 -> aus
// DEDGE  = 1 -> beide Taktflanken aktiv ein, = 0 -> aus
// Interpolation l��t sich nur einschalten, 
// wenn die Schrittaufl�sung 1/16 betr�gt
// ====== Strom f�r Endstufen festlegen =======================================
// Strom f�r Endstufen l��t sich in 32 Stufen einstellen 
// => 0 bis 31 Current Scale
// -- Endstufen mit 50mOhm Messwiderstand--------------------------------
// Versionsnummer =>
// bei 0,05 Ohm max 320/50 x 0,707 x (CS+1)/32 -> max 4,52A
// der Strom l��t sich nur als Vielfaches von 0,1414 einstellen
// 0,1414A; 0,2828A; 0,4242A; 0,5656A; 0,707A usw.
// Angabe Strom in A
// die Stromangaben im Datenblatt des eingesetzten Steppers 
// gelten f�r 1 Wicklung !! Datenblatt des jeweiligen Motors �berpr�fen !!
// da beide Wicklungen bestromt werden: 
// Strom* x 0,707 / 0,1414 --> 0,707 / 0,1414 = 5
// -- Endstufen mit 30mOhm Messwiderstand--------------------------------
// Versionsnummer =>
// bei 0,03 Ohm max 320/30 x 0,707 x (CS+1)/32 -> max 7,51A
// der Strom l��t sich nur als Vielfaches von 0,2357 einstellen
// Angabe Strom in A
// die Stromangaben im Datenblatt des eingesetzten Steppers
// gelten f�r 1 Wicklung !! Datenblatt des jeweiligen Motors �berpr�fen !!
// da beide Wicklungen bestromt werden:
// Strom* x 0,707 / 0,23566 --> 0,707 / 0,2357 = 3
//
// "Strom" 	Motorstrom in Ampere
// "Step" 	Schrittaufl�sung
// "intPol" 	Interpolation ja/nein  		bit1
// "DEDGE"	beide Flanken nutzen ja/nein	bit0
// "MRES" 	Mikroschrittresolution
// "cs"		Motorstrom in Skalierungswerten (Current Scale)
// "id"		bit1 und bit0

CMotAmp = ObjDef T_classDscr
"Strom"    T_double  0
"Step"	   T_long    0
"intPol"   T_long    0
"DEDGE"    T_long    0
;   

// == Voreinstellungen =============================================
// maximale Anzahl Achsen bei zwei Endstufen : 8
// die Variante bestimmt den max m�glichen Strom 
// wird in der EtInit definiert

local "Anzahl_Achsen";
Anzahl_Achsen = mtag[0].vDim;
if (Anzahl_Achsen > 8) {Anzahl_Achsen = 8;}
if ((Variante != "imes") and (Variante != "isel")) {Variante = "imes";}

MotAmp = gcnew CMotAmp Anzahl_Achsen;

// xml Element anlegen falls nicht vorhanden und vorbelegen
if(0 == MtApp.xml.Hardware._elExists "MotAmp")
 {
   local "i";
   i = 0; for Anzahl_Achsen
    {
     MotAmp[i].strom  = 1.8;	// Strom in Ampere
     MotAmp[i].Step   = 16;	// 1/16 Schritt
     MotAmp[i].intPol =  0; 	// Interpolation aus
     MotAmp[i].DEDGE  =  1; 	// beide Flanken aktiv
     i = i + 1;
    }
   MtApp.xml.Hardware.!E!MotAmp._Mt2Xml MotAmp;
 }
// ---------------------------------------------------------------------------

MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --


local "ax";
local "Step_Res";
local "Mot_Strom";
local "Int_Dedge";

Mot_Strom = 0;
Step_Res  = 0;
Int_Dedge = 0;
ax        = 0; 

for Anzahl_Achsen
 {
   // bei anderen eingegebenen Werten bleiben die Voreinstellungen wirksam
   if (MotAmp[ax].Step == 256) {Step_Res = 0;}
   if (MotAmp[ax].Step == 128) {Step_Res = 1;}
   if (MotAmp[ax].Step ==  64) {Step_Res = 2;}
   if (MotAmp[ax].Step ==  32) {Step_Res = 3;}
   if (MotAmp[ax].Step ==  16) {Step_Res = 4;}
   if (MotAmp[ax].Step ==   8) {Step_Res = 5;}
   if (MotAmp[ax].Step ==   4) {Step_Res = 6;}
   if (MotAmp[ax].Step ==   2) {Step_Res = 7;}
   if (MotAmp[ax].Step ==   1) {Step_Res = 8;}

   if ((MotAmp[ax].intPol < 0) or (MotAmp[ax].intPol > 1)) {MotAmp[ax].intPol = 0;}
   if ((MotAmp[ax].DEDGE  < 0) or (MotAmp[ax].DEDGE  > 1)) {MotAmp[ax].DEDGE  = 1;}

 // -- 4.5A RMS = 6,4A ; 7.5A RMS = 10A Spitzenstrom ----
   if (Variante == "imes")
    {
     if (MotAmp[ax].strom > 6.4) {MotAmp[ax].strom = 6.4;}
     Mot_Strom = (MotAmp[ax].strom * 5) + 0.5;
    }
   else 
    {
     if (MotAmp[ax].strom > 10) {MotAmp[ax].strom = 10;}
     Mot_Strom = (MotAmp[ax].strom * 3) + 0.5; 
    }

   Int_Dedge = (MotAmp[ax].intPol * 2) + MotAmp[ax].DEDGE;

 // -- Werte in die Endstufen schreiben --------------------
   Init_AX ax Mot_Strom Step_Res Int_Dedge;
   ax = ax + 1;
 }

// -- zweimal Freigabe, um ein eventuell gesetztes Fehlerflag zur�ckzusetzen ---
out_AX_enable 1;
out_AX_enable 0;
out_AX_enable 1;

//===============================================================================