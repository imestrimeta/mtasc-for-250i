/* ================================================================== */
/* Handler f�r Konturlisten                                           */
/* ================================================================== */
/* %P=2: Konturanfang                                                 */
/* %P=1: Konturende                                                   */ 
/*                                                                    */
/* %1: WarningStatus im Fall einer Warning:                           */
/*     0,1= Normalfall                                                */
/*       2= Warning Flag war nach dem 1. Durchlauf gesetzt,           */
/*          Kontur wird jetzt wiederholt                              */
/* %2 (am Konturanfang): Startpunktvektor                             */
/* ================================================================== */

//_mtStat.xData[2] = 0;

Thread_agi.mact.listHandler = &(
"if(%P == 2)                                      /* Konturanfang     */
 {
   z 10; mf;
   _ocNext = %2._; mf;
   if(%1 != 2)
   {
     if(_mode == 'G') {wait gbx.Wait;}

     if(gbx.crc)                                  /* Radius-Korr. ein */ 
     {
       _Crcpar.m_isClosed  = 1;                   /* geschlossen      */
       _Crcpar.Ori         = 1;                   /* nach rechts      */  
       _crcpar.BufSize     = gbx.bufSize;         /* Parameter setzen */
       _crcpar.nCmdOpen    = gbx.nOpen;
       _Crcpar.Rad         = gbx.rad; 
       _Crcpar.CheckLenMax = gbx.chkLenMax;
       CrcOpen;                                   /* aktivieren       */
     }

     z 0; ml;                                     /* abarbeiten       */
     //_mtStat.xData[2] = _mtStat.xData[2] + 1;
     //if(_mtStat.xData[2] == 7){__test;}
   }

   if(%1 == 2)                           /* Kein CRC beim Wiederholen */
   {
     if(_mode == 'G') {wait gbx.Wait;}
     SetDrawPar 'L' 0xFFL -1 -1 -1;             /* Wiederholen in rot */
   }
 }

 if(%P == 1)                                    /* Konturende         */
 {
   //if(_mtStat.xData[2] == 18){__test;}
   if(gbx.crc) {CrcClose;}                      /* Radius-Korr. aus   */ 

   z 10; mf;

   if(%1 == 2)                                  /* Farbe zur�cksetzen */
    {SetDrawPar 'L' Tools[_ToolSelEmu].Color -1 -1 -1;}
 }
");

Thread_agi.mact.lhExecCallLevel = 2;
Thread_agi.mact.ignorePStartForFastLines= 1;
Thread_agi.mact.warningState = 0;

if((gbx.Crc > 0) AND (_mode == 'G'))            /* Bei CRC-Warnung im */
{                                               /* Grafikmodus nochmal*/
  Thread_agi.mact.warningState = 1;
}

Thread_agi.outctrl.grmode = 1;
Thread_agi.mact.exec pbo.outlines;

END;

_ncMode = 0; _grMode = 1; mact.mfl.mType = 1;

Thread_agi.mact.lhExecCallLevel = 1;
Thread_agi.mact.exec pbo.outlines.<1>[31]
Thread_agi.mact.exec pbo.outlines.<1>[3]
