/* ================================================================== */
/* matrix and vector macros                                           */
/* ================================================================== */
/* Index zu 3x3-Matrix berechnen                                      */
/* ------------------------------------------------------------------ */
(MacroDef "_mti33") =
"local \"i0\" \"i1\";
 i0 = %1; if(i0 > 2){i0 = i0 - 3;}
 i1 = %2; if(i1 > 2){i1 = i1 - 3;}
 i1 + 3 * i0;";

/* ------------------------------------------------------------------ */
/* Get 3x3 Transformation matrix (= ARRAY 9), turning about one axis  */
/*   1. Arg.: Axis (0,1,2)                                            */
/*   2. Arg.: Angle (rad)                                             */
/* ------------------------------------------------------------------ */
(MacroDef "GetM33R") = 
"local \"m\";
 m = ARRAY 9 0.0;
 m[_mti33 (%1 + 1) (%1 + 1)] = m[_mti33 (%1 + 2) (%1 + 2)] = cos %2;
 m[_mti33 (%1 + 2) (%1 + 1)] =  sin %2;
 m[_mti33 (%1 + 1) (%1 + 2)] = -sin %2;
 m[_mti33 %1 %1] = 1.0;
 m;";

/* ------------------------------------------------------------------ */
/* Multiply vector and matrix                                         */
/* 1. Arg.: 3x3 matrix (= ARRAY 9)                                    */ 
/* 2. Arg.: vector                                                    */
/* ------------------------------------------------------------------ */
(MacroDef "M33Mul") =
"local \"i\" \"k\" \"v\";
 v = %2;
 i = 0; for 3
 { 
   v[i] = 0.0; k = 0; for 3
   { 
     v[i] = v[i] + %1[_mti33 i k] * %2[k];
     k = k + 1;
   }
 i = i + 1;
 }
 v;";

/* ------------------------------------------------------------------ */
/* Vector functions                                                   */ 
/* ------------------------------------------------------------------ */
(MacroDef "V3") =
"local \"v\"; v = ObjDef CVct3; v._ = array 3 (%1 + 0.0) (%2 + 0.0) (%3 + 0.0); v";

(MacroDef "L3") =
"sqrt (%1[0] * %1[0]) + (%1[1] * %1[1]) + (%1[2] * %1[2])";

(MacroDef "VLen") =
"local \"i\" \"len\"; i = 0; len = 0.0;

 for Thread_agi.vdim
 {
   len = len + %1[i] * %1[i];
   i = i + 1;
 }
sqrt(len)";

(MacroDef "V3?") =
"? sprint \"%.3lf %.3lf %.3lf\" %1";

(MacroDef "V3*") =                                   /* Skalarprodukt */
"(%1[0] * %2[0]) + (%1[1] * %2[1]) + (%1[2] * %2[2])";

(MacroDef "V3x") =                                   /* Kreuzprodukt  */
"V3 ((%1._[1] * %2._[2]) - (%1._[2] * %2._[1]))
    ((%1._[2] * %2._[0]) - (%1._[0] * %2._[2]))
    ((%1._[0] * %2._[1]) - (%1._[1] * %2._[0]))
";

(MacroDef "V3Str") =
"sprint \"%12.6lf %12.6lf %12.6lf\" %1";

(MacroDef "Vn") =
"local \"v\" \"i\" \"p\"; v = ObjDef CVctN; i = 0;
 p = \"%x\";
 for Thread_agi.vdim{
  p[1] = '1' + i;
  v._[i] = scall p;
  i = i + 1;
 }
 v";

if(mtag[0].agi)  // only if AGI is available
{
 Vn0 = Vn 0 0 0 0 0 0 0 0;                            // 0 vector
}

(MacroDef "SetAll") =
"local \"i\"; i = 0L;
 for(SizeOfArray %1._)
 {
  %1._[i] = %2;
  i = i + 1;
 }
";
