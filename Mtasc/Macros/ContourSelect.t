
// Auswahl von Teilcontouren einer zugeordneten Datei
// %1: xml-Datei
// %2: mode 0=normal, 1=draw all (optional), 2=search contour

local
"xml" "mode" "iMark"
 "list" "list_cnt" "list_iMark"
 "usecrc" "CallOnExit" "contour" "ncm" "grm";

mode = 0;
if(%P > 1) {mode = %2;}

xml => gcnew CXml 0 %1;
usecrc = 0;
list_cnt = -1;
iMark = list_iMark = 0;

//z 0; mf;
//x y 0; mf;

ncm = _ncMode;
grm = _grMode;
_ncMode = _grMode = 0;
list => fcallP (MtApp.path & "/Macros/GetMObjList.ts") CallPath & "/" & xml.ContourSelect.Source.File.!A?Name;
_ncMode = ncm;
_grMode = grm;
wait 0;
mreset;

(MacroDef "contour") =
"
  if(%2 > 0)
  {
    Thread_agi.mact.exec list[%1];
  }
  else
  {
    // inverted orientation
    Thread_agi.mact.exec list[%1] list[%1].t1 list[%1].t0;
  }
";
usecrc = 0;

/////////////////// ggf. Anzahl contouren anzeigen lassen:
//? sprint "Anzahl contouren: %d" list.n;


/////////////////// Liste der Konturen
/////////////////// Normalerweise werden nur die ungeraden Nummern verwendet
local "vi" "i" "xmlc"; i = 0;
if(mode >= 1)
{
  if(mode == 2)
  {
    Thread_agi.vci.Mode = 'p';
    Thread_agi.vci.iStart = 0;
    Thread_agi.vci.iEnd   = 0;
  }
  if(_grMode)
  {
    _mode = 'G';
    Thread_agi.vci.iCurr = 0;
    _ptr.Global.s = _ptr.WithTool.s = 0.0; // Reset accumulated path time values
    _ptr.Global.t = _ptr.WithTool.t = 0.0; 
  }
  for list.n   // draw all in natural sequence
  {
    contour i;
    i = i + 1;
  }
  if(mode == 2)
  {
    SetDrawPar 'L' 0xFF -1 -1 -1;  // show selected contour in red
    contour list_iMark;
    SetDrawPar 'L' Tools[_ToolSelEmu].Color -1 -1 -1;
  }
  return;
}

xmlc => xml.ContourSelect.Contours;
for scall xml.ContourSelect.Contours.!A?nData
{
  vi = sprint "v%d" i;
  // set positive orientation  as default
  if(((xmlc.(vi)).!A!Ori) == "") {(xmlc.(vi)).!A!Ori "1";}
  if(scall (xmlc.(vi)).!A?Used)
  {
    contour
      (scall (xmlc.(vi)).!A?CIndex)
      (scall (xmlc.(vi)).!A?Ori)
      //(scall (xmlc.(vi)).!A?nBridges)
      //(scall (xmlc.(vi)).!A?BridgesOffset)
      ;
  }
  i = i + 1;
}
