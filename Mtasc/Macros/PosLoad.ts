/*====================================================================*/
/* Load positions - called from SysAgInit.ts with local "AGI"         */
/*====================================================================*/
/* Macro- und Variablen-Definitionen                                  */
/*--------------------------------------------------------------------*/
local "PosFile"; PosFile = MtApp.path & sprint "/AxGroup%d/Positions.ini" mtagI;

/*--------------------------------------------------------------------*/
/* Maschinen-Positionen laden                                         */
/*--------------------------------------------------------------------*/
local "i" "LfdNr" "MtLine";
 
_nPosOfs = scall GetIniString PosFile "PosOfs" "n" "0";
if(_nPosOfs < 1) {_nPosOfs = 1;}

mtag[mtagI].pMcPos = &ObjDef CMc2Oc _nPosOfs;   /* Maschinen-Positionen */

i = 0L; for mtag[mtagI].nPosOfs
{
  LfdNr = sprint "%d" i;
  
  MtLine = GetIniString PosFile "PosOfs" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [PosOfs] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0.000";
  }
  McPos[i].ofs = ARRAY FagVDimMax sCall MtLine;

  MtLine = GetIniString PosFile "PosTxt" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [PosTxt] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0   0  \"New position\"";
  }

  McPos[i].iMt     = GetStkObj 0 scall MtLine;
  McPos[i].Flags   = GetStkObj 1 scall MtLine;
  (*McPos[i].Text) = GetStkObj 2 scall MtLine;

  i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Nullpunkte laden                                                   */
/*--------------------------------------------------------------------*/
_nOriOfs = scall GetIniString PosFile "OriOfs" "n" "0";
if(_nOriOfs < 1) {_nOriOfs = 1;}

mtag[mtagI].pMc2Oc = &ObjDef CMc2Oc _nOriOfs;      /* Nullpunkte        */

i = 0L; for mtag[mtagI].nOriOfs
{
  LfdNr = sprint "%d" i;
  
  MtLine = GetIniString PosFile "OriOfs" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [OriOfs] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0.000";
  }
  Mc2Oc[i].ofs = ARRAY FagVDimMax sCall MtLine;

  MtLine = GetIniString PosFile "OriTxt" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [OriTxt] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0   0  \"New position\"";
  }

  Mc2Oc[i].iMt     = GetStkObj 0 scall MtLine;
  Mc2Oc[i].Flags   = GetStkObj 1 scall MtLine;
  (*Mc2Oc[i].Text) = GetStkObj 2 scall MtLine;

  i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Werkzeugpositionen laden                                           */
/*--------------------------------------------------------------------*/
_nPickPos = scall GetIniString PosFile "PickPos" "n" "0";
if(_nPickPos < 1) {_nPickPos = 1;}

mtag[mtagI].pPickPos = &ObjDef CMc2Oc _nPickPos;  /* Werkzeugpositionen */

i = 0L; for mtag[mtagI].nPickPos
{
  LfdNr = sprint "%d" i;
  
  MtLine = GetIniString PosFile "PickPos" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [PickPos] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0.000";
  }
  PickPos[i].ofs = ARRAY FagVDimMax sCall MtLine;

  MtLine = GetIniString PosFile "PickTxt" LfdNr "";  
  if(MtLine == "")
  {
    // msgbox sprint "Missing line %d in [PickTxt] section!" i;
    SendError \"ERR_PosLoad_MissingLine\";
    MtLine =  "0   0  \"New position\"";
  }

  PickPos[i].iMt     = GetStkObj 0 scall MtLine;
  PickPos[i].Flags   = GetStkObj 1 scall MtLine;
  (*PickPos[i].Text) = GetStkObj 2 scall MtLine;

  i = i + 1;
}

/*--------------------------------------------------------------------*/
/* Load wear offsets                                                  */
/*--------------------------------------------------------------------*/
_nWearOfs = scall GetIniString PosFile "WearOfs" "n" "0";
if(_nWearOfs < 1) {_nWearOfs = 1;}

mtag[mtagI].pWearOfs = &ObjDef CMc2Oc _nWearOfs;     /* Werkzeug-Abrieb */

i = 0L; for mtag[mtagI].nWearOfs
{
  LfdNr = sprint "%d" i;
  
  MtLine = GetIniString PosFile "WearOfs" LfdNr "";  
  if(MtLine == "") {MtLine = "0.000";}

  WearOfs[i].ofs = ARRAY FagVDimMax sCall MtLine;

  MtLine = GetIniString PosFile "WearTxt" LfdNr "";  
  if(MtLine == "")
  {
    MtLine =  "0   0  \"No Wear Offset\"";
  }

  WearOfs[i].iMt     = 0;
  WearOfs[i].Flags   = 0;
  (*WearOfs[i].Text) = GetStkObj 2 scall MtLine;

  i = i + 1;
}
