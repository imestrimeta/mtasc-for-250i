// ------------------------------------------------------------------
// Ein- / Ausg�nge f�r Remote
// ------------------------------------------------------------------
CMctlIo = ObjDef T_classDscr
"nInPorts"    T_long   0
"iofsInputs"  T_long   0
"nOutPorts"   T_long   0
"iofsOutputs" T_long   0
"iStat"       T_char   8
"oStat"       T_char   8     // current status
"oStatSet"    T_char   8     // requested status
"oSet"        T_function  0  // set current=requested status
"readBit"     T_function  0
"readByte"    T_function  0
"readMask"    T_function  0
"writeBit"    T_function  0
"writeByte"   T_function  0
"writeMask"   T_function  0
"writeAnalog" T_function  0
"init"        T_function  0
;

// Initialize
// no Args
(MacroDef "CMctlIo_init") =
"
  local \"i\";
  i = 0; for %0.nOutPorts
  {
    %0.writeByte i 0;
    i = i + 1;
    sleep 100; // workaround !!!
  }
  i = 0; for %0.nInPorts
  {
    %0.readByte i;
    i = i + 1;
  }
";

// set current output status = requested output status
// no Args
(MacroDef "CMctlIo_oSet") =
"
  local \"i\" \"k\";
  i = 0; for %0.nOutPorts
  {
    k = 0; for 8
    {
      if(((1 << k) and %0.oStat[i]) != ((1 << k) and %0.oStatSet[i])) {%0.writeBit i k ((1 << k) and %0.oStatSet[i]);}
      k = k + 1;
    }
    i = i + 1;
  }
";

// Read Input Bit
// %1: input index
// %2: bit index (0-7)
(MacroDef "CMctlIo_readBit") =
"
  local \"i\" \"v\"; v = 0;
  i = %2 + %0.iofsInputs + 8 * %1;
  if(_iGet (i + 1)) {v = 1 << %2;}
  %0.iStat[%1] = (%0.iStat[%1] and (1 << %2) xor -1) or v;
  return v;
  //return %2 and %0.iStat[%1] = in _inpDsc[%0.iofsInputs + %1];
";

// Read Input Byte
// %1: input index
(MacroDef "CMctlIo_readByte") =
"
  local \"i\" \"k\" \"v\"; v = 0;
  k = 0; for 8
  {
    v = v or %0.readBit %1 k;   // workaround
    //v = v or %0.vRet;
    k = k + 1;
  }
  return v;
";

// Read Input Mask
// %1: port index
(MacroDef "CMctlIo_readMask") =
"
  return 0xFF;
  //return _inpDsc[%0.iofsInputs + %1].mask;
";

// Write Output Bit
// %1: port index
// %2: bit index (0-7)
// %3: value
(MacroDef "CMctlIo_writeBit") =
"
  local \"i\" \"v\"; v = 0;
  i = %2 + %0.iofsOutputs + 8 * %1;
  if(%3)
  {
    v = 1 << %2;
    _oSet (i + 1) 1;
  }
  else
  {
    v = 0;
    _oSet (i + 1) 0;
  }
  %0.oStat[%1] = (%0.oStat[%1] and (1 << %2) xor -1) or v;
  //out _outDsc[%0.iofsOutputs + %1] %0.oStat[%1];
";

// Write Output Byte
// %1: port index
// %2: value
(MacroDef "CMctlIo_writeByte") =
"
  local \"k\";
  k = 0; for 8
  {
    %0.writeBit %1 k (%2 and 1 << k);
    k = k + 1;
  }
";

// Read Output Mask
// %1: input index
(MacroDef "CMctlIo_writeMask") =
"
  return 0xFF;
  //return _outDsc[%0.iofsOutputs + %1].mask;
";

// Write Analog
// %1: port index
// %2: value in MV (0-9999)
(MacroDef "CMctlIo_writeAnalog") =
"
  if(%1 == 0) {_oaSet 60000 * %2 / 9999.0;}
  else        
  {
    // MsgBox \"Illegal Analog Channel\" \"IoMctl\";
    SendError \"ERR_MctlIO_IllegalAnalogChannel\";
  }
";

MctlIo = gcnew CMctlIo;
local "xd";
xd => MtApp.xml.Hardware.Inputs.!A!UsedByMctl;
if(xd[0] == 0)
{
  // MsgBox "Missing Entry 'UsedByMctl' in xml data 'Inputs'";
  SendError \"ERR_MctlIO_MissingEntry\";
  MctlIo.iofsInputs = 0;
  MctlIo.nInPorts = 0;
}
else
{
  MctlIo.iofsInputs = (array 2 scall xd)[0] - 1;
  MctlIo.nInPorts = ((array 2 scall xd)[1] + 7) / 8;
  //MctlIo.iStat = &(array MctlIo.nInPorts 0);
}
xd => MtApp.xml.Hardware.Outputs.!A!UsedByMctl;
if(xd[0] == 0)
{
  // MsgBox "Missing Entry 'UsedByMctl' in xml data 'Outputs'";
  SendError \"ERR_MctlIO_MissingEntry\";
  MctlIo.iofsOutputs = 0;
  MctlIo.nOutPorts = 0;
}
else
{
  MctlIo.iofsOutputs = (array 2 scall xd)[0] - 1;
  MctlIo.nOutPorts = ((array 2 scall xd)[1] + 7) / 8;
  //MctlIo.oStat = &(array MctlIo.nOutPorts 0);
}
