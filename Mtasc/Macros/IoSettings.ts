/* ================================================================== */
/* Ein- und Ausg�nge:                                                 */
/* ================================================================== */
_nInpMax  = scall MtApp.xml.Hardware.Inputs.!A?nDataMax;
_nOutMax  = scall MtApp.xml.Hardware.Outputs.!A?nDataMax;

_nIotMax = 100;                           /* create driver's IO table */
if(_drvIoDTab.dim != _nInpMax + _nOutMax + _nIotMax) {_drvIoDTab.dim= 0;}                                         
_drvIoDTab.dim= _nInpMax + _nOutMax + _nIotMax;

CINOut = ObjDef T_classDscr               /* Input/Output class       */
"Text"  T_object 0;

_iLabel = ObjDef CInOut _nInpMax + 1;     /* label definition         */
_oLabel = ObjDef CInOut _nOutMax + 1;

out (IoDscr "MMAP" 0x0L 0 -1L 2) 0;       /* clear hard limit bits    */

(*_iLabel[0].Text)   = "Eing�nge";        /* not used                 */
(*_oLabel[0].Text)   = "Ausg�nge";

i = 0L;                                   /* label initialization     */
for _nInpMax {(*_iLabel[i = i + 1].Text) = "";}

i = 0L;           
for _nOutMax {(*_oLabel[i = i + 1].Text) = "";}

_iStatPrv  = _iStat = ARRAY (_nINpMax + 1) 0;             /* Eing�nge */
_inpDsc    = ObjDef CIoDscr (_nINpMax + 1);   /* array of descriptors */

_oStatPrv  = _oStat = ARRAY (_nOutMax + 1) 0;             /* Ausg�nge */
_OutDsc    = ObjDef CIoDscr (_nOutMax + 1);   /* array of descriptors */

/* ------------------------------------------------------------------ */
/* Hardware Limits                                                    */
/* ------------------------------------------------------------------ */
(MacroDef "_drvHwLimWatch") =
"@if(%P)
 {
   if(%1 == 0)
    {Thread_agi.ptc.cfHwLim = Thread_agi.ptc.cfHwLim and -1L xor 0x10000L;}
   else
    {Thread_agi.ptc.cfHwLim = Thread_agi.ptc.cfHwLim or 0x10000L;}
   Thread_agi.ptcWrite;
 }
 0 != Thread_agi.ptc.cfHwLim and 0x10000L
";

/* ------------------------------------------------------------------ */
/* Modus-Tabelle Eing�nge                                             */
/* ------------------------------------------------------------------ */
/* Bit 0:   obsolet                                                   */
/* Bit 1=0: lesen �ber Deskriptor                                     */
/* Bit 1=2: lesen �ber Macro _iGetxx (xx ist die Nummer des Eingangs) */
/*          Falls _iMode[7] = 2, muss Makro _iGet7 definiert sein.    */
/* Beispiel Eing. 4 lesen:       print _iGet 4;                       */
/*          Falls _oMode[4] = 2: print _iGet4;                        */
/* ------------------------------------------------------------------ */
_iMode = ARRAY (_nINpMax + 1) 0;

/* ------------------------------------------------------------------ */
/* Modus-Tabelle Ausg�nge                                             */
/* ------------------------------------------------------------------ */
/* Bit 0:   obsolet                                                   */
/* Bit 1=0: schreiben �ber Deskriptor                                 */
/* Bit 1=1: schreiben �ber Macro _oSetxx %1; %1 ist Zustand 0 oder 1  */
/*          Falls _oMode[7] = 2, muss Makro _oSet7 definiert sein.    */
/* Bit 2=1: Bei Start / Stop / Progr.Ende autom. ein-/ausschalten     */
/* Beispiel Ausg. 5 setzen:	 _oSet 5 1;                           */
/* Beispiel Ausg. 2 r�cksetzen:	 _oSet 2 0;                           */
/*          Falls _oMode[2] = 2: _oSet2 0;                            */
/* ------------------------------------------------------------------ */
_oMode = ARRAY (_nOutMax + 1) 0;

/* ------------------------------------------------------------------ */
// Clear autoIo Input requests
/* ------------------------------------------------------------------ */
drv.laa.autoIo.n = 0;
drv.laa.autoIoWrite;

/* ------------------------------------------------------------------ */
/* Hard- und I/O-Deskriptoren und Aktionen aus MTasc.ini              */
/* ------------------------------------------------------------------ */
fcall MtApp.path & "/Macros/IoSetActions.ts";

/* ------------------------------------------------------------------ */
/* Eingang lesen                                                      */
/* ------------------------------------------------------------------ */
(MacroDef "_iGet") = 
"if(0 != _iMode[%1] and 8)
 {return  _iStat[%1];}
 if(0 != _iMode[%1] and 2)
 {return _iStat[%1] = scall sprint \"_iGet%d;\" 0 + %1;}
 else
 {return _iStat[%1] = 0 != in _INpDsc[%1];}
";

/* ------------------------------------------------------------------ */
/* Ausgang schreiben                                                  */
/* ------------------------------------------------------------------ */
(MacroDef "_oSet") =
"local \"stPrv\";
 stPrv = _oStat[%1];
 if(%2 == -1){_oStat[%1] = 1 - _oStat[%1];}
 if(%2 != -1){_oStat[%1] = %2;}
 if(_oStat[%1])
 {
  if(0 != _oMode[%1] and 2) {scall sprint \"_oSet%hd 1;\" %1;}
  if(0 == _oMode[%1] and 2) {out (_OutDsc[%1]) -1 + _oMode[%1] and 1;}
 }
 if(_oStat[%1] == 0)
 {
  if(0 != _oMode[%1] and 2) {scall sprint \"_oSet%hd 0;\" %1;}
  if(0 == _oMode[%1] and 2) {out (_OutDsc[%1]) 0 - _oMode[%1] and 1;}
 }
";

(MacroDef "oSet") =              /* Ausgang nur bei Ausf�hrung setzen */
"if(_mode != 'G'){wait 0; _oSet %1 %2; _dspUpdate;}
 _oStatPrv[%1] = %2;                /* _PrgCont soll ggf. einschalten */
";

/* ------------------------------------------------------------------ */
/* Motor                                                              */
/* ------------------------------------------------------------------ */
_MotStat = 0;

(MacroDef "_MotOn")  = "_MotStat = 1;";
(MacroDef "_MotOff") = "_MotStat = 0;";

(MacroDef "_MotOffErr") =       /* in case of error:                  */
"if(_MotStat)                   /* switch motors off (only once)      */
 {                              /* and write driver data to disk      */ 
  _PrgStop; _MotOff;
  _drvTraceBuffer \"DrvData.txt\";
 }
";

/* ------------------------------------------------------------------ */
/* Spindle                                                            */
/* ------------------------------------------------------------------ */
_spdStat  = 0;	    /* flag for spindle status                        */
_spdDir   = 0;	    /* direct. of rotation for spindle +-1, 0=default */

(MacroDef "_oaSet") = "";

if(T_undefined != (TypeOf "mtag").type)   /* if mtag  exists          */ 
{
  local "i"; i = 0;
  for sizeofarray mtag                    /* default settings         */
  {
    mtag[i].spdRpm = 0L;                  /* speed in RPM for spindle */
    mtag[i].oaVMin = 1000L;               /* limits for spindle speed */
    mtag[i].oaVMax = 10000L;
    mtag[i].oaV    = mtag[i].oaVMin;      /* current speed value      */
    i = i + 1;
  }
 
  (MacroDef "_oaSet") =                   /* default implementation   */
  "mtag[mtagI].oav = %1;
   if(%1 < mtag[mtagI].oaVMin){mtag[mtagI].oaV = mtag[mtagI].oaVMin;}
   if(%1 > mtag[mtagI].oaVMax){mtag[mtagI].oaV = mtag[mtagI].oaVMax;}
  ";
}

/*--------------------------------------------------------------------*/
// IO support in ProNC / Remote via IoMtasc.dll
/*--------------------------------------------------------------------*/
if(FileExists MtApp.path & "/Macros/MctlIo.ts")
 {fcall MtApp.path & "/Macros/MctlIo.ts";}     
