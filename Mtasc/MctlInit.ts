/* Initialization for Isel CNC API when calling Mtasc.dll directly (without ActiveXServer) */


if((typeof "drv").type == T_undefined){
 fcall MtApp.path & "\\system.ts"; /* basic initializations - avoid multiple calls */
}


if((GetVer.Mtasc.Major < 7) or (GetVer.Mtasc.Minor < 2))
{
  // MsgBox "Mtasc.dll Version must be >= 7.2.0.0!";
  SendError "ERR_MctlInit_InvalidMtVersion";
}

/*
  enable NC mode (axes will move with ML, MC, MF, etc.)
*/
_NcMode = 1;

/*
  disable graphic mode:
  commands will not be shown as lines in the device context passed to MtInitG()
*/
_GrMode = 0;

(MacroDef "_iGetPower") = "1;";   /* read input for "power is on" (if available) */
(MacroDef "_iGetSecur") = "1;";   /* read input for "security circuit" (if available) */
(MacroDef "_oSetLsBridge") = "";  /* allow limit switches without switching power off */

/*--------------------------------------------------------------------*/
/* container for MTASC and driver internal status data                */
/*--------------------------------------------------------------------*/
CMds = ObjDef T_classDscr
"nAxes" T_long   0
"m" CMtascStatus 0
"d" CFagCtStData 0
"ovr"     T_long  0       // override in %
"pwrStat" T_short 0       /* power status, bit 0: desired, bit1: real */
"sccStat" T_short 0                        /* security circuit status */
"initOK"  T_short 0                        /* initializing finished   */
"refOK"   T_short 0                        /* reference run finished  */
//"hwLim"   T_long 0   /* hardware limit switch status: */
//                     /* bit0,1: X-,+ / bit2,3: Y-+ / etc. / bit 16: enabled */
"speedFlag" T_long 0      // bit mask for axes in motion
"errCode" T_long 0        // ErrHandling
"read"    T_function 0
;

(MacroDef "CMds_read") =
"
  local \"r\";
  ObjEnterCS %0;
  %0.m = agi0.statRead; %0.d = agi0.fag.statRead; Mds.ovr = agi0.ptc.ovr.f[0] * 100;
  %0.speedFlag = 0; //trc.speedFlag;
  %0.pwrStat = _MotStat;
  %0.refOK = (RefDone == drv.laa.amask);
  r = %0;  // return local copy
  ObjLeaveCS %0;
  return r;
";
Mds = ObjDef CMds 0;

Mds.errCode = 0;

/* -------------------------- Global copy of speed and acceleration */
CMvp = ObjDef T_classDscr
"ml_f"  T_double 0      /* speed slow */
"ml_r"  T_double 0      /* accel slow */
"mf_f"  T_double 0      /* speed fast */
"mf_r"  T_double 0      /* accel fast */
;

Mvp = ObjDef CMvp;

if(MtApp.drvMode)
{
  Mds.pwrStat = 3;
  Mds.sccStat = 1;
  Mds.initOK  = 1;
  Mds.refOK   = 0;
  if(MtApp.AppName == "MctlApp")    // for mctl interface only (Remote)
  {
    fcall MtApp.path & "\\EtInit_" & (*getver.PdTypeS1) & ".ts"; // device type specific initialisations
  }
}
else
{
 Mds.pwrStat = 3;     /* default for running without driver */
 Mds.sccStat = 1;
 Mds.initOK  = 1;
 Mds.refOK   = 1;
}

/*
  maximum reentrance level (during calls of MtExec).
  Reentrance is needed e.g. to abort a currently running command with mreset.
*/
MtApp.spinLimit = 4;

/* Do not allow message processing in Mtasc.dll calls for this thread */
ThreadFlags "AllowPeekMessage" 0;

SetMc2Oc 0;

_MotOn; /*switch motors on */