/*====================================================================*/
/* Diese Datei enth�lt die Definition der Systemvariablen und Macros  */
/* f�r EdiTasc. Sie wird von EdiTasc und anderen Programmen beim      */
/* Programmstart ausgef�hrt und nicht ver�ndert.                      */
/* Die Werte der Variablen und der Macro-Code kann von anderen Teilen */
/* von EdiTasc und anderen Macros ver�ndert/�berschrieben werden.     */
/*                                                                    */
/* This file contains the definition of system variables and for      */
/* EdiTasc. It is executed by EditTasc and other programs on startup  */
/* and will not be changed.                                           */
/* The Values of the variables and the macro code may be changed by   */
/* EdiTasc and by other macros or system files.                       */
/*====================================================================*/
/* General parameters                                                 */
/*--------------------------------------------------------------------*/

_EditorLang0 = "scall";
_EditorLang1 = "DinSCall";
_EditorLang2 = "IselSCall";

M_PI     = PI = 2.0 * acos 0.0;
_d2r     = M_PI / 180.0;
_mm2Inch = 25.4;
_Bits8   = Array 8 1 2 4 8 16 32 64 128;
nullptr  = ObjDef T_object;

_lang    = "x";                  /* make language available as STRING */
_lang[0] = MtApp.lang;
_IDEflag = 0;                    /* set to 1 when VB runs in IDE mode */ 

//if(MtApp.traceMode == 1)       // we are a trace listener
//{
//  smc = gcnew CSmcMgr 0 "SmcDebug" 0;   now done by MtInit
//}

if(MtApp.traceMode == 2)         // we are a trace master
{
  //smc = gcnew CSmcMgr 0 "SmcDebug" 1;   now done by MtInit
  dbg = gcnew CMtascTraceList;
  CSmcProcTable = ObjDef T_classDscr
   "G5xEditor"  T_long 0
   "MtConfigDN" T_long 0
   "MtTrace"    T_long 0
   "JogMove"    T_long 0
   "_All"       T_long 0
  ;
  smcPT = ObjDef CSmcProcTable;

  (MacroDef "smcPClose") =
  "local \"cnt\" \"t0\"; cnt = smcPT.(%1);
   if(cnt == 0){return;}
   t0 = time 'S';
   while \"(smcPT.(%1)) > 0\"
   {
    smc.lock;
    smc.arg0 = %1 & \".close\";
    sleep 100;
    smc.unlock;
    sleep 10;
    if(((time 'S') - t0) > 5.0)
    {
      // MsgBox \"Timeout Error closing \" & %1; smc.unlock; 
      SendError \"ERR_System_ClosingTimeOutError\";
      return;}
   }
   if((smcPT.(%1)) == cnt) 
   {
    // MsgBox \"Internal Error closing \" & %1;
    SendError \"ERR_System_Closing\" 
   }
  ";
}

/*====================================================================*/
// Variables and Macros for reading configuration data
/*====================================================================*/
//MtApp_xml = gcnew CXml 0 MtApp.path & "/" & MtApp.appName & ".xml";

IniFileMt  = MtApp.path & "/Mtasc.ini";
IniFileTxt = MtApp.path & "/MtMacroTxt.ini";

(MacroDef "GetIniTxt") = "GetIniString IniFileTxt %1 (%2 & _lang)
                          (%2 & \": Error with missing text\");";
(MacroDef "_ErrHandler") =
"
 // MsgBox _sysMsg.errText \"" & (GetIniTxt "System" "ErrBoxCapt") & "\" MB_OK;
 SendError \"ERR_System_InternalSystemError\";
 LogWrite \"MsgBox: \" & _sysMsg.errText;
";

(MacroDef "GetXmlAppGrp") =
" Local \"key\";
  if(%P == 0) {key = sprint \"AxisGroup%d\" Thread_AGI.Fag.Address;}
  else        {key = sprint \"AxisGroup%d\" %1;}
  return MtApp.xml.Application.(key);
";

(MacroDef "GetXmlDrvFag") =
" Local \"key\";
  if(%P == 0) {key = sprint \"v%d\" Thread_AGI.Fag.Address;}
  else        {key = sprint \"v%d\" %1;}
  return MtApp.xml.MtDrv.fag.(key);
";

/*--------------------------------------------------------------------*/
// Read a string line from Mtasc.ini
/*--------------------------------------------------------------------*/
//(MacroDef "GetMtStr")  = "GetIniString IniFileMt \"Mtasc\" %1 \"\";";

//(MacroDef "GetAGIStr") =            // get string from active AGI
//"local \"AgKey\";
//AgKey = sprint \"AGI%d\" Thread_AGI.Fag.Address;
//GetIniString IniFileMt AGKey  %1 \"\";";

//(MacroDef "GetIniStr") =            // get string from AGI by argument
//"local \"AgKey\";
//AgKey = sprint \"AGI%d\" %1;
//GetIniString IniFileMt AGKey  %2 \"\";";

/* ------------------------------------------------------------------ */
// Convert General settings from Mtasc.ini to xml
/* ------------------------------------------------------------------ */
//fcall (MtApp.path & "/Macros/Ini2Xml.ts");

MhelAng0To2PiFlag = scall MtApp.xml.Application.!A?MhelAng0To2PiFlag;

(MacroDef "T_cast") = "return (gcnew %1) = %2;";  // for numerical type casts

/*--------------------------------------------------------------------*/
/* Format sTrings                                                     */
/*--------------------------------------------------------------------*/
(MacroDef "_fmtCoord") = "sprint \"%.3lf\"   %1 + 0.0";
(MacroDef "_fmtSpeed") = "sprint \"%.1lf\"   %1 + 0.0";
(MacroDef "_fmtFac")   = "sprint \"%.4lf\"   %1 + 0.0";
(MacroDef "_fmtI16")   = "sprint \"%d\"      %1 + 0L";
(MacroDef "_fmtI32")   = "sprint \"%dL\"     %1 + 0L";
(MacroDef "_fmtHex")   = "sprint \"0x%.8XL\" %1 + 0L";

/* ------------------------------------------------------------------ */
/* Interface macros                                                   */ 
/* ------------------------------------------------------------------ */
(MacroDef "_dspUpdate") = "_plc.DirtyFlags.IO = 1;";

// -------------------------------------------------------------------
// write msgbuf (e.g. driver messages) to string
// %1: message buffer
// %2 (optional): File name to save the result string
// -------------------------------------------------------------------
(MacroDef "MsgBuf") =
"local \"i\" \"txt\"; i = 0L; txt = \"\";
 for %1.nMsg{txt = txt & (%1.get i \"t:%3d '%s'\") & \"\r\n\"; i = i + 1;}
 if(%P == 2)
 {
  fwrites %2 txt;
 }
 return txt;
";

// Execute file in specified AGI
// %1 = AGI index
// %2 = File with path
(MacroDef "ExecFile")  =
"While(\"_plc.ExecAgiFlag[%1] > 0\") {sleep 100;} // wait until macro is free
 if(mtag[%1].thNc)
 {
  // MsgBox sprint \"ExecFile: Program is already running in CH %d\" %1 + 1; 
  SendError \"ERR_System_OneProgramIsRunning\";
  Exit;
 }
 _plc.ExecAgiFlag[%1] = 1;                  // set busy flag
 _Plc.ExecAgiFile[%1] = &(%2);
 _plc.ExecAgiFlag[%1] = 2;                  // set valid flag
";

/*====================================================================*/
// Create a global MTASC container mtag for axis groups (channels)
/*====================================================================*/
CMtAG = ObjDef T_classDscr              // MTASC axis group
"agi"      T_object 0                   // associated AGI
"thNc"     T_long   0         // this thread id is currently using us in ncMode
"thGr"     T_long   0         // this thread id is currently using us in grMode
"thAgi"    T_object 0         // this AGI is currently used in thread mode
"vDim"     T_short  0         // number of virtual axes for this AGI
"locked"   T_long   0         // this thread may not terminate if set
"maType"   T_long   0         // machine type 0:undefined, 1:mill, 2:lathe
"fileNc"   T_object 0         // this file is currently executed in ncMode
"fileGr"   T_object 0         // this file is currently executed in grMode
"fileCurr" T_object 0         // this file is currently loaded
"IniPath"  T_object 0         // path to Mtag dependent ini files

"pPParm"   T_object 0         // pointer to PParm entries
"pFParm"   T_object 0         // Pointer to FParm entries

"ObjMin"   T_double 8         // array for dimensions  		
"ObjMax"   T_double 8         // of the current object

"nPosOfs"  T_short  0         // number of positions
"pMcPos"   T_object 0         // Pointer to pickpos table 

"nOriOfs"  T_short  0         // number of origins
"pMc2Oc"   T_Object 0         // Pointer to origin table
"iMc2Oc"   T_long   0         // index of current origin
"tofsUsed" T_long   0         // tool offset used (0=no, 1=yes)
"refMask"  T_long   0         // bit mask for axes to be referenced
"mirrX"    T_double 0         // X Mirror position
"mirrMode" T_long   0         // Bit 0: 0= in Mc, 1= in Oc
                              // Bit 1: 0= invisible, 1= visible
"nPickPos" T_short  0         // number of pickpos entries
"pPickPos" T_Object 0         // Pointer to pickpos table

"nWearOfs" T_short  0         // number  of WearPos entries
"pWearOfs" T_Object 0         // Pointer to WearPos table
"iWear"    T_Long   0         // Index   to WearPos table

"coTrans"  T_Object 0         // defines the transformation for coordinate display
                              // Pointer to CTrans object if transformation is active
                              // Pointer to String "O" for plain Oc (no transformation)
"fmtDbl"   T_Object 0         // double format string

"Mode"     T_Char   0         // execution mode: '0', 'G', 'X'
"Pad1"     T_Char   0         
"Step"     T_Short  0         // Step mode

// data of last program stopped with _PrgStop
"vciStopped"    T_long   0    // command index
"flStopped"     T_object 0    // program file

"ToolFile"      T_Object 0    // Name and path of tool file
"nTools"        T_short  0    // number of tools in table
"pTools"        T_object 0    // Pointer to tool table 
"ToolsUsedByFile" T_long 0    // Bit mask of tools used by current file
"ToolsSelByUser"  T_long 0    // Bit mask of tools selected by user
"ToolAuto"      T_short  0    // 0...3
"ToolMode"      T_short  0    // direct/EMU/ident

"ToolNc"        T_long   0    // real tool in machine
"ToolSel"       T_long   0    // tool to be selected
"ToolNcEmu"     T_long   0    // emulated tool in machine 
"ToolSelEmu"    T_long   0    // emulated tool to be selected

"ChkToolDrop"   T_short  0    // => _tch!?
"TchToHome"     T_short  0    // => _tch!?
"ExecToPark"    T_short  0    // => _pParm?

"ovr"           T_double 3    // current override factors

"oavMin"        T_long   0    // min. spindle OaValue
"oavMax"        T_long   0    // min. spindle OaValue
"oav"           T_long   0    // cur. spindle OaValue
"SpdRpm"        T_long   0    // current  spindle Rpm
"RpmFromTool"   T_long   0    // take Rpm from tool
"SpeedFromTool" T_long   0    // take ML from tool
"ToolPrbState"  T_short  0    // tool probe not ready

"ToolWidthVar"  T_short  0    // Tool width is variable (>0)
"MM2Pix"        T_double 0    // conversion from mm to pixel

"ToolSpeedF"    T_double 0    // Werkzeugwechsel anfahren
"ToolSpeedS"    T_double 0    // Werkzeug ablegen/holen

"ToolProbeF"    T_double 0    // Taster anfahren
"ToolProbeS"    T_double 0    // Taster Feinmessung

"ToolFileAsked" T_object 0    // name of tool file

"ToolZup"       T_double 0    // Zum/vom Magazin
"ToolZup1"      T_double 0    // Magazin-Magazin
"TPrbZup"       T_double 0    // Hub f�r Werkzeugmessung
"TPrbZdn"       T_double 0    // Absenkung f�r Werkzeugmessung
"iTPrb"         T_short  0    // Index f�r Taster-Deskriptor
"manStepEnable" T_short  0    // allow manual jogging with AltGr+Cursor Keys
;

/*--------------------------------------------------------------------*/
// create macro access for AGI variables
/*--------------------------------------------------------------------*/
(MacroDef "_PParm")    = "(*mtag[mtagI].pPParm)";      // project parameters
(MacroDef "_FParm")    = "(*mtag[mtagI].pFParm)";      // file parameters

(MacroDef "_nPosOfs")  = "mtag[mtagI].nPosOfs";        // positions
(MacroDef "_nOriOfs")  = "mtag[mtagI].nOriOfs";
(MacroDef "_nPickPos") = "mtag[mtagI].nPickPos";
(MacroDef "_nWearOfs") = "mtag[mtagI].nWearOfs";

(MacroDef "McPos")     = "(*mtag[mtagI].pMcPos)";
(MacroDef "Mc2Oc")     = "(*mtag[mtagI].pMc2Oc)";
(MacroDef "PickPos")   = "(*mtag[mtagI].pPickPos)";
(MacroDef "WearOfs")   = "(*mtag[mtagI].pWearOfs)";

(MacroDef "_iMc2Oc") = "mtag[mtagI].iMc2Oc";   
(MacroDef "RefMask") = "mtag[mtagI].refMask";
(MacroDef "_iWear")  = "mtag[mtagI].iWear"; 

(MacroDef "_Mode")     = "mtag[mtagI].Mode";           // execution mode 

(MacroDef "_ToolFile") = "(*mtag[mtagI].ToolFile)";    // tool management
(MacroDef "_ToolN")    = "mtag[mtagI].nTools";
(MacroDef "Tools")     = "(*mtag[mtagI].pTools)";
(MacroDef "_ToolAuto") = "mtag[mtagI].ToolAuto";
(MacroDef "_ToolMode") = "mtag[mtagI].ToolMode";

(MacroDef "_ToolNc")      = "mtag[mtagI].ToolNc";
(MacroDef "_ToolSel")     = "mtag[mtagI].ToolSel";
(MacroDef "_ToolNcEmu")   = "mtag[mtagI].ToolNcEmu";
(MacroDef "_ToolSelEmu")  = "mtag[mtagI].ToolSelEmu";

(MacroDef "_ChkToolDrop") = "mtag[mtagI].ChkToolDrop";
(MacroDef "_TchToHome")   = "mtag[mtagI].TchToHome";

(MacroDef "_ExecToPark")  = "mtag[mtagI].ExecToPark";  // execution

(MacroDef "_RpmFromTool")   = "mtag[mtagI].RpmFromTool";
(MacroDef "_SpeedFromTool") = "mtag[mtagI].SpeedFromTool";
(MacroDef "_ToolPrbState")  = "mtag[mtagI].ToolPrbState";

(MacroDef "_ToolWidthVar")  = "mtag[mtagI].ToolWidthVar";
(MacroDef "_MM2Pix")        = "mtag[mtagI].MM2Pix";

(MacroDef "_ToolSpeedF")    = "mtag[mtagI].ToolSpeedF";
(MacroDef "_ToolSpeedS")    = "mtag[mtagI].ToolSpeedS";

(MacroDef "_ToolProbeF")    = "mtag[mtagI].ToolProbeF";
(MacroDef "_ToolProbeS")    = "mtag[mtagI].ToolProbeS";

(MacroDef "_ToolFileAsked") = "(*mtag[mtagI].ToolFileAsked)";

(MacroDef "_ToolZup")  = "mtag[mtagI].ToolZup";
(MacroDef "_ToolZup1") = "mtag[mtagI].ToolZup1";
(MacroDef "_TPrbZup")  = "mtag[mtagI].TPrbZup";
(MacroDef "_TPrbZdn")  = "mtag[mtagI].TPrbZdn";
(MacroDef "_iTPrb")    = "mtag[mtagI].iTPrb";

(MacroDef "_AGIpath")  = "(*mtag[mtagI].IniPath)";

// allow app specific commands before loading the driver
if(MtApp.traceMode == 2)
{
  //scall MtApp.xml.Application.!A?ExecSys0;
  if(FileExists MtApp.path & "/System0.ts") {fcall MtApp.path & "/System0.ts";}
}

/*====================================================================*/
// Initialize global drv (driver object) and FAG's
/*====================================================================*/
Thread_NewObjList;               // create thread local variable list

mtagI    = 0;                    // Index of current axis group
nMtag = 1;                       // max. # of axis groups
mtagMask = 1;                    // bit mask of active axis groups

if(MtApp.drvMode)                      // access/create driver object + LAA
{
  mtagMask = MtApp.xml.MtDrv.fagMask._Xml2Mt;
  nMtag    = MtApp.xml.MtDrv.nFag._Xml2Mt;
  if(nMtag <= 0) {exit "No Axis Group defined";}
  if(MtApp.drvMode == 2)               // create new driver object 
   {drv = gcnew CMtdrv 0 1;}
  else
   {drv = gcnew CMtdrv 0 0;}     // access existing driver object 

  Thread_SetDrv drv;
}
mtag     = ObjDef CMtAG nMtag; // global mtag array (one for each FAG)
mtag[0].vDim = 3;                 // default

if(MtApp.drvMode == 2)
{
  //====================================================================
  // convert laa array sizes if necessary
  //====================================================================
  if((MtApp.xml.MtDrv.laa.v0.vDim._Xml2Mt) !=  (MtApp.xml.MtDrv.laa.v0.vDimPrv._Xml2Mt))
  {
    fcallP (MtApp.path & "/Macros/LaaResize.ts") MtApp.xml.MtDrv.laa.v0;
  }
  drv.laaInitRq = 1;
  drv.fagInitRq = mtagMask;
  local "i"; i = 0; for nMtag                 // get fag data
  {
    if(mtagMask AND 1 << i)
    {
      local "fag"; fag => GetXmlDrvFag i;
      //====================================================================
      // convert fag array sizes if necessary
      //====================================================================
      if((fag.vDimMc._Xml2Mt) !=  (fag.vDimMcPrv._Xml2Mt))
      {
        fcallP (MtApp.path & "/Macros/FagResize.ts") i;
      }
      mtag[i].vDim = fag.vDimMc._Xml2Mt;
      mtag[i].maType = scall (GetXmlAppGrp i).!A?MaType;
      if(0 == fag._elExists "CheckHwLim")  // update versions without HW limit watching
      {
        fag.!E!CheckHwLim._Mt2Xml array mtag[i].vDim 0;;
      }
    }
    i = i + 1;
  }
  drv.init MtApp.xml.MtDrv;     // initialize driver
  if(MtApp.xml.MtDrv.laa.v0.PdReload._Xml2Mt)
  {
    MsgBox "Driver Devices have been reloaded" "System.ts" MB_OK;
    MtApp.xml.MtDrv.laa.v0.PdReload._Mt2Xml 0;
  }
}

/*--------------------------------------------------------------------*/
/* GCode settings (required for AGI's)                                */ 
/*--------------------------------------------------------------------*/
(MacroDef "DinCallX") = 
"
 // msgbox \"Din-Iso ist nicht implementiert\";
 SendError \"ERR_System_NoDinISOSupport\";
";

if((MtApp.drvMode == 2) or MtApp.g3Mode)
{
  if(FileExists MtApp.path & "/GcInit.ts")
   {fcall MtApp.path & "/GcInit.ts";}
}

/*====================================================================*/
/* Create AGI's in mtag                                               */
/*====================================================================*/
if((MtApp.drvMode == 2) or MtApp.g3Mode)
{
  local "i"; i = 0; For nMtag  // create axis and graphic interface
  {
    if(mtagMask And _Bits8[i])
    {
      if(MtApp.drvMode)
       {mtag[i].agi = &(gcnew CMtAgi 0 (drv.fag i).vDim (drv.fag i));}
      else
       {mtag[i].agi = &(gcnew CMtAgi 0 mtag[i].vDim);}
      scall sprint "mtIAg%d => *mtag[i].agi" i;  // global alias name for agi access

      (*mtag[i].agi).axisLett  (GetXmlAppGrp i).!A?AxisLett;
      if(MtApp.g3Mode){(*mtag[i].agi).g3open;}
      scall sprint "agi%d => *mtag[i].agi" i;  // global alias name for agi access
    }
    i = i + 1;
  }
}

/*--------------------------------------------------------------------*/
/* Offset and pickpos table (required for SysAgInit)                  */
/*--------------------------------------------------------------------*/
CMc2Oc = ObjDef T_classDscr             /* Offset class               */
"Text"  T_object 0                      /* offset description         */
"Flags" T_short  0                      /* Nullpunkts-Verwendung      */
"iMt"   T_short  0                      /* index for extended transf. */
"ofs"   T_double FagVDimMax;              /* one offset for each axis   */

/*--------------------------------------------------------------------*/
/*------------------- Move to TransParm.ts if possible ---------------*/
/*--------------------------------------------------------------------*/
if(MtApp.execMode)
 {fcall MtApp.path & "/Macros/FileParm.ts";}     /* file and project data  */

/*--------------------------------------------------------------------*/
/* Tool table (optional, required for SysAgInit)                      */
/*--------------------------------------------------------------------*/
/*====================================================================*/
/* Tool management variables and macros                               */
/*====================================================================*/
/* Tool class definition                                              */
/*--------------------------------------------------------------------*/
CToolData = ObjDef T_classDscr               /* Tool class =              */
"Crc"   T_short  0                       /* radius compensation       */
"EMU"   T_short  0                       /* index of to real tool     */
"Ident" T_short  0                       /* Tool identifier           */
"HpglPen" T_short  0                     // use for this HPGL pen number
"HpglZdn" T_double 0                     // use this Z for HPGL pen down
"Ofs"   T_double FagVDimMax              /* one offset for each axis  */
"Rad"   T_double 0                       /* tool radius               */
"Len"   T_double 0                       /* tool length               */ 
"Speed" T_double 0                       /* tool speed                */
"SpeedZdn" T_double 0                    // tool speed for mld
"Rpm"   T_Long   0                       /* rotations per minute      */ 
"Out"   T_short  0                       /* not used (output number)  */
"FPath" T_double 0                       /* path with MF              */
"FTime" T_double 0                       /* time with MF              */
"SPath" T_double 0                       /* path with ML              */
"STime" T_double 0                       /* time with ML              */
"DPath" T_double 0                       /* path with MLD             */
"DTime" T_double 0                       /* time with MLD             */
"BTC"   T_object 0                       /* before tool change macro  */
"ATC"   T_object 0                       /* after tool change macro   */
"DXF"   T_object 0                       /* DXF processing macro      */
"Flags" T_short  0                       /* flags for tool shape      */
"File"  T_object 0                       /* file name for tool shape  */
"Text"  T_object 0                       /* tool description          */           
"Color" T_Long   0                       /* tool color                */
;
if(FileExists MtApp.path & "/Macros/ToolMacros.ts")
{fcall MtApp.path & "/Macros/ToolMacros.ts";} /* "GetTool", "LoadTools"...*/

/*--------------------------------------------------------------------*/
/* Initialize AGI's (SysAgInit)                                       */
/*--------------------------------------------------------------------*/
if((MtApp.drvMode == 2) or MtApp.g3Mode)        // create axis and graphic interface
{
  local "mtagI"; mtagI = 0;
  for nMtag
  {
    if(mtagMask AND _Bits8[mtagI])
    {fcall MtApp.path & "/SysAgInit.ts";}
    mtagI = mtagI + 1;
  }
}

(MacroDef "AgiActivate") =
"mtagI = %1;
 fcall MtApp.path & \"/AgIActivate.ts\";
";

/*====================================================================*/
/* Connect AGI 0 to main thread => Move to VB code?                   */
/*====================================================================*/
if(MtApp.g3Mode or (MtApp.drvMode == 2))
 {Thread_SetAgi *mtag[mtagI].agi;}

if(MtApp.drvMode == 2)
 {Thread_agi.OutCtrl.drv = 1;}     /* enable output via Driver object */

//TODO: CheckVer should work without Thread_agi.OutCtrl.drv!
/*--------------------------------------------------------------------*/
/* Compatibility                                                      */
/*--------------------------------------------------------------------*/
if(0 > checkver 'M' 7 1 0 0)      /* check version of MTasc.dll      */ 
 {Mreset sprint (GetIniTxt "System" "ErrVersion") "7.1.x";} 

if(MtApp.g3Mode or (MtApp.drvMode == 2))
{
  AgiActivate 0;
  Mlimit '0';
  MlimitIsSuspended = 0;   /* suspended limit watching on tool change */
}

if(MtApp.peekMode)
 {ThreadFlags "AllowPeekMessage" 1;}/* Msg. handling (user interface) */

MtApp.spinLimit = 5;                     // reentrance level

/*--------------------------------------------------------------------*/
/* Macros and variables for origin handling                           */
/*--------------------------------------------------------------------*/
// default for saving current origin number - is application specific
(MacroDef "SaveIMc2oc") =  "";

(MacroDef "SetMc2Oc") =                    /* select workpiece origin */ 
"if(%1 >= _nOriOfs)
  {mreset (GetIniTxt \"System\" \"InvalidOc\") \"SetMc2Oc\";}

 if(Thread_agi.outCtrl.drv and ThreadFlags \"MtdrvIsSet\")
 {
   local \"ToolOfs\"; ToolOfs = array FagVDimMax 0.0;

   local \"tofs\"; tofs = 1;
   if(%P == 2) {if(%2 == 0) {tofs = 0;}}
   if(tofs)
   {
    if(mtag[mtagI].nTools)                         /* add tool offset  */ 
     {ToolOfs = WearOfs[_iWear].Ofs + Tools[_ToolNc].Ofs - Tools[0].Ofs;}

    if(T_Undefined != (Typeof \"GcMod\").type)     /* add G43 offset   */
     {ToolOfs = ToolOfs - GcMod.G43Ofs;}
   }
   local \"CallOnError\"; CallOnError = \"mreset; SetMc2Oc %1 tofs;\";
   wait 0; MpLock '+';
   local \"CallOnExit\"; CallOnExit = \"MpLock '-';\";
   _iMc2Oc = %1;
   mtag[mtagI].tofsUsed = tofs;
   mcall (*_MtList[Mc2Oc[%1].iMt]) Mc2Oc[%1].ofs ToolOfs;
   _Plc.DirtyFlags.Origin = 1;
   SaveIMc2oc;
 }

 if(0 == ThreadFlags \"MtdrvIsSet\") {_iMc2Oc = %1;}
";

/*====================================================================*/
/* Driver dependent settings and macros                               */
/*====================================================================*/
if(MtApp.drvMode > 0)
{
 /*--------------------------------------------------------------------*/
 /* Input and output definitions, settings and macros (_iget, _oset..) */
 /*--------------------------------------------------------------------*/
   if(FileExists MtApp.path & "/Macros/IoSettings.ts")
    {fcall MtApp.path & "/Macros/IoSettings.ts";}     
}

if(MtApp.drvMode == 2)
{
 /*--------------------------------------------------------------------*/
 /* Support searching of Reference Point (file must exist)             */
 /*--------------------------------------------------------------------*/
   fcall MtApp.path & "/Macros/RefMacros.ts";

 /*--------------------------------------------------------------------*/
 /* Jogwheel and Joystick settings and macros                          */
 /*--------------------------------------------------------------------*/
   if(FileExists MtApp.path & "/Macros/JogSettings.ts")
    {fcall MtApp.path & "/Macros/JogSettings.ts";}  
}

/*====================================================================*/
/* Execution parameters and macros (required if "Master" + "Exec")    */
/*====================================================================*/
/* Used by: EdiTasc                                                   */
/*--------------------------------------------------------------------*/
/* Start/Stop: _PrgOn,    _PrgOff,  _PrgAbort, _PrgCont               */ 
/*             _PrgStart, _PrgStop, _PrgEnd                           */
/*                                                                    */ 
/* Selecting:  _vciGotoStart,  _vciLeaveEnd                           */
/*             _vciGotoStartG, _vciLeaveEndG                          */ 
/*                                                                    */ 
/* Other:      _MouseMoveXY, _SetOcNull, _SetOcCurr                   */
/*                                                                    */ 
/* TransParm:  _MTransOn, _MTransOff                                  */ 
/*                                                                    */ 
/* FileParm:   Data structures for file and project management        */ 
/*                                                                    */ 
/* Bikub:      Macros for z level compensation                        */ 
/*--------------------------------------------------------------------*/

if(MtApp.execMode > 0)
{
  fcall MtApp.path & "/Macros/ExecMacros.ts";  /* execution macros         */
  fcall MtApp.path & "/Macros/ExecUtils.ts";   /* execution utilities      */
  fcall MtApp.path & "/Macros/TransParm.ts";   /* transformation macros    */

  if((options "bikub") and (FileExists MtApp.path & "/ZLevel/BiKubMacros.ts"))
   {fcall MtApp.path & "/ZLevel/BiKubMacros.ts";}

/*--------------------------------------------------------------------*/
// Info Table
/*--------------------------------------------------------------------*/
//TODO: Das ist f�r die Oberfl�che!
  CInfoTab = ObjDef T_classDscr          // Info class
  "title"    T_object 0                  // title
  "lbl"      T_object 4                  // Labels
  "lblDty"   T_long   0                  // Labels were changed
  "data"     T_object 4                  // Data
  "dataDty"  T_long   0                  // Data were changed
  "dataOK"   T_long   0                  // Data have been read and are valid
  "visible"  T_long   0;                 // Make Info table visible

  InfoTab = ObjDef CInfoTab 0;           // Global Info Object
  InfoTab.title = &("");
  local "i"; i = 0;
  for 4 {InfoTab.lbl[i] = &(""); InfoTab.data[i] = &(""); i = i + 1;}
}
  
/*====================================================================*/
/* Parameters and macros for cutter radius compensation               */ 
/*====================================================================*/
if((MtApp.drvMode == 2) or MtApp.g3Mode)
{
 _crcpar.BufSize  = 2;                    /* nach soviel S�tzen wird   */
 _crcpar.nCmdOpen = _crcpar.BufSize - 1;  /* "tool down" erzeugt       */
 _crcpar.iPlane   = 2;                    /* XY-Ebene f�r CRC          */

 _mtStat.crcState  = 0;                   /* Radiuskorrektur-STatus    */

 (MacroDef "MCrcSet") =                   /* Radiuskorrektur ein/aus   */
 "_Crcpar.Ori        = %1;
  _Crcpar.m_isClosed = %2;
  if(%1 != 0){_mtStat.crcState = 1;}
  if(%1 == 0){_mtStat.crcState = 0; crcClose;}
 ";

 /*====================================================================*/
 /* load Dxf-macros (optional)                                         */
 /*====================================================================*/
 if(FileExists MtApp.path & "/Macros/DxfInit.ts")
  {fcall MtApp.path & "/Macros/DxfInit.ts";}

 /*====================================================================*/
 /* load Gerber-macros (optional)                                      */
 /*====================================================================*/
 if(FileExists MtApp.path & "/Macros/GrbInit.ts")
  {fcall MtApp.path & "/Macros/GrbInit.ts";}

 /*====================================================================*/
 /* Macro definitions for circle, rectangle, spline, helix (optional)  */
 /*====================================================================*/
 if(FileExists MtApp.path & "/Macros/ShapeMacros.ts")
  {fcall MtApp.path & "/Macros/ShapeMacros.ts";}

 /*====================================================================*/
 /* Macro definitions for origin setting (optional)                    */
 /*====================================================================*/
 if(FileExists MtApp.path & "/Macros/SetOrigin.ts")
  {fcall MtApp.path & "/Macros/SetOrigin.ts";}
}

/*====================================================================*/
/* Matrix and vector macros (required)                                */
/*====================================================================*/
if(FileExists MtApp.path & "/Macros/VectorMacros.ts")
{
 fcall MtApp.path & "/Macros/VectorMacros.ts";   /* "_mti33", "GetM33R"... */
}

//====================================================================
// PLC / SPS timer
//====================================================================
// (MacroDef "_cmdSps")     = "";    obsolete but still supported...

// _cmdSps2 is replaced by this class:
// Test mit Custom-Version der PLC-handler

CDirtyFlags = ObjDef T_classDscr // dirty flags
"Origin"      T_short  0         // origin has changed
"Tool"        T_short  0         // tool has changed
"IO"          T_short  0         // refresh display
"Coord"       T_short  0         // coordinate display
;

CPLC = ObjDef T_classDscr      // PLC class
"tmr1"        T_object 0       // handler for 1. loop
"tmr2"        T_object 0       // handler for 2. loop
"tmr3"        T_object 0       // handler for 2. loop
"fcode1"      T_long   0
"fcode2"      T_long   0
"fcode3"      T_long   0
"tmrCnt"      T_long   3       // counters for tmr1,2,3
"setAuto"     T_long   0
"rqS"         T_short  100     // request status codes
"rqH"         T_object 100     // request handlers
"rqN"         T_short  0       // # of request handlers used
"rq1"         T_short  0       // current request for tmr1
"rq2"         T_short  0       // current request for tmr2
"rq3"         T_short  0       // current request for tmr3
"DirtyFlags"  CDirtyFlags 0    // status requests
"PrintText"   T_object 0       // text to be printed
"PrintFlag"   T_long   0       // print status
"ArgInt"      T_long   0       // Integer argument
"ArgStr"      T_object 0       // String argument
"ExecAgiFile" T_object nMtag // File to be executed in Channel
"ExecAgiFlag" T_long   nMtag // Flag for file execution
"StepMode"    T_long   0
"DoorOpen"    T_long   0
// add application specific fields here
"spdOvr"      T_double nMtag // spindle override factors for each channel
"spdRpmR"     T_long   nMtag // real spindle rpm
"spdG95"      T_long   nMtag // G95 mode: control ovr from spdRpmR
;

_plc = ObjDef CPLC;                     // global instance of PLC class
_plc.tmr1 = &("");                      // initialize handlers
_plc.tmr2 = &("");
_plc.tmr3 = &("");
_plc.PrintText = &("");                 // initialize text pointer
_plc.PrintFlag = 0L;                    // initialize text pointer

/*--------------------------------------------------------------------*/
/* miscellaneous                                                      */
/*--------------------------------------------------------------------*/
(MacroDef "_SetOvr")  = "SetOvr %1; mtag[mtagI].ovr[0] = %1 / 100.0;";

/*--------------------------------------------------------------------*/
// set override in channel and motion mode
// %1: channel
// %2: 0=global, 1=ml, 2=mf
// %3: value (factor)
/*--------------------------------------------------------------------*/
(MacroDef "SetOvrX") =
"(drv.fag %1).ptc.ovr.f[%2] = mtag[%1].ovr[%2] = %3;
 (drv.fag %1).setovr;
";

(MacroDef "OvrChg2Neg")  = (MacroDef "OvrChg2Pos") = "";

(MacroDef "_doorOpen")   = "0;";

(MacroDef "CustomMacro") = 
"
  // MsgBox \"CustomMacro ist nicht definiert\";
  SendError \"ERR_System_NoCustomMacro\";
";

(MacroDef "_g3Push") = 
"_p0 = _ocNext;
 SetDrawPar 'L' %1 -1 -1 -1;
 _g3mmStat = _octMinMax_stat; _octMinMax_stat = 0;";

(MacroDef "_g3Pop") = 
"_ocNext = _p0;
 SetDrawPar 'L' Tools[_toolSel].Color -1 -1 -1; MvPar;
 _grMode = 0; ml; _grMode = 1; mf;
 _octMinMax_stat = _g3mmStat;";

                                          /* for SP in HPGL files     */
(MacroDef "_HpglSP") =
"if(_HpglPar.ToolForPen[%1])
  {_HpglPar.CurrPen = %1; Gettool _HpglPar.ToolForPen[%1];}
 else
  {Print sprint \"Invalid tool 0 ignored for SP %d\" %1;}
";

/*====================================================================*/
/* Define default transformations                                     */
/*====================================================================*/
tr0 = ObjDef CMtransChain;

i = 0;                             /* ----------------- object offset */
SetAll tr0.d[i].vct 0.0;
tr0.d[i].ip  =  -1;
tr0.d[i].par = 1.0;

tr0.n = 1;                         /* number of instructions          */
i = 0;
tr0.cFw[i].func   = MTT_offset;    /* instruction definition          */
tr0.cFw[i].argF   = 0;
tr0.cFw[i].argIn  = MTT_use_vOut;
tr0.cFw[i].argOut = MTT_use_vOut;
tr0.cBw[i] = tr0.cFw[i];
tr0.cBw[i].func = tr0.cBw[i].func or 0x80;      /* back: inverted     */

trNMax = 1;                             // max. number of transformations
if((MtApp.drvMode == 2) or MtApp.g3Mode)
 {trNMax    = scall MtApp.xml.Application.!A?TrExtendedMax;}

_MtList = ObjDef T_object trNMax + 1;     /* Array of command strings   */

if(MtApp.drvMode == 1) {_MtList[0] = &("");}

if(MtApp.drvMode == 2)
{
 _MtList[0] =                           /* standard command string    */
 &("
  tr0.d[0].vct._ = %1 - %2;
  Thread_agi.oc2mc tr0;
 ");
}

i = 1; for(trNMax)
{                                       /* initialize transformations */
  _MtList[i] = &(*_MtList[0]);
  i = i + 1;
}

// allow app specific commands after loading the driver
if(MtApp.drvMode == 2) {scall MtApp.xml.Application.!A?ExecSys1;}
