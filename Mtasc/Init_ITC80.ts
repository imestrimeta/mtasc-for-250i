
if (CtType.ITC80)  
{
// ===========================================================================
// **  Motor Amplifier Properties *****************************************
// ------ Beschreibung siehe Achsen.txt -------------------------------------
// == maximale Anzahl Achsen  : 8
//===================================================================
// -- xml Element anlegen falls nicht vorhanden und vorbelegen ------

//----------------------------------------------------------------------------------------------------------  

  local "ax";
  local "Step_Res";
  local "Mot_Strom";
  Mot_Strom = 0;
  Step_Res  = 0;
  ax        = 0; 
	
   MtApp.xml.Hardware.!E!MotAmp._Xml2Mt MotAmp; // -- Werte aus der xml lesen --
   
   if (in_Power1_ok == 0)
   {
     // MsgBox "Spannung einschalten.";
     SendError "ERR_Controller_NoMainPower";
   }
   
   // while "in_Power1_ok == 0"  {MsgBox "Spannung einschalten."   "Power"  Mb_iconhand +  Mb_okcancel; }
   
   i = 1;    for 12 { _oset i 0; i = i + 1;_dspupdate;}   // alle Ausg�nge ausschalten
   sel_off; 				 	     	  // alle Achsselekte abschalten
   
   MinPower;
   
   for Anzahl_Achsen
   {
     Step_Res  = MotAmp[ax].Step;
     Set_Resol ax Step_Res;

     ax = ax + 1;
   }
}

else 
{
  // MsgBox "Fehler"; 
  SendError "ERR_Controller_InvalidController";
  exit;
}  
  
//===============================================================================