/* Set Spindle status (ON/OFF and RPM) */


/* if spindle should be switched off when             */ 
/* RPM (rotation per min.) = 0, remove comment signs! */
/* if(_spdRpm == 0){_SpdStat = 0;}                    */

// SPINDLE OFF in case spindle is on: switch off
if((_spdStat == 0) and (_oStat[1] != 0)) {_spdDir = 0;}

// SPINDLE CW in case spindle is off: switch on
if((_spdDir != 0) and (_oStat[1] == 0)) {_spdStat = 1;}

if(_ncMode and 1)
{
  if(_spdStat != _oStat[1])
  {
    wait 0;
    _oSet 1 _spdStat;
    _dspUpdate;
  }
}
else
{
  // In Graphic mode set _oStatPrv only
  // wird von _PrgOn bei Start ab Satz... erkannt
  if(_grMode != 0) {_oStatPrv[1] = _spdStat;}
}